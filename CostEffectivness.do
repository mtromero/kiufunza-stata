use "$basein/4 Intervention/TwaEL_2013/CODPayments_2013.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3

drop if HT==1
sum KiasiAtakacholipwa if treatment=="COD"
scalar Bonus_2013_COD=r(mean) 
sum KiasiAtakacholipwa if treatment=="Both"
scalar Bonus_2013_Both=r(mean) 

use "$basein/4 Intervention/TwaEL_2014/CODTchrPayments_2014.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
sum T_bonus2014_tzs if treatment=="COD"
scalar Bonus_2014_COD=r(mean)
sum T_bonus2014_tzs if treatment=="Both"
scalar Bonus_2014_Both=r(mean) 

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"

collapse (mean) t23 t24 t25, by(SchoolID)
sum t23
scalar compensation=r(mean)


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
collapse (mean) StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1, by(SchoolID)

merge 1:m SchoolID using "$basein/4 Intervention/TwaEL_2013/CODPayments_2013.dta", keepus(KiasiAtakacholipwa HT)
drop if _merge!=3
collapse (sum) KiasiAtakacholipwa (mean) StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1, by(SchoolID)
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3

gen PerChild=KiasiAtakacholipwa/(StudentsGr1_T1 +StudentsGr2_T1+ StudentsGr3_T1)
sum PerChild if treatment=="COD"
sum PerChild if treatment=="Both"

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
collapse (mean) StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1, by(SchoolID)

merge 1:m SchoolID using "$basein/4 Intervention/TwaEL_2014/CODTchrPayments_2014.dta", keepus(T_bonus2014_tzs)
drop if _merge!=3
collapse (sum) T_bonus2014_tzs (mean) StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1, by(SchoolID)
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3

*Mauro you multipllied by 1.2 since HT wasn't here
gen PerChild=1.2*T_bonus2014_tzs/(StudentsGr1_T1 +StudentsGr2_T1+ StudentsGr3_T1)
sum PerChild if treatment=="COD"
sum PerChild if treatment=="Both"



*********** CALCULATE WAGE INCREASE *****************
use "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", clear
sum t23
use "$basein/8 Endline 2014\Final Data\Teacher/R6Teacher_nopii.dta", clear
sum t23
use "$basein/11 Endline 2015\Final Data\Teacher Data\R8Teacher_nopii.dta", clear
sum t23
use "$basein/15 Endline 2016\Final Deliverable\Final Data\Teacher Data/R10Teacher_nopii.dta", clear
sum t23

