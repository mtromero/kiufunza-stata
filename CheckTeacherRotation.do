*Change Combo to Combination

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4 d_p d_p2
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
	reg `var' TD1 TD2 TD3 TD4 `if', nocons vce(cluster `clus_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p'  = nullmat(`d_p'),r(p)
	matrix A=e(b)
	matrix B=e(V)
	mat `mu_1' = nullmat(`mu_1'), A[1,1]
	mat `mu_2' = nullmat(`mu_2'), A[1,2]
	mat `mu_3' = nullmat(`mu_3'), A[1,3]
	mat `mu_4' = nullmat(`mu_4'), A[1,4]
	mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
	mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
	mat `se_3' = nullmat(`se_3'), sqrt(B[3,3])
	mat `se_4' = nullmat(`se_4'), sqrt(B[4,4])
	reghdfe `var' TD1 TD2 TD3 TD4 `if',  vce(cluster `clus_id') abs( i.`strat_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p2'  = nullmat(`d_p2'),r(p)
}
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	eret mat `mat' = ``mat''
}
end



use "$base_out/1 Baseline/Teacher/Teacher.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID) update replace
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"




egen NumGradeSubjects=rowtotal(SBJ_M_GRD1_T1-SBJ_O_GRD7_T1), missing
egen NumFocalGradeSubjects=rowtotal(SBJ_M_GRD1_T1 SBJ_K_GRD1_T1 SBJ_E_GRD1_T1 SBJ_M_GRD2_T1 SBJ_K_GRD2_T1 SBJ_E_GRD2_T1 SBJ_M_GRD3_T1 SBJ_K_GRD3_T1 SBJ_E_GRD3_T1), missing
replace NumFocalGradeSubjects=. if NumFocalGradeSubjects==0
egen NumGrd1=rowtotal(SBJ_M_GRD1_T1 SBJ_K_GRD1_T1 SBJ_E_GRD1_T1), missing
replace NumGrd1=. if NumGrd1==0
egen NumGrd2=rowtotal(SBJ_M_GRD2_T1 SBJ_K_GRD2_T1 SBJ_E_GRD2_T1), missing
replace NumGrd2=. if NumGrd2==0
egen NumGrd3=rowtotal(SBJ_M_GRD3_T1 SBJ_K_GRD3_T1 SBJ_E_GRD3_T1), missing
replace NumGrd3=. if NumGrd3==0

label var NumGradeSubjects "Total number of grade/subjects taught"
label var NumGrd1 "Focal subjects in Grade 1 (conditional on \$>\$0)"
label var NumGrd2 "Focal subjects in Grade 2 (conditional on \$>\$0)"
label var NumGrd3 "Focal subjects in Grade 2 (conditional on \$>\$0)"

eststo clear
eststo: my_ptest  NumGradeSubjects NumGrd1 NumGrd2 NumGrd3, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/TeacherBaseline_Rotation.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


reg NumGradeSubjects i.treatarm i.DistID, nocons vce(cluster SchoolID)
sum NumGradeSubjects if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_teachers_rotation.tex", write replace
file write newfile "`tempm'"
file close newfile



use "$base_out/Consolidated/Teacher.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID) update replace
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
egen NumFocalGradeSubjects=rowtotal(SBJ_M_GRD1_T1 SBJ_K_GRD1_T1 SBJ_E_GRD1_T1 SBJ_M_GRD2_T1 SBJ_K_GRD2_T1 SBJ_E_GRD2_T1 SBJ_M_GRD3_T1 SBJ_K_GRD3_T1 SBJ_E_GRD3_T1), missing

replace MovedOutFGS_T3=. if NumFocalGradeSubjects==0 | missing(NumFocalGradeSubjects)
replace MovedOutFGS_T7=. if NumFocalGradeSubjects==0 | missing(NumFocalGradeSubjects)
label var MovedOutFGS_T3 "No longer teaching a focal grade/subject (endline 2013)"
label var MovedOutFGS_T7 "No longer teaching a focal grade/subject (endline 2014)"



egen FGS_T1=rowtotal(SBJ_M_GRD1_T1 SBJ_K_GRD1_T1 SBJ_E_GRD1_T1 SBJ_M_GRD2_T1 SBJ_K_GRD2_T1 SBJ_E_GRD2_T1 SBJ_M_GRD3_T1 SBJ_K_GRD3_T1 SBJ_E_GRD3_T1), missing
replace FGS_T1=(FGS>0) if !missing(FGS)
collapse (sum) FGS_T1 FGS_T3 FGS_T7, by(SchoolID DistID treatarm)

label var FGS_T1 "Teachers eligible for bonuses (Yr0)"
label var FGS_T3 "Teachers eligible for bonuses (Yr1)"
label var FGS_T7 "Teachers eligible for bonuses (Yr2)"

eststo clear
eststo: my_ptest  FGS_T1 FGS_T3 FGS_T7, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/TeacherBaseline_Rotation_NumEligble.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles
exit



eststo clear
eststo: my_ptest  MovedOutFGS_T3   MovedOutFGS_T7  , by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/Teacher_Rotation2.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


reg MovedOutFGS_T3 i.treatarm i.DistID, nocons vce(cluster SchoolID)
sum MovedOutFGS_T3 if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_teachers_rotation2.tex", write replace
file write newfile "`tempm'"
file close newfile

reg MovedOutFGS_T7 i.treatarm i.DistID, nocons vce(cluster SchoolID)
sum MovedOutFGS_T7 if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_teachers_rotation3.tex", write replace
file write newfile "`tempm'"
file close newfile
