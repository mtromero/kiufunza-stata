**********************
**************Student
************************
use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear


label var passkis_T8 "Pass Kiswahili Twaweza"
label var passeng_T8 "Pass English Twaweza"
label var passmath_T8 "Pass Math Twaweza"

label var passkis_T7 "Pass Kiswahili EDI"
label var passeng_T7 "Pass English EDI"
label var passmath_T7 "Pass Math EDI"


foreach subject in kis eng math{
foreach grade in 1 2 3 4{

if("`grade'"!="4"){
estpost tabstat pass`subject'_T8 pass`subject'_T7 if attgrade_T7==`grade' &  TreatmentCG==0 & SchoolTWA_T7==1, by(treatment) statistics(mean sd) columns(statistics)
esttab using "$results\LatexCode\PassRate`subject'_`grade'.tex", replace booktabs main(mean) aux(sd) nostar unstack noobs nonote nomtitle nonumber label
}

if("`grade'"=="4"){
estpost tabstat pass`subject'_T8 pass`subject'_T7 if TreatmentCG==0 & SchoolTWA_T7==1, by(treatment) statistics(mean sd) columns(statistics)
esttab using "$results\LatexCode\PassRate`subject'_`grade'.tex", replace booktabs main(mean) aux(sd) nostar unstack noobs nonote nomtitle nonumber label
}

}
}






use "$basein\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents.dta", clear
drop SchoolID_15_s Grade_15_s _merge USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}

drop if treatment==.
drop if DistrictID==11
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)


*First we need to clean the data a bit
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6
 
gen Pass_Syll_Kis=(Kis_SI==4) & !missing(Kis_SI)
gen Pass_Word_Kis=(Kis_MA==4) & !missing(Kis_MA)
gen Pass_Sent_Kis=(Kis_SE==4) & !missing(Kis_SE)
gen Pass_Para_Kis=(Kis_A==1) & !missing(Kis_A)
gen Pass_Read_Kis=(Kis_H==1) & !missing(Kis_H)
gen Pass_Compre_Kis=(Kis_M==2) & !missing(Kis_M)


gen Pass_Syll_Eng=(Eng_L==4) & !missing(Eng_L)
gen Pass_Word_Eng=(Eng_W==4) & !missing(Eng_W)
gen Pass_Sent_Eng=(Eng_SE==4) & !missing(Eng_SE)
gen Pass_Para_Eng=(Eng_P==1) & !missing(Eng_P)
gen Pass_Read_Eng=(Eng_S==1) & !missing(Eng_S)
gen Pass_Compre_Eng=(Eng_C==2) & !missing(Eng_C)
   
       
gen Pass_Count_Math=(Math_ID==4) & !missing(Math_ID)
gen Pass_Numbers_Math=(Math_UTA==4) & !missing(Math_UTA)
gen Pass_Inequal_Eng=(Math_BWA==4) & !missing(Math_BWA)
gen Pass_Add_Eng=(Math_J==4) & !missing(Math_J)
gen Pass_Sub_Eng=(Math_T==4) & !missing(Math_T)
gen Pass_Mult_Eng=(Math_Z==4) & !missing(Math_Z)
gen Pass_Div_Eng=(Math_G==4) & !missing(Math_G)   


gen Kis_Pass2=(Pass_Syll_Kis==1 & Pass_Word_Kis==1 & Pass_Sent_Kis==1 ) if Grade==1 & TestedEL==1
replace Kis_Pass2=(Pass_Word_Kis==1 & Pass_Sent_Kis==1 & Pass_Para_Kis==1) if Grade==2 & TestedEL==1
replace Kis_Pass2=( Pass_Read_Kis==1 & Pass_Compre_Kis==1) if  Grade==3 & TestedEL==1


gen Eng_Pass2=(Pass_Syll_Eng==1 & Pass_Word_Eng==1 & Pass_Sent_Eng==1 ) if Grade==1 & TestedEL==1
replace Eng_Pass2=(Pass_Word_Eng==1 & Pass_Sent_Eng==1 & Pass_Para_Eng==1) if Grade==2 & TestedEL==1
replace Eng_Pass2=(Pass_Read_Eng==1 & Pass_Compre_Eng==1) if  Grade==3 & TestedEL==1


gen Math_Pass2=(Pass_Count_Math==1 & Pass_Numbers_Math==1 & Pass_Inequal_Eng==1 & Pass_Add_Eng==1 & Pass_Sub_Eng==1 ) if Grade==1 & TestedEL==1
replace Math_Pass2=(Pass_Inequal_Eng==1 & Pass_Add_Eng==1 & Pass_Sub_Eng==1 & Pass_Mult_Eng==1) if Grade==2 & TestedEL==1
replace Math_Pass2=(Pass_Add_Eng==1 & Pass_Sub_Eng==1 & Pass_Mult_Eng==1 & Pass_Div_Eng==1) if  Grade==3 & TestedEL==1

tab Kis_Pass2 Kis_Pass
tab Eng_Pass2 Eng_Pass
tab Math_Pass2 Math_Pass


tab Pass_Syll_Kis if Grade==1
tab Pass_Word_Kis if Grade==1
tab Pass_Sent_Kis if Grade==1

tab Pass_Syll_Eng if Grade==1
tab Pass_Word_Eng if Grade==1
tab Pass_Sent_Eng if Grade==1

tab Pass_Count_Math if Grade==1
tab Pass_Numbers_Math if Grade==1
tab Pass_Inequal_Eng if Grade==1
tab Pass_Add_Eng if Grade==1
tab Pass_Sub_Eng if Grade==1


tab Pass_Word_Kis if Grade==2
tab Pass_Sent_Kis if Grade==2
tab Pass_Para_Kis if Grade==2

tab Pass_Word_Eng if Grade==2
tab Pass_Sent_Eng if Grade==2
tab Pass_Para_Eng if Grade==2

tab Pass_Inequal_Eng if Grade==2
tab Pass_Add_Eng if Grade==2
tab Pass_Sub_Eng if Grade==2
tab Pass_Mult_Eng if Grade==2

tab Pass_Read_Kis if Grade==3
tab Pass_Compre_Kis if Grade==3


tab Pass_Read_Eng if Grade==3
tab Pass_Compre_Eng if Grade==3

tab Pass_Add_Eng if Grade==3
tab Pass_Sub_Eng if Grade==3
tab Pass_Mult_Eng if Grade==3
tab Pass_Div_Eng if Grade==3


tab Kis_Pass2 if Grade==1
tab Eng_Pass2 if Grade==1
tab Math_Pass2 if Grade==1
