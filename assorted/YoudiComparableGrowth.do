/*
BASELINE
FOR LANGUAGES EACH STUDENTS GET ASKED ABOUT: LETTERS, WORDS, PARARGAPH, STORY, 2 COMPREHENSION QS
FOR MATH EACH STUDENT GETS ASKED ABOUT: COUNT, NUMBERS, INEQ, ADD, SUB, MULT

FOR ENDLINE YR1
GRD 1: LANG= LETTERS, WORDS, SENTENCES
GRD 1: MATH=COUNT, NUMBERS, INEQ, ADD, SUB
GRD 2: LANG=  WORDS, SENTENCES, PARAGRAPH
GRD 2: MATH=, INEQ, ADD, SUB, MULT
GRD 3: LANG=   PARAGRAPH, QUESTIONS
GRD 3: MATH=,  ADD, SUB, MULT, DIVISION

FOR ENDLINE YR2
GRD 1: LANG= LETTERS, WORDS, SENTENCES
GRD 1: MATH=COUNT, NUMBERS, INEQ, ADD, SUB
GRD 2: LANG=  LETTERS,WORDS, SENTENCES, PARAGRAPH
GRD 2: MATH=COUNT, NUMBERS,, INEQ, ADD, SUB, MULT
GRD 3: LANG=   LETTERS,WORDS,PARAGRAPH, STORY, QUESTIONS
GRD 3: MATH=COUNT, NUMBERS,, INEQ,,  ADD, SUB, MULT, DIVISION

SO IN COMMON... TAKING INTO ACCOUNT GRADE PROGRESSION 1
GRADE 1 LANG=LETTERS, WORDS
GRADE 1 MATH=COUNT, NUMBERS, INEQ, ADD, SUB

GRADE 2 LANG=WORDS, PARAGRAPH
GRADE 2 MATH=INEQ, ADD, SUB, MULT

GRADE 3 LANG=PARAGRAPH, QUESTIONS
GRADE 3 MATH=ADD, SUB, MULT, DIVISION


*/


*******************************************************
*************BASE LINE
********************************************************
use "$basein/1 Baseline/Student/Sample_noPII.dta", clear
drop _merge
compress
saveold "$base_out/1 Baseline/Student/Student.dta", replace

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID using "$base_out/1 Baseline/Student/Student.dta"
drop if _merge==1
drop _merge

drop  Stream_ID StreamID totSampleStreams_ID streamSize_ID StudentID
*child did not answer the base test
drop if consentChild==.
*child refused to answer finish base test
*drop if result==2 

gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance
drop  consentChild result childAvailable timeEndTest timeStartTest


replace kiswahili_3=0 if kiswahili_3==2
replace kiswahili_4=0 if kiswahili_4==2
replace kiswahili_5=0 if kiswahili_5==2
replace kiswahili_6=0 if kiswahili_6==2
replace kiingereza_3=0 if kiingereza_3==2
replace kiingereza_4=0 if kiingereza_4==2
replace kiingereza_5=0 if kiingereza_5==2
replace kiingereza_6=0 if kiingereza_6==2
recode bonasi_1 bonasi_2 bonasi_3 (2=0)

*For grade 1 is the easiest as students started in the easier level and stopped when they couldnt go any further. Thus its "safe" to assume they would have score zero in the next sections
*Thus the first step is to replace the missing values for zeros (since they did not get to it, it must be because they couldn't answer easier stuff so we assume they score zero)
foreach var of varlist kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
replace `var'=0 if `var'==. & GradeID==1 & attendance==1
}


*For grade  2 and 3 is the same in math... sooo... first we replace with zero the levels the student did not get to answer: 
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==2 & attendance==1
}
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==3 & attendance==1
}


*For grade  2 and 3 is harder in lenguages: It started in question 3, if the student go it, then he moved on, if he didn't it moved to question 2 and if he scored zero then it moved to question 1. 
*So first, if the student got it, then lets assume he could read syllabus and letters, i.e. that he could achieve perfect score.
replace kiingereza_1 =5 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_1==. & attendance==1
replace kiingereza_2 =8 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_2==. & attendance==1
replace kiswahili_1 =5 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_1==. & attendance==1
replace kiswahili_2 =8 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_2==. & attendance==1
*If the student did not get question 4 (after he got question 3), then assume he would have gotten zero in questions 5 and 6
replace kiingereza_5 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, then lets assume he couldnt read the story or answer the questions i.e. that he could achieve perfect score. 
replace kiingereza_4 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_4==. & attendance==1
replace kiingereza_5 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_4 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_4==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, and got over 0 in the word section, then we need to estimate the number of letters he would have gotten
* to do this we run an OLS regression using Grade1 data (they all answer these two questions) 
poisson kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0 & attendance==1
*reg kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
predict input_kis1 if attendance==1, n 
*reg kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
poisson kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID i.SchoolID if GradeID==1 & kiswahili_2>0 & attendance==1
predict input_kiin1 if attendance==1, n 

replace kiswahili_1=max(round(input_kis1),5) if kiswahili_1==. & kiswahili_2>0 & kiswahili_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiingereza_1=max(round(input_kiin1),5)  if kiingereza_1==. & kiingereza_2>0 & kiingereza_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiswahili_1=5 if kiswahili_1>5 & attendance==1
replace kiingereza_1=5 if kiingereza_1>5 & attendance==1





gen KiswahiliLettersGrd1=.
replace KiswahiliLettersGrd1=1 if kiswahili_1>=4 & kiswahili_1!=. & GradeID==1
replace KiswahiliLettersGrd1=0 if kiswahili_1<4 & kiswahili_1!=. & GradeID==1


gen KiswahiliWordsGrd1=.
replace KiswahiliWordsGrd1=1 if kiswahili_2>=4 & kiswahili_2!=. & GradeID==1
replace KiswahiliWordsGrd1=0 if kiswahili_2<4 & kiswahili_2!=. & GradeID==1

gen EnglishLettersGrd1=.
replace EnglishLettersGrd1=1 if kiingereza_1>=4 & kiingereza_1!=. & GradeID==1
replace EnglishLettersGrd1=0 if kiingereza_1<4 & kiingereza_1!=. & GradeID==1

gen EnglishWordsGrd1=.
replace EnglishWordsGrd1=1 if kiingereza_2>=4 & kiingereza_2!=. & GradeID==1
replace EnglishWordsGrd1=0 if kiingereza_2<4 & kiingereza_2!=. & GradeID==1


gen MathCountGrd1=.
replace MathCountGrd1=1 if hisabati_1>=2 & hisabati_1!=. & GradeID==1
replace MathCountGrd1=0 if hisabati_1<2 & hisabati_1!=. & GradeID==1

gen MathNumbersGrd1=.
replace MathNumbersGrd1=1 if hisabati_2>=2 & hisabati_2!=. & GradeID==1
replace MathNumbersGrd1=0 if hisabati_2<2 & hisabati_2!=. & GradeID==1


gen MathIneqGrd1=.
replace MathIneqGrd1=1 if hisabati_3>=4 & hisabati_3!=. & GradeID==1
replace MathIneqGrd1=0 if hisabati_3<4 & hisabati_3!=. & GradeID==1


gen MathAddGrd1=.
replace MathAddGrd1=1 if hisabati_4>=4 & hisabati_4!=. & GradeID==1
replace MathAddGrd1=0 if hisabati_4<4 & hisabati_4!=. & GradeID==1


gen MathSubGrd1=.
replace MathSubGrd1=1 if hisabati_5>=4 & hisabati_5!=. & GradeID==1
replace MathSubGrd1=0 if hisabati_5<4 & hisabati_5!=. & GradeID==1



gen KiswahiliWordsGrd2=.
replace KiswahiliWordsGrd2=1 if kiswahili_2>=4 & kiswahili_2!=. & GradeID==2
replace KiswahiliWordsGrd2=0 if kiswahili_2<4 & kiswahili_2!=. & GradeID==2


gen KiswahiliParaGrd2=.
replace KiswahiliParaGrd2=1 if kiswahili_3==1 & kiswahili_3!=. & GradeID==2
replace KiswahiliParaGrd2=0 if kiswahili_3==0& kiswahili_3!=. & GradeID==2


gen EnglishWordsGrd2=.
replace EnglishWordsGrd2=1 if kiingereza_1>=4 & kiingereza_1!=. & GradeID==2
replace EnglishWordsGrd2=0 if kiingereza_1<4 & kiingereza_1!=. & GradeID==2

gen EnglishParaGrd2=.
replace EnglishParaGrd2=1 if kiingereza_3==1 & kiingereza_3!=. & GradeID==2
replace EnglishParaGrd2=0 if kiingereza_3==0& kiingereza_3!=. & GradeID==2




gen MathIneqGrd2=.
replace MathIneqGrd2=1 if hisabati_3>=4 & hisabati_3!=. & GradeID==2
replace MathIneqGrd2=0 if hisabati_3<4 & hisabati_3!=. & GradeID==2


gen MathAddGrd2=.
replace MathAddGrd2=1 if hisabati_4>=4 & hisabati_4!=. & GradeID==2
replace MathAddGrd2=0 if hisabati_4<4 & hisabati_4!=. & GradeID==2


gen MathSubGrd2=.
replace MathSubGrd2=1 if hisabati_5>=4 & hisabati_5!=. & GradeID==2
replace MathSubGrd2=0 if hisabati_5<4 & hisabati_5!=. & GradeID==2

gen MathMultGrd2=.
replace MathMultGrd2=1 if hisabati_6>=4 & hisabati_6!=. & GradeID==2
replace MathMultGrd2=0 if hisabati_6<4 & hisabati_6!=. & GradeID==2



gen KiswahiliParaGrd3=.
replace KiswahiliParaGrd3=1 if kiswahili_3==1 & kiswahili_3!=. & GradeID==3
replace KiswahiliParaGrd3=0 if kiswahili_3==0& kiswahili_3!=. & GradeID==3

gen KiswahiliQuestionsGrd3=.
replace KiswahiliQuestionsGrd3=1 if kiswahili_5==1 & kiswahili_6==1 & kiswahili_5!=. & kiswahili_6!=. & GradeID==3
replace KiswahiliQuestionsGrd3=0 if (kiswahili_5==0 | kiswahili_6==0) & kiswahili_5!=. & kiswahili_6!=. & GradeID==3


gen EnglishParaGrd3=.
replace EnglishParaGrd3=1 if kiingereza_3==1 & kiingereza_3!=. & GradeID==3
replace EnglishParaGrd3=0 if kiingereza_3==0& kiingereza_3!=. & GradeID==3

gen EnglishQuestionsGrd3=.
replace EnglishQuestionsGrd3=1 if kiingereza_5==1 & kiingereza_6==1 & kiingereza_5!=. & kiingereza_6!=. & GradeID==3
replace EnglishQuestionsGrd3=0 if (kiingereza_5==0 | kiingereza_6==0) & kiingereza_5!=. & kiingereza_6!=. & GradeID==3


gen MathAddGrd3=.
replace MathAddGrd3=1 if hisabati_4>=4 & hisabati_4!=. & GradeID==3
replace MathAddGrd3=0 if hisabati_4<4 & hisabati_4!=. & GradeID==3


gen MathSubGrd3=.
replace MathSubGrd3=1 if hisabati_5>=4 & hisabati_5!=. & GradeID==3
replace MathSubGrd3=0 if hisabati_5<4 & hisabati_5!=. & GradeID==3

gen MathMultGrd3=.
replace MathMultGrd3=1 if hisabati_6>=4 & hisabati_6!=. & GradeID==3
replace MathMultGrd3=0 if hisabati_6<4 & hisabati_6!=. & GradeID==3

keep SchoolID treatment treatarm DistrictID GradeID usid Age Gender preSchoolYN seenUwezoTests KiswahiliLettersGrd1- MathMultGrd3
tabstat KiswahiliLettersGrd1- MathMultGrd3, by(GradeID)
rename Kiswahili* =_T1
rename English* =_T1
rename Math* =_T1
saveold "$base_out/YoudiStudentGrowth_Baseline.dta", replace


*******************************************************
*************END LINE Yr1
**********************************************************

use "$basein/3 Endline/Student/ESTudent_noPII.dta", clear
compress
saveold "$base_out/3 Endline/Student/Student.dta", replace

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID

merge 1:m SchoolID using "$base_out/3 Endline/Student/Student.dta"

drop _merge
compress
saveold "$base_out/3 Endline/Student/Student.dta", replace

gen grade=stdgrd
rename grade GradeID

replace S2_kiswahili_3=0 if S2_kiswahili_3==2
replace S2_kiingereza_3=0 if S2_kiingereza_3==2
replace S3_kiswahili_1=0 if S3_kiswahili_1==2
replace S3_kiingereza_1=0 if S3_kiingereza_1==2


gen KiswahiliLettersGrd1=.
replace KiswahiliLettersGrd1=1 if S1_kiswahili_1>=4 & S1_kiswahili_1!=. & GradeID==1
replace KiswahiliLettersGrd1=0 if S1_kiswahili_1<4 & S1_kiswahili_1!=. & GradeID==1


gen KiswahiliWordsGrd1=.
replace KiswahiliWordsGrd1=1 if S1_kiswahili_2>=4 & S1_kiswahili_2!=. & GradeID==1
replace KiswahiliWordsGrd1=0 if S1_kiswahili_2<4 & S1_kiswahili_2!=. & GradeID==1

gen EnglishLettersGrd1=.
replace EnglishLettersGrd1=1 if S1_kiingereza_1>=4 & S1_kiingereza_1!=. & GradeID==1
replace EnglishLettersGrd1=0 if S1_kiingereza_1<4 & S1_kiingereza_1!=. & GradeID==1

gen EnglishWordsGrd1=.
replace EnglishWordsGrd1=1 if S1_kiingereza_2>=4 & S1_kiingereza_2!=. & GradeID==1
replace EnglishWordsGrd1=0 if S1_kiingereza_2<4 & S1_kiingereza_2!=. & GradeID==1


gen MathCountGrd1=.
replace MathCountGrd1=1 if S1_hisabati_1>=2 & S1_hisabati_1!=. & GradeID==1
replace MathCountGrd1=0 if S1_hisabati_1<2 & S1_hisabati_1!=. & GradeID==1

gen MathNumbersGrd1=.
replace MathNumbersGrd1=1 if S1_hisabati_2>=2 & S1_hisabati_2!=. & GradeID==1
replace MathNumbersGrd1=0 if S1_hisabati_2<2 & S1_hisabati_2!=. & GradeID==1


gen MathIneqGrd1=.
replace MathIneqGrd1=1 if S1_hisabati_3>=4 & S1_hisabati_3!=. & GradeID==1
replace MathIneqGrd1=0 if S1_hisabati_3<4 & S1_hisabati_3!=. & GradeID==1


gen MathAddGrd1=.
replace MathAddGrd1=1 if S1_hisabati_4>=4 & S1_hisabati_4!=. & GradeID==1
replace MathAddGrd1=0 if S1_hisabati_4<4 & S1_hisabati_4!=. & GradeID==1


gen MathSubGrd1=.
replace MathSubGrd1=1 if S1_hisabati_5>=4 & S1_hisabati_5!=. & GradeID==1
replace MathSubGrd1=0 if S1_hisabati_5<4 & S1_hisabati_5!=. & GradeID==1



gen KiswahiliWordsGrd2=.
replace KiswahiliWordsGrd2=1 if S2_kiswahili_1>=4 & S2_kiswahili_1!=. & GradeID==2
replace KiswahiliWordsGrd2=0 if S2_kiswahili_1<4 & S2_kiswahili_1!=. & GradeID==2


gen KiswahiliParaGrd2=.
replace KiswahiliParaGrd2=1 if S2_kiswahili_3==1 & S2_kiswahili_3!=. & GradeID==2
replace KiswahiliParaGrd2=0 if S2_kiswahili_3==0& S2_kiswahili_3!=. & GradeID==2


gen EnglishWordsGrd2=.
replace EnglishWordsGrd2=1 if S2_kiingereza_1>=4 & S2_kiingereza_1!=. & GradeID==2
replace EnglishWordsGrd2=0 if S2_kiingereza_1<4 & S2_kiingereza_1!=. & GradeID==2

gen EnglishParaGrd2=.
replace EnglishParaGrd2=1 if S2_kiingereza_3==1 & S2_kiingereza_3!=. & GradeID==2
replace EnglishParaGrd2=0 if S2_kiingereza_3==0& S2_kiingereza_3!=. & GradeID==2




gen MathIneqGrd2=.
replace MathIneqGrd2=1 if S2_hisabati_1>=4 & S2_hisabati_1!=. & GradeID==2
replace MathIneqGrd2=0 if S2_hisabati_1<4 & S2_hisabati_1!=. & GradeID==2


gen MathAddGrd2=.
replace MathAddGrd2=1 if S2_hisabati_2>=4 & S2_hisabati_2!=. & GradeID==2
replace MathAddGrd2=0 if S2_hisabati_2<4 & S2_hisabati_2!=. & GradeID==2


gen MathSubGrd2=.
replace MathSubGrd2=1 if S2_hisabati_3>=4 & S2_hisabati_3!=. & GradeID==2
replace MathSubGrd2=0 if S2_hisabati_3<4 & S2_hisabati_3!=. & GradeID==2

gen MathMultGrd2=.
replace MathMultGrd2=1 if S2_hisabati_4>=4 & S2_hisabati_4!=. & GradeID==2
replace MathMultGrd2=0 if S2_hisabati_4<4 & S2_hisabati_4!=. & GradeID==2



gen KiswahiliParaGrd3=.
replace KiswahiliParaGrd3=1 if S3_kiswahili_1==1 & S3_kiswahili_1!=. & GradeID==3
replace KiswahiliParaGrd3=0 if S3_kiswahili_1==0& S3_kiswahili_1!=. & GradeID==3

gen KiswahiliQuestionsGrd3=.
replace KiswahiliQuestionsGrd3=1 if S3_kiswahili_2>=2 & S3_kiswahili_2!=. & GradeID==3
replace KiswahiliQuestionsGrd3=0 if S3_kiswahili_2<2 & S3_kiswahili_2!=. & GradeID==3


gen EnglishParaGrd3=.
replace EnglishParaGrd3=1 if S3_kiingereza_1==1 & S3_kiingereza_1!=. & GradeID==3
replace EnglishParaGrd3=0 if S3_kiingereza_1==0& S3_kiingereza_1!=. & GradeID==3

gen EnglishQuestionsGrd3=.
replace EnglishQuestionsGrd3=1 if S3_kiingereza_2>=2 & S3_kiingereza_2!=. & GradeID==3
replace EnglishQuestionsGrd3=0 if S3_kiingereza_2<2 & S3_kiingereza_2!=. & GradeID==3


gen MathAddGrd3=.
replace MathAddGrd3=1 if S3_hisabati_1>=4 & S3_hisabati_1!=. & GradeID==3
replace MathAddGrd3=0 if S3_hisabati_1<4 & S3_hisabati_1!=. & GradeID==3


gen MathSubGrd3=.
replace MathSubGrd3=1 if S3_hisabati_2>=4 & S3_hisabati_2!=. & GradeID==3
replace MathSubGrd3=0 if S3_hisabati_2<4 & S3_hisabati_2!=. & GradeID==3

gen MathMultGrd3=.
replace MathMultGrd3=1 if S3_hisabati_3>=4 & S3_hisabati_3!=. & GradeID==3
replace MathMultGrd3=0 if S3_hisabati_3<4 & S3_hisabati_3!=. & GradeID==3

gen usid=subinstr(upid,"R1STU","",.)
drop if usid==""
keep SchoolID treatment treatarm DistID GradeID upid usid  KiswahiliLettersGrd1- MathMultGrd3
rename Kiswahili* =_T2
rename English* =_T2
rename Math* =_T2
tabstat KiswahiliLettersGrd1- MathMultGrd3, by(GradeID)
saveold "$base_out/YoudiStudentGrowth_EndlineYr1.dta", replace



*******************************************************
*************END LINE YR2
**********************************************************


** WE NEED A BREAK TO CREATE HOME TESTED DATA
use "$basein/8 Endline 2014/Final Data/Household/R6HHData_noPII.dta",clear
drop if consentChild!=1
keep HHID consentChild- edtstm
merge 1:m HHID using "$basein/8 Endline 2014/ID Linkage/R6_Student_HH_ID_Linkage_noPII.dta",keepus(upid HHID)
drop if _merge!=3
rename upid upidst 
drop if upidst==""
drop HHID _merge
saveold "$base_out/8 Endline 2014/Household/HomeTests.dta", replace 
****
***
*Now lets loading the data
use "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta",clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge==2
drop _merge
merge 1:1 upidst using "$base_out/8 Endline 2014/Household/HomeTests.dta", update
gen TestedAtHome=(_merge==4)
*drop if _merge==5
drop _merge
*Create a dummy for whether the student wwas in a school also tested by twaweza
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsTWA.dta"
gen SchoolTWA=(_merge==3)
drop if _merge==2
drop _merge

drop GradeID
gen GradeID=stdgrd


gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance

*drop if attendance==0

*drop if consentChild==2 | consentChild==.


gen date=dofc(time)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kissyl1_1 -othsub1_8{
	recode `var' (.=0) if GradeID==1 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl3_1- othsub3_24{
	recode `var' (.=0) if GradeID==3 & consentChild!=2 & consentChild!=.
}

foreach var of varlist kissyl1_1-othsub3_24 {
	recode `var' (2=0) (3=0) if consentChild!=2 & consentChild!=.
}



egen B_kisLtr1= rowtotal( kissyl1_1 - kissyl1_5),missing
egen B_kisWrd1= rowtotal( kiswrd1_1- kiswrd1_6),missing
egen B_kisSen1= rowtotal( kissen1_1 -kissen1_6),missing
egen B_kisWrit1= rowtotal( kiswri1_1 -kiswri1_2),missing
egen B_engLtr1= rowtotal( englet1_1 - englet1_5),missing
egen B_engWrd1= rowtotal( engwrd1_1- engwrd1_6),missing
egen B_engSen1= rowtotal( engsen1_1 -engsen1_6),missing
egen B_engWrit1= rowtotal( engwri1_1 -engwri1_2),missing
egen B_mathCount1= rowtotal( hiscou1_1 -hiscou1_3),missing
egen B_mathNumber1= rowtotal( hisidn1_1- hisidn1_3),missing
egen B_mathIneq1= rowtotal( hisinq1_1- hisinq1_4),missing
egen B_mathAdd1= rowtotal( hisadd1_1- hisadd1_4),missing
egen B_mathSub1= rowtotal(hissub1_1- hissub1_4),missing
egen B_others1= rowtotal(othsub1_1- othsub1_8),missing



egen B_kisLtr2= rowtotal( kissyl2_1 -kissyl2_4),missing
egen B_kisWrd2= rowtotal( kiswrd2_1 -kiswrd2_9),missing
egen B_kisSen2= rowtotal( kissen2_1- kissen2_9),missing
gen B_kisPara2=kispar2_1
egen B_kisWrit2= rowtotal( kiswri2_1 -kiswri2_4),missing
egen B_engLtr2= rowtotal( englet2_1- englet2_4),missing
egen B_engWrd2= rowtotal( engwrd2_1 -engwrd2_9),missing
egen B_engSen2= rowtotal( engsen2_1- engsen2_9),missing
gen B_engPara2=engpar2_1
egen B_engWrit2= rowtotal( engwri2_1 -engwri2_4),missing
egen B_mathCount2= rowtotal( hiscou2_1 - hiscou2_2),missing
egen B_mathNumber2= rowtotal( hisidn2_1 - hisidn2_2),missing
egen B_mathIneq2= rowtotal( hisinq2_1 -hisinq2_4),missing
egen B_mathAdd2= rowtotal( hisadd2_1- hisadd2_6),missing
egen B_mathSub2= rowtotal(hissub2_1- hissub2_6),missing
egen B_mathMult2= rowtotal(hismul2_1- hismul2_4),missing
egen B_others2= rowtotal(othsub2_1- othsub2_15),missing


egen B_kisLtr3= rowtotal( kissyl3_1 - kissyl3_4),missing
egen B_kisWrd3= rowtotal( kiswrd3_1- kiswrd3_8),missing
egen B_kisSen3= rowtotal( kissen3_1 -kissen3_8),missing
gen B_kisPara3=kispar3_1
egen B_kisRead3= rowtotal( kissto3_1 -kiscom3_3),missing /*4 questions*/
egen B_kisWrit3= rowtotal( kiswri3_1 -kiswri3_6),missing
egen B_engLtr3= rowtotal( englet3_1- englet3_4),missing
egen B_engWrd3= rowtotal( engwrd3_1 - engwrd3_8),missing
egen B_engSen3= rowtotal( engsen3_1- engsen3_8),missing
gen B_engPara3=engpar3_1
egen B_engRead3= rowtotal( engsto3_1 -engcom3_3),missing /*4 questions*/
egen B_engWrit3= rowtotal( engwri3_1 -engwri3_6),missing
egen B_mathCount3= rowtotal( hiscou3_1- hiscou3_2),missing
egen B_mathNumber3= rowtotal(hisidn3_1- hisidn3_2),missing
egen B_mathIneq3= rowtotal( hisinq3_1- hisinq3_4),missing
egen B_mathAdd3= rowtotal( hisadd3_1- hisadd3_7),missing
egen B_mathSub3= rowtotal(hissub3_1- hissub3_7),missing
egen B_mathMult3= rowtotal(hismul3_1- hismul3_5),missing
egen B_mathDivi3= rowtotal(hisdiv3_1- hisdiv3_3),missing
egen B_others3= rowtotal(othsub3_1- othsub3_24),missing









gen KiswahiliLettersGrd1=.
replace KiswahiliLettersGrd1=1 if B_kisLtr2>=4 & B_kisLtr2!=. & GradeID==2
replace KiswahiliLettersGrd1=0 if B_kisLtr2<4 & B_kisLtr2!=. & GradeID==2


gen KiswahiliWordsGrd1=.
replace KiswahiliWordsGrd1=1 if B_kisWrd2>=4 & B_kisWrd2!=. & GradeID==2
replace KiswahiliWordsGrd1=0 if B_kisWrd2<4 & B_kisWrd2!=. & GradeID==2

gen EnglishLettersGrd1=.
replace EnglishLettersGrd1=1 if B_engLtr2>=4 & B_engLtr2!=. & GradeID==2
replace EnglishLettersGrd1=0 if B_engLtr2<4 & B_engLtr2!=. & GradeID==2

gen EnglishWordsGrd1=.
replace EnglishWordsGrd1=1 if B_engWrd2>=4 & B_engWrd2!=. & GradeID==2
replace EnglishWordsGrd1=0 if B_engWrd2<4 & B_engWrd2!=. & GradeID==2


gen MathCountGrd1=.
replace MathCountGrd1=1 if B_mathCount2>=2 & B_mathCount2!=. & GradeID==2
replace MathCountGrd1=0 if B_mathCount2<2 & B_mathCount2!=. & GradeID==2

gen MathNumbersGrd1=.
replace MathNumbersGrd1=1 if B_mathNumber2>=2 & B_mathNumber2!=. & GradeID==2
replace MathNumbersGrd1=0 if B_mathNumber2<2 & B_mathNumber2!=. & GradeID==2


gen MathIneqGrd1=.
replace MathIneqGrd1=1 if B_mathIneq2>=4 & B_mathIneq2!=. & GradeID==2
replace MathIneqGrd1=0 if B_mathIneq2<4 & B_mathIneq2!=. & GradeID==2


gen MathAddGrd1=.
replace MathAddGrd1=1 if B_mathAdd2>=4 & B_mathAdd2!=. & GradeID==2
replace MathAddGrd1=0 if B_mathAdd2<4 & B_mathAdd2!=. & GradeID==2


gen MathSubGrd1=.
replace MathSubGrd1=1 if B_mathSub2>=4 & B_mathSub2!=. & GradeID==2
replace MathSubGrd1=0 if B_mathSub2<4 & B_mathSub2!=. & GradeID==2






gen KiswahiliWordsGrd2=.
replace KiswahiliWordsGrd2=1 if B_kisWrd3>=4 & B_kisWrd3!=. & GradeID==3
replace KiswahiliWordsGrd2=0 if B_kisWrd3<4 & B_kisWrd3!=. & GradeID==3


gen KiswahiliParaGrd2=.
replace KiswahiliParaGrd2=1 if B_kisPara3==1 & B_kisPara3!=. & GradeID==3
replace KiswahiliParaGrd2=0 if B_kisPara3==0& B_kisPara3!=. & GradeID==3


gen EnglishWordsGrd2=.
replace EnglishWordsGrd2=1 if B_engWrd3>=4 & B_engWrd3!=. & GradeID==3
replace EnglishWordsGrd2=0 if B_engWrd3<4 & B_engWrd3!=. & GradeID==3

gen EnglishParaGrd2=.
replace EnglishParaGrd2=1 if B_engPara3==1 & B_engPara3!=. & GradeID==3
replace EnglishParaGrd2=0 if B_engPara3==0& B_engPara3!=. & GradeID==3



gen MathIneqGrd2=.
replace MathIneqGrd2=1 if B_mathIneq3>=4 & B_mathIneq3!=. & GradeID==3
replace MathIneqGrd2=0 if B_mathIneq3<4 & B_mathIneq3!=. & GradeID==3


gen MathAddGrd2=.
replace MathAddGrd2=1 if B_mathAdd3>=4 & B_mathAdd3!=. & GradeID==3
replace MathAddGrd2=0 if B_mathAdd3<4 & B_mathAdd3!=. & GradeID==3


gen MathSubGrd2=.
replace MathSubGrd2=1 if B_mathSub3>=4 & B_mathSub3!=. & GradeID==3
replace MathSubGrd2=0 if B_mathSub3<4 & B_mathSub3!=. & GradeID==3

gen MathMultGrd2=.
replace MathMultGrd2=1 if B_mathMult3>=4 & B_mathMult3!=. & GradeID==3
replace MathMultGrd2=0 if B_mathMult3<4 & B_mathMult3!=. & GradeID==3
drop if GradeID==1



rename upidst upid
keep SchoolID treatment treatarm  GradeID upid  KiswahiliLettersGrd1- MathMultGrd2
rename Kiswahili* =_T3
rename English* =_T3
rename Math* =_T3

tabstat KiswahiliLettersGrd1- MathMultGrd2, by(GradeID)
saveold "$base_out/YoudiStudentGrowth_EndlineYr2.dta", replace


use "$base_out/YoudiStudentGrowth_Baseline.dta", replace
merge m:1 usid using "$base_out/YoudiStudentGrowth_EndlineYr1.dta", keepus(upid KiswahiliLettersGrd1- MathMultGrd3)
drop if _merge!=3
drop _merge
merge 1:1 upid using "$base_out/YoudiStudentGrowth_EndlineYr2.dta", keepus(KiswahiliLettersGrd1- MathMultGrd2)
drop if _merge==2
drop _merge

saveold "$base_out/YoudiStudentGrowth.dta", replace
preserve
reshape long KiswahiliLettersGrd1@ KiswahiliWordsGrd1@ KiswahiliWordsGrd2@ KiswahiliParaGrd2@ KiswahiliParaGrd3@ KiswahiliQuestionsGrd3@ EnglishLettersGrd1@ EnglishWordsGrd1@ EnglishWordsGrd2@ EnglishParaGrd2@ EnglishParaGrd3@ EnglishQuestionsGrd3@ MathCountGrd1@ MathNumbersGrd1@ MathIneqGrd1@ MathAddGrd1@ MathSubGrd1@ MathIneqGrd2@ MathAddGrd2@ MathSubGrd2@ MathMultGrd2@ MathAddGrd3@ MathSubGrd3@ MathMultGrd3@, i(upid) j(Period) string
saveold "$base_out/YoudiStudentGrowth_Long.dta", replace
restore

collapse (mean) Kiswahili* English* Math* , by(SchoolID treatment treatarm)
preserve
collapse (mean) Kiswahili* English* Math* , by(treatment treatarm)
saveold "$base_out/YoudiStudentGrowth_Treatment.dta", replace
restore
saveold "$base_out/YoudiStudentGrowth_School.dta", replace
reshape long KiswahiliLettersGrd1@ KiswahiliWordsGrd1@ KiswahiliWordsGrd2@ KiswahiliParaGrd2@ KiswahiliParaGrd3@ KiswahiliQuestionsGrd3@ EnglishLettersGrd1@ EnglishWordsGrd1@ EnglishWordsGrd2@ EnglishParaGrd2@ EnglishParaGrd3@ EnglishQuestionsGrd3@ MathCountGrd1@ MathNumbersGrd1@ MathIneqGrd1@ MathAddGrd1@ MathSubGrd1@ MathIneqGrd2@ MathAddGrd2@ MathSubGrd2@ MathMultGrd2@ MathAddGrd3@ MathSubGrd3@ MathMultGrd3@, i(SchoolID) j(Period) string
saveold "$base_out/YoudiStudentGrowth_School_Long.dta", replace
collapse (mean) Kiswahili* English* Math* , by(treatment treatarm Period)
saveold "$base_out/YoudiStudentGrowth_Treatment_Long.dta", replace

