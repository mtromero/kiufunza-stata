use  "$basein\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents.dta", clear
*drop _merge
drop SchoolID_15_s Grade_15_s USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}


*drop if DistrictID==11
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)

merge m:1 SchoolID using "$basein\TreatmentStatusYr3-4\RandomizeStatus.dta", keepus(treatment2 treatarm2)
drop _merge

merge m:1 SchoolID using "$basein\4 Intervention\TwaBL_2014\PilotSchData.dta", keepus(SITotEnrol SINumAllTeach SITotEnrolBoy SITotEnrolGirl)
drop _merge
gen TeacherPupilRatio=SITotEnrol/SINumAllTeach
gen BoyFemaleRatio=SITotEnrolBoy/SITotEnrolGirl
drop SITotEnrol SINumAllTeach SITotEnrolBoy SITotEnrolGirl


merge m:1 SchoolID using "$mipath\CommScripts\Kigoma Sample\KFII_KigomaSample.dta", keepus(treatment2 treatarm2) update

replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440


drop _merge
drop if treatarm2==.

**Calculate enrollmente not EDI, but previous TWA
preserve
collapse (count) Enrollment=TestedEL, by(SchoolID Grade treatment2 treatarm2 DistrictID)
drop if Grade==.
reshape wide Enrollment, i(SchoolID) j(Grade)
save "$base_out\Consolidated\EnroollmnetGradeTWA.dta", replace
restore
***Back to main code

tabstat Grade, by(treatment2) statistics (count)
tabstat Grade if treatment2=="Gains", by(Grade) statistics (count)
tabstat Grade if treatment2=="Levels", by(Grade) statistics (count)


 
 
 *First we need to clean the data a bit
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6
 
 *Now we generate LEVELS DATA!!
gen Pass_Syll_Kis=(Kis_SI==4) if !missing(Kis_SI)
gen Pass_Word_Kis=(Kis_MA==4) if !missing(Kis_MA)
gen Pass_Sent_Kis=(Kis_SE==4) if !missing(Kis_SE)
gen Pass_Para_Kis=(Kis_A==1) if !missing(Kis_A)
gen Pass_Read_Kis=(Kis_H==1) if !missing(Kis_H)
gen Pass_Compre_Kis=(Kis_M==2) if !missing(Kis_M)


gen Pass_Syll_Eng=(Eng_L==4) if !missing(Eng_L)
gen Pass_Word_Eng=(Eng_W==4) if !missing(Eng_W)
gen Pass_Sent_Eng=(Eng_SE==4) if !missing(Eng_SE)
gen Pass_Para_Eng=(Eng_P==1) if !missing(Eng_P)
gen Pass_Read_Eng=(Eng_S==1) if !missing(Eng_S)
gen Pass_Compre_Eng=(Eng_C==2) if !missing(Eng_C)
   
       
gen Pass_Count_Math=(Math_ID==4) if !missing(Math_ID)
gen Pass_Numbers_Math=(Math_UTA==4) if !missing(Math_UTA)
gen Pass_Inequal_Math=(Math_BWA==4) if !missing(Math_BWA)
gen Pass_Add_Math=(Math_J==4) if !missing(Math_J)
gen Pass_Sub_Math=(Math_T==4) if !missing(Math_T)
gen Pass_Mult_Math=(Math_Z==4) if !missing(Math_Z)
gen Pass_Div_Math=(Math_G==4) if !missing(Math_G)   


gen Kis_Pass2=(Pass_Syll_Kis==1 & Pass_Word_Kis==1 & Pass_Sent_Kis==1 ) if Grade==1 /* & TestedEL==1 */
replace Kis_Pass2=(Pass_Word_Kis==1 & Pass_Sent_Kis==1 & Pass_Para_Kis==1) if Grade==2 /* & TestedEL==1 */
replace Kis_Pass2=( Pass_Read_Kis==1 & Pass_Compre_Kis==1) if  Grade==3 /* & TestedEL==1 */


gen Eng_Pass2=(Pass_Syll_Eng==1 & Pass_Word_Eng==1 & Pass_Sent_Eng==1 ) if Grade==1 /* & TestedEL==1 */
replace Eng_Pass2=(Pass_Word_Eng==1 & Pass_Sent_Eng==1 & Pass_Para_Eng==1) if Grade==2 /* & TestedEL==1 */
replace Eng_Pass2=(Pass_Read_Eng==1 & Pass_Compre_Eng==1) if  Grade==3 /* & TestedEL==1 */


gen Math_Pass2=(Pass_Count_Math==1 & Pass_Numbers_Math==1 & Pass_Inequal_Math==1 & Pass_Add_Math==1 & Pass_Sub_Math==1 ) if Grade==1 /* & TestedEL==1 */
replace Math_Pass2=(Pass_Inequal_Math==1 & Pass_Add_Math==1 & Pass_Sub_Math==1 & Pass_Mult_Math==1) if Grade==2 /* & TestedEL==1 */
replace Math_Pass2=(Pass_Add_Math==1 & Pass_Sub_Math==1 & Pass_Mult_Math==1 & Pass_Div_Math==1) if  Grade==3 /* & TestedEL==1 */

tab Kis_Pass2 Kis_Pass
tab Eng_Pass2 Eng_Pass
tab Math_Pass2 Math_Pass


tab Pass_Syll_Kis if Grade==1 & treatment2=="Levels"
tab Pass_Word_Kis if Grade==1 & treatment2=="Levels"
tab Pass_Sent_Kis if Grade==1 & treatment2=="Levels"

tab Pass_Syll_Eng if Grade==1 & treatment2=="Levels"
tab Pass_Word_Eng if Grade==1 & treatment2=="Levels"
tab Pass_Sent_Eng if Grade==1 & treatment2=="Levels"

tab Pass_Count_Math if Grade==1 & treatment2=="Levels"
tab Pass_Numbers_Math if Grade==1 & treatment2=="Levels"
tab Pass_Inequal_Math if Grade==1 & treatment2=="Levels"
tab Pass_Add_Math if Grade==1 & treatment2=="Levels"
tab Pass_Sub_Math if Grade==1 & treatment2=="Levels"


tab Pass_Word_Kis if Grade==2 & treatment2=="Levels"
tab Pass_Sent_Kis if Grade==2 & treatment2=="Levels"
tab Pass_Para_Kis if Grade==2 & treatment2=="Levels"

tab Pass_Word_Eng if Grade==2 & treatment2=="Levels"
tab Pass_Sent_Eng if Grade==2 & treatment2=="Levels"
tab Pass_Para_Eng if Grade==2 & treatment2=="Levels"

tab Pass_Inequal_Math if Grade==2 & treatment2=="Levels"
tab Pass_Add_Math if Grade==2 & treatment2=="Levels"
tab Pass_Sub_Math if Grade==2 & treatment2=="Levels"
tab Pass_Mult_Math if Grade==2 & treatment2=="Levels"

tab Pass_Read_Kis if Grade==3 & treatment2=="Levels"
tab Pass_Compre_Kis if Grade==3 & treatment2=="Levels"


tab Pass_Read_Eng if Grade==3 & treatment2=="Levels"
tab Pass_Compre_Eng if Grade==3 & treatment2=="Levels"

tab Pass_Add_Math if Grade==3 & treatment2=="Levels"
tab Pass_Sub_Math if Grade==3 & treatment2=="Levels"
tab Pass_Mult_Math if Grade==3 & treatment2=="Levels"
tab Pass_Div_Math if Grade==3 & treatment2=="Levels"


replace Pass_Syll_Kis=. if Grade!=1
replace Pass_Syll_Eng=. if Grade!=1
replace Pass_Count_Math=. if Grade!=1
replace Pass_Numbers_Math=. if Grade!=1

replace Pass_Para_Kis=. if Grade!=2
replace Pass_Para_Eng=. if Grade!=2

replace Pass_Read_Kis=. if Grade!=3
replace Pass_Compre_Kis=. if Grade!=3
replace Pass_Read_Eng=. if Grade!=3
replace Pass_Compre_Eng=. if Grade!=3

eststo clear
estpost tabstat Pass*, by(Grade) columns(statistics)
esttab using "$results\ExcelTables\PassRateTwaYr2.csv", replace main(mean)




sum if Grade==1 & treatment2=="Levels"
sum if Grade==2 & treatment2=="Levels"
sum if Grade==3 & treatment2=="Levels"


tab Kis_Pass2 if Grade==1 & treatment2=="Levels"
tab Eng_Pass2 if Grade==1 & treatment2=="Levels"
tab Math_Pass2 if Grade==1 & treatment2=="Levels"
tab Kis_Pass2 if Grade==2 & treatment2=="Levels"
tab Eng_Pass2 if Grade==2 & treatment2=="Levels"
tab Math_Pass2 if Grade==2 & treatment2=="Levels"
tab Kis_Pass2 if Grade==3 & treatment2=="Levels"
tab Eng_Pass2 if Grade==3 & treatment2=="Levels"
tab Math_Pass2 if Grade==3 & treatment2=="Levels"


 forvalues i=2/2{
foreach var of varlist Kis_SI- Math_Pass{
tab `var' if Grade==`i'
}
}



capture drop KisLevel1
gen KisLevel1=1 if Kis_SI==0 & Math_UTA<=3 & Grade==1 & TestedEL==1
replace KisLevel1=2 if Kis_SI==0 & ((Math_UTA>=3 & Math_BWA<=3) | (Math_BWA==4 & Math_J==0 & Math_T==0)) & Grade==1 & TestedEL==1
replace KisLevel1=3 if Kis_SI==0 & (Math_J>0 | Math_T>0) & Grade==1 & TestedEL==1
replace KisLevel1=4 if Kis_SI>=1 & Kis_SI<=2 & Grade==1 & TestedEL==1
replace KisLevel1=5 if Kis_SI>=3 & Kis_MA<=3 & Grade==1 & TestedEL==1
replace KisLevel1=6 if Kis_MA==4 & Kis_SE<=3 & Grade==1 & TestedEL==1
replace KisLevel1=7 if  Kis_SE==4 & Eng_L==0  & Grade==1 & TestedEL==1
replace KisLevel1=8 if  Kis_SE==4 & Eng_L>=1 & Eng_L<=3  & Grade==1 & TestedEL==1
replace KisLevel1=9 if  Kis_SE==4 &  Eng_L==4 & Grade==1 & TestedEL==1
replace KisLevel1=0 if Grade==1 & TestedEL==0
tab KisLevel1 if treatment2=="Gains"

capture drop EngLevel1
gen EngLevel1=1 if Eng_L==0 & (Math_ID<=3 | (Math_ID==4 & Math_UTA<=2)) &  Grade==1 & TestedEL==1
replace EngLevel1=2 if Eng_L==0 & Math_UTA>=3 & Math_BWA<=3 & Grade==1 & TestedEL==1
replace EngLevel1=3 if Eng_L==0 & Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==1 & TestedEL==1
replace EngLevel1=4 if Eng_L==0 & (Math_J>0 | Math_T>0) & (Math_J<=3 | Math_T<=3) & Grade==1 & TestedEL==1
replace EngLevel1=5 if Eng_L==0 & Math_J==4 & Math_T==4 & Grade==1 & TestedEL==1
replace EngLevel1=6 if Eng_L==0 & Math_J==4 & Math_T==4 & Kis_SE==4 & Grade==1 & TestedEL==1
replace EngLevel1=7 if Eng_L==1  & Grade==1 & TestedEL==1
replace EngLevel1=8 if Eng_L>=2 & Eng_L<=3 & Grade==1 & TestedEL==1
replace EngLevel1=9 if Eng_L==4 & Eng_SE<=3 & Grade==1 & TestedEL==1
replace EngLevel1=10 if Eng_SE==4 & Grade==1
replace EngLevel1=0 if Grade==1 & TestedEL==0
tab EngLevel1 if treatment2=="Gains"

capture drop MathLevel1
gen MathLevel1=1 if (Math_ID<=3 | Math_UTA<=2) & Grade==1 & TestedEL==1
replace MathLevel1=2 if Math_UTA>=3 & Math_BWA<=3 & Grade==1 & TestedEL==1
replace MathLevel1=3 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==1 & TestedEL==1
replace MathLevel1=4 if (Math_J>0 | Math_T>0) & (Math_J<=3 | Math_T<=3) & Grade==1 & TestedEL==1
replace MathLevel1=5 if Math_J==4 & Math_T==4 & Kis_SE<4 & Grade==1 & TestedEL==1
replace MathLevel1=6 if Math_J==4 & Math_T==4 & Kis_SE==4 & Grade==1 & TestedEL==1
replace MathLevel1=7 if Math_J==4 & Math_T==4 & Eng_L>=2 & Kis_SE==4 & Grade==1 & TestedEL==1
replace MathLevel1=8 if Math_J==4 & Math_T==4 & Eng_SE>=1 & Kis_SE==4 & Grade==1 & TestedEL==1
replace MathLevel1=0 if Grade==1 & TestedEL==0
tab MathLevel1 if treatment2=="Gains"

************************
capture drop KisLevel2
gen KisLevel2=1 if Math_BWA==0 & Kis_MA==0 & Grade==2 & TestedEL==1
replace KisLevel2=2 if Math_BWA>0 & Math_BWA<=3  & Kis_MA==0 & Grade==2 & TestedEL==1
replace KisLevel2=3 if Math_BWA==4 & Kis_MA==0 & Grade==2 & TestedEL==1
replace KisLevel2=4 if Kis_MA>=1 & Kis_MA<=3 & Grade==2 & TestedEL==1
replace KisLevel2=5 if Kis_MA==4 & Kis_SE<=4 & Grade==2 & TestedEL==1
replace KisLevel2=6 if (Math_J+Math_T>0 ) & (Math_J+Math_T<=7) &  Kis_SE==4  & Grade==2 & TestedEL==1
replace KisLevel2=7 if Math_J==4 & Math_T==4 & Math_Z<4 & Kis_SE==4  & Grade==2 & TestedEL==1
replace KisLevel2=8 if Math_Z==4 & Kis_SE==4  & Grade==2 & TestedEL==1
replace KisLevel2=9 if Math_Z==4 & Eng_W>0 & Kis_SE==4  & Grade==2 & TestedEL==1
replace KisLevel2=0 if Grade==2 & TestedEL==0
tab KisLevel2 if treatment2=="Gains"



capture drop EngLevel2
gen EngLevel2=1 if Eng_W==0 & Math_BWA==0 & Grade==2 & TestedEL==1
replace EngLevel2=2 if Eng_W==0 & Math_BWA>=1 & Math_BWA<=3 & Grade==2 & TestedEL==1
replace EngLevel2=3 if Eng_W==0 & Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==2 & TestedEL==1
replace EngLevel2=4 if Eng_W==0 & (Math_J+Math_T>0 ) & (Math_J+Math_T<=4) & Grade==2 & TestedEL==1
replace EngLevel2=5 if Eng_W==0 & Math_J+Math_T>=5 & Math_J+Math_T<=7 &  Grade==2 & TestedEL==1
replace EngLevel2=6 if Eng_W==0 & Math_J==4 & Math_T==4 & Math_Z<4 &  Grade==2 & TestedEL==1
replace EngLevel2=7 if Eng_W==0 & Math_Z==4 &  Grade==2 & TestedEL==1
replace EngLevel2=8 if Eng_W>0 & Eng_W<=2 & Grade==2 & TestedEL==1
replace EngLevel2=9 if Eng_W>=3 & Grade==2 & TestedEL==1
replace EngLevel2=10 if  Eng_SE==4  & Grade==2 & TestedEL==1
replace EngLevel2=0 if Grade==2 & TestedEL==0
tab EngLevel2 if treatment2=="Gains"

capture drop MathLevel2
gen MathLevel2=1 if Math_BWA==0 & Grade==2 & TestedEL==1
replace MathLevel2=2 if Math_BWA>=1 & Math_BWA<=3  & Grade==2 & TestedEL==1
replace MathLevel2=3 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==2 & TestedEL==1
replace MathLevel2=4 if (Math_J+Math_T>0 ) & (Math_J+Math_T<=4)  & Grade==2 & TestedEL==1
replace MathLevel2=5 if Math_J+Math_T>=5 & Math_J+Math_T<=7  & Grade==2 & TestedEL==1
replace MathLevel2=6 if Math_J==4 & Math_T==4 & Math_Z<4 & Grade==2 & TestedEL==1
replace MathLevel2=7 if Math_J==4 & Math_T==4 & Math_Z==4 & Grade==2 & TestedEL==1
replace MathLevel2=8 if Math_J==4 & Math_T==4 & Math_Z==4 & Eng_SE==4 & Grade==2 & TestedEL==1
replace MathLevel2=0 if Grade==2 & TestedEL==0
tab MathLevel2 if treatment2=="Gains"

save "$base_out\Consolidated\TWA_StudentLevelsBatches.dta", replace

drop if Grade!=1
keep if treatment2=="Gains"
collapse (mean) Eng_L Eng_W Eng_SE Kis_SI Kis_MA Kis_SE Math_ID Math_UTA Math_BWA Math_J Math_T TeacherPupilRatio BoyFemaleRatio, by(SchoolID)

/*
pca Eng_L Eng_W Eng_SE, components(1)
predict Eng, score
xtile EngQ = Eng, nquantiles(10) 

pca Kis_SI Kis_MA Kis_SE, components(1)
predict Kis, score
xtile KisQ = Kis, nquantiles(10) 


pca Math_ID Math_UTA Math_BWA Math_J Math_T, components(1)
predict Math, score
xtile MathQ = Math, nquantiles(10) 
*/

pca Eng_L Eng_W Eng_SE Kis_SI Kis_MA Kis_SE Math_ID Math_UTA Math_BWA Math_J Math_T, components(1)
predict Total, score
xtile TotalQ = Total, nquantiles(10) 
save "$base_out\Consolidated\TWA_SchoolLevelsBatches.dta", replace

use "$base_out\Consolidated\TWA_StudentLevelsBatches.dta", clear

merge m:1 SchoolID using "$base_out\Consolidated\TWA_SchoolLevelsBatches.dta", keepus(TotalQ)
drop _merge
save "$base_out\Consolidated\TWA_StudentLevelsBatches.dta", replace

tab TotalQ


************************
************************
************************
*Here I calculate total numbers to get a budget estimate

use  "$basein\4 Intervention\TwaBL_2014\PilotSchData.dta", clear
keep if SchoolID==441
keep SIStd1Enrol SIStd2Enrol SIStd3Enrol SchoolID DistrictID
destring, replace
save "$base_out\Consolidated\EnroollmnetGrade441.dta", replace

use  "$basein\9 Baseline 2015\Final Data\School\R7Grade_noPII.dta", clear
gen EnroolmentEDI=s184+s185
keep SchoolID R7GradeID Enroolment
reshape wide Enroolment, i(SchoolID) j(R7GradeID)
drop EnroolmentEDI4 EnroolmentEDI5 EnroolmentEDI6 EnroolmentEDI7

merge 1:1 SchoolID using "$base_out\Consolidated\EnroollmnetGradeTWA.dta"
drop _merge
merge 1:1 SchoolID using "$base_out\Consolidated\EnroollmnetGrade441.dta"
drop _merge
replace treatarm2=1 if SchoolID==441
replace treatment2="Levels" if SchoolID==441
replace treatarm2=2 if SchoolID==440
replace treatment2="Gains" if SchoolID==440
drop if treatarm2==.
gen EnroolmentFinal1=EnroolmentEDI1 if EnroolmentEDI1!=.
gen EnroolmentFinal2=EnroolmentEDI2 if EnroolmentEDI2!=.
gen EnroolmentFinal3=EnroolmentEDI3 if EnroolmentEDI3!=.

replace EnroolmentFinal1=Enrollment1 if Enrollment1!=. & missing(EnroolmentFinal1)
replace EnroolmentFinal2=Enrollment2 if Enrollment1!=. & missing(EnroolmentFinal2)
replace EnroolmentFinal3=Enrollment3 if Enrollment1!=. & missing(EnroolmentFinal3)

replace EnroolmentFinal1=SIStd1Enro if SIStd1Enro!=. & missing(EnroolmentFinal1)
replace EnroolmentFinal2=SIStd2Enro if SIStd2Enro!=. & missing(EnroolmentFinal2)
replace EnroolmentFinal3=SIStd3Enro if SIStd3Enro!=. & missing(EnroolmentFinal3)


*egen EnroolmentUse1=rowtotal(EnroolmentEDI1 Enrollment1)
*egen EnroolmentUse2=rowtotal(EnroolmentEDI2 Enrollment2)
*egen EnroolmentUse3=rowtotal(EnroolmentEDI3 Enrollment3)
*gen EnroolmentUse=EnroolmentUse1+EnroolmentUse2+EnroolmentUse3
collapse (sum) EnroolmentFinal1 EnroolmentFinal2 EnroolmentFinal3,  by( treatment2 treatarm2)


