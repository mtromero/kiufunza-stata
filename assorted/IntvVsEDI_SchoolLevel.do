use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
keep SchoolID
sort SchoolID
quietly by SchoolID:  gen dup = cond(_N==1,0,_n)
drop if dup>1		
save "$base_out\Consolidated\SchoolsTWA.dta", replace

		
use "$basein\8 Endline 2014\Final Data\Student\R6Student_noPII.dta",clear
merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "$base_out\Consolidated\SchoolsTWA.dta"
drop if _merge!=3
drop _merge


gen grade=0
drop if consentChild==2 | consentChild==.
drop kiswri1_1 kiswri1_2
drop engwri1_1 engwri1_2
drop kissyl2_1 kissyl2_2 kissyl2_3 kissyl2_4
drop kiswri2_1 kiswri2_2 kiswri2_3 kiswri2_4
drop englet2_1 englet2_2 englet2_3 englet2_4 engwri2_1 engwri2_2 engwri2_3 engwri2_4
drop hiscou2_1 hiscou2_2 hisidn2_1 hisidn2_2
drop kissyl3_1- kispar3_1
drop kiswri3_1- kiswri3_6
drop englet3_1- engpar3_1
drop engwri3_1- engwri3_6
drop hiscou3_1- hisinq3_4
 

foreach var of varlist kissyl1_1- othsub1_8 {
	replace `var'=0 if `var'!=1 & stdgrd==1
}


foreach var of varlist kiswrd2_1- othsub2_15 {
	replace `var'=0 if `var'!=1 & stdgrd==2
}


foreach var of varlist kissto3_1- othsub3_24 {
	replace `var'=0 if `var'!=1 & stdgrd==3
}



egen kisLtr1= rowtotal( kissyl1_1 - kissyl1_5),missing
egen kisWrd1= rowtotal( kiswrd1_1- kiswrd1_6),missing
egen kisSen1= rowtotal( kissen1_1 -kissen1_6),missing
egen engLtr1= rowtotal( englet1_1 - englet1_5),missing
egen engWrd1= rowtotal( engwrd1_1- engwrd1_6),missing
egen engSen1= rowtotal( engsen1_1 -engsen1_6),missing
egen mathCount1= rowtotal( hiscou1_1 -hiscou1_3),missing
egen mathNumber1= rowtotal( hisidn1_1- hisidn1_3),missing
egen mathIneq1= rowtotal( hisinq1_1- hisinq1_4),missing
egen mathAdd1= rowtotal( hisadd1_1- hisadd1_4),missing
egen mathSub1= rowtotal(hissub1_1- hissub1_4),missing
egen others1= rowtotal(othsub1_1- othsub1_8),missing




egen kisWrd2= rowtotal( kiswrd2_1 -kiswrd2_9),missing
egen kisSen2= rowtotal( kissen2_1- kissen2_9),missing
gen kisPara2=kispar2_1
egen engWrd2= rowtotal( engwrd2_1 -engwrd2_9),missing
egen engSen2= rowtotal( engsen2_1- engsen2_9),missing
gen engPara2=engpar2_1
egen mathIneq2= rowtotal( hisinq2_1 -hisinq2_4),missing
egen mathAdd2= rowtotal( hisadd2_1- hisadd2_6),missing
egen mathSub2= rowtotal(hissub2_1- hissub2_6),missing
egen mathMult2= rowtotal(hismul2_1- hismul2_4),missing
egen others2= rowtotal(othsub2_1- othsub2_15),missing


egen kisRead3= rowtotal( kissto3_1 -kiscom3_3),missing /*4 questions*/
egen engRead3= rowtotal( engsto3_1 -engcom3_3),missing /*4 questions*/
egen mathAdd3= rowtotal( hisadd3_1- hisadd3_7),missing
egen mathSub3= rowtotal(hissub3_1- hissub3_7),missing
egen mathMult3= rowtotal(hismul3_1- hismul3_5),missing
egen mathDivi3= rowtotal(hisdiv3_1- hisdiv3_3),missing
egen others3= rowtotal(othsub3_1- othsub3_24),missing


foreach var of varlist kisLtr1- others3 {
sum `var' if treatarm==4
gen Z_`var'= (`var'-r(mean))/r(sd)
}


egen Z_kiswahili_B=rowtotal(Z_kis*),missing
egen Z_kiingereza_B=rowtotal(Z_eng*),missing
egen Z_hisabati_B=rowtotal(Z_math*),missing
egen Z_sayansi_B=rowtotal(Z_oth*),missing

***Now we standarize by grade subject
forvalues val=1/3{
foreach var of varlist Z_kiswahili_B Z_kiingereza_B Z_hisabati_B Z_sayansi_B {
sum `var' if GradeID==`val' & treatarm==4
replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
}
}


foreach var of varlist Z_kiswahili_B Z_kiingereza_B Z_hisabati_B Z_sayansi_B {
sum `var' if  treatarm==4
replace `var'=(`var'-r(mean))/r(sd)
}
keep SchoolID GradeID Z_kiswahili_B Z_kiingereza_B Z_hisabati_B time

save "$base_out\Consolidated\StudentLevelEDIvsTWA.dta", replace

egen TimeMode= mode(time),by(SchoolID) maxmode
gen dateEDI=dofc(TimeMode)
format dateEDI %dN/D/Y
format dateEDI %td
collapse (mean) Z_kiswahili_B Z_kiingereza_B Z_hisabati_B (first) dateEDI=dateEDI, by(SchoolID GradeID)
save "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta", replace


*2014_InterventionDataAnalysis
use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
*First we need to clean the data a bit
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6

 
forvalues grd=1/3{
foreach var of varlist Kis_SI- Kis_M Eng_L- Eng_C Math_ID- Math_G{
sum `var' if Grade==`grd' & treatment==4
capture gen Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd'
capture replace Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd'
}
}



egen Z_kiswahili=rowtotal(Z_Kis*), missing
egen Z_kiingereza=rowtotal(Z_Eng*), missing
egen Z_hisabati=rowtotal(Z_Math*), missing

***Now we standarize by grade subject
forvalues val=1/3{
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if Grade==`val' & treatment==4
replace `var'=(`var'-r(mean))/r(sd) if Grade==`val'
}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if  treatment==4
replace `var'=(`var'-r(mean))/r(sd)
}

rename Grade GradeID
keep SchoolID GradeID Z_kiswahili Z_kiingereza Z_hisabati date
append using "$base_out\Consolidated\StudentLevelEDIvsTWA.dta"
merge m:1 SchoolID using "$base_out\Consolidated\School.dta", keepus(treatment treatarm DistrictID  s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1)
drop _merge
save "$base_out\Consolidated\StudentLevelEDIvsTWA.dta", replace

egen dateTWA= mode(date),by(SchoolID) maxmode
format dateTWA %td

collapse (mean) Z_kiswahili Z_kiingereza Z_hisabati (first) dateTWA=dateTWA, by(SchoolID GradeID)

merge 1:1 SchoolID GradeID using "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta"
drop _merge
merge m:1 SchoolID using "$base_out\Consolidated\School.dta", keepus(treatment treatarm DistrictID  s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1)
drop _merge
save "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta", replace

use "$basein\1 Baseline\Student\Sample_noPII.dta", clear
drop _merge
compress
save "$base_out\1 Baseline\Student\Student.dta", replace

use "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID using "$base_out\1 Baseline\Student\Student.dta"
drop if _merge==1
drop _merge
drop  Stream_ID StreamID totSampleStreams_ID streamSize_ID StudentID
*child did not answer the base test 
drop if childAvailable==2  
*child did not answer the base test
drop if consentChild==2
*child refused to answer finish base test
drop if result==2 
drop  consentChild result childAvailable timeEndTest timeStartTest


replace kiswahili_3=0 if kiswahili_3==2
replace kiswahili_4=0 if kiswahili_4==2
replace kiswahili_5=0 if kiswahili_5==2
replace kiswahili_6=0 if kiswahili_6==2
replace kiingereza_3=0 if kiingereza_3==2
replace kiingereza_4=0 if kiingereza_4==2
replace kiingereza_5=0 if kiingereza_5==2
replace kiingereza_6=0 if kiingereza_6==2

*For grade 1 is the easiest as students started in the easier level and stopped when they couldnt go any further. Thus its "safe" to assume they would have score zero in the next sections
*Thus the first step is to replace the missing values for zeros (since they did not get to it, it must be because they couldn't answer easier stuff so we assume they score zero)
foreach var of varlist kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
replace `var'=0 if `var'==. & GradeID==1
}


*For grade  2 and 3 is the same in math... sooo... first we replace with zero the levels the student did not get to answer: 
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==2 
}
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==3
}



*For grade  2 and 3 is harder in lenguages: It started in question 3, if the student go it, then he moved on, if he didn't it moved to question 2 and if he scored zero then it moved to question 1. 
*So first, if the student got it, then lets assume he could read syllabus and letters, i.e. that he could achieve perfect score.
replace kiingereza_1 =5 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_1==.
replace kiingereza_2 =8 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_2==.
replace kiswahili_1 =5 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_1==.
replace kiswahili_2 =8 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_2==.
*If the student did not get question 4 (after he got question 3), then assume he would have gotten zero in questions 5 and 6
replace kiingereza_5 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_5==.
replace kiingereza_6 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_6==.
replace kiswahili_5 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_5==.
replace kiswahili_6 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_6==.
*If the student did not get it, then lets assume he couldnt read the story or answer the questions i.e. that he could achieve perfect score. 
replace kiingereza_4 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_4==.
replace kiingereza_5 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_5==.
replace kiingereza_6 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_6==.
replace kiswahili_4 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_4==.
replace kiswahili_5 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_5==.
replace kiswahili_6 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_6==.
*If the student did not get it, and got over 0 in the word section, then we need to estimate the number of letters he would have gotten
* to do this we run an OLS regression using Grade1 data (they all answer these two questions) 
poisson kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
*reg kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
predict input_kis1, n 
*reg kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
poisson kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID i.SchoolID if GradeID==1 & kiswahili_2>0
predict input_kiin1, n 

replace kiswahili_1=input_kis1 if kiswahili_1==. & kiswahili_2>0 & kiswahili_3==0 & (GradeID==3 | GradeID==2)
replace kiingereza_1=input_kiin1 if kiingereza_1==. & kiingereza_2>0 & kiingereza_3==0 & (GradeID==3 | GradeID==2)
replace kiswahili_1=5 if kiswahili_1>5
replace kiingereza_1=5 if kiingereza_1>5  
sum kiswahili_1- hisabati_7
sum kiswahili_1- hisabati_7 if GradeID==1
sum kiswahili_1- hisabati_7 if GradeID==2
sum kiswahili_1- hisabati_7 if GradeID==3
********************************
drop input_kis1 input_kiin1
*Now we can standarize for each question
foreach var of varlist kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
forvalues grade=1/3{
sum `var' if GradeID==`grade' & treatarm==4
capture gen Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade'
capture replace Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade'
}
}


*****now we combine by subject
egen Z_kiswahili=rowtotal(Z_kiswahili_1 Z_kiswahili_2 Z_kiswahili_3 Z_kiswahili_4 Z_kiswahili_5 Z_kiswahili_6),missing
egen Z_kiingereza=rowtotal( Z_kiingereza_1  Z_kiingereza_2 Z_kiingereza_3 Z_kiingereza_4 Z_kiingereza_5 Z_kiingereza_6),missing
egen Z_hisabati=rowtotal( Z_hisabati_1 Z_hisabati_2 Z_hisabati_3 Z_hisabati_4 Z_hisabati_5 Z_hisabati_6 Z_hisabati_7),missing

***Now we standarize by grade subject
forvalues val=1/3{
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if GradeID==`val' & treatarm==4
replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
}
}

***Now we standarize by subject
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if treatarm==4
replace `var'=(`var'-r(mean))/r(sd)
}

keep SchoolID GradeID Z_kiswahili Z_kiingereza Z_hisabati
collapse (mean) Z_kiswahili Z_kiingereza Z_hisabati, by(SchoolID)
rename Z_kiswahili Z_kiswahili_T1 
rename Z_kiingereza Z_kiingereza_T1
rename Z_hisabati Z_hisabati_T1

merge 1:m SchoolID using "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta"
drop _merge
save "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta", replace



******** DATA ANLYSIS
use "$base_out\Consolidated\StudentLevelEDIvsTWA.dta", clear

		
		
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
drop if TreatmentCG==1
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

local schoolcontrol s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1

foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo clear
eststo:  reg `var'_B  TreatmentCOD TreatmentBoth `schoolconrol' i.(DistrictID GradeID), vce(cluster SchoolID)
eststo:  reg `var' TreatmentCG TreatmentCOD TreatmentBoth `schoolconrol' i.(DistrictID GradeID), vce(cluster SchoolID)

if "`var'"=="Z_kiswahili" local name Swahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math

esttab using "$results\LatexCode\RegEDIvsTwareza_`var'_StudentLevel.tex", se ar2 booktabs label /// 
replace title("Treatment effect on standardized test scores for `name'")   b(a2) se(a2) nocon mtitles ("EDI" "Twaweza") ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01)  stats(N, labels ("N. of obs.")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

}

use "$base_out\Consolidated\SchoolLevelEDIvsTWA.dta", clear
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
drop if TreatmentCG==1
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
drop if Z_kiswahili==.
local schoolcontrol s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1

foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo clear
eststo:  reg `var'_B  TreatmentCOD TreatmentBoth `schoolconrol' `var'_T1 i.(DistrictID GradeID)
eststo:  reg `var' TreatmentCG TreatmentCOD TreatmentBoth `schoolconrol' `var'_T1 i.(DistrictID GradeID)

if "`var'"=="Z_kiswahili" local name Swahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math

esttab using "$results\LatexCode\RegEDIvsTwareza_`var'_SchoolLevel.tex", se ar2 booktabs label /// 
replace title("Treatment effect on standardized test scores for `name'")   b(a2) se(a2) nocon mtitles ("EDI" "Twaweza") ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01)  stats(N, labels ("N. of obs.")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

}
