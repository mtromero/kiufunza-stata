*Erin's modifications, 3.1.2015
*Mauricio, this won't run b/c it has my attempt at new code and your own original code in it

clear
global randomize "$mipath\Codes\Erin\2015 Randomization"

set seed 30081986
use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
drop if Kis_Pass==.
keep SchoolID DistrictID
sort SchoolID
quietly by SchoolID:  gen dup = cond(_N==1,0,_n)
drop if dup>1	
drop dup

merge 1:1 SchoolID using "$base_out\Consolidated\SchoolsAverageScores_T7.dta", keepusing(Z_kiswahili* Z_kiingereza* Z_hisabati*)
drop if _merge!=3
drop _merge
merge 1:1 SchoolID using "$base_out\8 Endline 2014\School\School.dta", keepus(factyn8 factyn9 factyn10 factyn11 factyn12 factyn13 studentsTeacherRatio)
drop if _merge!=3
drop _merge

capture drop Total
pca Z_kiswahili_C Z_kiingereza_C Z_hisabati_C Z_kiswahili Z_kiingereza Z_hisabati Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 factyn8 factyn9 factyn10 factyn11 factyn12 factyn13 studentsTeacherRatio, components(1) /*Use EDI with all questions and TWA*/
predict Total, score

sort DistrictID treatment Total
bys DistrictID treatment: g dist_treat_score_rank=_n
bys DistrictID treatment: g dist_treat_score_Total=_N
gen StrataScore=1 if dist_treat_score_rank>ceil(dist_treat_score_Total/2)
replace StrataScore=0 if dist_treat_score_rank<=floor(dist_treat_score_Total/2)
bys DistrictID : gen rand = runiform() if StrataScore==. & treatment=="COD"
sum rand,d
replace StrataScore=1 if rand>r(p50) & missing(StrataScore) & treatment=="COD"
replace StrataScore=0 if rand<r(p50) & missing(StrataScore) & treatment=="COD"
drop  rand
bys DistrictID : gen rand = runiform() if StrataScore==. & treatment=="Both"
sum rand,d
replace StrataScore=1 if rand>r(p50) & missing(StrataScore) & treatment=="Both"
replace StrataScore=0 if rand<r(p50) & missing(StrataScore) & treatment=="Both"
drop  rand

bys DistrictID : gen rand = runiform()
by DistrictID: egen RandControl=max(rand)
sum RandControl,d
gen DummyAssignControl=(RandControl>r(p50))
drop RandControl rand
tab StrataScore treatment
tab StrataScore DistrictID




*merging in enrollment for stratifying and school data for balance tests
sort SchoolID
merge 1:1 SchoolID using "$randomize\enrollment.dta"
drop if _merge!=3
drop _merge

sort SchoolID
merge 1:1 SchoolID using "$randomize\avgtests_school.dta"
drop if _merge!=3
drop _merge

sort SchoolID
merge 1:1 SchoolID using "$randomize\num_teachers.dta"
drop if _merge!=3
drop _merge

*creating dummies for paired schools
gen paired = SchoolID
*isaac's 2013 list
recode paired (135 134=1) /*combo schools*/
recode paired (428 405=2) /*COD schools*/
recode paired (804 802=3) /*combo schools*/ 
recode paired (130 131=4)
recode paired (413 401=5)
recode paired (420 403=6)
recode paired (431 404=7)
recode paired (425 406=8)
recode paired (434 407=9)
recode paired (422 409=10)
recode paired (429 411=11)
recode paired (424 412=12)
recode paired (722 721=13)
recode paired (730 729=14)

*i think these are also close
recode paired (524 529=15)
recode paired (426 418=16)

*creating tot_en to use for stratifying
*bys paired: egen adj_school_size =sum(tot_en)

/*Here's my attempt to stratify on size using groups of 5. I abandoned the attempt
to pair, based on our gchat discussion.

gsort DistrictID treatment -tot_en
by DistrictID treatment: g dist_size_treat_rank=_n
*create groups of 5 based on size
gen groupbyrank =cond(dist_size_treat_rank/5-trunc(dist_size_treat_rank/5)>0, trunc(dist_size_treat_rank/5)+1,dist_size_treat_rank/5)

set seed 69514
gen rnd_num = uniform()
gsort DistrictID treatment groupbyrank rnd_num  /* descending order of size of random number within each "group" so we pair them randomly */
by DistrictID treatment groupbyrank: gen group2_rank= _n
gen within_group_rank = cond(groupbyrank>1,group2_rank, cond(group2_rank/2-trunc(group2_rank/2)>0, trunc(group2_rank/2)+1,group2_rank/2))

*reset seed
set seed 19824
gen rnd_num2 = uniform()

gsort DistrictID groupbyrank within_group_rank -rnd_num

gsort DistrictID treatment groupbyrank -rnd_num2
bys DistrictID treatment groupbyrank: egen rct_rank = rank(rnd_num2), track

gen treatment2="COD" if treatment=="Control" & rct_rank<=1
replace treatment2="Gains" if treatment=="Control" & rct_rank>1 & rct_rank<=2
replace treatment2="Control" if treatment=="Control" & rct_rank>2 & rct_rank<=4

replace treatment2="COD" if treatment=="COD" & rct_rank>=1 & rct_rank<=2
replace treatment2="Gains" if treatment=="COD" & rct_rank>2 & rct_rank<=4
replace treatment2="Control" if treatment=="COD" & rct_rank==5

replace treatment2="COD" if treatment=="Both" & rct_rank==5
replace treatment2="Gains" if treatment=="Both" & rct_rank==1
replace treatment2="Gains" if treatment=="Both" & rct_rank==3
replace treatment2="Control" if treatment=="Both" & rct_rank==2
replace treatment2="Control" if treatment=="Both" & rct_rank==4*/

*this (lines 87 - 107) is your original randomization code
gen rand = runiform()
sort DistrictID treatment StrataScore rand
egen lrank = rank(_n), by(DistrictID treatment StrataScore)
bys DistrictID treatment StrataScore: gen lnumb = _N
bro DistrictID treatment Total StrataScore lrank lnumb rand

*For the random cases where there are 4 schools we do a 2,1,1 split
gen treatment2="Levels" if treatment=="COD"  & lnumb==4 & lrank<=2
replace treatment2="Gains" if treatment=="COD"  & lnumb==4 & lrank>2 & lrank<=3
replace treatment2="Control" if treatment=="COD"  & lnumb==4 &  lrank>3 & lrank<=4

*For the random cases where there are 3 schools we do a 2,1,0 split
replace treatment2="Levels" if treatment=="COD" & lnumb==3 & lrank<=2
replace treatment2="Gains" if treatment=="COD" & lnumb==3 & lrank>2 & lrank<=3


*For the random cases where there are 4 schools we do a 0,2,2 split
replace treatment2="Gains" if treatment=="Both" & lnumb==4 & lrank<=2
replace treatment2="Control" if treatment=="Both" & lnumb==4 &  lrank>2 & lrank<=4

*For the random cases where there are 3 schools we do a 1,1,1 split
replace treatment2="Levels" if treatment=="Both" & lnumb==3 & lrank<=1
replace treatment2="Gains" if treatment=="Both" & lnumb==3 & lrank>1 & lrank<=2
replace treatment2="Control" if treatment=="Both" & lnumb==3 &  lrank>2 & lrank<=3


*Now for control I created a dummy randomly assigned to half of the distrcits that indicates whether we assing cod and control or gains and control.. in one we assing the "best" schools to  
*to COD and the worst to Gains, and in the other is the other way around... there is always one control school from each group
replace treatment2="Levels" if treatment=="Control" & lrank<=1 & StrataScore==0 & DummyAssignControl==1
replace treatment2="Control" if treatment=="Control" & lrank>1 & lrank<=2 & StrataScore==0 & DummyAssignControl==1

replace treatment2="Gains" if treatment=="Control" & lrank<=1 & StrataScore==1 & DummyAssignControl==1
replace treatment2="Control" if treatment=="Control" & lrank>1 & lrank<=2 & StrataScore==1 & DummyAssignControl==1


replace treatment2="Gains" if treatment=="Control" & lrank<=1 & StrataScore==0 & DummyAssignControl==0
replace treatment2="Control" if treatment=="Control" & lrank>1 & lrank<=2 & StrataScore==0 & DummyAssignControl==0

replace treatment2="Levels" if treatment=="Control" & lrank<=1 & StrataScore==1 & DummyAssignControl==0
replace treatment2="Control" if treatment=="Control" & lrank>1 & lrank<=2 & StrataScore==1 & DummyAssignControl==0



gen treatarm2=1 if treatment2=="Levels"
replace treatarm2=2 if treatment2=="Gains"
replace treatarm2=3 if treatment2=="Control"


bro
capture  log close
log using "$randomize\from MR\balance_tests_Yr3.log", replace
*check assignment of paired schools
foreach i of numlist 1/16 {
list treatment treatment2 if paired==`i'
}

fvset base none treatarm2	
local stuff " no_teachers no_classrooms urban vol_teachers_dummy z_uwezo tot_enroll" /*deleted ptr b/c i don't know where it came from in 2013 data*/
foreach x of local stuff{
reg `x' i.treatarm2, nocons
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
}



/*
*generate groupvars for simple T-test
g cod_vs_con = treatarm2
recode cod_vs_con (2 = .)

g gains_vs_con = treatarm2
recode gains_vs_con (1 = .)

local stuff " no_teachers no_classrooms urban vol_teachers_dummy z_uwezo tot_enroll" /*deleted ptr b/c i don't know where it came from in 2013 data*/
foreach x of local stuff{
ttest `x', by(cod_vs_con)
}

foreach x of local stuff {
ttest `x', by(gains_vs_con)
}
*/
*check balance
local stuff " no_teachers no_classrooms urban vol_teachers_dummy z_uwezo tot_enroll" /*deleted ptr b/c i don't know where it came from in 2013 data*/

foreach x of local stuff {
oneway `x' treatarm2, tabulate
}



keep SchoolID treatment2 treatarm2 DistrictID StrataScore
save "$base_out\Consolidated\RandomizeStatusyr2.dta", replace

merge 1:m SchoolID using "$base_out\Consolidated\StudentLevelEDIvsTWA.dta"
drop if _merge!=3
drop _merge

fvset base none treatarm2
fvset base 4 treatarm
fvset base 1 DistrictID



foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo clear
eststo:  reg `var'_B i.treatarm2 i.treatarm i.GradeID i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

eststo:  reg `var' i.treatarm2 i.treatarm i.GradeID i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p4=r(p4)

esttab using "$randomize\CheckTestEDITWA`var'.csv", se ar2 label  b(a2) se(a2) nocon mtitles ("Yr2 EDI Restr" "Yr2 Twa") ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") replace
}

foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo clear
eststo:  reg `var'_B i.treatarm2 , vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

eststo:  reg `var' i.treatarm2 , vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

esttab using "$randomize\CheckTestEDITWA`var'_NoControl.csv", se ar2 label  b(a2) se(a2) nocon mtitles ("Yr2 EDI Restr" "Yr2 Twa") ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") replace
}


use "$base_out\Consolidated\RandomizeStatusyr2.dta", clear
merge 1:m SchoolID using "$base_out\Consolidated\Student_School_House_Teacher_Char.dta"
*drop if _merge!=3
drop _merge

gen treatment3="COD-Long Term" if treatment2=="Levels" & treatment=="COD"
replace treatment3="Control-Long Term" if treatment=="Control" & treatment2==""

gen treatarm3=1 if treatment3=="COD-Long Term"
replace treatarm3=2 if treatment3=="Control-Long Term"




foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
reg `var'_B_T7 `var'_T1 i.treatarm i.stdgrd_T7 i.DistrictID, vce(cluster SchoolID)
predict `var'_resid,resid
}

fvset base none treatarm2
fvset base 4 treatarm
fvset base 1 DistrictID

eststo clear
foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo:  reg `var'_T1 i.treatarm2 i.treatarm i.stdgrd_T3 i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)


eststo:  reg `var'_B_T7 i.treatarm2 i.treatarm i.stdgrd_T7 i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

/*
eststo:  reg `var'_resid i.treatarm2 i.treatarm i.stdgrd_T7 i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)
*/
}
esttab using "$randomize\CheckEDIFULL.csv", se ar2 label  b(a2) se(a2) nocon mtitles ("Swa Yr0" "Swa Yr2" "Swa VA" "Eng Yr0" "Eng Yr2" "Eng VA" "Math Yr0" "Math Yr2" "Math VA") ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") replace


eststo clear
fvset base none treatarm3
foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo:  reg `var'_T1 i.treatarm3 i.stdgrd_T3 i.DistrictID, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm3] = _b[2.treatarm3])
estadd scalar p=r(p)

eststo:  reg `var'_T1 i.treatarm3, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm3] = _b[2.treatarm3])
estadd scalar p=r(p)
}
esttab using "$randomize\CheckEDI_LongTerm.csv", se ar2 label  b(a2) se(a2) nocon mtitles ("Swa Yr0-FE" "Swa Yr0"  "Eng Yr0-FE" "Eng Yr0" "Math Yr0-FE" "Math Yro") ///
keep(*treatarm3) coef(1.treatarm3 COD-Long Term Term 2.treatarm3 Control-Long Term) star(* 0.10 ** 0.05 *** 0.01)  stats(N p ymean, labels ("N" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") replace



eststo clear
foreach var in Z_kiswahili Z_kiingereza Z_hisabati{
eststo:  reg `var'_T1 i.treatarm2, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)


eststo:  reg `var'_B_T7 i.treatarm2, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

/*
eststo:  reg `var'_resid i.treatarm2, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)
*/
}
esttab using "$randomize\CheckEDIFULL_NoControls.csv", se ar2 label  b(a2) se(a2) nocon mtitles ("Swa Yr0" "Swa Yr2" "Swa VA" "Eng Yr0" "Eng Yr2" "Eng VA" "Math Yr0" "Math Yr2" "Math VA") ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }") replace



use "$base_out\Consolidated\RandomizeStatusyr2.dta", clear
merge 1:m SchoolID using "$base_out\8 Endline 2014\School\School.dta"
drop if _merge!=3
drop _merge

forvalue grade=1/7{
label variable gdrctb`grade'PS "\\$ Textbooks/Student Grd `grade'"
}

label variable administrative_expensesPS "\\$ Admin./Student"
label variable teaching_aid_expensesPS "\\$ Teaching Aid/Student"
label variable student_expensesPS "\\$ Student/Student"
label variable Teacher_expensesPS "\\$ Teacher/Student"
label variable Construction_expensesPS "\\$ Construction/Student" 

label variable administrative_twawezaPS "\\$ Admin./Student"
label variable teaching_aid_twawezaPS "\\$ Teaching Aid/Student"
label variable student_twawezaPS "\\$ Student/Student"
label variable Teacher_twawezaPS "\\$ Teacher/Student"
label variable Construction_twawezaPS "\\$ Construction/Student" 

label variable studentsTeacherRatio "Teacher Ratio"
label variable studentsVolunteerRatio "Vol. Ratio"
label variable strategicly_change_teachers "HT changed teachers"
label variable strategicly_change_teachers "HT changed students"
label variable administrative_expenses "\\$ Admin."
label variable student_expenses "\\$ Student"  
label variable Teacher_expenses "\\$ Teacher"

local varChecks NumberOfTeachers_S1 NumberOfTeachers_S2 NumberOfTeachers_S3 NumberOfTeachers_S4 NumberOfTeachers_S5 NumberOfTeachers_S6 NumberOfTeachers_S7 NumberOfTeachers_S7 NumberOfVolunteers factyn8 factyn9 factyn10 factyn11 factyn12 factyn13 gdrctb1PS gdrctb2PS gdrctb3PS gdrctb4PS gdrctb5PS gdrctb6PS gdrctb7PS administrative_expensesPS student_expensesPS teaching_aid_expensesPS Teacher_expensesPS Construction_expensesPS Savings_expensesPS StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7 TotalNumberStudents NumberOfTeachers studentsTeacherRatio studentsVolunteerRatio studentsTeacherRatio_S1 studentsTeacherRatio_S2 studentsTeacherRatio_S3 studentsTeacherRatio_S4 studentsTeacherRatio_S5 studentsTeacherRatio_S6 studentsTeacherRatio_S7
eststo clear
foreach x in `varChecks'{
eststo: 	reg `x' i.treatarm2 i.treatarm i.DistrictID i.StrataScore#i.DistrictID#i.treatarm, vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

}
esttab using "$randomize\CheckSchoollevel.csv", se ar2 label  b(a2) se(a2) nocon replace ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")

eststo clear
foreach x in `varChecks'{
eststo: 	reg `x' i.treatarm2 , vce(cluster SchoolID) nocons
estadd ysumm
test (_b[1.treatarm2] = _b[2.treatarm2])
estadd scalar p1=r(p)
test (_b[1.treatarm2] = _b[3.treatarm2])
estadd scalar p2=r(p)
test (_b[2.treatarm2] = _b[3.treatarm2])
estadd scalar p3=r(p)
test (_b[1.treatarm2] = _b[2.treatarm2]= _b[3.treatarm2])
estadd scalar p=r(p)

}
esttab using "$randomize\CheckSchoollevel_NoControls.csv", se ar2 label  b(a2) se(a2) nocon replace ///
keep(*treatarm2) coef(1.treatarm2 Levels 2.treatarm2 Gains 3.treatarm2  Control) star(* 0.10 ** 0.05 *** 0.01)  stats(N p1 p2 p3 p ymean, labels ("N" "Levels-Gains" "Levels-Control" "Gains-Control" "Joint Equality" "Meand Dev. Var")) ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


capture log close






