*creating scores, EL2014
foreach var of varlist kissyl1_1-kissen1_6 {
	replace `var'=. if `var'!=1
}
egen kis1=anycount(kissyl1_1-kissen1_6) if stdgrd==1 & consentChild==1, values(1)

foreach var of varlist englet1_1-engsen1_6 {
	replace `var'=. if `var'!=1
}
egen eng1=anycount(englet1_1-engsen1_6) if stdgrd==1 & consentChild==1, values(1)

foreach var of varlist hiscou1_1-hissub1_4 {
	replace `var'=. if `var'!=1
}
egen math1=anycount(hiscou1_1-hissub1_4) if stdgrd==1 & consentChild==1, values(1)

foreach var of varlist kissyl2_1-kispar2_1 {
	replace `var'=. if `var'!=1
}
egen kis2=anycount(kissyl2_1-kispar2_1) if stdgrd==2 & consentChild==1, values(1)

foreach var of varlist englet2_1-engpar2_1 {
	replace `var'=. if `var'!=1
}
egen eng2=anycount(englet2_1-engpar2_1) if stdgrd==2 & consentChild==1, values(1)

foreach var of varlist hiscou2_1-hismul2_4 {
	replace `var'=. if `var'!=1
}
egen math2=anycount(hiscou2_1-hismul2_4) if stdgrd==2 & consentChild==1, values(1)

foreach var of varlist kissyl3_1-kiscom3_3 {
	replace `var'=. if `var'!=1
}
egen kis3=anycount(kissyl3_1-kiscom3_3) if stdgrd==3 & consentChild==1, values(1)

foreach var of varlist englet3_1-engcom3_3 {
	replace `var'=. if `var'!=1
}
egen eng3=anycount(englet3_1-engcom3_3) if stdgrd==3 & consentChild==1, values(1)

foreach var of varlist hiscou3_1-hisdiv3_3 {
	replace `var'=. if `var'!=1
}
egen math3=anycount(hiscou3_1-hisdiv3_3) if stdgrd==3 & consentChild==1, values(1)

foreach var of varlist kis1 eng1 math1 {
	replace `var'=. if stdgrd!=1
	replace `var'=. if consentChild!=1
}
foreach var of varlist kis2 eng2 math2 {
	replace `var'=. if stdgrd!=2
	replace `var'=. if consentChild!=1
}
foreach var of varlist kis3 eng3 math3 {
	replace `var'=. if stdgrd!=3
	replace `var'=. if consentChild!=1
}

sum kis1 eng1 math1 if stdgrd==1 & consentChild==1, detail
sum kis2 eng2 math2 if stdgrd==2 & consentChild==1, detail
sum kis3 eng3 math3 if stdgrd==3 & consentChild==1, detail

drop _merge
merge m:1 SchoolID using "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatarm)
drop if _merge==2
drop _merge

sum kis1 eng1 math1 if stdgrd==1 & consentChild==1 & treatarm==4, detail
sum kis2 eng2 math2 if stdgrd==2 & consentChild==1 & treatarm==4, detail
sum kis3 eng3 math3 if stdgrd==3 & consentChild==1 & treatarm==4, detail

save "$base_out\Consolidated\Student_EL2013_2014.dta", replace
