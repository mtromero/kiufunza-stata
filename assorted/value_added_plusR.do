use "$base_out\3 Endline\Student\StudentConsolidated.dta", replace

rename passkiswahili_1  PR_1_k_svy
rename passkiingereza_1  PR_1_e_svy
rename passhisabati_1  PR_1_m_svy
rename passkiswahili_2  PR_2_k_svy
rename passkiingereza_2  PR_2_e_svy
rename passhisabati_2  PR_2_m_svy
rename passkiswahili_3  PR_3_k_svy
rename passkiingereza_3  PR_3_e_svy
rename passhisabati_3  PR_3_m_svy

   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "Combo"

   rename Z_kiswahili_BL BLZ_kiswahili
   rename Z_kiingereza_BL BLZ_kiingereza
   rename Z_hisabati_BL BLZ_hisabati

  keep TreatmentCG TreatmentCOD TreatmentBoth BLZ_kiswahili BLZ_kiingereza BLZ_hisabati Z_kiswahili Z_kiingereza Z_hisabati usid DistID stdgrd


reshape long Z_ BLZ_  , i(usid) j(Subject kiswahili kiingereza hisabati)
encode Subject, generate(Subject2)

 clear matrix
 qui reg Z_ BLZ_
 mat A  = nullmat(A), e(r2)
 qui reg Z_ BLZ_  TreatmentCG TreatmentCOD TreatmentBoth
 mat A  = nullmat(A), e(r2)
 qui reg Z_ BLZ_  TreatmentCG TreatmentCOD TreatmentBoth i.Subject2
 mat A  = nullmat(A), e(r2)
 qui reg Z_ BLZ_  TreatmentCG TreatmentCOD TreatmentBoth i.Subject2 i.stdgrd
 mat A  = nullmat(A), e(r2)
 qui reg Z_ BLZ_  TreatmentCG TreatmentCOD TreatmentBoth i.Subject2##i.stdgrd 
 mat A  = nullmat(A), e(r2)
 qui reg Z_ BLZ_  TreatmentCG TreatmentCOD TreatmentBoth i.Subject2##i.stdgrd  i.DistID
 mat A  = nullmat(A), e(r2)
 matrix A= A'
 svmat A

/*
pdf("Modelos.pdf")
barplot(A[1:2],col=4,names.arg=c("Baseline","Baseline+Treatment Effect"),main="R squared for different models",xlab="Model",ylab="R^2")
dev.off()
*/



