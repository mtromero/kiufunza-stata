set seed 04011989
use "$base_out\1 Baseline\School\School.dta", clear
merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "$basein\TreatmentStatusYr3-4\RandomizeStatus.dta"
drop _merge


keep SchoolID DistrictID treatment treatarm treatment2 treatarm2
sort SchoolID


gen rand = runiform() if treatment=="Control" & (treatment2=="")
sort DistrictID rand 
egen lrank = rank(_n), by(DistrictID)

gen SchoolLTStudy=0
replace SchoolLTStudy=1 if lrank<=4

replace SchoolLTStudy=1 if treatment=="Control" & treatment2=="Control"
replace SchoolLTStudy=1 if treatment=="COD" & treatment2=="Levels"


drop rand lrank

compress
save "$basein\TreatmentStatusYr3-4\SchoolLTStudy.dta", replace


******Now do some Balance checks!! ****************
/*

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 se_1 se_2 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2  `if', nocons vce(cluster `clus_id')
 test (_b[TD1] == _b[TD2])
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_1' = nullmat(`mu_1'), A[1,1]
 mat `mu_2' = nullmat(`mu_2'), A[1,2]
 mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
 mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
}
foreach mat in mu_1 mu_2 se_1 se_2 d_p {
 mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 se_1 se_2 d_p {
 eret mat `mat' = ``mat''
}
end

use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear
drop _merge
merge m:1 SchoolID using "$basein\TreatmentStatusYr3-4\SchoolLTStudy.dta"
local studentcontrol LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza
local schoolcontrol MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 

label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Swahili test score"
label var LagZ_hisabati "Math test score"
label var LagZ_kiingereza "English test score"

keep if SchoolLTStudy==1

eststo clear
eststo: xi: my_ptest  `studentcontrol', by(treatarm) clus_id(SchoolID)
esttab using "$results\LatexCode\summaryLongRunStudent.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("COD" "Control" "p-value (equal)")  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2))  d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par)  .") ///
addnotes("\specialcell{Standard errors, clustered at the school level, in parenthesis \\ \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")



  foreach v of varlist s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 {
 	local l`v' : variable label `v'
        if `"`l`v''"' == "" {
 		local l`v' "`v'"
 	}
 }
 
collapse (mean) `schoolcontrol' treatarm (first) treatment, by (SchoolID)
label var MeanGrade_LagZ_kiswahili "Average Swahili test score"
label var MeanGrade_LagZ_kiingereza "Average Math test score"
label var MeanGrade_LagZ_hisabati "Average English test score"
label var s1451_T1 "Kitchen"
label var s1452_T1 "Library"
label var s1453_T1 "Playground"
label var s1454_T1 "Staff room"
label var s1455_T1 "Outer wall"
label var s1456_T1 "Newspaper"
label var SingleShift_T1 "Single shift"
label var computersYN_T1 "Computers"
label var s120_T1  "Electricity"
label var s118_T1 "Classes outside"
label var PipedWater_T1 "Piped Water"
label var NoWater_T1 "No Water"
label var ToiletsStudents_T1 "Toilets/Students"
label var ClassRoomsStudents_T1 "Classrooms/Students"
label var TeacherStudents_T1 "Teachers/Students"
label var s188_T1 "Breakfast"
label var s175_T1 "Preschool"
label var s200_T1 "Track students"
label var s108_T1 "Urban"
label var SizeSchoolCommittee_T1 "Size School Committee"
label var KeepRecords_T1 "Spending records"
label var noticeboard_T1 "Noticeboard with spending information"
label var PropCommitteeFemale_T1 "Female/Committee"
label var PropCommitteeTeachers_T1 "Teacher/Committee"
label var PropCommitteeParents_T1 "Parents/Committee"
label var StudentsTotal_T1 "Enrolled students"


 
   foreach v of varlist s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 {
 	label var `v' "`l`v''"
  }
 
 

local schoolcontro2l s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1    computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 

eststo clear
eststo: xi: my_ptest  `schoolcontro2l', by(treatarm) clus_id(SchoolID)
esttab using "$results\LatexCode\summaryLongRunSchool.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("COD" "Control" "p-value (difference)")  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) .") ///
addnotes("\specialcell{Standard errors in parenthesis \\ \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }")


*/

