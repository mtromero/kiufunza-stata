*use "E:\Box Sync\01_KiuFunza\CreatedData\Consolidated\Student_School_House_Teacher_Char.dta", clear
use "C:\Users\Mauricio\Box Sync\01_KiuFunza\CreatedData\Consolidated\Student_School_House_Teacher_Char.dta", clear

gen StudentsGr_T1=StudentsGr1_T1 if stdgrd_T3==1
replace StudentsGr_T1=StudentsGr2_T1 if stdgrd_T3==2
replace StudentsGr_T1=StudentsGr3_T1 if stdgrd_T3==3


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"


local AggregateDep 	Z_hisabati_T3 Z_kiswahili_T3 Z_kiingereza_T3 Z_sayansi_T3 Z_ScoreKisawMath_T3 Z_ScoreFocal_T3 Z_ScoreTotal_T3 attendance_T2 attendance_T3


local treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
local week  i.WeekIntvTest_T3
local schoolcontrol s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsGr_T1 StudentsTotal_T1
local TeacherCharac maleAverage t05Average t07Average t08Average t10Average t21Average t23Average higher_degreeAverage
local housecontrol NumHHMembers_T1 Expenditure_FC_2012_T1 Expenditure_FC_2013_T1 wall_mud_T1 floor_mud_T1 roof_durable_T1 asset_1_T1 asset_2_T1 asset_3_T1 asset_4_T1 asset_5_T1 asset_6_T1 asset_7_T1 asset_8_T1 CompletePrimary_T1 SomeSecondary_T1 PostSecondary_T1 crowding_T1 workyn_T1 improveWater_T1 improveSanitation_T1 HHElectricty_T1 prdyyn_T1 ltcbyn_T1 breakfast_T1 HH_Kis_T1 HH_Kin_T1 HH_His_2_T1
local studentcontrol seenUwezoTests_T1 preSchoolYN_T1 male_T1 Age_T1 Z_kiswahili_T1 Z_hisabati_T1 Z_kiingereza_T1
local TeacherCharacSubjectGrade maleMath t05Math t07Math t08Math t10Math t21Math higher_degreeMath t23Math NoTeacherM

*`TeacherCharacSubjectGrade'
reg Z_hisabati_T3  i.stdgrd_T3  i.DistID `studentcontrol' `week'  `schoolcontrol'  `TeacherCharac'  `week' , vce(cluster SchoolID)
predict error, resid


