use "$base_out\Consolidated\Student.dta", replace

icc Z_ScoreFocal_T1 SchoolID
icc Z_ScoreKisawMath_T1 SchoolID
icc Z_hisabati_T1 SchoolID
icc Z_kiingereza_T1 SchoolID
icc Z_kiswahili_T1 SchoolID

icc PassDistance_M_S1_BL SchoolID
icc PassDistance_M_S2_BL SchoolID
icc PassDistance_M_S3_BL SchoolID
 
 
 gen PassM=1 
 replace PassM=0 if PassDistance_M_S1_BL<0
 replace PassM=0 if PassDistance_M_S2_BL<0
 replace PassM=0 if PassDistance_M_S3_BL<0
 
 
 gen PassE=1 
 replace PassE=0 if PassDistance_E_S1_BL<0
 replace PassE=0 if PassDistance_E_S2_BL<0
 replace PassE=0 if PassDistance_E_S3_BL<0
 
  gen PassK=1 
 replace PassK=0 if PassDistance_K_S1_BL<0
 replace PassK=0 if PassDistance_K_S2_BL<0
 replace PassK=0 if PassDistance_K_S3_BL<0
 
 
icc PassM SchoolID
icc PassE SchoolID
icc PassK SchoolID
 

