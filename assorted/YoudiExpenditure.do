use "$basein/3 Endline/School/EGrade_noPII.dta", clear

keep SchoolID EGradeID  s184 s185 gdrctb
replace gdrctb=. if gdrctb==-99
replace gdrctb=0 if gdrctb==.
reshape wide s184 s185 gdrctb, i(SchoolID) j(EGradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

label variable gdrctb1 "Expenditure in textbooks grade 1"
label variable gdrctb2 "Expenditure in textbooks grade 2"
label variable gdrctb3 "Expenditure in textbooks grade 3"
label variable gdrctb4 "Expenditure in textbooks grade 4"
label variable gdrctb5 "Expenditure in textbooks grade 5"
label variable gdrctb6 "Expenditure in textbooks grade 6"
label variable gdrctb7 "Expenditure in textbooks grade 7"


gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"


egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7)
keep SchoolID TotalNumberStudents
save "$base_out/Consolidated/School_Exp_Youdi_2013.dta", replace	

 

use "$basein/3 Endline/School/EExpenses_noPII.dta", clear 

replace s168=0 if s168==.
replace cgspct=0 if cgspct==.
rename EExpenseID ExpenseID

merge m:1 SchoolID using "$base_out/Consolidated/School_Exp_Youdi_2013.dta"	
drop _merge
gen Year=2013

save "$base_out/Consolidated/School_Exp_Youdi_2013.dta", replace	




**********

*******************************************************

use "$basein/8 Endline 2014/Final Data/Teacher/R6Grade_noPII.dta" , clear

keep SchoolID R6GradeID  s184 s185
reshape wide s184 s185, i(SchoolID) j(R6GradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"

egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7)

keep SchoolID TotalNumberStudents
gen Year=2014	
save "$base_out/Consolidated/School_Exp_Youdi_2014.dta", replace

   
  
use "$basein/8 Endline 2014/Final Data/School/R6Expenses_noPII.dta", clear

drop if R6ExpenseID==-95
rename R6ExpenseID ExpenseID
replace s168=0 if s168==.
replace cgspct=0 if cgspct==.


gen Year=2014

merge m:1 SchoolID Year using "$base_out/Consolidated/School_Exp_Youdi_2014.dta"	
drop _merge


append using "$base_out/Consolidated/School_Exp_Youdi_2013.dta"
save  "$base_out/Consolidated/School_Exp_Youdi.dta", replace

   foreach var of varlist s168 cgspct{
   gen `var'PS=`var'/TotalNumberStudents

   }
   
   
merge m:1 SchoolID using  "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge==2
drop _merge

drop if treatment!="Both" & treatment!="CG"
drop if ExpenseID == .


forvalues year=2013/2014{
foreach trea in "Both" "CG"{
di `year'
estpost tabstat  cgspctPS if Year==`year' & treatment=="`trea'", by(ExpenseID) statistics(mean sd) columns(statistics) 
 
esttab using "$exceltables\ExpYS_`year'_`trea'.csv", cells(mean sd) noobs nomtitle nonumber varlabels(`e(labels)') varwidth(20)
}
}
