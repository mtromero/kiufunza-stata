use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear

rename passkiswahili_1  PR_1_k_svy
rename passkiingereza_1  PR_1_e_svy
rename passhisabati_1  PR_1_m_svy
rename passkiswahili_2  PR_2_k_svy
rename passkiingereza_2  PR_2_e_svy
rename passhisabati_2  PR_2_m_svy
rename passkiswahili_3  PR_3_k_svy
rename passkiingereza_3  PR_3_e_svy
rename passhisabati_3  PR_3_m_svy

collapse (mean) treatarm BL_StudentsGr1 BL_StudentsGr2 BL_StudentsGr3 PR_1_k_svy PR_1_e_svy PR_1_m_svy PR_2_k_svy PR_2_e_svy PR_2_m_svy PR_3_k_svy PR_3_e_svy PR_3_m_svy, by(SchoolID)
gen PayE_1=BL_StudentsGr1*PR_1_e_svy*5000
gen PayE_2=BL_StudentsGr2*PR_2_e_svy*5000
gen PayE_3=BL_StudentsGr3*PR_3_e_svy*5000

gen PayK_1=BL_StudentsGr1*PR_1_k_svy*5000
gen PayK_2=BL_StudentsGr2*PR_2_k_svy*5000
gen PayK_3=BL_StudentsGr3*PR_3_k_svy*5000

gen PayM_1=BL_StudentsGr1*PR_1_m_svy*5000
gen PayM_2=BL_StudentsGr2*PR_2_m_svy*5000
gen PayM_3=BL_StudentsGr3*PR_3_m_svy*5000


egen Pay_Total_E=rowtotal( PayE_1 PayE_2 PayE_3), missing
egen Pay_Total_K=rowtotal( PayK_1 PayK_2 PayK_3), missing
egen Pay_Total_M=rowtotal( PayM_1 PayM_2 PayM_3), missing

keep SchoolID Pay_Total_E Pay_Total_K Pay_Total_M treatarm
reshape long Pay_Total_, i(SchoolID) j(Subject E K M)


twoway (kdensity  Pay_Total_ if treatarm==1) (kdensity  Pay_Total_ if treatarm==2) (kdensity  Pay_Total_ if treatarm==3) (kdensity  Pay_Total_ if treatarm==4) ///
 ,legend( lab(1 "COD")  lab(2 "CG") lab(3 "Combo") lab(4 "Control"))   title("Distribution of payments") ytitle("Density")
graph export "$results\Graphs\DistributionPayments.pdf", as(pdf) replace

tabstat Pay_Total_ if treatarm==4, by( Subject)
tabstat Pay_Total_ if treatarm==1, by( Subject)
/*
575000+55728.97
575000+599423
575000+230976.9
59878.17
668482.7
283748.1
*/

use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear




rename Z_kiswahili_BL BLZ_kiswahili
rename Z_kiingereza_BL BLZ_kiingereza
rename Z_hisabati_BL BLZ_hisabati



keep Z_kiswahili Z_kiingereza Z_hisabati BLZ_hisabati BLZ_kiingereza BLZ_kiswahili SchoolID stdgrd treatarm usid passkiswahili passkiingereza passhisabati
reshape long Z_ BLZ_ pass , i(usid) j(Subject kiswahili kiingereza hisabati)

gen TeacherID=string(SchoolID)+string(stdgrd)+Subject
drop if Z_==.
encode TeacherID, generate(TeacherID2)

reg Z_ BLZ_
predict resid, residuals


reg pass BLZ_ if treatarm==1
reg pass BLZ_ if treatarm==2
reg pass BLZ_ if treatarm==3
reg pass BLZ_ if treatarm==4

encode Subject, generate(Subject2)
reg pass BLZ_ 
*if treatarm==1
*reg pass BLZ_ i.Subject2##i.stdgrd i.SchoolID if treatarm==1
*predict yhat, xb
*gen yhat2= round(yhat)
*replace yhat2=1 if yhat2>=1
*replace yhat2=0 if yhat2<=0
tab yhat2 pass
di 1765/5115


collapse (mean) resid treatarm SchoolID, by(TeacherID2)

twoway (kdensity  resid if treatarm==1) (kdensity  resid if treatarm==2) (kdensity  resid if treatarm==3) (kdensity  resid if treatarm==4) ///
,legend( lab(1 "COD")  lab(2 "CG") lab(3 "Combo") lab(4 "Control"))   title("Distribution of teacher value-added") ytitle("Density")
graph export "$results\Graphs\DistributionTeacherVA.pdf", as(pdf) replace









