use "$basein\4 Intervention\TwaEL_2014\TwaTestData_2014_allstudents.dta", clear
drop SchoolID_15_s Grade_15_s _merge USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}

drop if treatment==.
drop if DistrictID==11
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)





 
 forvalues i=2/2{
foreach var of varlist Kis_SI- Math_Pass{
tab `var' if Grade==`i'
}
}
/*
drop if Grade==3
forvalues grd=1/2{
foreach var of varlist Kis_SI- Kis_M Eng_L- Eng_C Math_ID- Math_G{
sum `var' if Grade==`grd'
capture gen Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd'
capture replace Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd'
}
}


egen Z_Kis=rowtotal(Z_Kis_SI-Z_Kis_M), missing
egen Z_Eng=rowtotal(Z_Eng_L- Z_Eng_C), missing
egen Z_Math=rowtotal(Z_Math_ID- Z_Math_G), missing


forvalues grd=1/2{
foreach var of varlist Z_Kis Z_Eng Z_Math{
sum `var' if Grade==`grd'
replace `var'=(`var'-r(mean))/r(sd) if Grade==`grd'
}
}
*/

*This wre the original levels
/*
gen KisLevel1=1 if Kis_SI==0 & Grade==1
replace KisLevel1=2 if Kis_SI>=1 & Kis_SI<=2 & Grade==1
replace KisLevel1=3 if Kis_SI>=3 & Kis_MA<=3 & Grade==1
replace KisLevel1=4 if Kis_MA==4 & Kis_SE<=3 & Grade==1
replace KisLevel1=5 if  Kis_SE==4  & Grade==1

gen EngLevel1=1 if Eng_L==0 & Grade==1
replace EngLevel1=2 if Eng_L==1  & Grade==1
replace EngLevel1=3 if Eng_L>=2 & Eng_L<=3 & Grade==1
replace EngLevel1=4 if Eng_L==4 & Eng_SE<=3 & Grade==1
replace EngLevel1=5 if  Eng_SE==4  & Grade==1

gen MathLevel1=1 if Math_ID<=3 & Grade==1
replace MathLevel1=2 if Math_ID==4 & Math_UTA<=2  & Grade==1
replace MathLevel1=3 if Math_UTA>=3 & Math_BWA<=3  & Grade==1
replace MathLevel1=4 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==1
replace MathLevel1=5 if (Math_J>0 | Math_T>0) & (Math_J<=3 | Math_T<=3) & Grade==1
replace MathLevel1=6 if Math_J==4 & Math_T==4 & Grade==1

************************

gen KisLevel2=1 if Kis_MA==0 & Grade==2
replace KisLevel2=2 if Kis_MA>=1 & Kis_MA<=3 & Grade==2
replace KisLevel2=3 if Kis_MA==4 & Kis_SE<=3 & Grade==2
replace KisLevel2=4 if  Kis_SE==4  & Grade==2

gen EngLevel2=1 if Eng_W==0 & Grade==2
replace EngLevel2=2 if Eng_W==1  & Grade==2
replace EngLevel2=3 if Eng_W>=2 & Eng_W<=3 & Grade==2
replace EngLevel2=4 if Eng_W==4 & Eng_SE<=3 & Grade==2
replace EngLevel2=5 if  Eng_SE==4  & Grade==2

gen MathLevel2=1 if Math_BWA==0 & Grade==2
replace MathLevel2=2 if Math_BWA>=1 & Math_BWA<=3  & Grade==2
replace MathLevel2=3 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==2
replace MathLevel2=4 if (Math_J+Math_T>0 ) & (Math_J+Math_T<=4)  & Grade==2
replace MathLevel2=5 if Math_J+Math_T>=5 & Math_J+Math_T<=7  & Grade==2
replace MathLevel2=6 if Math_J==4 & Math_T==4 & Math_Z<4 & Grade==2
replace MathLevel2=7 if Math_Z==4 & Grade==2
*/
capture drop KisLevel1
gen KisLevel1=1 if Kis_SI==0 & Math_UTA<=3 & Grade==1
replace KisLevel1=2 if Kis_SI==0 & ((Math_UTA>=3 & Math_BWA<=3) | (Math_BWA==4 & Math_J==0 & Math_T==0)) & Grade==1
replace KisLevel1=3 if Kis_SI==0 & (Math_J>0 | Math_T>0) & Grade==1
replace KisLevel1=4 if Kis_SI>=1 & Kis_SI<=2 & Grade==1
replace KisLevel1=5 if Kis_SI>=3 & Kis_MA<=3 & Grade==1
replace KisLevel1=6 if Kis_MA==4 & Kis_SE<=3 & Grade==1
replace KisLevel1=7 if  Kis_SE==4 & Eng_L==0  & Grade==1
replace KisLevel1=8 if  Kis_SE==4 & Eng_L>=1 & Eng_L<=3  & Grade==1
replace KisLevel1=9 if  Kis_SE==4 &  Eng_L==4 & Grade==1
tab KisLevel1

capture drop EngLevel1
gen EngLevel1=1 if Eng_L==0 & (Math_ID<=3 | (Math_ID==4 & Math_UTA<=2)) &  Grade==1
replace EngLevel1=2 if Eng_L==0 & Math_UTA>=3 & Math_BWA<=3 & Grade==1
replace EngLevel1=3 if Eng_L==0 & Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==1
replace EngLevel1=4 if Eng_L==0 & (Math_J>0 | Math_T>0) & (Math_J<=3 | Math_T<=3) & Grade==1
replace EngLevel1=5 if Eng_L==0 & Math_J==4 & Math_T==4 & Grade==1
replace EngLevel1=6 if Eng_L==0 & Math_J==4 & Math_T==4 & Kis_SE==4 & Grade==1
replace EngLevel1=7 if Eng_L==1  & Grade==1
replace EngLevel1=8 if Eng_L>=2 & Eng_L<=3 & Grade==1
replace EngLevel1=9 if Eng_L==4 & Eng_SE<=3 & Grade==1
replace EngLevel1=10 if Eng_SE==4 & Grade==1
tab EngLevel1

capture drop MathLevel1
gen MathLevel1=1 if (Math_ID<=3 | Math_UTA<=2) & Grade==1
replace MathLevel1=2 if Math_UTA>=3 & Math_BWA<=3 & Grade==1
replace MathLevel1=3 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==1
replace MathLevel1=4 if (Math_J>0 | Math_T>0) & (Math_J<=3 | Math_T<=3) & Grade==1
replace MathLevel1=5 if Math_J==4 & Math_T==4 & Kis_SE<4 & Grade==1
replace MathLevel1=6 if Math_J==4 & Math_T==4 & Kis_SE==4 & Grade==1
replace MathLevel1=7 if Math_J==4 & Math_T==4 & Eng_L>=2 & Kis_SE==4 & Grade==1
replace MathLevel1=8 if Math_J==4 & Math_T==4 & Eng_SE>=1 & Kis_SE==4 & Grade==1
tab MathLevel1

************************
capture drop KisLevel2
gen KisLevel2=1 if Math_BWA==0 & Kis_MA==0 & Grade==2
replace KisLevel2=2 if Math_BWA>0 & Math_BWA<=3  & Kis_MA==0 & Grade==2
replace KisLevel2=3 if Math_BWA==4 & Kis_MA==0 & Grade==2
replace KisLevel2=4 if Kis_MA>=1 & Kis_MA<=3 & Grade==2
replace KisLevel2=5 if Kis_MA==4 & Kis_SE<=4 & Grade==2
replace KisLevel2=6 if (Math_J+Math_T>0 ) & (Math_J+Math_T<=7) &  Kis_SE==4  & Grade==2
replace KisLevel2=7 if Math_J==4 & Math_T==4 & Math_Z<4 & Kis_SE==4  & Grade==2
replace KisLevel2=8 if Math_Z==4 & Kis_SE==4  & Grade==2
replace KisLevel2=9 if Math_Z==4 & Eng_W>0 & Kis_SE==4  & Grade==2
tab KisLevel2



capture drop EngLevel2
gen EngLevel2=1 if Eng_W==0 & Math_BWA==0 & Grade==2
replace EngLevel2=2 if Eng_W==0 & Math_BWA>=1 & Math_BWA<=3 & Grade==2
replace EngLevel2=3 if Eng_W==0 & Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==2
replace EngLevel2=4 if Eng_W==0 & (Math_J+Math_T>0 ) & (Math_J+Math_T<=4) & Grade==2
replace EngLevel2=5 if Eng_W==0 & Math_J+Math_T>=5 & Math_J+Math_T<=7 &  Grade==2
replace EngLevel2=6 if Eng_W==0 & Math_J==4 & Math_T==4 & Math_Z<4 &  Grade==2
replace EngLevel2=7 if Eng_W==0 & Math_Z==4 &  Grade==2
replace EngLevel2=8 if Eng_W>0 & Eng_W<=2 & Grade==2
replace EngLevel2=9 if Eng_W>=3 & Grade==2
replace EngLevel2=10 if  Eng_SE==4  & Grade==2
tab EngLevel2

capture drop MathLevel2
gen MathLevel2=1 if Math_BWA==0 & Grade==2
replace MathLevel2=2 if Math_BWA>=1 & Math_BWA<=3  & Grade==2
replace MathLevel2=3 if Math_BWA==4 & Math_J==0 & Math_T==0 & Grade==2
replace MathLevel2=4 if (Math_J+Math_T>0 ) & (Math_J+Math_T<=4)  & Grade==2
replace MathLevel2=5 if Math_J+Math_T>=5 & Math_J+Math_T<=7  & Grade==2
replace MathLevel2=6 if Math_J==4 & Math_T==4 & Math_Z<4 & Grade==2
replace MathLevel2=7 if Math_J==4 & Math_T==4 & Math_Z==4 & Grade==2
replace MathLevel2=8 if Math_J==4 & Math_T==4 & Math_Z==4 & Eng_SE==4 & Grade==2
tab MathLevel2

drop if Grade!=1
collapse (mean) Eng_L Eng_W Eng_SE Kis_SI Kis_MA Kis_SE Math_ID Math_UTA Math_BWA Math_J Math_T, by(SchoolID)
pca Eng_L Eng_W Eng_SE, components(1)
predict Eng, score
xtile EngQ = Eng, nquantiles(10) 

pca Kis_SI Kis_MA Kis_SE, components(1)
predict Kis, score
xtile KisQ = Kis, nquantiles(10) 


pca Math_ID Math_UTA Math_BWA Math_J Math_T, components(1)
predict Math, score
xtile MathQ = Math, nquantiles(10) 



pca Eng_L Eng_W Eng_SE Kis_SI Kis_MA Kis_SE Math_ID Math_UTA Math_BWA Math_J Math_T, components(1)
predict Total, score
xtile TotalQ = Total, nquantiles(10) 


