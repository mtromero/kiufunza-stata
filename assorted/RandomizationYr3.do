set seed 30081986
use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
drop if Kis_Pass==.
keep SchoolID DistrictID
sort SchoolID
quietly by SchoolID:  gen dup = cond(_N==1,0,_n)
drop if dup>1	

merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge!=3
drop _merge
drop dup



gen rand = runiform()
sort DistrictID treatment rand
egen lrank = rank(_n), by(DistrictID treatment)


gen treatment2="COD" if treatment=="COD" & lrank<=4
replace treatment2="Gains" if treatment=="COD" & lrank>4 & lrank<=6
replace treatment2="Control" if treatment=="COD" & lrank>6 & lrank<=7

replace treatment2="COD" if treatment=="Both" & lrank<=1
replace treatment2="Gains" if treatment=="Both" & lrank>1 & lrank<=4
replace treatment2="Control" if treatment=="Both" & lrank>4 & lrank<=7

replace treatment2="COD" if treatment=="Control" & lrank<=1
replace treatment2="Gains" if treatment=="Control" & lrank>1 & lrank<=2
replace treatment2="Control" if treatment=="Control" & lrank>2 & lrank<=4

gen treatarm2=1 if treatment2=="COD"
replace treatarm2=2 if treatment2=="Gains"
replace treatarm2=3 if treatment2=="Control"

bro
