
use "E:\Box Sync\GPS\schoolgps\schoolgps.dta",clear
split s141, parse(",")
rename s1411 latitud
rename s1412 longitud
rename s1413 altitud

replace altitud="1291.2" if SchoolID==120
replace longitud="39.1602" if SchoolID==419 
replace altitud="126.4" if SchoolID==419 
replace altitud="1479.7" if SchoolID==918 
replace longitud="30.917782" if SchoolID==316 
destring,replace
drop s1414

save "E:\Box Sync\GPS\schoolgps\schoolgps_clean.dta", replace
rename  * =_1
cross using "E:\Box Sync\GPS\schoolgps\schoolgps_clean.dta"
geodist latitud_1 longitud_1 latitud longitud, gen(distance)
drop s141_1 latitud_1 longitud_1 altitud_1 s141 latitud longitud altitud
drop if SchoolID_1==SchoolID 
save "E:\Box Sync\01_KiuFunza\RawData\1 Baseline\School\DistanceData.dta", replace

