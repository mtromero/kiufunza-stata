use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", replace

rename passkiswahili_T3 PR_k_T3
rename passkiingereza_T3 PR_e_T3
rename passhisabati_T3 PR_m_T3



drop if stdgrd_T3==.
collapse (mean) StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1 PR_k_T3 PR_e_T3 PR_m_T3, by(SchoolID stdgrd_T3)
reshape wide PR_k_T3 PR_e_T3 PR_m_T3 , i(SchoolID) j(stdgrd_T3)

forvalues i=1/3{
foreach sub in e k m{
rename PR_`sub'_T3`i' PR_`i'_`sub'_T3
}
}

merge 1:1 SchoolID using "$base_out\Consolidated\School.dta"
*use  "$base_out\3 Endline\School\SchoolConsolidated.dta", clear
drop if treatment=="CG"
capture drop if _merge==2
label var PR_1_k_T4 "K S1"
label var PR_2_k_T4 "K S2"
label var PR_3_k_T4 "K S3"
label var PR_1_e_T4 "E S1"
label var PR_2_e_T4 "E S2"
label var PR_3_e_T4 "E S3"
label var PR_1_m_T4 "M S1"
label var PR_2_m_T4 "M S2"
label var PR_3_m_T4 "M S3"


label var PR_1_k_T3 "K S1 T3 "
label var PR_2_k_T3  "K S2 T3"
label var PR_3_k_T3  "K S3 T3"
label var PR_1_e_T3  "E S1 T3"
label var PR_2_e_T3  "E S2 T3"
label var PR_3_e_T3  "E S3 T3"
label var PR_1_m_T3  "M S1 T3"
label var PR_2_m_T3  "M S2 T3"
label var PR_3_m_T3  "M S3 T3"

qui estpost tabstat PR_1_k_T4 PR_1_k_T3 PR_2_k_T4 PR_2_k_T3  PR_3_k_T4 PR_3_k_T3 PR_1_e_T4 PR_1_e_T3 PR_2_e_T4 PR_2_e_T3 PR_3_e_T4 PR_3_e_T3 PR_1_m_T4 PR_1_m_T3 PR_2_m_T4 PR_2_m_T3 PR_3_m_T4 PR_3_m_T3 , by( treatment) columns(statistics) stats(mean count) listwise
esttab using "$results\LatexCode\PassRatesSumary.tex", label replace booktabs main(mean) unstack


*Now to calculate how much money was "wasted" we need to calculcate the total aount of money given and assume the pass rate would have been the same...(sounds though?)

gen TotalPays=TotPassKis_1c_T4+TotPassEng_1c_T4+TotPassMath_1c_T4+TotPassKis_2c_T4+TotPassEng_2c+TotPassMath_2c_T4+TotPassKis_3c_T4+TotPassEng_3c_T4+TotPassMath_3c_T4
sum TotalPays if treatment=="COD" 
scalar MoneyTotalCOD=r(sum)*6000
sum TotalPays if treatment=="Both" 
scalar MoneyTotalBoth=r(sum)*6000
di MoneyTotalCOD
di MoneyTotalBoth

scalar MoneyFreeCOD=0
scalar MoneyFreeBoth=0
foreach name in k e m{
forval i=1/3  {
qui sum PR_`i'_`name'_T4 if treatment=="Control"
scalar TP=r(mean)
qui sum TotStud_`i'_T4 if treatment=="COD"
scalar MoneyFreeCOD=MoneyFreeCOD+ r(sum)*TP*6000
qui sum TotStud_`i'_T4 if treatment=="Both"
scalar MoneyFreeBoth=MoneyFreeBoth+ r(sum)*TP*6000
}
}
di MoneyFreeCOD
di MoneyFreeBoth


scalar MoneyFreeCOD2=0
scalar MoneyFreeBoth2=0
foreach name in k e m{
forval i=1/3  {
qui sum PR_`i'_`name'_T3 if treatment=="Control"
scalar TP=r(mean)
qui sum TotStud_`i'_T4 if treatment=="COD"
scalar MoneyFreeCOD2=MoneyFreeCOD2+ r(sum)*TP*6000
qui sum TotStud_`i'_T4 if treatment=="Both"
scalar MoneyFreeBoth2=MoneyFreeBoth2+ r(sum)*TP*6000

}
}
di MoneyFreeCOD2
di MoneyFreeBoth2


di MoneyFreeCOD/MoneyTotalCOD
di MoneyFreeCOD2/MoneyTotalCOD

di MoneyFreeBoth/MoneyTotalBoth
di MoneyFreeBoth2/MoneyTotalBoth

