*******************************************************
*************BASE LINE
********************************************************
use "$basein\1 Baseline\Student\Sample_noPII.dta", clear
drop _merge

merge m:1 SchoolID  using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepus(treatment treatarm DistID)
drop if _merge==1
drop _merge

drop  Stream_ID StreamID totSampleStreams_ID streamSize_ID StudentID
*child did not answer the base test 
drop if childAvailable==2  
*child did not answer the base test
drop if consentChild==2
*child refused to answer finish base test
drop if result==2 
drop  consentChild result childAvailable timeEndTest timeStartTest

*Now if put the children in different levels depending on how far they went in the Test
gen KiswahiliLevel=.
replace KiswahiliLevel=4 if kiswahili_4==1 & (kiswahili_5+kiswahili_6)>=1 & kiswahili_4!=. & kiswahili_4!=. & kiswahili_6!=. & KiswahiliLevel==.
replace KiswahiliLevel=3 if kiswahili_3==1 & kiswahili_3!=. & KiswahiliLevel==.
replace KiswahiliLevel=2 if kiswahili_2>=4 & kiswahili_2!=. & KiswahiliLevel==. 
replace KiswahiliLevel=1 if kiswahili_1>=4 & kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=0 if KiswahiliLevel==.

gen EnglishLevel=.
replace EnglishLevel=4 if kiingereza_4==1 & (kiingereza_5+kiingereza_6)>=1 & kiingereza_4!=. & kiingereza_5!=. & kiingereza_6!=. & EnglishLevel==.
replace EnglishLevel=3 if kiingereza_3==1 & kiingereza_3!=. & EnglishLevel==.
replace EnglishLevel=2 if kiingereza_2>=4 & kiingereza_2!=. & EnglishLevel==. 
replace EnglishLevel=1 if kiingereza_1>=4 & kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=0 if EnglishLevel==.


label define reading 0 "Nothing"  1 "Syllable" 2 "Words"  3 "Paragraph" 4 "Read"
label values KiswahiliLevel reading
label variable KiswahiliLevel "Kiswahili reading level"
label values EnglishLevel reading
label variable EnglishLevel "English reading level"

	   		   
gen MathLevel=.
replace MathLevel=6 if  hisabati_6>=4 & hisabati_6!=.
replace MathLevel=5 if  hisabati_5>=4 & hisabati_5!=. &  MathLevel==.
replace MathLevel=4 if  hisabati_4>=4 & hisabati_4!=. &  MathLevel==.
replace MathLevel=3 if  hisabati_3>=4 & hisabati_3!=. &  MathLevel==.
replace MathLevel=2 if  hisabati_2>=4 & hisabati_2!=. &  MathLevel==.
replace MathLevel=1 if  hisabati_1>=4 & hisabati_1!=. & MathLevel==. 
replace MathLevel=0 if  MathLevel==. 

label define math 0 "Nothing"  1 "Counting" 2 "Numbers"  3 "Values" 4 "Addition" 5 "Subtraction" 6 "Multiplication"
label values MathLevel  math
label variable MathLevel "Math level"


compress
drop if usid==""
rename * =_T1
rename usid_T1 usid

save "$base_out\Consolidated\Student_YoudiGains.dta", replace

use "$basein\3 Endline\Student\ESTudent_noPII.dta", clear
merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta",keepus(treatment treatarm DistID)
drop _merge

gen grade=1 if stdgrd==1 & atrgrd==.
replace grade=1 if atrgrd==1
replace grade=2 if stdgrd==2 & atrgrd==.
replace grade=2 if atrgrd==2
replace grade=3 if stdgrd==3 & atrgrd==.
replace grade=3 if atrgrd==3

gen student_present=.
replace student_present=1 if  attstd==1
replace student_present=0 if  attstd==2
replace student_present=1 if  attstd==3 & attstd2==1
replace student_present=0 if  attstd==3 & attstd2==2	 
rename student_present attendance


drop if attendance==0

gen KiswahiliLevel=.
replace KiswahiliLevel=3 if S1_kiswahili_3>=4 & S1_kiswahili_3!=. & KiswahiliLevel==.
replace KiswahiliLevel=2 if S1_kiswahili_2>=4 & S1_kiswahili_2!=. & KiswahiliLevel==.
replace KiswahiliLevel=1 if S1_kiswahili_1>=4 & S1_kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=4 if S2_kiswahili_3==1 & S2_kiswahili_3!=. & KiswahiliLevel==.
replace KiswahiliLevel=3 if S2_kiswahili_2>=4 & S2_kiswahili_2!=. & KiswahiliLevel==.
replace KiswahiliLevel=2 if S2_kiswahili_1>=4 & S2_kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=5 if S3_kiswahili_2>=1 & S3_kiswahili_2!=. & KiswahiliLevel==.
replace KiswahiliLevel=4 if S3_kiswahili_1==1 & S3_kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=0 if KiswahiliLevel==.


gen EnglishLevel=.
replace EnglishLevel=3 if S1_kiingereza_3>=4 & S1_kiingereza_3!=. & EnglishLevel==.
replace EnglishLevel=2 if S1_kiingereza_2>=4 & S1_kiingereza_2!=. & EnglishLevel==.
replace EnglishLevel=1 if S1_kiingereza_1>=4 & S1_kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=4 if S2_kiingereza_3==1 & S2_kiingereza_3!=. & EnglishLevel==.
replace EnglishLevel=3 if S2_kiingereza_2>=4 & S2_kiingereza_2!=. & EnglishLevel==.
replace EnglishLevel=2 if S2_kiingereza_1>=4 & S2_kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=5 if S3_kiingereza_2>=1 & S3_kiingereza_2!=. & EnglishLevel==.
replace EnglishLevel=4 if S3_kiingereza_1==1 & S3_kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=0 if EnglishLevel==.

label define reading_t3 0 "Nothing"  1 "Syllable" 2 "Words" 3 "Sentences"  4 "Paragraph" 5 "Read"
label values KiswahiliLevel reading_t3
label variable KiswahiliLevel "Kiswahili reading level"
label values EnglishLevel reading_t3
label variable EnglishLevel "English reading level"

gen MathLevel=.
replace MathLevel=5 if  S1_hisabati_5>=4 & S1_hisabati_5!=. &  MathLevel==.
replace MathLevel=4 if  S1_hisabati_4>=4 & S1_hisabati_4!=. &  MathLevel==.
replace MathLevel=3 if  S1_hisabati_3>=4 & S1_hisabati_3!=. &  MathLevel==.
replace MathLevel=2 if  S1_hisabati_2>=4 & S1_hisabati_2!=. &  MathLevel==.
replace MathLevel=1 if  S1_hisabati_1>=4 & S1_hisabati_1!=. &  MathLevel==. 
replace MathLevel=6 if  S2_hisabati_4>=4 & S2_hisabati_4!=. &  MathLevel==.
replace MathLevel=5 if  S2_hisabati_3>=4 & S2_hisabati_3!=. &  MathLevel==.
replace MathLevel=4 if  S2_hisabati_2>=4 & S2_hisabati_2!=. &  MathLevel==.
replace MathLevel=3 if  S2_hisabati_1>=4 & S2_hisabati_1!=. &  MathLevel==.
replace MathLevel=6 if  S3_hisabati_3>=4 & S3_hisabati_3!=. &  MathLevel==.
replace MathLevel=5 if  S3_hisabati_2>=4 & S3_hisabati_2!=. &  MathLevel==.
replace MathLevel=4 if  S3_hisabati_1>=4 & S3_hisabati_1!=. &  MathLevel==.
replace MathLevel=0 if  MathLevel==. 

label define math_t3 0 "Nothing"  1 "Counting" 2 "Numbers"  3 "Values" 4 "Addition" 5 "Subtraction" 6 "Multiplication" 
label values MathLevel  math_t3
label variable MathLevel "Math level"


gen usid=subinstr(upid,"R1STU","",.)
drop if usid==""

rename * =_T3
rename usid_T3 usid

merge 1:1 usid using "$base_out\Consolidated\Student_YoudiGains.dta"
drop if _merge!=3
drop _merge

set more off
forvalues grade=1/3{
foreach subject in Math English Kiswahili{
estpost tabulate `subject'Level_T1 `subject'Level_T3 if GradeID_T1==`grade'
esttab using "$exceltables\Gains_`subject'_GR`grade'.csv", nostar unstack not noobs replace 
}
}


