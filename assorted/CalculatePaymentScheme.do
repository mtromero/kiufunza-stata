*******************************************************
*************END LINE 2014
********************************************************
set more off
use "$basein\8 Endline 2014\Final Data\Household\R6HHData_noPII.dta", clear
keep if consentChild==1
keep HHID DistID SchoolID consentChild- edtstm
merge 1:m HHID using "$basein\8 Endline 2014\ID Linkage\R6_Student_HH_ID_Linkage_noPII.dta", keepus(upid) 
drop if  _merge!=3
drop _merge
rename upid upidst
merge 1:1 upidst using "$basein\8 Endline 2014\Final Data\Student\R6Student_noPII.dta", update replace
drop _merge
drop if consentChild!=1
merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatarm)
drop if _merge==2
drop _merge

foreach var of varlist kissyl1_1- othsub3_24 {
	replace `var'=0 if `var'!=1
}


egen kisLtr1= rowtotal( kissyl1_1 - kissyl1_5)
egen kisWrd1= rowtotal( kiswrd1_1- kiswrd1_6)
egen kisSen1= rowtotal( kissen1_1 -kissen1_6)
egen engLtr1= rowtotal( englet1_1 - englet1_5)
egen engWrd1= rowtotal( engwrd1_1- engwrd1_6)
egen engSen1= rowtotal( engsen1_1 -engsen1_6)
egen mathCount1= rowtotal( hiscou1_1 -hiscou1_3)
egen mathNumber1= rowtotal( hisidn1_1- hisidn1_3)
egen mathIneq1= rowtotal( hisinq1_1- hisinq1_4)
egen mathAdd1= rowtotal( hisadd1_1- hisadd1_4)
egen mathSub1= rowtotal(hissub1_1- hissub1_4)

egen kisLtr2= rowtotal( kissyl2_1 -kissyl2_4)
egen kisWrd2= rowtotal( kiswrd2_1 -kiswrd2_9)
egen kisSen2= rowtotal( kissen2_1- kissen2_9)
gen kisPara2=kispar2_1
egen engLtr2= rowtotal( englet2_1- englet2_4)
egen engWrd2= rowtotal( engwrd2_1 -engwrd2_9)
egen engSen2= rowtotal( engsen2_1- engsen2_9)
gen engPara2=engpar2_1
egen mathCount2= rowtotal( hiscou2_1 - hiscou2_2)
egen mathNumber2= rowtotal( hisidn2_1 - hisidn2_2)
egen mathIneq2= rowtotal( hisinq2_1 -hisinq2_4)
egen mathAdd2= rowtotal( hisadd2_1- hisadd2_6)
egen mathSub2= rowtotal(hissub2_1- hissub2_6)
egen mathMult2= rowtotal(hismul2_1- hismul2_4)

egen kisLtr3= rowtotal( kissyl3_1 - kissyl3_4)
egen kisWrd3= rowtotal( kiswrd3_1- kiswrd3_8)
egen kisSen3= rowtotal( kissen3_1 -kissen3_8)
gen kisPara3=kispar3_1
egen kisRead3= rowtotal( kissto3_1 -kiscom3_3) /*4 questions*/
egen engLtr3= rowtotal( englet3_1- englet3_4)
egen engWrd3= rowtotal( engwrd3_1 - engwrd3_8)
egen engSen3= rowtotal( engsen3_1- engsen3_8)
gen engPara3=engpar3_1
egen engRead3= rowtotal( engsto3_1 -engcom3_3) /*4 questions*/

egen mathCount3= rowtotal( hiscou3_1- hiscou3_2)
egen mathNumber3= rowtotal(hisidn3_1- hisidn3_2)
egen mathIneq3= rowtotal( hisinq3_1- hisinq3_4)
egen mathAdd3= rowtotal( hisadd3_1- hisadd3_7)
egen mathSub3= rowtotal(hissub3_1- hissub3_7)
egen mathMult3= rowtotal(hismul3_1- hismul3_5)
egen mathDivi3= rowtotal(hisdiv3_1- hisdiv3_3)


foreach var of varlist kisLtr1- mathDivi3{
sum `var'
gen `var'_prop=`var'/r(max)
}
preserve
collapse (mean) kisLtr1_prop- mathDivi3_prop if treatarm!=4
export excel using "$exceltables\MeanControl2014_2.xls", firstrow(varlabels) replace
save "$base_out\Consolidated\MeanCorrectControl2014.dta", replace
restore

egen kis1=anycount(kissyl1_1-kissen1_6) if stdgrd==1 & consentChild==1, values(1)
egen eng1=anycount(englet1_1-engsen1_6) if stdgrd==1 & consentChild==1, values(1)
egen math1=anycount(hiscou1_1-hissub1_4) if stdgrd==1 & consentChild==1, values(1)
egen kis2=anycount(kissyl2_1-kispar2_1) if stdgrd==2 & consentChild==1, values(1)
egen eng2=anycount(englet2_1-engpar2_1) if stdgrd==2 & consentChild==1, values(1)
egen math2=anycount(hiscou2_1-hismul2_4) if stdgrd==2 & consentChild==1, values(1)
egen kis3=anycount(kissyl3_1-kiscom3_3) if stdgrd==3 & consentChild==1, values(1)
egen eng3=anycount(englet3_1-engcom3_3) if stdgrd==3 & consentChild==1, values(1)
egen math3=anycount(hiscou3_1-hisdiv3_3) if stdgrd==3 & consentChild==1, values(1)

foreach var of varlist kis1 eng1 math1 {
	replace `var'=. if stdgrd!=1
	replace `var'=. if consentChild!=1
}
foreach var of varlist kis2 eng2 math2 {
	replace `var'=. if stdgrd!=2
	replace `var'=. if consentChild!=1
}
foreach var of varlist kis3 eng3 math3 {
	replace `var'=. if stdgrd!=3
	replace `var'=. if consentChild!=1
}


drop  consentChild-edtstm  timeStartTest timeEndTest result result_other persontype
sum kis1 eng1 math1 if stdgrd==1, detail
sum kis2 eng2 math2 if stdgrd==2, detail
sum kis3 eng3 math3 if stdgrd==3, detail
gen kis1_prop= kis1/17
gen eng1_prop= eng1/17
gen math1_prop= math1/18
gen kis2_prop= kis2/23
gen eng2_prop= eng2/23
gen math2_prop= math2/24
gen kis3_prop= kis3/25
gen eng3_prop= eng3/25
gen math3_prop= math3/30


egen kis=rowtotal(kis1_prop kis2_prop kis3_prop)
egen eng=rowtotal(eng1_prop eng2_prop eng3_prop)
egen math=rowtotal(math1_prop math2_prop math3_prop)


foreach var of varlist kis eng math{
rename `var' `var'_2014
}
keep HHID DistID SchoolID upidst GradeID R6StudentID stdgrd stdgrp stdage stdsex kis_2014 eng_2014 math_2014 treatarm
save "$base_out\Consolidated\Student_EL2013_2014.dta", replace




use "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID

merge 1:m SchoolID using "$basein\3 Endline\Student\ESTudent_noPII.dta"
drop _merge
compress
drop if consentChild!=1

replace S2_kiswahili_3=0 if S2_kiswahili_3==2
replace S2_kiingereza_3=0 if S2_kiingereza_3==2
replace S3_kiswahili_1=0 if S3_kiswahili_1==2
replace S3_kiingereza_1=0 if S3_kiingereza_1==2

foreach var of varlist S3_hisabati_1 S3_hisabati_2 S3_hisabati_3 S3_hisabati_4 S3_hisabati_5 S3_kiingereza_1 S3_kiingereza_2 S3_kiswahili_1 S3_kiswahili_2 S2_hisabati_1 S2_hisabati_2 S2_hisabati_3 S2_hisabati_4 S2_kiingereza_1 S2_kiingereza_2 S2_kiingereza_3 S2_kiswahili_1 S2_kiswahili_2 S2_kiswahili_3 S1_kiswahili_1 S1_kiswahili_2 S1_kiswahili_3 S1_kiingereza_1 S1_kiingereza_2 S1_kiingereza_3 S1_hisabati_1 S1_hisabati_2 S1_hisabati_3 S1_hisabati_4 S1_hisabati_5 {
sum `var'
gen `var'_prop=`var'/r(max)
}
preserve
collapse (mean) S3_hisabati_1_prop- S1_hisabati_5_prop if treatarm!=4
export excel using "$exceltables\MeanControl2013_2.xls", firstrow(varlabels) replace
save "$base_out\Consolidated\MeanControl2013_2.dta", replace
restore

egen kis1= rowtotal(S1_kiswahili_1 S1_kiswahili_2 S1_kiswahili_3  )
egen eng1= rowtotal(S1_kiingereza_1 S1_kiingereza_2 S1_kiingereza_3 )
egen math1= rowtotal(S1_hisabati_1 S1_hisabati_2 S1_hisabati_3 S1_hisabati_4 S1_hisabati_5)




egen kis2= rowtotal(S2_kiswahili_1 S2_kiswahili_2 S2_kiswahili_3 )
egen eng2= rowtotal(S2_kiingereza_1 S2_kiingereza_2 S2_kiingereza_3 )
egen math2= rowtotal(S2_hisabati_1 S2_hisabati_2 S2_hisabati_3 S2_hisabati_4)


foreach var of varlist kis1 eng1 math1 {
	replace `var'=. if stdgrd!=1
	replace `var'=. if consentChild!=1
}
foreach var of varlist kis2 eng2 math2 {
	replace `var'=. if stdgrd!=2
	replace `var'=. if consentChild!=1
}


replace kis1= kis1/22
replace eng1= eng1/22
replace math1= math1/30
replace kis2= kis2/15
replace eng2= eng2/15
replace math2= math2/24

egen kis=rowtotal(kis1 kis2)
egen eng=rowtotal(eng1 eng2)
egen math=rowtotal(math1 math2)

drop S1_kiswahili_1- timeEndTest
drop if stdgrd==3





foreach var of varlist kis eng math{
rename `var' `var'_2013
}

keep upid kis_2013 eng_2013 math_2013
save "$base_out\Consolidated\Student_EL2013BL2014.dta", replace


use "$basein\6 Baseline 2014\Data\student\R4Student_noPII.dta",clear
merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge==2
drop _merge
drop if consentChild==2 | consentChild==.

egen kis= rowtotal(Akiswahili_1 Akiswahili_2 Akiswahili_3  ) 
egen eng= rowtotal(Akiingereza_1 Akiingereza_2 Akiingereza_3 )
egen math= rowtotal(Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5)

replace kis= kis/22
replace eng= eng/22
replace math= math/30

foreach var of varlist kis eng math{
rename `var' `var'_2013
}
keep upid kis_2013 eng_2013 math_2013

append using "$base_out\Consolidated\Student_EL2013BL2014.dta"

rename upid upidst

merge 1:1 upidst using "$base_out\Consolidated\Student_EL2013_2014.dta"
drop if _merge!=3
drop _merge
keep upidst kis_2013 eng_2013 math_2013 SchoolID GradeID R6StudentID stdgrd stdgrp stdage stdsex kis_2014 eng_2014 math_2014 treatarm

sum kis_2013 eng_2013 math_2013 kis_2014 eng_2014 math_2014
sum kis_2013 eng_2013 math_2013 kis_2014 eng_2014 math_2014 if stdgrd==1
sum kis_2013 eng_2013 math_2013 kis_2014 eng_2014 math_2014 if stdgrd==2
sum kis_2013 eng_2013 math_2013 kis_2014 eng_2014 math_2014 if stdgrd==3


merge m:1  SchoolID using "$base_out\Consolidated\School.dta"
drop if _merge==2
drop _merge
drop IDDate_T4-HrsFocal3_T3  NumberOfTeachers_S7_T3-StudentsGr7_T3 intvwr_T3-ifotyn_T2
sum DistrictID
local controlVars c.stdage##c.stdage i.stdsex i.DistrictID s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 s108_T1 s118_T1 s120_T1 computersYN_T1 PipedWater_T1 NoWater_T1 SingleShift_T1

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

eststo clear
forvalues grade = 1/3{
foreach subject in math eng kis{
eststo:  reg `subject'_2014 TreatmentCG TreatmentCOD TreatmentBoth `subject'_2013 c.`subject'_2013#c.`subject'_2013  `controlVars' if stdgrd==`grade'
eststo:  reg `subject'_2014 `subject'_2013 c.`subject'_2013#c.`subject'_2013  `controlVars' if stdgrd==`grade' & treatment=="Control"

 }
 }
 esttab using "$results\LatexCode\Regs_NormalProgress.csv", se ar2 label /// 
replace title("Treatment effect on standardized test scores for Kiswahili")   b(a2) se(a2)   ///
keep(TreatmentCG TreatmentCOD TreatmentBoth math_2013 c.math_2013#c.math_2013 eng_2013 c.eng_2013#c.eng_2013 kis_2013 c.kis_2013#c.kis_2013 _cons) ///
star(* 0.10 ** 0.05 *** 0.01)

 
