***********************************************************
******************************** 2013 *********************
***********************************************************

use "$radar/RawData/ResultadosEstudiantesTotal_PSLE_2013.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
merge m:1 school_codePSL_Final using  "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta",keepus(SchoolID DistID)
drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2013.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2013.dta", replace

use "$radar/RawData/ResultadosSchoolTotal_PSLE_2013.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final
merge 1:1 school_codePSL_Final using  "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2013.dta",keepus(SchoolID DistID)
drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2013.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2013.dta", replace

***********************************************************
******************************** 2014 *********************
***********************************************************




use "$radar/RawData/ResultadosEstudiantesTotal_PSLE_2014.dta", clear
capture renvars _all, subst("`=char(69)'" "")
capture renvars _all, subst("`=char(69)'" "")
rename AVRAG AVERAGE
qui foreach sub in AVERAGE kiswahili english maarifa hisabati sayansi{
	replace `sub'="5" if `sub'=="A"
	replace `sub'="4" if `sub'=="B"
	replace `sub'="3" if `sub'=="C"
	replace `sub'="2" if `sub'=="D"
	replace `sub'="1" if `sub'=="E"
	replace `sub'="0" if `sub'=="F"
	replace `sub'="" if `sub'=="NA"
	replace `sub'="" if `sub'=="X"
	replace `sub'="" if `sub'=="*S"
	replace `sub'="" if `sub'=="*W"
}
qui destring AVERAGE kiswahili english maarifa hisabati sayansi, replace
drop if AVERAGE==.
keep CAND SX AVERAGE kiswahili- sayansi school_codePSLE
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
merge m:1 school_codePSL_Final using  "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta",keepus(SchoolID DistID)
drop if _merge!=3
drop _merge
save "$base_out/Student_PSLE_2014.dta", replace
gen Pass=(AVERAGE>=3)
collapse (sum) Pass=Pass (mean) PassRate=Pass AverageOwn=AVERAGE kiswahili- sayansi, by(school_codePSL_Final)
save "$base_out/School_PSLE_2014.dta", replace

use "$radar/RawData/ResultadosSchoolTotal_PSLE_2014.dta", clear
replace school_codePSLE=regexr(school_codePSLE,"P","PS") 
rename school_codePSLE school_codePSL_Final
keep Students Average RankingDistrict TotalDistrict RankingRegion TotalRegion RankingNation TotalNation school_codePSL_Final
merge 1:1 school_codePSL_Final using  "$base_out/CrossWalk_PSLE/CrossWalk_PSLE_2014.dta",keepus(SchoolID DistID)
drop if _merge!=3
drop _merge
merge 1:1 school_codePSL_Final using "$base_out/School_PSLE_2014.dta"
drop if _merge!=3
drop _merge
save "$base_out/School_PSLE_2014.dta", replace
