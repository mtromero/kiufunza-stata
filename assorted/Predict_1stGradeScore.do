*2014_InterventionDataAnalysis
use "$basein\4 Intervention\TwaEL_2013\TwaStudResults.dta", clear
replace kis_a=. if kis_a>1
replace kis_h=. if kis_h>1
replace eng_se=. if eng_se>4
replace  math_bwa=. if  math_bwa>4
replace  math_j=. if  math_j>4
replace  math_t=. if  math_t>4
drop if Grade!=1

collapse (mean) kis_si- kis_m eng_l -eng_c math_id- math_g, by( SchoolID)
drop kis_a kis_h kis_m eng_p eng_s eng_c math_z math_g
gen T=1
save "$base_out\4 Intervention\TwaEL_2013\SchoolLevel.dta", replace

*2014_InterventionDataAnalysis
use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
drop if Grade!=1
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6
 rename _all, lower
 rename schoolid SchoolID
collapse (mean) kis_si- kis_m eng_l -eng_c math_id- math_g, by( SchoolID)
drop kis_a kis_h kis_m eng_p eng_s eng_c math_z math_g
gen T=2
append using "$base_out\4 Intervention\TwaEL_2013\SchoolLevel.dta"
tsset SchoolID T
reshape wide kis_si- math_t, i(SchoolID) j(T)

pca kis_si1 kis_ma1 kis_se1, components(1)
predict kis1, score
xtile kisQ1 = kis1, nquantiles(10) 

pca kis_si2 kis_ma2 kis_se2, components(1)
predict kis2, score
xtile kisQ2 = kis2, nquantiles(10) 


pca eng_l1 eng_w1 kis_se1, components(1)
predict eng1, score
xtile engQ1 = eng1, nquantiles(10) 

pca eng_l2 eng_w2 kis_se2, components(1)
predict eng2, score
xtile engQ2 = eng2, nquantiles(10) 


pca math_id1 math_uta1 math_bwa1 math_j1 math_t1, components(1)
predict math1, score
xtile mathQ1 = math1, nquantiles(10) 

pca math_id2 math_uta2 math_bwa2 math_j2 math_t2, components(1)
predict math2, score
xtile mathQ2 = math2, nquantiles(10) 
keep SchoolID kis1- mathQ2 

