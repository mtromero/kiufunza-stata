*Erin's modifications, 3.1.2015
*Mauricio, this won't run b/c it has my attempt at new code and your own original code in it

clear
*global basein "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData"
global randomize "$mipath\Codes\Erin\2015 Randomization"

set seed 30081986
use "$basein\4 Intervention\TwaEL_2014\TwaTestData_stutested.dta", clear
drop if treatment==.
drop if Kis_Pass==.
keep SchoolID DistrictID
sort SchoolID
quietly by SchoolID:  gen dup = cond(_N==1,0,_n)
drop if dup>1	

merge m:1 SchoolID using "$basein\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge!=3
drop _merge
drop dup

*merging in enrollment for stratifying and school data for balance tests
sort SchoolID
merge 1:1 SchoolID using "$randomize\enrollment.dta"
drop if _merge!=3
drop _merge

sort SchoolID
merge 1:1 SchoolID using "$randomize\avgtests_school.dta"
drop if _merge!=3
drop _merge

sort SchoolID
merge 1:1 SchoolID using "$randomize\num_teachers.dta"
drop if _merge!=3
drop _merge

*creating dummies for paired schools
gen paired = SchoolID
recode paired (135 134=1) /*combo schools*/
recode paired (428 405=2) /*COD schools*/
recode paired (804 802=3) /*combo schools*/ 

*creating tot_en to use for stratifying
bys paired: egen adj_school_size =sum(tot_en)

/*This is where I'm not sure how to use paired and tot_en to stratify.
In Isaac's 2013 do file he broke them into groups of 5 by size, but with 7 schools per distr/treatment,
I'm not sure a good size to use. This is my start of an attempt, but I get stuck b/c 
I don't know how to maintain the pairs when, within each group of seven, there are times
when only one school is assigned to a treatment group in a district (for example, only one COD
school in Korogwe district will be assigned to Control). So then does having the pairs
undermine the whole randomization?*/

gsort DistrictID treatment -adj_school_size
by DistrictID treatment: g dist_size_treat_rank=_n
*create groups of 5 based on size
gen groupbyrank =cond(dist_size_treat_rank/5-trunc(dist_size_treat_rank/5)>0, trunc(dist_size_treat_rank/5)+1,dist_size_treat_rank/5)

*draw one set of random numbers to pair schools in groups
gen rnd_num = uniform()
bys paired: egen adj_rnd_num =mean(rnd_num) 
replace adj_rnd_num= adj_rnd_num+1 if paired<99 /* this ensures that paired schools are at the top when we start randomly pairing */
gsort DistrictID treatment groupbyrank -adj_rnd_num  /* descending order of size of random number within each "group" so we pair them randomly */
by DistrictID treatment groupbyrank: gen group2_rank= _n
gen within_group_rank = cond(groupbyrank>1,group2_rank, cond(group2_rank/2-trunc(group2_rank/2)>0, trunc(group2_rank/2)+1,group2_rank/2))

*reset seed
set seed 19824
gen rnd_num2 = uniform()

gsort DistrictID groupbyrank within_group_rank -rnd_num

bys DistrictID treatment groupbyrank within_group_rank: gen adj_rnd_num2_tmp = rnd_num2 if _n==1
bys DistrictID treatment groupbyrank within_group_rank: egen adj_rnd_num2 = max(adj_rnd_num2_tmp)
drop adj_rnd_num2_tmp

gsort DistrictID treatment groupbyrank -adj_rnd_num2
bys DistrictID treatment groupbyrank: egen rct_rank = rank(adj_rnd_num2), track

gen treatment2="COD" if treatment=="Control" & rct_rank<=1
replace treatment2="Gains" if treatment=="Control" & rct_rank>1 & rct_rank<=2
replace treatment2="Control" if treatment=="Control" & rct_rank>2 & rct_rank<=4
**and here I got confused b/c I don't think it can work w/ the pairs**

*this (lines 87 - 107) is your original randomization code
gen rand = runiform()
sort DistrictID treatment rand
egen lrank = rank(_n), by(DistrictID treatment)

gen treatment2="COD" if treatment=="COD" & lrank<=4
replace treatment2="Gains" if treatment=="COD" & lrank>4 & lrank<=6
replace treatment2="Control" if treatment=="COD" & lrank>6 & lrank<=7

replace treatment2="COD" if treatment=="Both" & lrank<=1
replace treatment2="Gains" if treatment=="Both" & lrank>1 & lrank<=4
replace treatment2="Control" if treatment=="Both" & lrank>4 & lrank<=7

replace treatment2="COD" if treatment=="Control" & lrank<=1
replace treatment2="Gains" if treatment=="Control" & lrank>1 & lrank<=2
replace treatment2="Control" if treatment=="Control" & lrank>2 & lrank<=4

gen treatarm2=1 if treatment2=="COD"
replace treatarm2=2 if treatment2=="Gains"
replace treatarm2=3 if treatment2=="Control"

bro

*generate groupvars for simple T-test
g cod_vs_con = treatarm2
recode cod_vs_con (2 = .)

g gains_vs_con = rct_rank
recode gains_vs_con (1 = .)


*check balance
log using "$randomize\balance_tests_Yr3.log", replace

local stuff " no_teachers no_classrooms urban ptr vol_teachers_dummy z_uwezo tot_enroll"

foreach x of local stuff {
oneway `x' treat, tabulate
}

log close
