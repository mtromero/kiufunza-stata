

use "$basein\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage_noPII.dta", clear 
drop if TeacherID==.
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R4TeacherID
merge 1:1 DistrictID SchoolID TeacherID using "$basein\1 Baseline\Teacher\Teacher_noPII.dta", keepus(t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25) 
drop _merge
drop if t05==. & t06==. & t07==. & t08==.  
save "$base_out\Consolidated\Teacher_YD.dta", replace

use "$basein\2 Midline\MTeacher_noPII.dta",clear
gen upidold=upid
save "$base_out\2 Midline\MTeacher_noPII.dta",replace

use "$basein\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage_noPII.dta", clear 
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R4TeacherID
drop if upidold==""

merge 1:1 upidold using "$base_out\2 Midline\MTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25) 
drop _merge
drop if t05==. & t06==. & t07==. & t08==.  

append using "$base_out\Consolidated\Teacher_YD.dta"
save "$base_out\Consolidated\Teacher_YD.dta", replace 

use "$basein\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage_noPII.dta", clear 
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R4TeacherID
drop if  ETTeacherID==.
merge 1:1 ETTeacherID using "$basein\3 Endline\Teacher\ETTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25) 
drop _merge
drop if t05==. & t06==. & t07==. & t08==. 
replace t23=t23*1.08
append using "$base_out\Consolidated\Teacher_YD.dta"
drop t23
save "$base_out\Consolidated\Teacher_YD.dta", replace 



use "$basein\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage_noPII.dta", clear 
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R4TeacherID
merge 1:1 upid using "$basein\6 Baseline 2014\Data\school\R4Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25) 
drop _merge
drop if t05==. & t06==. & t07==. & t08==. 
append using "$base_out\Consolidated\Teacher_YD.dta"
save "$base_out\Consolidated\Teacher_YD.dta", replace 
duplicates drop upid, force
merge 1:1 upid using "$basein\6 Baseline 2014\Data\school\R4Teacher_noPII.dta", keepus(at23) 
drop if _merge==2
drop if _merge==1
drop _merge
replace t23=. if t23==-99
replace at23=. if at23==-99
replace t23=. if t23==-97
replace at23=. if at23==-97
replace t23=at23 if t23==. & at23!=.
drop if t23==-99

 eststo clear
estpost tabstat t23 , by(t10) statistics(mean count) columns(statistics) listwise
esttab using "$exceltables\WageByExperience.csv", main(mean a2 ) aux(count) nostar unstack noobs nonote  nonumber label replace varlabels(`e(labels)')
eststo clear
estpost tabstat t23 , by(t16) statistics(mean count) columns(statistics) listwise
esttab using "$exceltables\WageByEdu.csv", main(mean a2 ) aux(count)nostar unstack noobs nonote  nonumber label replace varlabels(`e(labels)')
 eststo clear
estpost tabstat t23 , by(t17) statistics(mean count) columns(statistics) listwise
esttab using "$exceltables\WageByCert.csv", main(mean a2 ) aux(count)nostar unstack noobs nonote  nonumber label replace varlabels(`e(labels)')
 eststo clear
estpost tabstat t23 , by(tchsex ) statistics(mean count) columns(statistics) listwise
esttab using "$exceltables\WageByGender.csv", main(mean a2 ) aux(count)nostar unstack noobs nonote  nonumber label replace varlabels(`e(labels)')


gen tc=1 if t17==1
replace tc=0 if t17==2
ttest t23, by(tc)
