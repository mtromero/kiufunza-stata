clear mata
clear all
set matsize 1100
set seed 23498516
local it = 1000

*****THIS SAVES THE PASS RATE IN THE CONTROL GROUP IN THE HIGH-STAKES
use "$basein/4 Intervention/TwaEL_2013/TwaTestData.dta", clear
merge 1:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
drop if treatarm!=4
collapse (sum) NrTests_1- Passed_3m
foreach name in "k" "e" "m"{
	forval i=1/3  {
		gen PR_`i'_`name'=Passed_`i'`name'/NrTests_`i'
		if "`name'"=="k" label var PR_`i'_`name' "K S`i'"
		if "`name'"=="e" label var PR_`i'_`name' "E S`i'"
		if "`name'"=="m" label var PR_`i'_`name' "M S`i'"
	}
}

keep  PR_1_k- PR_3_m
gen id=_N
reshape long PR_@_k PR_@_e PR_@_m, i(id) j(Grade)
drop id
rename PR__k Z_kiswahili_Pass_T3
rename PR__e Z_kiingereza_Pass_T3
rename PR__m Z_hisabati_Pass_T3
tempfile temp1
save `temp1'


use "$basein/4 Intervention/TwaEL_2014/TwaTestData_stutested", clear
drop if treatment!=4
collapse  (mean) Kis_Pass Eng_Pass Math_Pass, by(Grade )
rename Kis_Pass Z_kiswahili_Pass_T7
rename Eng_Pass Z_kiingereza_Pass_T7
rename Math_Pass Z_hisabati_Pass_T7
merge 1:1  Grade  using `temp1'
drop if _merge!=3
drop _merge
save "$base_out/Consolidated/TempPassRate.dta", replace

*THIS CREATES A BAREBONE DATASET
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
capture drop Z_*_T1
capture drop Z_*_T5
capture drop Z_*_T4
capture drop Z_*_T8
capture drop Z_*_B_T7
capture drop Z_*_C_T7

drop if upid==""
bys upid: gen N=_N
drop if Z_hisabati_T7==. & N==2
keep treatarm treatment Z*_T* $treatmentlist DistID LagZ_kiswahili LagZ_hisabati LagZ_kiingereza upid SchoolID LagGrade
keep LagZ_hisabati- Z_hisabati_T7 SchoolID treatment treatarm  DistID LagGrade

save "$base_out/Consolidated/Student_Pcentile_temp.dta", replace

*THIS ACTUALLY DOES THE BOOTSTRAP
foreach year in T3 T7{
	foreach treat in COD Both CG{
		foreach subject in Z_kiswahili Z_kiingereza Z_hisabati {
			foreach grade in 1 2 3 {
				
				use "$base_out/Consolidated/TempPassRate.dta", clear
				drop if Grade!=`grade'
				qui sum `subject'_Pass_`year'
				scalar averagePass=1-r(mean)

				use "$base_out/Consolidated/Student_Pcentile_temp.dta", clear
				drop if (treatment!="`treat'" & treatment!="Control")
				if ("`year'"=="T3") replace LagGrade=LagGrade-1
				drop if LagGrade!=`grade'
				keep treatment SchoolID Lag`subject' `subject'_`year' DistID
				compress
				save "$base_out/temp/TempBoot_Pass", replace

				qui egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1
				mkmat kernel_range if kernel_range != .
				matrix diff = kernel_range
				matrix x = kernel_range


				quietly forvalues j = 1(1)`it' {
				use "$base_out/temp/TempBoot_Pass", clear
				bsample, strata(treatment DistID) cluster(SchoolID)


				bysort treatment: egen rank`subject' = rank(Lag`subject'), unique
				bysort treatment: egen max_rank`subject' = max(rank`subject')
				bysort treatment: gen LagPctile_`subject' = rank`subject'/max_rank`subject' 



				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1

				*regressing endline scores on percentile rankings
				lpoly `subject'_`year' LagPctile_`subject' if treatment=="Control" , gen(xcon pred_con) at (kernel_range) nograph
				lpoly `subject'_`year' LagPctile_`subject' if treatment=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph
					
					
				mkmat pred_tre if pred_tre != . 
				mkmat pred_con if pred_con != . 
				matrix diff = diff, pred_tre - pred_con

				}

				matrix diff = diff'



				*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
				svmat diff
				keep diff* 

				matrix conf_int = J(100, 2, 100)
				qui drop if _n == 1

				*sort each column (percentile) and saving 25th and 975th place in a matrix
				forvalues i = 1(1)100{
				sort diff`i'
				matrix conf_int[`i', 1] = diff`i'[0.025*`it']
				matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
				}

				*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
				use "$base_out/temp/TempBoot_Pass", clear
				  
				bysort treatment: egen rank`subject' = rank(Lag`subject'), unique
				bysort treatment: egen max_rank`subject' = max(rank`subject')
				bysort treatment: gen LagPctile_`subject' = rank`subject'/max_rank`subject' 


				egen kernel_range = fill(.01(.01)1)
				qui replace kernel_range = . if kernel_range>1


				lpoly `subject'_`year' LagPctile_`subject' if treatment=="Control" , gen(xcon pred_con) at (kernel_range) nograph
				lpoly `subject'_`year' LagPctile_`subject' if treatment=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph

				gen diff = pred_tre - pred_con

				*variables for confidence interval bands
				svmat conf_int


				if "`subject'"=="Z_kiswahili" local name3 Swahili
				else if "`subject'"=="Z_kiingereza"  local name3 English	
				else if "`subject'"=="Z_hisabati" local name3 Math
				else if "`subject'"=="Z_Index" local name3 "Index (PCA)"


				if "`grade'"=="1" local name4 1
				else if "`grade'"=="2"  local name4 2	
				else if "`grade'"=="3" local name4 3
				else if "`grade'"=="4" local name4 All

				if "`treat'"=="COD" local name5 "Incentives"
				else if "`treat'"=="Both"  local name5 "Combo"	
				else if "`treat'"=="CG" local name5 "Grants"


				save "$base_out/Consolidated/Lowess_Resid_Percentile_`subject'_`treat'_`year'_`grade'.dta", replace

				graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
				(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
				(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
				(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
				(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
				,yline(0, lcolor(gs10)) xtitle(Percentile of baseline score) ytitle(Test score) legend(order(1 2 3 4)) ///
				xline(`=scalar(averagePass)') ///
				 graphregion(color(white))
				 *title("`name5'-`name3'")
				graph export "$graphs/Lowess_Percentile_PassRate_`subject'_`treat'_`year'_`grade'.pdf", as(pdf) replace

			}
		}
	}
}

