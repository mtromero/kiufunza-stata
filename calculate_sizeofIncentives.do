**** I want to estimate the folllowing 3 numbers

**Average class size

**Average wage

**Payment if everyone passes

**Ratio of the two


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"

collapse (mean) t23 t24 t25 (count) N=t23, by(SchoolID)
sum t23
scalar compensation=r(mean)
gen TeacherSalaries= t23* N
sum TeacherSalaries
scalar compensationTotal=r(mean)*12

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
collapse (mean) StudentsGr*_T1 studentsTe~1_T3 studentsTe~2_T3 studentsTe~3_T3, by(SchoolID)

egen meanclass=rowmean(StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1)
egen meanclass2=rowmean(studentsTe~1_T3 studentsTe~2_T3 studentsTe~3_T3)
egen totalstudents=rowtotal( StudentsGr*_T1), missing


sum meanclass
scalar average=r(mean)
sum meanclass2
scalar average2=r(mean)
sum totalstudents
scalar totalstudents=r(mean)


di `=scalar(average)'*5000/`=scalar(compensation)'
di `=scalar(average2)'*5000/`=scalar(compensation)'


di `=scalar(compensationTotal)'/`=scalar(totalstudents)'
