**zero, figure out number of test takers
/*
use "$basein/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents.dta", clear
drop SchoolID_15_s Grade_15_s USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}
drop if DistrictID==11
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)
collapse (count) Denom=TestedEL, by( Grade SchoolID)
rename Grade GradeID_T8
tempfile temp_enroll
save `temp_enroll', replace
*/

**Lets do it a different way
use "$basein/6 Baseline 2014/Data/school/R4Grade_noPII.dta", clear
egen Denom_Survey=rowtotal( s184 s185), missing
rename R4GradeID GradeID_T8
keep GradeID_T8 SchoolID Denom_Survey
tempfile temp_enroll2
save `temp_enroll2', replace

/*
use "$basein/1 Baseline/School/GradesHead_noPII.dta", clear
egen Denom_Survey=rowtotal( s184 s185), missing
rename GradeID GradeID_T8
keep GradeID_T8 SchoolID Denom_Survey
tempfile temp_enroll2
save `temp_enroll2', replace
*/
*merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(  treatment treatarm DistID)


*First, lets figure out how much to cut
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear

collapse (count)  TestTakes=Z_hisabati_T8, by(GradeID_T8 SchoolID)
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(  treatment treatarm DistID)
drop if _merge!=3
drop _merge
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" 
label var TreatmentBoth "Combination"
drop if TestTakes==0
merge 1:1 GradeID_T8 SchoolID using `temp_enroll2'
drop if _merge!=3
drop _merge
*gen TestTakes_prop=TestTakes/Denom
gen TestTakes_prop2=TestTakes/Denom_Survey
replace TestTakes_prop2=1 if TestTakes_prop2>1 & TestTakes_prop2!=.



eststo clear
eststo test_takers: reg TestTakes_prop2 TreatmentCOD TreatmentBoth i.GradeID_T8 i.DistID
estadd ysumm
sum TestTakes_prop2 if TreatmentCOD==0 & TreatmentBoth==0
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		

matrix M=e(b)
local cutCOD=M[1,1]
local cutBoth=M[1,2]

local cutBoth2=_b[TreatmentBoth] - _b[TreatmentCOD]

label var TreatmentCOD "Incentives (\$\beta_2\$)"
label var TreatmentCG "Inputs (\$\beta_1\$)"
label var TreatmentBoth "Combination (\$\beta_3\$)"

esttab  using "$latexcodesfinals/RegTestScores_highstakes_TestTakers_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc %9.2fc a2 a2) labels("N. of obs." "Mean control group" "\$\alpha_3 = \alpha_2-\alpha_1\$" "p-value(\$\alpha_3=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if Z_hisabati_T8==.
pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist  Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}



eststo clear
foreach var in $AggregateDep_Karthik{
	eststo m_`var': reg `var'_T8 ${treatmentlist_int} i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
	estadd ysumm
	test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
	preserve
		gsort SchoolID GradeID_T8 -`var'_T8
		by SchoolID GradeID_T8: gen conteo=_n
		by SchoolID GradeID_T8: gen denom=_N
		gen prop_conteo=conteo/denom
		drop if prop_conteo<=`cutCOD' & treatment=="COD"
		drop if prop_conteo<=`cutBoth' & treatment=="Both"
		reg `var'_T8 ${treatmentlist_int} i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID)
		lincom _b[TreatmentCOD]
		scalar CI_COD_1=r(estimate)+1.96*r(se)
		scalar CI_COD_2=r(estimate)-1.96*r(se)
		lincom _b[TreatmentBoth]
		scalar CI_Both_1=r(estimate)+1.96*r(se)
		scalar CI_Both_2=r(estimate)-1.96*r(se)
		lincom (_b[TreatmentBoth]-_b[TreatmentCOD] )
		scalar CI_Interact_1=r(estimate)+1.96*r(se)
		scalar CI_Interact_2=r(estimate)-1.96*r(se)
		
	restore
	preserve
		gsort SchoolID GradeID_T8 `var'_T8
		by SchoolID GradeID_T8: gen conteo=_n
		by SchoolID GradeID_T8: gen denom=_N
		gen prop_conteo=conteo/denom
		drop if prop_conteo<=`cutCOD' & treatment=="COD"
		drop if prop_conteo<=`cutBoth' &  treatment=="Both"
		reg `var'_T8 ${treatmentlist_int} i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID)
		lincom _b[TreatmentCOD]
		scalar CI_COD_3=r(estimate)+1.96*r(se)
		scalar CI_COD_4=r(estimate)-1.96*r(se)
		lincom _b[TreatmentBoth]
		scalar CI_Both_3=r(estimate)+1.96*r(se)
		scalar CI_Both_4=r(estimate)-1.96*r(se)
		lincom (_b[TreatmentBoth]-_b[TreatmentCOD] )
		scalar CI_Interact_3=r(estimate)+1.96*r(se)
		scalar CI_Interact_4=r(estimate)-1.96*r(se)
	restore
	
	
	preserve
		gsort SchoolID GradeID_T8 -`var'_T8
		by SchoolID GradeID_T8: gen conteo=_n
		by SchoolID GradeID_T8: gen denom=_N
		gen prop_conteo=conteo/denom
		drop if prop_conteo<=`cutBoth2' &  treatment=="Both"
		reg `var'_T8 TreatmentBoth i.GradeID_T8  i.DistID  $schoolcontrol if TreatmentCOD==1  | TreatmentBoth==1, vce(cluster SchoolID)

		lincom (_b[TreatmentBoth])
		scalar CI_Interact_5=r(estimate)+1.96*r(se)
		scalar CI_Interact_6=r(estimate)-1.96*r(se)
	restore
	
	
	preserve
		gsort SchoolID GradeID_T8 `var'_T8
		by SchoolID GradeID_T8: gen conteo=_n
		by SchoolID GradeID_T8: gen denom=_N
		gen prop_conteo=conteo/denom
		drop if prop_conteo<=`cutBoth2' &  treatment=="Both"
		reg `var'_T8 TreatmentBoth i.GradeID_T8  i.DistID  $schoolcontrol if TreatmentCOD==1  | TreatmentBoth==1, vce(cluster SchoolID)

		lincom (_b[TreatmentBoth])
		scalar CI_Interact_7=r(estimate)+1.96*r(se)
		scalar CI_Interact_8=r(estimate)-1.96*r(se)
	restore
	
	
	estadd scalar Lee_COD_1=min(`=scalar(CI_COD_1)',`=scalar(CI_COD_2)',`=scalar(CI_COD_3)',`=scalar(CI_COD_4)'): m_`var'
	estadd scalar Lee_COD_2=max(`=scalar(CI_COD_1)',`=scalar(CI_COD_2)',`=scalar(CI_COD_3)',`=scalar(CI_COD_4)'): m_`var'
	estadd scalar Lee_Both_1=min(`=scalar(CI_Both_1)',`=scalar(CI_Both_2)',`=scalar(CI_Both_3)',`=scalar(CI_Both_4)'): m_`var'
	estadd scalar Lee_Both_2=max(`=scalar(CI_Both_1)',`=scalar(CI_Both_2)',`=scalar(CI_Both_3)',`=scalar(CI_Both_4)'): m_`var'
	estadd scalar Lee_Interact_1=min(`=scalar(CI_Interact_1)',`=scalar(CI_Interact_2)',`=scalar(CI_Interact_3)',`=scalar(CI_Interact_4)'): m_`var'
	estadd scalar Lee_Interact_2=max(`=scalar(CI_Interact_1)',`=scalar(CI_Interact_2)',`=scalar(CI_Interact_3)',`=scalar(CI_Interact_4)'): m_`var'
	
	estadd scalar Lee_Interact_3=min(`=scalar(CI_Interact_5)',`=scalar(CI_Interact_6)',`=scalar(CI_Interact_7)',`=scalar(CI_Interact_8)'): m_`var'
	estadd scalar Lee_Interact_4=max(`=scalar(CI_Interact_5)',`=scalar(CI_Interact_6)',`=scalar(CI_Interact_7)',`=scalar(CI_Interact_8)'): m_`var'
}
label var TreatmentCOD "Incentives (\$\beta_2\$)"
label var TreatmentCG "Inputs (\$\beta_1\$)"
label var TreatmentBoth "Combo (\$\beta_3\$)"

esttab  using "$latexcodesfinals/RegTestScores_highstakes_Lee_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\beta_4 = \beta_3-\beta_2\$" "p-value (\$H_0:\beta_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_Lee_two_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_COD_1 Lee_COD_2, fmt(a2 a2) labels("Lower 95\% CI (\$\beta_2\$)" "Higher 95\% CI (\$\beta_2\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_Lee_three_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Both_1 Lee_Both_2, fmt(a2 a2) labels("Lower 95\% CI (\$\beta_3\$)" "Higher 95\% CI (\$\beta_3\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_Lee_four_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Interact_1 Lee_Interact_2, fmt(a2 a2) labels("Lower 95\% CI (\$\beta_4\$)" "Higher 95\% CI (\$\beta_4\$)" )) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_Lee_five_prop.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines nogaps /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
drop(*) stats(Lee_Interact_3 Lee_Interact_4, fmt(a2 a2) labels("Lower 95\% CI (\$\beta_4'\$)" "Higher 95\% CI (\$\beta_4'\$)" )) ///
nonotes substitute(\_ _)
