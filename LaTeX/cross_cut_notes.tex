%\input{tcilatex}
\documentclass{article}
\usepackage{threeparttable}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{footnote}
\usepackage{fullpage}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tabularx}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}
%\usepackage{ctable}
\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}
\makeatletter\let\expandableinput\@@input\makeatother
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\doublespacing
\usepackage{etoolbox}
\AtBeginEnvironment{table}{\singlespacing}
\AtBeginEnvironment{threeparttable}{\singlespacing}
\newenvironment{figurenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize}{\end{minipage}}
%\newenvironment{tablenotes}[1][Note]{\begin{minipage}[t]{\linewidth} \footnotesize }{\end{minipage}}
\title{Note on cross-cut designs and critical value corrections}
\begin{document}
\section{Introduction}
Cross-cut designs are popular among developing economist. These designs are characterized by several treatments randomly assigned in which each randomization is independent from each other. Since data collection, as opposed to the treatment itself, is often the most expensive portion of a randomize control trial, cross-cut designs allow researchers to conduct several experiments with limited funds. However, ``there ain't no such thing as a free lunch'', and cross-cut designs come at a cost. As \citeA{Kremer2003} puts it: ``Conducting a series of evaluations in the same area allows substantial cost savings. Once staff is trained, they can work on multiple projects. Since data collection is the most costly element of these evaluations, cross-cutting the sample reduces costs dramatically.... This tactic can be problematic, however, if there are significant interactions between programs''.

Specifically, suppose you have a cross-cut design with two interventions $T_1$ and $T_2$, and let us assume the following Data Generating Process (DGP):

\begin{equation}\label{eq:inter}
Y=\beta_0+\beta_1 T_1+\beta_2 T_2+\beta_3 T_1 \times T_2+\varepsilon,
\end{equation}

\noindent where $\varepsilon$ is the error term which satisfies $\mathbb{E}(\varepsilon)=0$, $V(\varepsilon)<\infty$, $\mathbb{E}(\varepsilon T_1)=0$, and $\mathbb{E}(\varepsilon T_2)=0$. Aditionally, assume that the randomizations of $T_1$ and $T_2$ were independent from each other, and therefore $\mathbb{E}(T_2 T_1)=0$.

If $\beta_3=0$ and that is known to the researcher, then he can can consistently estimate $\beta_1$ and $\beta_2$. In other words, if the researcher estimates the following equation

\begin{equation}\label{eq:sep}
Y=\beta_0+\beta_1 T_1+\beta_2 T_2+\underbrace{U}_{\beta_3 T_1 \times T_2+\varepsilon}
\end{equation}

\noindent since $\mathbb{E} T_1U=0$ then the estimator of $\beta_1$ from OLS is consistent. Similarly, the researcher can estimate $\beta_2$ consistently. However, if $\beta_3 \neq 0$ then $\mathbb{E} T_1U \neq 0$ and OLS no longer yields consistent estimators of $\beta_1$ and $\beta_2$. These are the sort of problems that \citeA{Kremer2003} refers to.

On the other hand, the researcher might be intrinsically interested in $\beta_3$ and would therefore like to get a consistent estimator of it. This, however, comes at a ``power'' cost. Intuitively, if one estimates equation \ref{eq:inter}, the identifying variation for $\beta_1$ is coming from individuals that get $T_1$ but not $T_2$. If one estimates \ref{eq:sep} then the identifying variation comes from all individuals that get $T_1$ (regardless of whether they get $T_2$). 

Unfortunately, one cannot simply do a two-step procedure to determine whether one should estimate equation \ref{eq:inter} or \ref{eq:sep}. If a researcher estimates equation \ref{eq:inter}, and depending on whether he rejects the null hypothesis that $\beta_3=0$ decides to keep his estimates of $\beta_1$, $\beta_2$, and $\beta_3$ or to estimate equation \ref{eq:sep} to get new estimates of $\beta_1$ and $\beta_2$ (and assume $\beta_3=0$), then the finite-sample and asymptotic distribution of these estimators are complicated and discontinuous \cite{Leeb2005,Leeb2006,Leeb2008}, making the usual t-statistics and p-values highly misleading.

Many cross-cut designed RCTs are not adequately powered to test for interactions. This leaves researches in somewhat of a cross-road. They can either assume that $\beta_3=0$ and estimate equation \ref{eq:sep} to \textit{hopefully} get consistent estimates of $\beta_1$ and $\beta_2$, or they can estimate equation \ref{eq:inter} and lose some of the identifying variation for $\beta_1$ and $\beta_2$ even if $\beta_3$ is truly zero.  A third alternative is to do model selection and use Bonferroni-style correction to adjust critical values \cite{McCloskey2012}. Intuitively, if one is certain that $\beta_3$ is within a certain range, one can study the behavior of $\hat{\beta_1}$  (under the null distribution) for each possible value of $\beta_3$ and keep the highest critical value (to be conservative). One can take this argument a step further, and do this for an asymptotically valid confidence set for $\beta_3$ (taken from the first step of the model selection), and adjust the critical values accordingly \cite{McCloskey2012}. Intuitively, the smaller the confidence set on $\beta_3$, the smaller the adjustment on the critical values. An important caveat, is that this is not a standard error adjustment, as the asymptotic distribution of the estimators is discontinuous and it no longer resembles a normal distribution as $n$ approaches infinity. Therefore, \citeA{McCloskey2012} Bonferroni-style correction leads to new critical values, not to new standard errors.

To illustrate the effect of model selection on critical values we apply \citeA{McCloskey2012}'s Bonferroni-style correction to our own data. To do so, we re-estimate our main results as if we had done model selection. In other words, we estimate equation \ref{eq:inter} where $T_1$ is CG and $T_2$ is COD, and if we cannot reject the null hypothesis that $\beta_3=0$, then we estimate equation \ref{eq:sep}. We adjust our critical values for $\beta_1$ and $\beta_2$ in each case. Table \ref{tab:interactions} shows the results from estimating equation \ref{eq:inter}, which is isomorphic to Table \ref{tab:test_scores}, but explicitly estimates the interaction term ($\beta_3$). To do inference on the null that $\beta=0$, and in order to apply \citeA{McCloskey2012}'s Bonferroni-style correction, we will use as a test statistic the coefficient itself ($\hat{\beta}$). The critical value, without any correction, with significance at the 10\% level is $\approx  s.e. (\hat{\beta})z_{1-\alpha/2}$, where $s.e. (\hat{\beta})$ is the standard error of $\hat{\beta}$ and $z_{1-\alpha/2}$ is the $1-\alpha/2$ quantile of the normal distribution. Notice that is isomorphic to using $t=\frac{\hat{\beta}}{s.e.(\hat{\beta})}$ as the t-statistics and $z_{1-\alpha/2}$ as the critical value. The adjusted critical values depend on whether one performs consistent or conservative model selection. When performing conservative model selection the selection threshold is fix and does not depend on sample size (e.g., $(\hat{\beta})z_{1-\alpha/2}$). In consistent model selection the threshold grows as the sample size increases (e.g., the Hannan-Quinn information criterion $\sqrt{\ln(n)}$). The former is more common in applied work, but leads to lower powered post-selection test's in general \cite{McCloskey2012}. 

\begin{center}
\begin{table}[H]
\caption{Effect on test scores \label{tab:interactions}}
\begin{threeparttable}
\centering
\resizebox{\textwidth}{!}{
\expandableinput RegInteractionsExplictly.tex
}
\begin{tablenotes}
\footnotesize
\item Critical values (for a significance level of 10\%) in parenthesis. Inference is based on clustered standard errors (at the school level). \sym{*} significant at the 10\% level.
\end{tablenotes}
\end{threeparttable}
\end{table}
\end{center}

\subsubsection{Conservative model selection}

Table \ref{tab:corrections_conserv} shows the results from applying conservative model selection (using $s.e.(\hat{\beta})1.64$ as the threshold). The columns in Table \ref{tab:interactions} where $\hat{\beta_3}<s.e.(\hat{\beta})1.64$ are re-estimated using equation \ref{eq:sep}, and $\beta_3$ is assumed to be zero. In parenthesis is the critical value without correction ($s.e. (\hat{\beta})1.64$), in square parenthesis the adjusted critical value from \citeA{McCloskey2012}'s Bonferroni-style correction after conservative model selection. Notice that without adjustment the effect of COD is now significant (at the 10\%) level for Swahili (Yr1), Index (Yr1) and English (Yr1 and Yr2). Once we adjust critical values  English (Yr2) is no longer significant. According to these results, COD has an effect of its own on Swahili (Yr1), Index (Yr1) and English (Yr1), even after adjust critical values. Notice that the adjusted critical values are, in general, larger than the critical values from those in Table \ref{tab:interactions}. Also, the COD effect for Swahili (Yr1) and Index (Yr1) is only marginally significant at the 10\% level (and would not be significant, either before or after adjustment at the 5\% level).
\begin{center}
\begin{table}[H]
\caption{Effect on test scores \label{tab:corrections_conserv}}
\begin{threeparttable}
\centering
\resizebox{\textwidth}{!}{
{
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\begin{tabular}{l*{8}{c}}
\toprule
                    &\multicolumn{4}{c}{Year 1}                                                             &\multicolumn{4}{c}{Year 2}                                                             \\\cmidrule(lr){2-5}\cmidrule(lr){6-9}
                    &\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Index}&\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Index}\\
\midrule
\expandableinput Adjusted_conserv.tex
\midrule
N. of obs.          &        9141         &        9141         &        9141         &        9141         &        9436         &        9436         &        9436         &        9436         \\              
\bottomrule
\end{tabular}
}
}
\begin{tablenotes}
\footnotesize
\item Non-adjusted critical values (for a significance level of 10\%) in parenthesis. Adjusted critical values (for a significance level of 10\%) in square parenthesis. Inference is based on clustered standard errors (at the school level). \sym{*} significant at the 10\% level.
\end{tablenotes}
\end{threeparttable}
\end{table}
\end{center}

\subsubsection{Consistent model selection}

Table \ref{tab:corrections_consistent} shows the results from applying consistent model selection (using $\sqrt{\ln(n)}$ as the threshold). The columns in table \ref{tab:interactions} where $\hat{\beta_3}<\sqrt{\ln(n)}$ are re-estimated using equation \ref{eq:sep}, and $\beta_3$ is assumed to be zero. In parenthesis is the critical value without correction ($s.e.(\hat{\beta})1.64$), in square parenthesis the adjusted critical value from \citeA{McCloskey2012}'s Bonferroni-style correction after consistent model selection. Notice that without adjustment the effect of COD is now significant for every subject (in both years), and that the effect of CG is significant for Swahili (Yr2) and Index (Yr2). However, after the adjustment only the effect of COD for Swahili (Yr1), English (Yr1), and Math (Yr2) is significant at the 10\% level. 
\begin{center}
\begin{table}[H]
\caption{Effect on test scores \label{tab:corrections_consistent}}
\begin{threeparttable}
\centering
\resizebox{\textwidth}{!}{
{
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\begin{tabular}{l*{8}{c}}
\toprule
                    &\multicolumn{4}{c}{Year 1}                                                             &\multicolumn{4}{c}{Year 2}                                                             \\\cmidrule(lr){2-5}\cmidrule(lr){6-9}
                    &\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Index}&\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Index}\\
\midrule
\expandableinput Adjusted_consistent.tex
\midrule
N. of obs.          &        9141         &        9141         &        9141         &        9141         &        9436         &        9436         &        9436         &        9436         \\              
\bottomrule
\end{tabular}
}
}
\begin{tablenotes}
\footnotesize
\item Non-adjusted critical values (for a significance level of 10\%) in parenthesis. Adjusted critical values (for a significance level of 10\%) in square parenthesis. Inference is based on clustered standard errors (at the school level). \sym{*} significant at the 10\% level.
\end{tablenotes}
\end{threeparttable}
\end{table}
\end{center}
\bibliographystyle{apacite}
\bibliography{TanBib}
\end{document}
