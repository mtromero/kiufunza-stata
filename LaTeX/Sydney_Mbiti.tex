\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}
\usepackage{apacite}
\usepackage{xcolor,colortbl}
\makeatletter\let\expandableinput\@@input\makeatother

\newcommand{\foo}{\hspace{-2.3pt}$\bullet$ \hspace{5pt}}

\mode<presentation>{}
\usetheme{Warsaw}                  % Option: 2.0 and 1.5 spacing package.
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\let\oldtabular\tabular
\renewcommand{\tabular}{\tiny\oldtabular}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Inputs, Incentives, and Complementarities]{Inputs, Incentives, and Complementarities in Primary Education: Experimental Evidence from Tanzania }

\author[Mbiti et. al.]{Isaac Mbiti (UVA)  \\ Karthik Muralidharan (UCSD) \\ Mauricio Romero (UCSD)  \\ Youdi Schipper (Twaweza) \\ Constantine Manda (Twaweza) \\ Rakesh Rajani (Twaweza) }


\normalsize
\date{}
\begin{document}
\frame{\titlepage}







\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}


\section{Introduction}


\begin{frame}[plain]{Background/Motivation}
\begin{itemize}
\item Achieving universal primary education in developing countries is a top priority for global education policy (MDG)
\item However, while developing countries have greatly improved school enrollment, learning outcomes are still poor \pause
\item In Tanzania, the net enrollment rate in primary school rose from 53\% in 2000 to close to 90\% in 2012
\item But less than one third of grade 3 students could pass basic tests of second grade numeracy or literacy (Uwezo, 2013) \pause
\item Inadequate resources and poor teacher motivation are widely considered to be two key constraints to improving school quality in developing countries (including Tanzania)
\begin{itemize}
\item Almost 25\% teachers absent from school and 50\% of teachers absent from classroom (World Bank, 2012)
\item 3\% of schools have sufficient infrastructure Oon average 5 students per math textbook (World Bank, 2012)
\end{itemize}

\end{itemize}
\end{frame}

\begin{frame}[plain]{This paper}
\begin{itemize}
\item We study the effectiveness of policies aiming to alleviate both of these constraints with a large-scale RCT conducted across a representative sample of 350 schools in Tanzania \pause

\item Interventions studied include:
\begin{itemize}
\item A block capitation grant (CG) to schools
\begin{itemize}
\item Large grant (doubled per-child non-teacher spending)
\item Subject to same guidelines as national CG program \pause
\end{itemize}

\item A Teacher performance pay program referred to as "Cash on Delivery" (CoD)
\begin{itemize}
\item Paid to each teacher based on the number of students passing a basic test in each of 3 subjects (Math, Swahili, English)
\item NOT optimally designed (discuss reasons) \pause
\end{itemize}

\item A "Combination" (Combo) treatment arm that provided both interventions
\begin{itemize}
\item Key innovation over existing work is that the design was explicitly powered to test for complementarities
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[plain]{Summary of Results}
\begin{itemize}

\item We find that
\begin{itemize}
\item Increasing inputs (using capitation grants) has no impact on test scores \pause
\item This particular teacher incentive program had no impact on test scores on average (but some suggestive evidence of positive effects on students near the passing threshold) \pause
\item The combination of the two programs had a positive and significant impact on test scores
\item The combination is more than the sums of the parts (i.e., there are complementarities between the two) \pause
\end{itemize}
\item If complementarities matter, cross-cutting designs (which are widely used) could yield biased estimates if they ignore interactions
\end{itemize}
\end{frame}


\begin{frame}[plain]{Literatures we contribute to}\
\small{
\begin{itemize}
\item\textbf{School Resources:} Glewwe, Kremer and Moulin (2009); Das, Dercon, Habyarimana, Krishnan, Muralidharan, and Sundararaman (2013); Sabarwal, Evans, and Marshak (2014); Blimpo and Evans (2011) ; Berry, Karlan, Pradhan, and Ratan (2012)
\item \textbf{Teacher Incentives:} Glewwe, Holla, and Kremer (2009); Muralidharan and Sundararaman (2011); Muralidharan (2012); Duflo, Hanna, and Ryan (2012)
\item \textbf{Estimation of Education Production Functions:} Todd and Wolpin (2003); Kremer (2003)
\item \textbf{Complementarities in education production:} Muralidharan and Sundararaman (2011); Muralidharan 2012; No experimental study to date
\end{itemize}
}
\end{frame}



\section{Design and data}

%\begin{frame}[plain]{Design}
%\begin{figure}[H]
%\centering
%\caption{Relative size of Tanzania}
%\includegraphics[width=0.7\textwidth]{OverlapCalifornia.pdf}
%\end{figure}
%\end{frame}

\begin{frame}[plain]{Design}
\begin{figure}[H]
\centering
\caption{Districts in Tanzania from which schools are selected\label{fig:mapa}}
\includegraphics[width=0.7\textwidth]{map_rct_1.pdf}
\end{figure}
\end{frame}




\begin{frame}[plain]{Design}
Implemented across a representative sample of 350 schools across 10 districts in Tanzania.

\begin{table}[h]
\resizebox{0.7\textwidth}{!}{%
\begin{tabular}{ll|l|l|l}
\cline{3-4}
                                                   & \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{CoD}} &  \\ \cline{3-4}
                                                   &                       & \textit{Yes}     & \textit{No}    &  \\ \cline{1-4}
\multicolumn{1}{|l|}{\multirow{2}{*}{\textbf{CG}}} & \textit{Yes}          & 70               & 70             &  \\ \cline{2-4}
\multicolumn{1}{|l|}{}                             & \textit{No}           & 70               & 140            &  \\ \cline{1-4}
\end{tabular}
}
\end{table}
\end{frame}



\begin{frame}[plain]{Capitation grant (CG)}
According to policy, the government is to disburse a capitation grant of TZS 10,000
per primary school pupil per year to school
\begin{itemize}
\item school committees may spend the funds as they please in accordance with grant policy
\item 85 Billion TZS ($\sim$ \$40M ) per year
\item Funds transferred to district councils, and not directly to schools
\item 37\% leakage rate (World Bank, 2012)
\item Govt. budget allocation much smaller than the promised 10000
\item Lower budgetary alllocation + leakage = schools are resource constrained
\begin{itemize}
\item 86\% of schools do not have electricity
\item 60\% of schools do not have a single classroom with a desk/chair for the teacher
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[plain]{Capitation grant (CG)}
\begin{itemize}
\item Transfer capitation grants (CG) to reach schools in full (as per current government plan at TZS 10,000 per pupil/per year)
\item The grant amount is about 3 times the mean pre-treatment school expenditure per student (excluding teachers salaries)
\item Fund can be used to: buy books, booklets, small repairs, etc. but not for salaries or major construction
\item Accountability as per existing government policy %and mechanisms (inform communities on how funds are spent using notice boards)
\item School leadership and communities informed on the intervention
\end{itemize}
\end{frame}

\begin{frame}[plain]{Cash on Delivery (CoD)}
\begin{itemize}
\item For every child in Grades 1, 2 and 3 who passes the proven Uwezo literacy (English and Kiswahili) and numeracy (Mathematics) assessment at the end of the school year, the child's teacher will be paid TZS 5,000 per subject the child passes (or up to TZS. 15,000 per each child who is able to pass all three literacy and numeracy tests)
\item Head teachers will be paid TZS 1,000 per subject the child passes (or up to TZS. 3,000 per each child who is able to pass all three literacy and numeracy tests)
\item Rewards absolute levels of learning, not gains
\item Simple to implement and scale up
\item Initially, low levels of learning (82\% below the passing threshold for Swahili, 97\% for English and 93\% for Math)
\end{itemize}
\end{frame}


\begin{frame}[plain]{Combination (CG+CoD)}
\begin{itemize}
\item Give schools both a capitation grant (CG) and the Cash on Delivery (CoD).
\item Adequately powered to test for complementarities
\end{itemize}
\end{frame}

%\begin{frame}[plain]{Combination (CG+CoD)}
%``Conducting a series of evaluations in the same area allows substantial cost savings. Once staff are trained, they can work on multiple projects. Since data collection is the most costly element of these evaluations, cross-cutting the sample reduces costs dramatically.... This tactic can be problematic,
%however, if there are significant interactions between programs'' (Kremer AER, 2003)
%\begin{itemize}
%\item 4 different RCT fidning no effects to extra schools resources, but all with different interpretations
%\begin{itemize}
%\item
%\end{itemize}
%\item Partial derivatives are hard to interpret
%
%$$Y=\beta_0+\beta_1 T_1+\beta_2 T_2+\beta_3 T_1 \times T_2+\varepsilon$$
%$$\frac{\partial Y}{\partial T_1}=\beta_1+\beta_3 T_2$$
%
%We usually ignore $\beta_3$
%\end{itemize}
%\end{frame}



\begin{frame}[plain]{Timeline}
\begin{figure}[H]
\centering
\includegraphics[width=1\textwidth]{timeline.pdf}
\end{figure}
\end{frame}


\begin{frame}[plain]{Data}
\begin{itemize}
\item School data: facilities, expenditure, enrollment, etc.
\item Teacher data: socio-demographic characteristics, qualifications, experience, time use, etc.
\item Student data: We test 10 students from each focal grade (grades 1, 2 and 3), in all three focal subjects (Math, English and Swahili) and in Science.
\begin{itemize}
\item This is a low stakes exam different from the ``intervention'' exam.
\end{itemize}
\item Household data (for 1/3 of students): household and dwelling socio-demographic characteristics, household expenditure in education, parents engagement in children's education, etc.
\end{itemize}
\end{frame}



\begin{frame}[plain]{Design validity}
\begin{itemize}
\item No difference in baseline characteristics by  \hyperlink{balance_student}{\beamergotobutton{student}}, \hyperlink{balanceschool_house}{\beamergotobutton{household}}, \hyperlink{balanceschool_house}{\beamergotobutton{school}}, or \hyperlink{balance_teacher}{\beamergotobutton{teachers}}
\item No differential attrition in test-taking sample \hyperlink{attrition}{\beamergotobutton{attrition}}
\end{itemize}
\end{frame}






\section{Results - Expenditure}


\begin{frame}[plain]{How are schools spending the money?}
\begin{center}
\begin{tabular}{l*{1}{ccc}}
\toprule
                    &        Combo&          CG&        Diff         \\
\textbf{Year 1} & &  &    \\
\expandableinput SummaryTwawezaExp_T3.tex
\midrule
\textbf{Year 2} & &  &    \\
\expandableinput SummaryTwawezaExp_T7.tex
\midrule
\bottomrule
\end{tabular}
\end{center}

 \hyperlink{catexp}{\beamergotobutton{Expenditure categories}}
\end{frame}


\begin{frame}[plain]{Expenditure and funding (Year 1 + Year 2)}
\begin{center}
\resizebox{\textwidth}{!}{\begin{tabular}{l*{3}{c}}
\toprule
                    & School Expenditure & Other funding & Household expenditure\\
\expandableinput Expenditure_Substitution_HH_TT.tex

\bottomrule
\end{tabular}}
\end{center}
 \hyperlink{tot_exp}{\beamergotobutton{Total Expenditure}}
 \hyperlink{tot_sub}{\beamergotobutton{Other sources of funding}}
 \hyperlink{tot_HHexp}{\beamergotobutton{HH Expenditure}}
\end{frame}


\begin{frame}[plain]{Text book expenditure}
\begin{center}
%\resizebox{\textwidth}{!}{
\begin{tabular}{l*{2}{c}}
\toprule
                    & Year 1 & Year 2\\
\expandableinput School_textbook_extra2_T3T7.tex

\bottomrule
\end{tabular}
%}
\end{center}
\end{frame}






\section{Results - Test scores}

\begin{frame}[plain,label=reg1]{Test Scores}
\begin{center}
\resizebox{\textwidth}{!}{
\expandableinput RegKarthik.tex
}
\end{center}
\end{frame}



\begin{frame}[plain]{Other grades and subjects}
\begin{center}
\resizebox{\textwidth}{!}{
\expandableinput RegOtherGradesSubjects.tex
}
\end{center}
\end{frame}

\begin{frame}[plain]{High-stakes vs. Low-stakes}
\begin{center}
%\resizebox{\textwidth}{!}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{low-stakes}&\multicolumn{1}{c}{high-stakes}&\multicolumn{1}{c}{Diff}\\
\midrule                 
\multicolumn{4}{l}{\textbf{Panel A: Math}}\\
\expandableinput RegZ_hisabati_InvVsEDI_Comp.tex
\midrule
\multicolumn{4}{l}{\textbf{Panel B: Swahili}}\\
\expandableinput RegZ_kiswahili_InvVsEDI_Comp.tex
\midrule
\multicolumn{4}{l}{\textbf{Panel C: English}}\\
\expandableinput RegZ_kiingereza_InvVsEDI_Comp.tex
\bottomrule
\end{tabular}
%}
\end{center}
\end{frame}

\begin{frame}[plain]{On cross-cut design}
``Conducting a series of evaluations in the same area allows substantial cost savings. Once staff are trained, they can work on multiple projects. Since data collection is the most costly element of these evaluations, cross-cutting the sample reduces costs dramatically.... This tactic can be problematic,
however, if there are significant interactions between programs'' (Kremer AER, 2003)

\vfill
Early wave of experiments
\begin{itemize}
\item Low budgets
\item Cross-cut designs were very popular
\item Interaction effects were considered a second order issue
\end{itemize}

\end{frame}


%\begin{frame}[plain]{Extra teacher project (ETP) in Kenya}
%\includegraphics[width=\textwidth]{crosscutETP.pdf}
%\end{frame}

\begin{frame}[plain]{Effect of ignoring interaction effects: yr2 effects}
\begin{center}
\resizebox{!}{0.4\textwidth}{
\begin{tabular}{l*{2}{c}}
\toprule
                    &\multicolumn{1}{c}{(1)}&\multicolumn{1}{c}{(2)}\\
\multicolumn{3}{l}{\textbf{Panel A: Math}} \\
\expandableinput IgnoreInteract_T7_Z_hisabati.tex 
\midrule
\multicolumn{3}{l}{\textbf{Panel B: Swahili}} \\
\expandableinput IgnoreInteract_T7_Z_kiswahili.tex 
\midrule
\multicolumn{3}{l}{\textbf{Panel C: English}} \\
\expandableinput IgnoreInteract_T7_Z_kiingereza.tex 

\bottomrule
\multicolumn{3}{l}{\footnotesize \specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{frame}


\section{Mechanisms}

\begin{frame}[plain]{Mechanisms}
\begin{itemize}
\item No systematic evidence of more teacher effort (time teaching, time spend at schools, etc.)  \hyperlink{tchtime}{\beamergotobutton{Time usage}}
\item No systematic evidence of change in pedagogy (tests, homework, tutoring, etc.)  \hyperlink{pedag}{\beamergotobutton{Pedagogy}}
\end{itemize}
\end{frame}






\section{Heterogeneity}

\begin{frame}[plain]{Heterogeneity}
\begin{itemize}
\item No heterogeneity by baseline characteristics  \hyperlink{heter}{\beamergotobutton{Student characteristics}}
\item No heterogeneity by baseline test score: parametric and non-parametric (\hyperlink{heter_math}{\beamergotobutton{Math}}, \hyperlink{heter_swa}{\beamergotobutton{Swahili}}, \hyperlink{heter_eng}{\beamergotobutton{English}})
\end{itemize}
\end{frame}



\begin{frame}[plain]{Non parametric analysis }
\begin{itemize}
\item No differential impact for CG (not surprising - zero on average)
\item Suggestive evidence of larger effects in incentive treatments closer to the passing threshold (stronger for Combo)
\end{itemize}
\end{frame}

\begin{frame}[plain,label=heter_math]{Lowess Math}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_hisabati_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_hisabati_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Math test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}



\begin{frame}[plain,label=heter_swa]{Lowess Swahili}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiswahili_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiswahili_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Swahili test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}




\begin{frame}[plain,label=heter_eng]{Lowess English}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiingereza_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiingereza_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on English test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}





\begin{frame}[plain]{Conclusions}
\begin{itemize}
\item No impact on test scores of additional school grants
\item Insignificant positive impacts of teacher bonuses on average, but find suggestive evidence of threshold effects
\item Test scores in schools with both programs were significantly higher than those in control schools, and we find strong evidence of complementary between inputs and incentives
\item Effectiveness of teacher performance pay programs are likely to crucially depend on how they are designed
\item Cross-cutting experimental designs that ignore interactions may yield biased estimates of the main treatment effects of interest
\end{itemize}
\end{frame}


\appendix


\begin{frame}[plain,label=catexp]{Categories}
\begin{itemize}
\item Admin: administrative cost (including staff wages), rent and utilities, and general maintenance and repairs
\item  Student: food, scholarships and utilities (notebooks, pens, etc.)
\item Teaching aid: classroom furnishings, textbooks, maps, charts, blackboards, practice exams, etc.
\item Teachers: salaries, bonuses and teacher training
\end{itemize}
\end{frame}



\begin{frame}[plain,label=pedag]{Focal years tutoring, tests and remedial}
\begin{center}
\resizebox{0.6\textwidth}{!}{\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{Remedial}&\multicolumn{1}{c}{Tests}&\multicolumn{1}{c}{Tutoring}\\
\midrule
\textbf{Panel A: Year 1} & & &    \\
\expandableinput RegTeacher8_T3.tex
\midrule
\textbf{Panel B: Year 2} & & &    \\
\expandableinput RegTeacher8_T7.tex

\bottomrule
\end{tabular}}
\end{center}
\end{frame}




\begin{frame}[plain,label=tchtime]{Time Use}
\begin{center}
\resizebox{\textwidth}{!}{\begin{tabular}{l*{5}{c}}
\toprule
                    &\multicolumn{1}{c}{time preparing class}&\multicolumn{1}{c}{time teaching}&\multicolumn{1}{c}{time extra classes}&\multicolumn{1}{c}{time socializing colleague}&\multicolumn{1}{c}{Time (hrs) spend at school}\\
\midrule
\textbf{Panel A: Year 1} & & & & &   \\
\expandableinput RegTeacher2_T3.tex
\midrule
\textbf{Panel B: Year 2} & & & & &    \\
\expandableinput RegTeacher2_T7.tex
\bottomrule
\end{tabular}}
\end{center}
\end{frame}



\begin{frame}[plain,label=balanceschool_house]{Baseline descriptive/balance: school/household}
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{lccccc}
 &        Combo&          CG&         COD&     Control&p-value (all equal)    \\
 \toprule
\multicolumn{6}{l}{\textbf{Panel A: Students}}\\
\addlinespace 
\expandableinput summaryStudentsTotal.tex
\bottomrule
\end{tabular}
}
\end{frame}

\begin{frame}[plain,label=balance_student]{Baseline descriptives/balance: student}
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{lccccc}
 &        Combo&          CG&         COD&     Control&p-value (all equal)    \\
 \toprule
 
\multicolumn{6}{l}{\textbf{Panel B: Households}}\\ 
\addlinespace
\expandableinput summaryHouseholds.tex
\midrule
\multicolumn{6}{l}{\textbf{Panel C: Schools}}\\ 
\addlinespace
\expandableinput summarySchoolTotal.tex
\bottomrule
\end{tabular}
}
\end{frame}

\begin{frame}[plain,label=balance_teacher]{Baseline descriptives/balance: teachers}
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{lccccc}
 &        Combo&          CG&         COD&     Control&p-value (all equal)    \\
 \toprule
\multicolumn{6}{l}{\textbf{Panel D: Teachers}}\\ 
\addlinespace
\expandableinput summaryTeacherTotal.tex
\bottomrule
\end{tabular}
}
\end{frame}

\begin{frame}[plain,label=attrition]{Student present for low-stakes exam}
\begin{center}
\resizebox{!}{0.45\textheight}{
\begin{tabular}{lcc}
\toprule
\expandableinput Regattendance.tex
\bottomrule
\end{tabular}
}
\end{center}
\end{frame}


\begin{frame}[plain,label=tot_exp]{Total expenditure}
\begin{center}
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{6}{c}}
\toprule
                    &\multicolumn{1}{c}{\$ Total.}&\multicolumn{1}{c}{\$ Admin.}&\multicolumn{1}{c}{\$ Student}&\multicolumn{1}{c}{\$ Teaching Aid}&\multicolumn{1}{c}{\$ Teacher}&\multicolumn{1}{c}{\$ Construction}\\

\textbf{Panel A: Year 1} & & & & & &   \\
\expandableinput School2_T3.tex
\midrule
\textbf{Panel B: Year 2} & & & & &  &  \\
\expandableinput School2_T7.tex
\midrule
\textbf{Panel C: Year 1 + Year 2} & & & & &  &  \\
\expandableinput School2_TT.tex
\bottomrule
\end{tabular}
}
\end{center}
\end{frame}


\begin{frame}[plain,label=tot_sub]{Substitution from other sources}
\begin{center}
\resizebox{\textwidth}{!}{
\begin{tabular}{l*{7}{c}}
\toprule
\textbf{Panel A: Year 1} & & & & & & &  \\
\expandableinput OtherFunding_T3.tex
\midrule
\textbf{Panel B: Year 2} & & & & &  & &  \\
\expandableinput OtherFunding_T7.tex
\midrule
\textbf{Panel C: Year 1+ Year 2} & & & & &  & &  \\
\expandableinput OtherFunding_TT.tex
\bottomrule
\end{tabular}
}
\end{center}
\end{frame}


\begin{frame}[plain,label=tot_HHexp]{Household expenditure}
\begin{center}
\resizebox{\textwidth}{!}{

\begin{tabular}{l*{9}{c}}
\toprule
\textbf{Panel A: Year 1} & & & & & & & & &  \\
\expandableinput Household_Expenditure_T3.tex
\midrule
\textbf{Panel B: Year 2} & & &  & & & & & &  \\
\expandableinput Household_Expenditure_T7.tex
\midrule
\textbf{Panel C: Year 1 + Year 2} & & &  & & & & & &  \\
\expandableinput Household_Expenditure_TT.tex
\bottomrule
\end{tabular}
}
\end{center}
\end{frame}



\begin{frame}[plain,label=heter]{Heterogeneity by student characteristics}
\begin{center}
\resizebox{!}{0.45\textheight}{
\begin{tabular}{l*{3}{c}}
\toprule
                    &\multicolumn{1}{c}{Gender}&\multicolumn{1}{c}{Age}&\multicolumn{1}{c}{Lag test score}\\
\midrule
\multicolumn{4}{l}{\textbf{Panel A: Math}}\\
\expandableinput Heter_StudZ_hisabati_T7.tex
\multicolumn{4}{l}{\textbf{Panel B: Swahili}}\\
\expandableinput Heter_StudZ_kiswahili_T7.tex
\multicolumn{4}{l}{\textbf{Panel C: English}}\\
\expandableinput Heter_StudZ_kiingereza_T7.tex
\bottomrule
\end{tabular}
}
\end{center}
\end{frame}



\begin{frame}[plain]{Quantile Math}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_hisabati_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_hisabati_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Math test scores by endline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}



\begin{frame}[plain]{Quantile Swahili}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_kiswahili_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_kiswahili_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Swahili test scores by endline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}


\begin{frame}[plain]{Quantile English}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_kiingereza_COD_T7_4.pdf} \quad
\includegraphics[width=0.45\textwidth]{Lowess_Quantile_Z_kiingereza_Both_T7_4.pdf}
\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on English test scores by endline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
\end{center}
\end{figure}
\end{frame}


\end{document}

