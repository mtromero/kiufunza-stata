\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}
\usepackage{apacite}
\mode<presentation>{}
\usetheme{Warsaw}                  % Option: 2.0 and 1.5 spacing package.
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\let\oldtabular\tabular
\renewcommand{\tabular}{\tiny\oldtabular}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[KiuFunza: 2nd year results]{KiuFunza}

\normalsize
\date{}
\begin{document}
\frame{\titlepage}







\begin{frame}{Background/Motivation}
\begin{itemize}
\item Developing countries have greatly improved school enrollment, but learning outcomes are still poor
\item In Tanzania, the net enrollment rate in primary school rose from 53\% in 2000 to close to 90\% in 2012
\item But less than one third of grade 3 students could pass basic tests of second grade numeracy or literacy (Uwezo, 2013)
\item Inadequate resources and poor teacher motivation are considered to be two key constraints to better learning
\item We study the effectiveness of alleviating both constraints with a large-scale RCT that was also designed to test for complementarities between inputs and incentives.
\end{itemize}
\end{frame}

\begin{frame}{Intervention}
The treatment arms are:
\begin{itemize}
\item A capitation grant (CG) to schools that provides them with block grants (the ``input'' treatment). The grant amount is 10,000 TZ Shillings per student (about 3 times the mean pre-treatment school expenditure per student - excluding teachers salaries).
\item A ``cash on delivery'' (CoD) treatment to schools that provides teachers and head teachers with bonus payments conditional on the number of students who pass basic literacy and numeracy tests (the ``incentive'' treatment)
\item A combination (Combo) treatment arm where schools were provided with both the CG and the CoD treatments.
\end{itemize}
\end{frame}

\begin{frame}{Design}
Implemented across a representative sample of 350 schools across 10 districts in Tanzania.

\begin{table}[h]
\resizebox{\textwidth}{!}{%
\begin{tabular}{ll|l|l|l}
\cline{3-4}
                                                   & \multicolumn{1}{c|}{} & \multicolumn{2}{c|}{\textbf{CoD}} &  \\ \cline{3-4}
                                                   &                       & \textit{Yes}     & \textit{No}    &  \\ \cline{1-4}
\multicolumn{1}{|l|}{\multirow{2}{*}{\textbf{CG}}} & \textit{Yes}          & 70               & 70             &  \\ \cline{2-4}
\multicolumn{1}{|l|}{}                             & \textit{No}           & 70               & 140            &  \\ \cline{1-4}
\end{tabular}
}
\end{table}
\end{frame}

\begin{frame}{Design}
\begin{figure}[H]
\centering
\caption{Districts in Tanzania from which schools are selected\label{fig:mapa}}
\includegraphics[width=0.7\textwidth]{Map.pdf}
\end{figure}
\end{frame}


\begin{frame}{Data}
\begin{itemize}
\item School data: facilities, expenditure, enrollment, etc.
\item Teacher data: socio-demographic characteristics, qualifications, experience, time use, etc.
\item Student data: We test 10 students from each focal grade (grades 1, 2 and 3), in all three focal subjects (Math, English and Swahili) and in Science.
\item Household data (for 1/3 of students): household and dwelling socio-demographic characteristics, household expenditure in education, parents engagement in children's education, etc.
\end{itemize}
\end{frame}

\begin{frame}{Results}
\begin{itemize}
\item Point estimates for CG are close to zero or negative
\item Point estimates for COD are positive but not statistically significant
\item Positive, significant effects for Combo schools
\begin{enumerate}
\item Kiswahili $\rightarrow$ Combo schools $\rightarrow$ 0.2 standard deviations higher
\item English $\rightarrow$ Combo schools $\rightarrow$ 0.1 standard deviations higher
\item Math $\rightarrow$ Combo schools $\rightarrow$ 0.19 standard deviations higher
\item The interaction of CG and COD is significant for Swahili and Math
\end{enumerate}
\end{itemize}
\end{frame}
%
\begin{frame}[plain]{Test Scores}
\begin{center}
\resizebox{\textwidth}{!}{%
\begin{tabular}{l*{8}{c}}
\toprule
                    &\multicolumn{4}{c}{Year 1}                                                             &\multicolumn{4}{c}{Year 2}                                                             \\\cmidrule(lr){2-5}\cmidrule(lr){6-9}
                    &\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Average Subjects}&\multicolumn{1}{c}{Math}&\multicolumn{1}{c}{Swahili}&\multicolumn{1}{c}{English}&\multicolumn{1}{c}{Average Subjects}\\
\midrule
CG                  &      -0.051         &      0.0093         &      -0.021         &      -0.025         &     -0.0035         &      -0.021         &      0.0066         &     -0.0069         \\
                    &     (0.040)         &     (0.041)         &     (0.037)         &     (0.037)         &     (0.049)         &     (0.051)         &     (0.043)         &     (0.045)         \\
\addlinespace
COD                 &       0.044         &       0.059         &       0.071\sym{*}  &       0.069\sym{*}  &       0.071         &       0.016         &       0.025         &       0.044         \\
                    &     (0.039)         &     (0.039)         &     (0.041)         &     (0.036)         &     (0.046)         &     (0.048)         &     (0.039)         &     (0.042)         \\
\addlinespace
Combo               &        0.10\sym{**} &        0.12\sym{***}&       0.078\sym{*}  &        0.12\sym{***}&        0.19\sym{***}&        0.20\sym{***}&       0.096\sym{*}  &        0.19\sym{***}\\
                    &     (0.045)         &     (0.040)         &     (0.043)         &     (0.040)         &     (0.047)         &     (0.045)         &     (0.051)         &     (0.045)         \\
\addlinespace
District F.E.       &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         \\
\addlinespace
Student Charac.     &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         \\
\addlinespace
Week F.E.           &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         &         Yes         \\
\addlinespace
School Charac.      &          Yes         &          Yes         &          Yes         &          Yes         &          Yes         &          Yes         &          Yes         &          Yes         \\
\midrule
N. of obs.          &        8774         &        8774         &        8774         &        8774         &        9083         &        9083         &        9083         &        9083         \\
Combo-COD-CG        &        0.11\sym{*}  &       0.055         &       0.028         &       0.078         &        0.12\sym{*}  &        0.20\sym{***}&       0.064         &        0.15\sym{**} \\
p-value ($ H\_0$:Combo-COD-CG=0)&       0.068         &        0.36         &        0.66         &        0.17         &       0.088         &      0.0051         &        0.34         &       0.019         \\
\bottomrule
\multicolumn{9}{l}{\footnotesize \specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{frame}



%
%
%\begin{frame}{Test Scores}
%\begin{itemize}
%\item Most of the impact comes from student around the median of the distribution of lagged test scores (except for English where the passing threshold is well above most students initial learning levels).
%\item For those with very low or very high  starting test scores, there is no treatment effect.
%\item  This is consistent with the program introducing threshold effects, where teachers only focus on students near the qualification threshold.
%\end{itemize}
%\end{frame}
%
%
%\begin{frame}[plain]{Lowess Math}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_hisabati_COD_B_T7_4.pdf}
%\quad
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_hisabati_Both_B_T7_4.pdf}
%\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Math test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
%\end{center}
%\end{figure}
%\end{frame}
%
%
%\begin{frame}[plain]{Lowess Swahili}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiswahili_COD_B_T7_4.pdf}
%\quad
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiswahili_Both_B_T7_4.pdf}
%\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Math test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
%\end{center}
%\end{figure}
%\end{frame}
%
%
%
%\begin{frame}[plain]{Lowess English}
%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiingereza_COD_B_T7_4.pdf}
%\quad
%\includegraphics[width=0.45\textwidth]{Lowess_Resid_Percentile_Z_kiingereza_Both_B_T7_4.pdf}
%\caption{\tiny Effect of Cash on Delivery (left) and Combo (right) on Math test scores by baseline score using a lowess regression. Standard errors are calculated using bootstrapping and clustered at the school level. }
%\end{center}
%\end{figure}
%\end{frame}

\end{document}

