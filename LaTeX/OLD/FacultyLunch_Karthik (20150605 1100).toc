\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@sectionintoc {2}{Design and data}{15}{0}{2}
\beamer@sectionintoc {3}{Results - Expenditure}{26}{0}{3}
\beamer@sectionintoc {4}{Results - Test scores}{31}{0}{4}
\beamer@sectionintoc {5}{Mechanisms}{37}{0}{5}
\beamer@sectionintoc {6}{Heterogeneity}{39}{0}{6}
