********************************************************************
*********************** EDI VS TWA Yr1
********************************************************************


use "$base_out/Consolidated/Student_School_House_Teacher_Char_TWA.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

*First lets create the tables Karthik Wants

label var Z_kiswahili_T3 Swahili
label var Z_kiswahili_T7 Swahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"
pca Z_kiswahili_C~7  Z_kiingere~C_T7 Z_hisabati_C_T7
predict Z_ScoreFocal_C_T7,score
pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist Z_ScoreFocal_C_T7 Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}




*Main comparison, difference in means
foreach var in $AggregateDep_Karthik{
eststo clear


*With common supoort and common students-EDI 
eststo `var'_1_edi:   reg `var'_T3 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if   `var'_T3!=. & `var'_T4!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]


*With common supoort and common students-TWA test
eststo `var'_1_twa:   reg `var'_T4 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if  `var'_T3!=. & `var'_T4!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
preserve
drop if `var'_T3==.
drop if `var'_T4==.
rename `var'_T3 `var'_EDI
rename `var'_T4 `var'_TWA
drop if upid==""
keep `var'_EDI `var'_TWA $treatmentlist TreatmentCOD2 TreatmentBoth2 DistID  $schoolcontrol HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  WeekIntvTest_T* upid SchoolID
reshape long `var', i(upid) j(Test) string
gen Twaweza=(Test=="_TWA")
eststo `var'_1: reg `var'  (c.TreatmentCOD2 c.TreatmentBoth2  i.LagGrade  i.DistID c.(${studentcontrol}  ${schoolcontrol} ${HHcontrol}))##c.Twaweza, vce(cluster SchoolID)  
estadd ysumm
test (_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]

restore


*With common supoort and common students-EDI 
eststo `var'_2_edi:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 &  `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]


*With common supoort and common students-TWA test
eststo `var'_2_twa:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 &  `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

preserve
drop if `var'_C_T7==.
drop if `var'_T8==.
drop if attgrade_T7!=GradeID_T7
rename `var'_C_T7 `var'_EDI
rename `var'_T8 `var'_TWA
drop if upid==""
keep `var'_EDI `var'_TWA $treatmentlist TreatmentCOD2 TreatmentBoth2 DistID  $schoolcontrol HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  WeekIntvTest_T* upid SchoolID
reshape long `var', i(upid) j(Test) string
gen Twaweza=(Test=="_TWA")
eststo `var'_2: reg `var'  (c.TreatmentCOD2 c.TreatmentBoth2  i.LagGrade  i.DistID c.(${studentcontrol}  ${schoolcontrol} ${HHcontrol}))##c.Twaweza, vce(cluster SchoolID)  
estadd ysumm
test (_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]
restore


esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp_Yr1Yr2.tex", se ar2 booktabs label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combo c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combo-COD" "p-value" "") star(p))

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp_Yr1Yr2.csv", se ar2 label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combo c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combo-COD" "p-value" "") star(p))

}







