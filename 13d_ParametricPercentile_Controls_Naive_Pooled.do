**********************
**************Student
************************
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear

capture drop Z_*_T1
capture drop Z_*_T5
capture drop Z_*_T4
capture drop Z_*_T8
capture drop Z_*_B_T7
capture drop Z_*_C_T7

capture drop GradeID_T1
capture drop GradeID_T5
capture drop GradeID_T4
capture drop GradeID_T8


global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza   /*Z_kiingereza  Z_ScoreFocal this should be added in the future...*/
global treatmentlist TreatmentCOD TreatmentCG TreatmentBoth



label var Z_kiswahili_T3 Swahili
label var Z_kiswahili_T7 Swahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Focal Subjects"
label var Z_ScoreFocal_T7 "Focal Subjects"


foreach var in Z_hisabati Z_kiswahili Z_kiingereza Z_ScoreFocal{
	egen Decile_`var' = fastxtile(Lag`var') , by(LagGrade) nq(5)
}


reghdfe Z_hisabati_T3 c.(${treatmentlist})#i.Decile_Z_hisabati i.Decile_Z_hisabati i.LagGrade $studentcontrol $schoolcontrol if GradeID_T3<=3, vce(cluster SchoolID) ab(DistrictID)

foreach time in T3 T7{

	foreach var in $AggregateDep_Karthik {
		capture drop Decile
	    gen Decile=Decile_`var'			
		qui reghdfe `var'_`time' c.(${treatmentlist})#i.Decile i.Decile i.LagGrade $studentcontrol $schoolcontrol if GradeID_`time'<=3, vce(cluster SchoolID) ab(DistrictID)
		test (_b[1.Decile#TreatmentCOD] - _b[5.Decile#TreatmentCOD]=0)
		local pCOD=string(r(p), "%9.2gc")
		test (_b[1.Decile#TreatmentCG] - _b[5.Decile#TreatmentCG]=0)
		local pCG=string(r(p), "%9.2gc")
		test (_b[1.Decile#TreatmentBoth] - _b[5.Decile#TreatmentBoth]=0)
		local pBoth=string(r(p), "%9.2gc")
		
	
		test (_b[1.Decile#c.TreatmentCOD]=_b[2.Decile#c.TreatmentCOD]= _b[3.Decile#c.TreatmentCOD]= _b[4.Decile#c.TreatmentCOD]= _b[5.Decile#c.TreatmentCOD])
		local pCOD_all=string(r(p), "%9.2gc")
		test (_b[1.Decile#c.TreatmentBoth]=_b[2.Decile#c.TreatmentBoth]= _b[3.Decile#c.TreatmentBoth]= _b[4.Decile#c.TreatmentBoth]= _b[5.Decile#c.TreatmentBoth])
		local pBoth_all=string(r(p), "%9.2gc")
		test (_b[1.Decile#c.TreatmentCG]=_b[2.Decile#c.TreatmentCG]= _b[3.Decile#c.TreatmentCG]= _b[4.Decile#c.TreatmentCG]= _b[5.Decile#c.TreatmentCG])
		local pCG_all=string(r(p), "%9.2gc")
		
	
		
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentCOD*) ci ///
				rename(1.Decile#c.TreatmentCOD="1" 2.Decile#c.TreatmentCOD="2" 3.Decile#c.TreatmentCOD="3" 4.Decile#c.TreatmentCOD="4" ///
				5.Decile#c.TreatmentCOD="5"  /* 6.Decile#c.TreatmentLevels="6" 7.Decile#c.TreatmentLevels="7" 8.Decile#c.TreatmentLevels="8" 9.Decile#c.TreatmentLevels="9" 10.Decile#c.TreatmentLevels="10" */ ) ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Incentives",size(medlarge)) saving(COD,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pCOD'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pCOD_all'",size(medlarge)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.2)0.5) ///
				
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentBoth*) ci  ///
				rename(1.Decile#c.TreatmentBoth="1" 2.Decile#c.TreatmentBoth="2" 3.Decile#c.TreatmentBoth="3" 4.Decile#c.TreatmentBoth="4" ///
				5.Decile#c.TreatmentBoth="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Combination",size(medlarge)) saving(Both,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pBoth'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pBoth_all'",size(medlarge)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.2)0.5) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentCG*) ci  ///
				rename(1.Decile#c.TreatmentCG="1" 2.Decile#c.TreatmentCG="2" 3.Decile#c.TreatmentCG="3" 4.Decile#c.TreatmentCG="4" ///
				5.Decile#c.TreatmentCG="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Grants",size(medlarge)) saving(CG,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pCG'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pCG_all'",size(medlarge)) yscale(range(-0.5 0.5)) ylabel(-0.5(0.2)0.5) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
		
				
		gr combine COD.gph CG.gph Both.gph, graphregion(color(white)) cols(3)
		graph export "$graphs/HeteroQuintile_`var'_`time'.pdf", replace
			
		gr combine COD.gph Both.gph, graphregion(color(white)) cols(2)
		graph export "$graphs/HeteroQuintile_`var'_`time'_OnlyCGBoth.pdf", replace
	}  
}

keep $treatmentlist LagGrade LagseenUwezoTests LagpreSchoolYN LagAge Lagmale  LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol GradeID_* Decile_*  SchoolID DistrictID  Z_hisabati* Z_kiswahili* Z_kiingereza* upid
drop if upid==""
drop if GradeID_T7==. & GradeID_T3==.
reshape long Z_hisabati@ Z_kiswahili@ Z_kiingereza@ GradeID@, i(upid) j(time) string
encode time, gen(time2)
	
foreach var in Z_hisabati Z_kiswahili Z_kiingereza {
	capture drop Decile
	gen Decile=Decile_`var'		
	qui reghdfe `var' c.(${treatmentlist})#i.Decile i.Decile##i.time2 time2##(i.LagGrade c.($studentcontrol $schoolcontrol)), vce(cluster SchoolID) ab(time2##DistrictID)
	
	
	test (_b[1.Decile#TreatmentCOD] - _b[5.Decile#TreatmentCOD]=0)
		local pCOD=string(r(p), "%9.2gc")
		test (_b[1.Decile#TreatmentCG] - _b[5.Decile#TreatmentCG]=0)
		local pCG=string(r(p), "%9.2gc")
		test (_b[1.Decile#TreatmentBoth] - _b[5.Decile#TreatmentBoth]=0)
		local pBoth=string(r(p), "%9.2gc")
		
	
		test (_b[1.Decile#c.TreatmentCOD]=_b[2.Decile#c.TreatmentCOD]= _b[3.Decile#c.TreatmentCOD]= _b[4.Decile#c.TreatmentCOD]= _b[5.Decile#c.TreatmentCOD])
		local pCOD_all=string(r(p), "%9.2gc")
		test (_b[1.Decile#c.TreatmentBoth]=_b[2.Decile#c.TreatmentBoth]= _b[3.Decile#c.TreatmentBoth]= _b[4.Decile#c.TreatmentBoth]= _b[5.Decile#c.TreatmentBoth])
		local pBoth_all=string(r(p), "%9.2gc")
		test (_b[1.Decile#c.TreatmentCG]=_b[2.Decile#c.TreatmentCG]= _b[3.Decile#c.TreatmentCG]= _b[4.Decile#c.TreatmentCG]= _b[5.Decile#c.TreatmentCG])
		local pCG_all=string(r(p), "%9.2gc")
	
	
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentCOD*) ci ///
				rename(1.Decile#c.TreatmentCOD="1" 2.Decile#c.TreatmentCOD="2" 3.Decile#c.TreatmentCOD="3" 4.Decile#c.TreatmentCOD="4" ///
				5.Decile#c.TreatmentCOD="5"  /* 6.Decile#c.TreatmentLevels="6" 7.Decile#c.TreatmentLevels="7" 8.Decile#c.TreatmentLevels="8" 9.Decile#c.TreatmentLevels="9" 10.Decile#c.TreatmentLevels="10" */ ) ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Incentives",size(medlarge)) saving(COD,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pCOD'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pCOD_all'",size(medlarge)) yscale(range(-0.4 0.4)) ylabel(-0.4(0.2)0.4) ///
				
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentBoth*) ci  ///
				rename(1.Decile#c.TreatmentBoth="1" 2.Decile#c.TreatmentBoth="2" 3.Decile#c.TreatmentBoth="3" 4.Decile#c.TreatmentBoth="4" ///
				5.Decile#c.TreatmentBoth="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Combination",size(medlarge)) saving(Both,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pBoth'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pBoth_all'",size(medlarge)) yscale(range(-0.4 0.4)) ylabel(-0.4(0.2)0.4) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				
		coefplot, graphregion(color(white)) baselevels keep(*c.TreatmentCG*) ci  ///
				rename(1.Decile#c.TreatmentCG="1" 2.Decile#c.TreatmentCG="2" 3.Decile#c.TreatmentCG="3" 4.Decile#c.TreatmentCG="4" ///
				5.Decile#c.TreatmentCG="5" /*6.Decile#c.TreatmentGains="6" 7.Decile#c.TreatmentGains="7" 8.Decile#c.TreatmentGains="8" 9.Decile#c.TreatmentGains="9" 10.Decile#c.TreatmentGains="10"*/ )   ///
				levels(95) ciopts(recast(rcap)) graphregion(color(white)) ///
				vertical yline(0) ylab(, ang(h) nogrid labsize(medlarge) labgap(-3)) xlab(,labsize(medlarge)) xtitle("Quintile",size(medlarge)) ytitle("Treatment effect",size(medlarge)) title("Grants",size(medlarge)) saving(CG,replace) ///
				note("p-value(H{sub:0}:Q{sub:1}=Q{sub:5})= `pCG'" "p-value(H{sub:0}:Q{sub:1}=Q{sub:2}=Q{sub:3}=Q{sub:4}=Q{sub:5})= `pCG_all'",size(medlarge)) yscale(range(-0.4 0.4)) ylabel(-0.4(0.2)0.4) 
				*graph export "$graphs/HeteroBaseline_`kind'_`subject'_`time'.pdf", replace
				

		gr combine COD.gph CG.gph Both.gph, graphregion(color(white)) cols(3)
		graph export "$graphs/HeteroQuintile_`var'.pdf", replace
				
		gr combine COD.gph Both.gph, graphregion(color(white)) cols(2)
		graph export "$graphs/HeteroQuintile_`var'_OnlyCGBoth.pdf", replace
}  
