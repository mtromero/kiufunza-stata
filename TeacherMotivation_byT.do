use "$basein/1 Baseline/Teacher/Teacher_noPII.dta" , clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID)

recode t66 (2=0) (3=.)

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
reg t66 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)


use "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_nopii.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID)

recode t66 (2=0) (3=.)
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
reg t66 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)

reg t66 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
