use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
drop if TeacherID==.
keep DistrictID SchoolID TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
merge 1:1 DistrictID SchoolID TeacherID using "$basein/1 Baseline/Teacher/Teacher_noPII.dta", keepus(t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t66 missedClasses) update replace
drop _merge
drop if substr(upid,1,2)!="R1"

save "$base_out/Consolidated/Teacher.dta", replace

use "$basein/2 Midline/MTeacher_noPII.dta",clear
gen upidold=upid
save "$base_out/2 Midline/MTeacher_noPII.dta",replace

use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
drop if upidold==""
merge 1:1 upidold using "$base_out/2 Midline/MTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update replace
drop _merge
drop if substr(upid,1,2)!="R2"

append using "$base_out/Consolidated/Teacher.dta"
save "$base_out/Consolidated/Teacher.dta", replace
merge 1:1 upidold using "$base_out/2 Midline/MTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
save "$base_out/ConsolidatedYr34/Teacher.dta", replace 


use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
drop if ETTeacherID==.
merge 1:1 ETTeacherID using "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update replace
drop _merge
drop if substr(upid,1,2)!="R3"

append using "$base_out/Consolidated/Teacher.dta"
rename t24 t25
rename t23 t24
save "$base_out/Consolidated/Teacher.dta", replace 
merge 1:1 ETTeacherID using "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  )  update
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace
 

use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
drop if upid==""
merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24  tdgtrc3 -tdgtrc11 ) update replace
drop _merge
drop if substr(upid,1,2)!="R4"
append using "$base_out/Consolidated/Teacher.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/Consolidated/Teacher.dta", replace 

merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 ) update 
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 
merge 1:1 upid using "$basein/6 Baseline 2014/Data/school/R4Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 tdgtrc3 -tdgtrc11) update
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 


use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
drop if upid==""
merge 1:1 upid using "$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11 )  update replace
drop _merge
drop if substr(upid,1,2)!="R5"
append using "$base_out/Consolidated/Teacher.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/Consolidated/Teacher.dta", replace 

merge 1:1 upid  using"$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 )  update 
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 
merge 1:1 upid using "$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11)  update
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 



use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
keep DistrictID SchoolID  TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
drop if R6TeacherID==.
merge 1:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11 ) update replace
drop _merge
drop if substr(upid,1,2)!="R6"
append using "$base_out/Consolidated/Teacher.dta"
bys upid: gen dup=_n
drop if dup==2
drop dup
save "$base_out/Consolidated/Teacher.dta", replace 


use "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", clear
keep SchoolID R6TeacherID t23
drop if R6TeacherID==.
drop if t23==.
merge 1:m SchoolID R6TeacherID using "$base_out/Consolidated/Teacher.dta", update
drop if _merge==1
drop _merge
compress
save "$base_out/Consolidated/Teacher.dta", replace 


merge m:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(tdgtrc3 -tdgtrc11 ) update
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 

merge m:1 SchoolID R6TeacherID using "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", keepus(topbpm tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t24 t25  tdgtrc3 -tdgtrc11) update
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 




recode tdgtrc3- tdgtrc11 (2=0)
forvalues i=4/11{
	recode tdgtrc`i' (.=0) if tdgtrc`=scalar(`i'-1)'==0
}
pca tdgtrc3- tdgtrc11
predict TeacherIndexRecall, score
drop if upid==""
save "$base_out/Consolidated/Teacher_charac.dta", replace 



use "$basein/1 Baseline/Teacher/Subjects_noPII.dta" , clear
replace SubjectID=4 if SubjectID>=5
label define I_Subjects 5 "", modify
label define I_Subjects 6 "", modify
label define I_Subjects 7 "", modify
label define I_Subjects 8 "", modify
label define I_Subjects 9 "", modify
label define I_Subjects 4 "Other", modify
drop syllabus  prevSubject combinationCode
collapse (sum) currentSubject, by(DistrictID SchoolID TeacherID CurrentGradeID SubjectID)
replace currentSubject=0 if currentSubject==.
replace currentSubject=1 if currentSubject>1

reshape wide currentSubject, i(DistrictID SchoolID TeacherID CurrentGradeID) j( SubjectID)
rename currentSubject1 SBJ_M
rename currentSubject2 SBJ_K
rename currentSubject3 SBJ_E
rename currentSubject4 SBJ_O
gen CurrentGradeID2="_GRD"+string(CurrentGradeID)
drop CurrentGradeID
reshape wide SBJ_M SBJ_K SBJ_E SBJ_O, i(DistrictID SchoolID TeacherID ) j( CurrentGradeID2) string

rename * =_T1
rename DistrictID_T1 DistrictID
rename SchoolID_T1 SchoolID
rename TeacherID_T1 TeacherID

save "$base_out/1 Baseline/Teacher/Subjets.dta" , replace

use "$basein/3 Endline/Supplementing/IDLinkTeachers_R3_noPII.dta",clear
drop if TeacherID==.
keep upid upidold DistrictID SchoolID TeacherID
merge 1:1 DistrictID SchoolID TeacherID using "$base_out/1 Baseline/Teacher/Subjets.dta"
drop _merge  

merge 1:1 upid using "$base_out/Consolidated/Teacher_charac.dta"
drop if _merge==1
drop _merge
save "$base_out/Consolidated/Teacher.dta", replace 
save "$base_out/1 Baseline/Teacher/Teacher.dta" , replace

*****************************
*****************************
*****************************
*****************************

**********************************
******** T_2 Data ****************
**********************************

use "$base_out/2 Midline/MTeacher_noPII.dta", clear
drop if tchavl==.
gen correct_earnings=1 if tktpyo==200000
replace correct_earnings=0 if tktpyo!=200000 & tktpyo!=.
gen knows_NoPen=1 if tktpen==2
replace knows_NoPen=0 if tktpen!=2 & tktpen!=.
gen KnowsCG=1 if tkgamt>800 & tkgamt<10000
replace KnowsCG=0 if (tkgamt<800| tkgamt>10000) & tkgamt!=.

label var correct_earnings "Correct earnings if 40 students pass"
label var knows_NoPen "Knows there is no penalization for students failing"
label var KnowsCG "Knows the amount of the capitation grant (between 800 and 1000 per month)"


merge 1:1 upidold using "$basein/3 Endline/Supplementing/IDLinkTeachers_R3_noPII.dta", keepus(DistrictID SchoolID  TeacherID ETTeacherID upid tchsex)
drop if _merge==2
drop _merge

keep ETTeacherID correct_earnings knows_NoPen KnowsCG
rename * =_T2
rename ETTeacherID_T2 ETTeacherID
merge 1:m ETTeacherID using "$base_out/Consolidated/Teacher.dta"
drop if _merge==1
drop _merge

save "$base_out/Consolidated/Teacher.dta", replace 


**********************************
******** T_3 Data ****************
**********************************

use "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", clear

gen NewTechnique=.
replace NewTechnique=1 if tntmyn==1
replace NewTechnique=0 if tntmyn==2
label variable 	NewTechnique "Start using new teaching technique this year"


gen splitAbility=.
replace splitAbility=1 if  ictkyn==1
replace splitAbility=0 if  ictkyn==2
label variable 	splitAbility "Split students according to ability"

gen EcnourageTopHelp=.
replace EcnourageTopHelp=1 if  stspyn==1
replace EcnourageTopHelp=0 if  stspyn==2
label variable 	EcnourageTopHelp "Encourage top students to help those behind"

gen InServiceTraining=.
replace InServiceTraining=1 if  t55==1
replace InServiceTraining=0 if  t55==2
label variable 	InServiceTraining "In services training this year"

gen OtherTraining=.
replace OtherTraining=1 if  t56==1
replace OtherTraining=0 if  t56==2
label variable 	OtherTraining "Other training this year"

gen ResourcesImproveTeaching=.
replace ResourcesImproveTeaching=1 if  t61==1
replace ResourcesImproveTeaching=0 if  t61==2
label variable 	ResourcesImproveTeaching "Use resources to improve teaching"

gen DaysAbsent=.
replace DaysAbsent=1 if  t36>=1
replace DaysAbsent=0 if  t36==0
label variable 	DaysAbsent "Days absent last term"

replace missedClasses=0 if missedClasses==2

recode *tme (-99=.) (-98=.) (99=.) (98=.) 
gen TimeAtSchool=(tdptme- tartme)*15/60
label variable 	TimeAtSchool "Time at school (Hrs)"

gen TeachingCareer=.
replace TeachingCareer=1 if  t66==1
replace TeachingCareer=0 if  t66==2
label variable 	TeachingCareer "Would you choose a teaching career"
save "$base_out/3 Endline/Teacher/Teacher.dta", replace


use "$basein/3 Endline/Teacher/ETTime_noPII.dta", clear
reshape wide t27, i(ETTeacherID) j(ETTimeID)

merge 1:1 ETTeacherID using "$base_out/3 Endline/Teacher/Teacher.dta"
drop _merge
compress
save "$base_out/3 Endline/Teacher/Teacher.dta", replace


label variable t271 "Preparing class (Mins)"
label variable t272 "Grading homework (Mins)"
label variable t273 "Grading tests (Mins)"
label variable t274 "Getting children to school (Mins)"
label variable t275 "Regular classes (Mins)"
label variable t276 "Extra classes (Mins)"
label variable t277 "Socializing (Mins)"
recode t271- t277 (-99=.) (-98=.) (99=.) (98=.) 
rename t23 Compensation_2013
rename t24 Compensation_2012
rename t25 Compensation_2011

rename ipmsyn Inputs_Missing
replace Inputs_Missing=0 if  Inputs_Missing==2

gen teaching_inputs=1 if trtgti<=3 & trtgti!=.
replace teaching_inputs=0 if trtgti>3 & trtgti!=.
label variable teaching_inputs "Above average teaching inputs"

gen teaching_assistance=1 if trtgas<=3 & trtgas!=.
replace teaching_assistance=0 if trtgas>3 & trtgas!=.
label variable teaching_assistance "Above average teaching assistance"

gen Help_head=1 if trtght<=3 & trtght!=.
replace Help_head=0 if trtght>3 & trtght!=.
label variable Help_head "Above average help from head teacher"  

gen Help_otherTeachers=1 if trtgot<=3 & trtgot!=.
replace Help_otherTeachers=0 if trtgot>3 & trtgot!=.
label variable Help_otherTeachers "Above average help from other teachers"  

gen Help_Community=1 if trtgpt<=3 & trtgpt!=.
replace Help_Community=0 if trtgpt>3 &trtgpt!=.
label variable Help_Community "Above average help from community"    

gen knowaboutBonus=1 if codeyn==1
replace knowaboutBonus=0 if codeyn==2 | codeyn==3
label variable knowaboutBonus "Knows about bonuses"    

gen teachers_jelous=.
replace teachers_jelous=1 if codotn==1 | codotn==2 | codotn==6 | codotn==7
replace teachers_jelous=0 if codotn==4 | codotn==5 | codotn==8
label variable teachers_jelous "are other teachers jelous of bonus"    


gen work_unconfortable=.
replace work_unconfortable=1 if codowe==1 | codowe==2 | codowe==6 | codowe==7
replace work_unconfortable=0 if codowe==4 | codowe==5 | codowe==8
label variable work_unconfortable "Has work enviroment become uncofortable"   

gen resources_taken=.
replace resources_taken=1 if codort==1 | codort==2 | codort==6 | codort==7
replace resources_taken=0 if codort==4 | codort==5 | codort==8
label variable resources_taken "Resources taken away because of bonus"   

gen preassure_share=.
replace preassure_share=1 if codops==1 | codops==2 | codops==6 | codops==7
replace preassure_share=0 if codops==4 | codops==5 | codops==8
label variable preassure_share "Preassure to share bonus"   

gen feel_guilty=.
replace feel_guilty=1 if codogt==1 | codogt==2 | codogt==6 | codogt==7
replace feel_guilty=0 if codogt==4 | codogt==5 | codogt==8
label variable feel_guilty "Feel guility because of bonus" 

gen others_interfere=.
replace others_interfere=1 if codoif==1 | codoif==2 | codoif==6 | codoif==7
replace others_interfere=0 if codoif==4 | codoif==5 | codoif==8
label variable others_interfere "Others interfere with your teaching" 

gen parents_support_bonus=.
replace parents_support_bonus=1 if codopt==1 | codopt==2 | codopt==6 | codopt==7
replace parents_support_bonus=0 if codopt==4 | codopt==5 | codopt==8
label variable parents_support_bonus "Parents support effort to get bonus" 

gen share_bonus=1 if tbshyn==1 | atbshyn==1
replace share_bonus=0 if tbshyn==2 & atbshyn==2
label variable share_bonus "Plans to share bonus" 

compress
save "$base_out/3 Endline/Teacher/Teacher.dta", replace


save "$base_out/3 Endline/Teacher/Teacher.dta", replace

use "$basein/3 Endline/Teacher/ETGrdSub_noPII.dta", clear  

replace notest=. if notest==98 | anotest==-95 | notest==-98
sum notest,d
replace notest=r(p99) if notest>r(p99) & !missing(notest)

replace tremyn=0 if tremyn==2
replace ttutyn=0 if ttutyn==2

gen MovedToFGS=0
gen MovedOutFGS=0

replace MovedOutFGS=1 if !missing(grstmt) & ETGradeID<=3 & ETGrdSubID<=3
replace MovedToFGS=1 if !missing(grbgmt) & ETGradeID<=3 & ETGrdSubID<=3

gen FGS=(ETGradeID<=3 & ETGrdSubID<=3 & tgrdyn==1)

gen test= anotest
replace test=. if anotest==-98 | anotest==-95
sum test,d
replace test=r(p99) if test>r(p99) & !missing(test)
label variable test "test in school year"



collapse (max) FGS=FGS (max) MovedOutFGS MovedToFGS  (median) test_F_sub_F_yrs=notest (max) remedial_F_sub_F_yrs=tremyn tutoring_F_sub_F_yrs=ttutyn, by(ETTeacherID)
gen ind=(MovedOutFGS==1 & MovedToFGS==1)
replace MovedOutFGS=0 if ind==1
replace MovedToFGS=0 if ind==1
drop ind



merge 1:1 ETTeacherID using "$base_out/3 Endline/Teacher/Teacher.dta"
compress
drop _merge
rename * =_T3
rename ETTeacherID_T3 ETTeacherID


save "$base_out/3 Endline/Teacher/Teacher.dta", replace


use "$basein/3 Endline/Teacher/ETGrdSub_noPII.dta", clear 

keep  ETTeacherID tgrdyn ETGradeID  ETGrdSubID

replace ETGrdSubID=4 if ETGrdSubID>=5
label define I_Subjects 5 "", modify
label define I_Subjects 6 "", modify
label define I_Subjects 7 "", modify
label define I_Subjects 8 "", modify
label define I_Subjects 9 "", modify
label define I_Subjects 4 "Other", modify
collapse (sum) tgrdyn, by(ETTeacherID ETGradeID  ETGrdSubID)
replace tgrdyn=0 if tgrdyn==.
replace tgrdyn=1 if tgrdyn>1

reshape wide tgrdyn, i(ETTeacherID ETGradeID) j(ETGrdSubID )
rename tgrdyn1 SBJ_M
rename tgrdyn2 SBJ_K
rename tgrdyn3 SBJ_E
rename tgrdyn4 SBJ_O
gen ETGradeID2="_GRD"+string(ETGradeID)
drop ETGradeID
reshape wide SBJ_M SBJ_K SBJ_E SBJ_O, i(ETTeacherID ) j( ETGradeID2) string
foreach var of varlist SBJ_M_GRD1- SBJ_E_GRD7{
replace `var'=0 if `var'==.
}

rename * =_T3
rename ETTeacherID_T3 ETTeacherID

save "$base_out/3 Endline/Teacher/Subjets.dta" , replace

merge 1:m ETTeacherID using "$base_out/Consolidated/Teacher.dta"
drop _merge





*collapse (sum) SBJ_M_GRD1_T3 SBJ_K_GRD1_T3 SBJ_E_GRD1_T3 SBJ_M_GRD2_T3 SBJ_K_GRD2_T3 SBJ_E_GRD2_T3 SBJ_M_GRD3_T3 SBJ_K_GRD3_T3 SBJ_E_GRD3_T3, by( SchoolID)
egen FGS=rowtotal(SBJ_M_GRD1_T3 SBJ_K_GRD1_T3 SBJ_E_GRD1_T3 SBJ_M_GRD2_T3 SBJ_K_GRD2_T3 SBJ_E_GRD2_T3 SBJ_M_GRD3_T3 SBJ_K_GRD3_T3 SBJ_E_GRD3_T3), missing
drop if FGS==0
egen FGS_1=rowtotal(SBJ_M_GRD1_T3 SBJ_K_GRD1_T3 SBJ_E_GRD1_T3), missing
egen FGS_2=rowtotal(SBJ_M_GRD2_T3 SBJ_K_GRD2_T3 SBJ_E_GRD2_T3), missing
egen FGS_3=rowtotal(SBJ_M_GRD3_T3 SBJ_K_GRD3_T3 SBJ_E_GRD3_T3), missing


merge m:1 ETTeacherID using "$base_out/3 Endline/Teacher/Teacher.dta"
drop DistrictID_T3- Compensation_2011_T3
drop _merge
gen male=1 if tchsex==1
replace male=0 if tchsex==2
replace t16=. if t16==7 | t16==-96
gen higher_degree=1 if t16>=3
replace higher_degree=0 if t16<=2

replace t23=. if t23<0
replace t24=. if t24<0
replace t25=. if t25<0

save "$base_out/Consolidated/Teacher.dta", replace 
keep ETTeacherID upid DistrictID SchoolID TeacherID upidold tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25 male higher_degree
save "$base_out/Consolidated/Teacher_Charac.dta", replace 
***********************************
***********************************************************
***********************************************************
******** Find average per school *************************
**********************************************************
use "$base_out/Consolidated/Teacher.dta", clear 
recode t66 (2=0) (3=.)



keep male TeacherIndexRecall t05 t07 t08 t10 t21 higher_degree t23 t66 ///
remedial_F_sub_F_yrs_T3  ///
tutoring_F_sub_F_yrs_T3  ///
test_F_sub_F_yrs_T3 ///
t271_T3 t272_T3 t273_T3 t274_T3 t275_T3 t276_T3 t277_T3 ///
Inputs_Missing_T3 missedClasses_T3 splitAbility_T3 EcnourageTopHelp_T3 TimeAtSchool_T3 TeachingCareer_T3 teaching_inputs_T3 ///
teaching_assistance_T3 Help_head_T3 Help_otherTeachers_T3 Help_Community_T3 correct_earnings_T2 knows_NoPen_T2 KnowsCG_T2 SchoolID
replace t10=0 if t10==2

collapse (mean) male TeacherIndexRecall t05 t07 t08 t10 t21 higher_degree t23 t66 ///
remedial_F_sub_F_yrs_T3  ///
tutoring_F_sub_F_yrs_T3  ///
test_F_sub_F_yrs_T3 ///
t271_T3 t272_T3 t273_T3 t274_T3 t275_T3 t276_T3 t277_T3 ///
Inputs_Missing_T3 missedClasses_T3 splitAbility_T3 EcnourageTopHelp_T3 TimeAtSchool_T3 TeachingCareer_T3 teaching_inputs_T3 ///
teaching_assistance_T3 Help_head_T3 Help_otherTeachers_T3 Help_Community_T3 correct_earnings_T2 knows_NoPen_T2 KnowsCG_T2, by(SchoolID)  

label var t05 "Year Born"
label var t07 "Year Start Teaching"
label var t07 "Year Start School"
label var t10 "Private Exp."
label var t21 "Travel Time"
label var higher_degree "Higher Ed."
label var t23 "Conpensation"

compress
save "$base_out/3 Endline/School/TeacherAverage.dta", replace

***********************************
***********************************************************
***********************************************************
******** Find average per grade, subject *************************
**********************************************************
clear
save "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", replace empty

use "$base_out/Consolidated/Teacher.dta", clear
foreach grade in 1 2 3{
foreach sub in M E K{
replace SBJ_`sub'_GRD`grade'_T3=SBJ_`sub'_GRD`grade'_T1 if SBJ_`sub'_GRD`grade'_T3==.
}
}
recode t66* (2=0) (3=.)

gen Grade1=0
replace Grade1=1 if SBJ_M_GRD1_T3==1 | SBJ_K_GRD1_T3==1 |SBJ_E_GRD1_T3==1 | SBJ_O_GRD1_T3==1

gen Grade2=0
replace Grade2=1 if SBJ_M_GRD2_T3==1 | SBJ_K_GRD2_T3==1 |SBJ_E_GRD2_T3==1 | SBJ_O_GRD2_T3==1

gen Grade3=0
replace Grade3=1 if SBJ_M_GRD3_T3==1 | SBJ_K_GRD3_T3==1 |SBJ_E_GRD3_T3==1 | SBJ_O_GRD3_T3==1

gen GradeOther=0
replace GradeOther=1 if SBJ_M_GRD4_T3==1 | SBJ_K_GRD4_T3==1 |SBJ_E_GRD4_T3==1 | SBJ_O_GRD4_T3==1 | SBJ_M_GRD5_T3==1 | SBJ_K_GRD5_T3==1 |SBJ_E_GRD5_T3==1 | SBJ_O_GRD5_T3==1 | SBJ_M_GRD6_T3==1 | SBJ_K_GRD6_T3==1 |SBJ_E_GRD6_T3==1 | SBJ_O_GRD6_T3==1  | SBJ_M_GRD7_T3==1 | SBJ_K_GRD7_T3==1 |SBJ_E_GRD7_T3==1 | SBJ_O_GRD7_T3==1

gen Math=0
replace Math=1 if SBJ_M_GRD1_T3==1 | SBJ_M_GRD2_T3==1 | SBJ_M_GRD3_T3==1 | SBJ_M_GRD4_T3==1 | SBJ_M_GRD5_T3==1 | SBJ_M_GRD6_T3==1 | SBJ_M_GRD7_T3==1

gen Kiswahili =0
replace Kiswahili =1 if SBJ_K_GRD1_T3==1 | SBJ_K_GRD2_T3==1 | SBJ_K_GRD3_T3==1 | SBJ_K_GRD4_T3==1 | SBJ_K_GRD5_T3==1 | SBJ_K_GRD6_T3==1 | SBJ_K_GRD7_T3==1

gen English=0
replace English=1 if SBJ_E_GRD1_T3==1 | SBJ_E_GRD2_T3==1 | SBJ_E_GRD3_T3==1 | SBJ_E_GRD4_T3==1 | SBJ_E_GRD5_T3==1 | SBJ_E_GRD6_T3==1 | SBJ_E_GRD7_T3==1

gen Other=0
replace Other=1 if SBJ_O_GRD1_T3==1 | SBJ_O_GRD2_T3==1 | SBJ_O_GRD3_T3==1 | SBJ_O_GRD4_T3==1 | SBJ_O_GRD5_T3==1 | SBJ_O_GRD6_T3==1 | SBJ_O_GRD7_T3==1


foreach grade in 1 2 3 Other{
	foreach subject in Math Kiswahili English Other{
	preserve
	collapse (count) NumbTeachers=male (mean) male t05 t07 t08 t10 t21 higher_degree t23 t66, by(SchoolID Grade`grade' `subject')
	drop if Grade`grade'==0
	drop if `subject'==0
	gen str5 Geade="`grade'"
	gen Subject="`subject'"
	drop Grade`grade' `subject'
	append using "$base_out/Consolidated/TeacherAverage_GradeSubject.dta" 
	save "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", replace
	restore
	}
}
clear all
use "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", clear


drop if Geade=="Other"
destring Geade, replace
rename Geade stdgrd_T3
drop if Subject=="Other"
drop if SchoolID==.
reshape wide NumbTeachers male t05 t07 t08 t10 t21 higher_degree t23 t66, i(SchoolID stdgrd_T3) j(Subject) string

replace NumbTeachersEnglish=0 if NumbTeachersEnglish==.   
replace NumbTeachersKiswahili=0 if NumbTeachersKiswahili==.   
replace NumbTeachersMath=0 if NumbTeachersMath==.   

foreach subject in Math Kiswahili English{
gen NoTeacher`subject'=(NumbTeachers`subject'==0)
foreach var in male t05 t07 t08 t10 t21 higher_degree t23{
replace `var'`subject'=0 if NoTeacher`subject'==1
}
}

save "$base_out/Consolidated/TeacherAverage_GradeSubject_Merge.dta", replace




*****************************************
*****************************************
************************* 2014 ENDLINE***
*****************************************
*****************************************


use "$basein/8 Endline 2014/Final Data/Teacher/R6Teacher_noPII.dta", clear

gen NewTechnique=.
replace NewTechnique=1 if tntmyn==1
replace NewTechnique=0 if tntmyn==2
label variable 	NewTechnique "Start using new teaching technique this year"


gen splitAbility=.
replace splitAbility=1 if  ictkyn==1
replace splitAbility=0 if  ictkyn==2
label variable 	splitAbility "Split students according to ability"

gen EcnourageTopHelp=.
replace EcnourageTopHelp=1 if  stspyn==1
replace EcnourageTopHelp=0 if  stspyn==2
label variable 	EcnourageTopHelp "Encourage top students to help those behind"

gen InServiceTraining=.
replace InServiceTraining=1 if  t55==1
replace InServiceTraining=0 if  t55==2
label variable 	InServiceTraining "In services training this year"

gen OtherTraining=.
replace OtherTraining=1 if  t56==1
replace OtherTraining=0 if  t56==2
label variable 	OtherTraining "Other training this year"

gen SupportKG=.
replace SupportKG=1 if  kspprt==1
replace SupportKG=0 if  kspprt==2
label variable 	SupportKG "Support KG"

gen ResourcesImproveTeaching=.
replace ResourcesImproveTeaching=1 if  t61==1
replace ResourcesImproveTeaching=0 if  t61==2
label variable 	ResourcesImproveTeaching "Use resources to improve teaching"

gen DaysAbsent=.
replace DaysAbsent=1 if  t36>=1
replace DaysAbsent=0 if  t36==0
label variable 	DaysAbsent "Days absent last term"

replace missedClasses=0 if missedClasses==2

recode *tme(-99=.) (-98=.) (99=.) (98=.) 
gen TimeAtSchool=(tdptme- tartme)*15/60
label variable 	TimeAtSchool "Time at school (Hrs)"

gen TeachingCareer=.
replace TeachingCareer=1 if  t66==1
replace TeachingCareer=0 if  t66==2
label variable 	TeachingCareer "Would you choose a teaching career"


save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace


use "$basein/8 Endline 2014/Final Data/Teacher/R6Time_noPII.dta", clear
reshape wide t27, i(SchoolID R6TeacherID) j(R6TimeID)
 
merge 1:1 SchoolID R6TeacherID using "$base_out/8 Endline 2014/Teacher/Teacher.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace


label variable t271 "Preparing class (Mins)"
label variable t272 "Grading homework (Mins)"
label variable t273 "Grading tests (Mins)"
label variable t274 "Getting children to school (Mins)"
label variable t275 "Regular classes (Mins)"
label variable t276 "Extra classes (Mins)"
label variable t277 "Socializing (Mins)"
recode t271- t277 (-99=.) (-98=.) (99=.) (98=.) 
rename t23 Compensation_2014
rename t24 Compensation_2013
rename t25 Compensation_2012

gen MoreInputs=(tainyn==1) & !missing(tainyn)
gen teaching_inputs=(trtgti<=3) & !missing(trtgti)
label variable teaching_inputs "Above average teaching inputs"

gen teaching_assistance=1 if trtgas<=3 & trtgas!=.
replace teaching_assistance=0 if trtgas>3 & trtgas!=.
label variable teaching_assistance "Above average teaching assistance"


gen Help_head=1 if trtght<=3 & trtght!=.
replace Help_head=0 if trtght>3 & trtght!=.
label variable Help_head "Above average help from head teacher"  

gen Help_otherTeachers=1 if trtgot<=3 & trtgot!=.
replace Help_otherTeachers=0 if trtgot>3 & trtgot!=.
label variable Help_otherTeachers "Above average help from other teachers"  

gen Help_Community=1 if trtgpt<=3 & trtgpt!=.
replace Help_Community=0 if trtgpt>3 &trtgpt!=.
label variable Help_Community "Above average help from community"    


compress
save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace


import delimited "$basein/7 Monitoring 2014/Data/R5GrdSub.csv", clear 
keep gradeid subjectid subject egrdsubid
rename egrdsubid fgradesubjectid
merge 1:m fgradesubjectid using "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSub3_noPII.dta"
drop if _merge==1
drop _merge
gen FGS=1
gen test= notest
replace test=. if notest==-98 | notest==-95
sum test,d
replace test=r(p99) if test>r(p99) & !missing(test)
label variable test "test in school year"

gen HWWeekly=.
replace HWWeekly=1 if nohmwk<=3 & !missing(nohmwk)
replace HWWeekly=0 if nohmwk>=3 & !missing(nohmwk)
label variable HWWeekly "HW at least once a week"	 

gen HelpTeacher=.
replace HelpTeacher=1 if tcspsal==1 & !missing(tcspsal)
replace HelpTeacher=0 if tcspsal==2 & !missing(tcspsal)
label variable HelpTeacher "Help from other teacher"	 

gen tutoring=.
replace tutoring=1 if ttutyn==1
replace tutoring=0 if ttutyn==2
label variable tutoring "Offers private tutoring"	 

gen remedial=.
replace remedial=1 if tremyn==1 
replace remedial=0 if tremyn==2
label variable remedial "Offers remedial teaching"	  


keep HWWeekly HelpTeacher test tutoring remedial SchoolID R6TeacherID R6TGrdSub3ID fgradesubjectid subject gradeid FGS

collapse (mean) HWWeekly HelpTeacher  (median) test  (max) FGS tutoring remedial, by(SchoolID R6TeacherID)

rename * =_F_sub_F_yrs
rename SchoolID_F_sub_F_yrs SchoolID
rename FGS_F_sub_F_yrs FGS
rename R6TeacherID_F_sub_F_yrs R6TeacherID

merge 1:1 SchoolID R6TeacherID using "$base_out/8 Endline 2014/Teacher/Teacher.dta", update replace
drop _merge
compress
save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace

import delimited "$basein/7 Monitoring 2014/Data/R5GrdSub.csv", clear 
keep gradeid subjectid subject egrdsubid
rename egrdsubid tgradesubjectid
merge 1:m tgradesubjectid using  "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSub_noPII.dta"
drop if _merge==1
gen MovedToFGS=(grbgyn==1)
collapse (max) MovedToFGS, by(SchoolID R6TeacherID)
merge 1:1 SchoolID R6TeacherID using "$base_out/8 Endline 2014/Teacher/Teacher.dta"
replace MovedToFGS=0 if MovedToFGS==.
drop _merge
compress
save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace

import delimited "$basein/7 Monitoring 2014/Data/R5GrdSub.csv", clear 
keep gradeid subjectid subject egrdsubid
rename egrdsubid xgradesubjectid
merge 1:m xgradesubjectid using  "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSubExit_noPII.dta"
drop if _merge==1
gen MovedOutFGS=(grstyn==1)
collapse (max) MovedOutFGS, by(SchoolID R6TeacherID)
merge 1:1 SchoolID R6TeacherID using "$base_out/8 Endline 2014/Teacher/Teacher.dta"
replace MovedOutFGS=0 if MovedOutFGS==.
drop _merge
compress


gen ind=(MovedOutFGS==1 & MovedToFGS==1)
replace MovedOutFGS=0 if ind==1
replace MovedToFGS=0 if ind==1
drop ind

save "$base_out/8 Endline 2014/Teacher/Teacher.dta", replace


rename * =_T7
rename R6TeacherID_T7 R6TeacherID
rename SchoolID_T7 SchoolID

merge 1:m SchoolID R6TeacherID using "$base_out/Consolidated/Teacher.dta"
drop _merge

compress
save "$base_out/Consolidated/Teacher.dta", replace

exit

preserve
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID) update replace
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
reg MovedOutFGS_T3 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
reg MovedToFGS_T3 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
reg MovedOutFGS_T7 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
reg MovedToFGS_T7 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
restore


preserve
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID) update replace
drop if _merge!=3
drop _merge
collapse (sum) FGS_T7, by(SchoolID DistID  treatment treatarm)
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
reg FGS_T7 TreatmentCG TreatmentCOD TreatmentBoth i.DistID, vce(cluster SchoolID)
restore


***********************************
***********************************************************
***********************************************************
******** Find average per school *************************
**********************************************************
use "$base_out/Consolidated/Teacher.dta", clear 

recode t66 (2=0) (3=.)


keep male TeacherIndexRecall t05 t07 t08 t10 t21 higher_degree t23 t24 t25 t66 ///
remedial_F_sub_F_yrs_T3  ///
tutoring_F_sub_F_yrs_T3  ///
test_F_sub_F_yrs_T3 ///
t271_T3 t272_T3 t273_T3 t274_T3 t275_T3 t276_T3 t277_T3 ///
Inputs_Missing_T3 missedClasses_T3 splitAbility_T3 EcnourageTopHelp_T3 TimeAtSchool_T3 TeachingCareer_T3 teaching_inputs_T3 ///
teaching_assistance_T3 Help_head_T3 Help_otherTeachers_T3 Help_Community_T3 correct_earnings_T2 knows_NoPen_T2 KnowsCG_T2 SchoolID
replace t10=0 if t10==2

collapse (mean) male TeacherIndexRecall t05 t07 t08 t10 t21 higher_degree t23 t24 t25 t66 ///
remedial_F_sub_F_yrs_T3  ///
tutoring_F_sub_F_yrs_T3  ///
test_F_sub_F_yrs_T3 ///
t271_T3 t272_T3 t273_T3 t274_T3 t275_T3 t276_T3 t277_T3 ///
Inputs_Missing_T3 missedClasses_T3 splitAbility_T3 EcnourageTopHelp_T3 TimeAtSchool_T3 TeachingCareer_T3 teaching_inputs_T3 ///
teaching_assistance_T3 Help_head_T3 Help_otherTeachers_T3 Help_Community_T3 correct_earnings_T2 knows_NoPen_T2 KnowsCG_T2, by(SchoolID)  

label var t05 "Year Born"
label var t07 "Year Start Teaching"
label var t07 "Year Start School"
label var t10 "Private Exp."
label var t21 "Travel Time"
label var higher_degree "Higher Ed."
label var t23 "Conpensation 2014"
label var t24 "Conpensation 2013"
label var t25 "Conpensation 2012"

compress
save "$base_out/3 Endline/School/TeacherAverage.dta", replace

***********************************
***********************************************************
***********************************************************
******** Find average per grade, subject *************************
**********************************************************
clear
save "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", replace empty

use "$base_out/Consolidated/Teacher.dta", clear
foreach grade in 1 2 3{
foreach sub in M E K{
replace SBJ_`sub'_GRD`grade'_T3=SBJ_`sub'_GRD`grade'_T1 if SBJ_`sub'_GRD`grade'_T3==.
}
}
recode t66* (2=0) (3=.)
gen Grade1=0
replace Grade1=1 if SBJ_M_GRD1_T3==1 | SBJ_K_GRD1_T3==1 |SBJ_E_GRD1_T3==1 | SBJ_O_GRD1_T3==1

gen Grade2=0
replace Grade2=1 if SBJ_M_GRD2_T3==1 | SBJ_K_GRD2_T3==1 |SBJ_E_GRD2_T3==1 | SBJ_O_GRD2_T3==1

gen Grade3=0
replace Grade3=1 if SBJ_M_GRD3_T3==1 | SBJ_K_GRD3_T3==1 |SBJ_E_GRD3_T3==1 | SBJ_O_GRD3_T3==1

gen GradeOther=0
replace GradeOther=1 if SBJ_M_GRD4_T3==1 | SBJ_K_GRD4_T3==1 |SBJ_E_GRD4_T3==1 | SBJ_O_GRD4_T3==1 | SBJ_M_GRD5_T3==1 | SBJ_K_GRD5_T3==1 |SBJ_E_GRD5_T3==1 | SBJ_O_GRD5_T3==1 | SBJ_M_GRD6_T3==1 | SBJ_K_GRD6_T3==1 |SBJ_E_GRD6_T3==1 | SBJ_O_GRD6_T3==1  | SBJ_M_GRD7_T3==1 | SBJ_K_GRD7_T3==1 |SBJ_E_GRD7_T3==1 | SBJ_O_GRD7_T3==1

gen Math=0
replace Math=1 if SBJ_M_GRD1_T3==1 | SBJ_M_GRD2_T3==1 | SBJ_M_GRD3_T3==1 | SBJ_M_GRD4_T3==1 | SBJ_M_GRD5_T3==1 | SBJ_M_GRD6_T3==1 | SBJ_M_GRD7_T3==1

gen Kiswahili =0
replace Kiswahili =1 if SBJ_K_GRD1_T3==1 | SBJ_K_GRD2_T3==1 | SBJ_K_GRD3_T3==1 | SBJ_K_GRD4_T3==1 | SBJ_K_GRD5_T3==1 | SBJ_K_GRD6_T3==1 | SBJ_K_GRD7_T3==1

gen English=0
replace English=1 if SBJ_E_GRD1_T3==1 | SBJ_E_GRD2_T3==1 | SBJ_E_GRD3_T3==1 | SBJ_E_GRD4_T3==1 | SBJ_E_GRD5_T3==1 | SBJ_E_GRD6_T3==1 | SBJ_E_GRD7_T3==1

gen Other=0
replace Other=1 if SBJ_O_GRD1_T3==1 | SBJ_O_GRD2_T3==1 | SBJ_O_GRD3_T3==1 | SBJ_O_GRD4_T3==1 | SBJ_O_GRD5_T3==1 | SBJ_O_GRD6_T3==1 | SBJ_O_GRD7_T3==1
preserve
foreach grade in 1 2 3 Other{
foreach subject in Math Kiswahili English Other{
collapse (count) NumbTeachers=male (mean) male t05 t07 t08 t10 t21 higher_degree t23 t66, by(SchoolID Grade`grade' `subject')
drop if Grade`grade'==0
drop if `subject'==0
gen str5 Geade="`grade'"
gen Subject="`subject'"
drop Grade`grade' `subject'
append using "$base_out/Consolidated/TeacherAverage_GradeSubject.dta" 
save "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", replace
restore, preserve
}
}
clear all
use "$base_out/Consolidated/TeacherAverage_GradeSubject.dta", clear


drop if Geade=="Other"
destring Geade, replace
rename Geade stdgrd_T3
drop if Subject=="Other"
drop if SchoolID==.
reshape wide NumbTeachers male t05 t07 t08 t10 t21 higher_degree t23 t66, i(SchoolID stdgrd_T3) j(Subject) string

replace NumbTeachersEnglish=0 if NumbTeachersEnglish==.   
replace NumbTeachersKiswahili=0 if NumbTeachersKiswahili==.   
replace NumbTeachersMath=0 if NumbTeachersMath==.   

foreach subject in Math Kiswahili English{
gen NoTeacher`subject'=(NumbTeachers`subject'==0)
foreach var in male t05 t07 t08 t10 t21 higher_degree t23{
replace `var'`subject'=0 if NoTeacher`subject'==1
}
}

save "$base_out/Consolidated/TeacherAverage_GradeSubject_Merge.dta", replace


***********************************
***********************************************************
***********************************************************
******** Find if same teacher does math, english, etc *************************
**********************************************************
use "$basein/8 Endline 2014/Final Data/Teacher/R6GroupSubject_noPII.dta", clear
drop gpstch whynot whynot_other
reshape wide gpmtch, i( SchoolID R6GradeID R6GroupID ) j( R6GroupSubjectID)
gen SameTeacherEngMath=(gpmtch1==gpmtch3)
gen SameTeacherEngSwahili=(gpmtch3==gpmtch2)
gen SameTeacherSwahiliMath=(gpmtch1==gpmtch2)
gen SameTeacherAll=(gpmtch1==gpmtch2 & gpmtch2==gpmtch3)
keep SchoolID R6GradeID R6GroupID SameTeacherEngMath SameTeacherEngSwahili SameTeacherSwahiliMath
rename R6GroupID stdgrp_T7
rename R6GradeID GradeID_T7
save "$base_out/Consolidated/TeachingSchedulle_T7.dta", replace



***********************************
***********************************************************
**********Teacher Abseentesim *************************
**********************************************************


use "$basein/2 Midline/MTeacher_noPII.dta", clear
 
recode atttch (2=0) 
*collapse (mean) atttch,by( SchoolID)
keep atttch SchoolID upid
rename atttch atttch_T2
drop if upid==""
rename upid upidold
capture drop _merge
save "$base_out/Consolidated/TAttendace.dta", replace

use "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", clear
drop if TeacherID==.
keep DistrictID SchoolID TeacherID ETTeacherID upid tchsex upidold R6TeacherID 
merge 1:1 SchoolID upid using "$basein/7 Monitoring 2014/Data/School and Teacher/R5Teacher_noPII.dta"
drop if _merge!=3
drop _merge

recode attbok atttch attcls (2=0)
recode attact (-99=.) (2=1) (3=0)
replace attcls=0 if atttch==0
replace attact=0 if atttch==0 | attcls==0
label var attact "Teaching"
gen OffTask=1-(attact==1 | attact2==2 | attact2==3 | attact2==4) if !missing(attact)
label var OffTask "Off-task"
gen conditionalattcls=attcls if atttch==1
gen conditionalattact=attact if attcls==1

*ollapse (mean) attbok atttch attcls attact conditionalattcls conditionalattact OffTask,by(SchoolID)
keep attbok atttch attcls attact conditionalattcls conditionalattact OffTask SchoolID upid upidold
rename * =_T6
rename SchoolID_T6 SchoolID
rename upid_T6 upid
rename upidold_T6 upidold
drop if upid==""
merge 1:1 SchoolID upidold  using "$base_out/Consolidated/TAttendace.dta"
drop _merge
capture drop _merge
drop if upid==""
*collapse (mean) atttch_T2 atttch_T6, by(SchoolID)
save "$base_out/Consolidated/TAttendace.dta", replace

/*
use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID  using "$base_out/Consolidated/TAttendace.dta"
drop if _merge!=3
drop _merge
drop if upid==""
merge 1:m SchoolID upidold using "$base_out/Consolidated/Teacher.dta", keepus (t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t66  missedClasses tchsex)
drop if _merge!=3
drop _merge
save "$base_out/Consolidated/TAttendace.dta", replace


recode t66 missedClasses (3=.) (2=0) (1=1) 

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" 
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

label var attbok_T6 "Sign into attendance book"
label var atttch_T6 "At the school"
label var atttch_T2 "At the school"
label var attcls_T6 "In a classroom"
label var attact_T6 "Activily teaching"
label var OffTask_T6 "Off-Task"
reghdfe atttch_T2 TreatmentCG##t66 TreatmentCOD##t66 TreatmentBoth##t66 , a(DistID)


foreach var in  atttch_T6   atttch_T2{
reghdfe `var' TreatmentCG##t66 TreatmentCOD##t66 TreatmentBoth##t66 , a(DistID) vce(cluster SchoolID)
}

foreach var in  atttch_T6  atttch_T2{
reghdfe `var' TreatmentCG##missedClasses TreatmentCOD##missedClasses TreatmentBoth##missedClasses t05 t07 t10, a(DistID) vce(cluster SchoolID)
}


foreach var in  atttch_T6   atttch_T2{
reghdfe `var' TreatmentCG##t66 TreatmentCOD##t66 TreatmentBoth##t66 t05 t07 t10, a(DistID) vce(cluster SchoolID)
}

foreach var in  atttch_T6  atttch_T2{
reghdfe `var' TreatmentCG TreatmentCOD TreatmentBoth t05 t07 t10 tchsex t21 , a(DistID) vce(cluster SchoolID)
}


reghdfe attcls_T6 TreatmentCG TreatmentCOD TreatmentBoth if atttch_T6==1, a(DistID)
reghdfe attact_T6  TreatmentCG TreatmentCOD TreatmentBoth if atttch_T6==1 & attcls_T6==1, a(DistID)
*/

