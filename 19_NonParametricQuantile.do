clear mata
clear all
set matsize 1100
set seed 1234
local it = 1000

use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear


keep Z_*_T3 Z_*_T7 SchoolID treatment treatarm  GradeID_T*
save "$base_out\Consolidated\Student_Quantile_NP.dta", replace




foreach time in T3 T7 {
	foreach treat in COD Both CG{
		foreach subject in Z_kiswahili Z_kiingereza Z_hisabati Z_ScoreFocal {
			foreach grade in  4 {
				if("`time'"!="T8" | "`treat'"!="CG"){
					use "$base_out\Consolidated\Student_Quantile_NP.dta", clear

					drop if (treatment!="`treat'" & treatment!="Control")
					if(`grade'!=4){
						drop if GradeID_`time'!=`grade'
					}

					keep treatment SchoolID `subject'_`time' 
					compress
					save "$base_out\temp\TempBoot_Pass", replace


					qui egen kernel_range = fill(.01(.01)1)
					qui replace kernel_range = . if kernel_range>1
					mkmat kernel_range if kernel_range != .
					matrix diff = kernel_range
					matrix x = kernel_range


					quietly forvalues j = 1(1)`it' {
					use "$base_out\temp\TempBoot_Pass", clear

					bsample, strata(treatment) cluster(SchoolID)

					bysort treatment: egen rank = rank(`subject'_`time'), unique
					bysort treatment: egen max_rank = max(rank)
					bysort treatment: gen percentile = rank/max_rank 
						
					drop max_rank rank

					egen kernel_range = fill(.01(.01)1)
					qui replace kernel_range = . if kernel_range>1

					*regressing endline scores on percentile rankings
					lpoly `subject'_`time' percentile if treatment=="Control" , gen(xcon pred_con) at (kernel_range) nograph
					lpoly `subject'_`time' percentile if treatment=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph
						
					mkmat pred_tre if pred_tre != . 
					mkmat pred_con if pred_con != . 
					matrix diff = diff, pred_tre - pred_con
						

					}
					matrix diff = diff'

					*each variable is a percentile that is being estimated (can sort by column to get 2.5th and 97.5th confidence interval)
					svmat diff
					keep diff* 

					matrix conf_int = J(100, 2, 100)
					qui drop if _n == 1

					*sort each column (percentile) and saving 25th and 975th place in a matrix
					forvalues i = 1(1)100{
					sort diff`i'
					matrix conf_int[`i', 1] = diff`i'[0.025*`it']
					matrix conf_int[`i', 2] = diff`i'[0.975*`it']	
					}


					*******************Graphs for control, treatment, and difference using actual data (BASELINE)*************************************
					use "$base_out\temp\TempBoot_Pass", clear
					*local subject Z_kiswahili
					*local time T7
					bysort treatment: egen rank = rank(`subject'_`time'), unique
					bysort treatment: egen max_rank = max(rank)
					bysort treatment: gen percentile = rank/max_rank 


					  
					egen kernel_range = fill(.01(.01)1)
					qui replace kernel_range = . if kernel_range>1

					lpoly `subject'_`time' percentile if treatment=="Control" , gen(xcon pred_con) at (kernel_range) nograph
					lpoly `subject'_`time' percentile if treatment=="`treat'" , gen(xtre pred_tre) at (kernel_range) nograph

					gen diff = pred_tre - pred_con

					*variables for confidence interval bands
					svmat conf_int

		
					if "`time'"=="T7" local name "Year 2"
					else if "`time'"=="T3" local name "Year 1"

					if "`time'"=="T8" local name2 "2"
					else if "`time'"=="B_T7"  local name2 "2"
					else if "`time'"=="T7" local name2 "2"
					else if "`time'"=="T3" local name2 "1"

					if "`subject'"=="Z_kiswahili" local name3 Swahili
					else if "`subject'"=="Z_kiingereza"  local name3 English	
					else if "`subject'"=="Z_hisabati" local name3 Math

					if "`grade'"=="1" local name4 1
					else if "`grade'"=="2"  local name4 2	
					else if "`grade'"=="3" local name4 3
					else if "`grade'"=="4" local name4 All

					save "$base_out\Consolidated\Lowess_Quantile_`subject'_`treat'_`time'_`grade'.dta", replace



					graph twoway (line pred_con xcon, lcolor(blue) lpattern("--.....") legend(lab(1 "Control"))) ///
					(line pred_tre xtre, lcolor(red) lpattern(longdash) legend(lab(2 "Treatment"))) ///
					(line diff xcon, lcolor(black) lpattern(solid) legend(lab(3 "Difference"))) ///
					(line conf_int1 xcon, lcolor(black) lpattern(shortdash) legend(lab(4 "95% Confidence Band"))) ///
					(line conf_int2 xcon, lcolor(black) lpattern(shortdash) legend(lab(5 "95% Confidence Band"))) ///
					,yline(0, lcolor(gs10)) xtitle(Percentile of Endline Score) ytitle(`name' Endline Score) legend(order(1 2 3 4))
					
					graph export "$graphs/Lowess_Quantile_`subject'_`treat'_`time'_`grade'.pdf", as(pdf) replace

				}
			}
		}
	}
}
