
use "$base_out/3 Endline/Household/Household_Expenditure.dta", clear
merge m:1 HHID  using "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", keepus(SchoolID)
drop if _merge==2
drop if smschl==2
drop if smschl==.
collapse (mean) expn13, by(SchoolID lvledu)
rename expn13 expn13_T3
save "$base_out/Consolidated/Household_Expenditure.dta", replace

use "$base_out/8 Endline 2014/Household/Household_Expenditure.dta", clear
drop if smschl==2
drop if smschl==.
collapse (mean) expn13, by(SchoolID lvledu)
rename expn13 expn13_T7
merge 1:1  SchoolID lvledu using "$base_out/Consolidated/Household_Expenditure.dta"
drop _merge


merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(SchoolID treatment treatarm DistID)
drop if _merge!=3
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolExpenditure_HH.dta", keepus($schoolcontrol)
drop _merge
drop if lvledu==.
drop if treatment==""
drop if DistID==.
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
recode lvledu (-99/-1=.)
keep if lvledu>=11 & lvledu<=17
gen FG=(lvledu>=11 & lvledu<=13)




preserve
keep expn13_T* SchoolID DistID $schoolcontrol $treatmentlist treatment lvledu
reshape long expn13_T@ , i(SchoolID lvledu) j(T)
drop if T!=3 & T!=7
gen FG=(lvledu>=11 & lvledu<=13)
eststo exp_hh_members_0: reg expn13_T c.($treatmentlist) (c.($schoolcontrol)  i.DistID)##T if FG==0, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum expn13_T if treatment=="Control" & FG==0
estadd scalar ymean2=r(mean)

eststo exp_hh_members_1: reg expn13_T c.($treatmentlist) (c.($schoolcontrol)  i.DistID)##T if FG==1, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum expn13_T if treatment=="Control" & FG==1
estadd scalar ymean2=r(mean)

rename TreatmentCG TreatmentCG2
rename TreatmentCOD TreatmentCOD2
rename TreatmentBoth TreatmentBoth2
eststo exp_hh_members_2: reg expn13_T c.($treatmentlist)##c.FG c.FG##(c.($schoolcontrol)  i.DistID)##T, vce(cluster SchoolID)
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]
sum expn13_T if treatment=="Control" & FG==1
scalar def mean1=r(mean)
sum expn13_T if treatment=="Control" & FG==0
scalar def mean2=r(mean)
estadd scalar ymean2=scalar(mean1)-scalar(mean2)


esttab exp_hh_members_0 exp_hh_members_1 exp_hh_members_2  using "$latexcodesfinals/HH_Member_Expenditure.tex", se booktabs label fragment nolines nogaps nomtitles ///
rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))

esttab exp_hh_members_0 exp_hh_members_1 exp_hh_members_2  using "$latexcodesfinals/HH_Member_Expenditure.csv", se label fragment nolines nogaps nomtitles ///
rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))
restore



/*
reg expn13_T3 c.($treatmentlist) i.lvledu i.DistID $schoolcontrol
reg expn13_T7 c.($treatmentlist) i.lvledu i.DistID $schoolcontrol

reg expn13_T3 c.($treatmentlist)#lvledu i.lvledu i.DistID $schoolcontrol
reg expn13_T7 c.($treatmentlist)#lvledu i.lvledu i.DistID $schoolcontrol

foreach time in T3 T7{
reg expn13_`time' c.($treatmentlist)#lvledu i.lvledu i.DistID c.($schoolcontrol)
coefplot , keep(*c.TreatmentBoth*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(11.lvledu#c.TreatmentBoth="1" 12.lvledu#c.TreatmentBoth="2" 13.lvledu#c.TreatmentBoth="3" 14.lvledu#c.TreatmentBoth="4" 15.lvledu#c.TreatmentBoth="5" ///
16.lvledu#c.TreatmentBoth="6" 17.lvledu#c.TreatmentBoth="7")
graph export "$graphs/ExpenditureGrade_Both_`time'.pdf", replace

coefplot , keep(*c.TreatmentCOD*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(11.lvledu#c.TreatmentCOD="1" 12.lvledu#c.TreatmentCOD="2" 13.lvledu#c.TreatmentCOD="3" 14.lvledu#c.TreatmentCOD="4" 15.lvledu#c.TreatmentCOD="5" ///
16.lvledu#c.TreatmentCOD="6" 17.lvledu#c.TreatmentCOD="7")
graph export "$graphs/ExpenditureGrade_COD_`time'.pdf", replace


coefplot , keep(*c.TreatmentCG*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(11.lvledu#c.TreatmentCG="1" 12.lvledu#c.TreatmentCG="2" 13.lvledu#c.TreatmentCG="3" 14.lvledu#c.TreatmentCG="4" 15.lvledu#c.TreatmentCG="5" ///
16.lvledu#c.TreatmentCG="6" 17.lvledu#c.TreatmentCG="7")
graph export "$graphs/ExpenditureGrade_CG_`time'.pdf", replace

}





foreach time in T3 T7{
reg expn13_`time' c.($treatmentlist)#FG  (i.DistID c.($schoolcontrol))##FG
coefplot , keep(*c.TreatmentBoth*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(1.FG#c.TreatmentBoth="1-3" 0.FG#c.TreatmentBoth="4-7")
graph export "$graphs/ExpenditureGrade_FG_Both_`time'.pdf", replace

coefplot , keep(*c.TreatmentCOD*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(1.FG#c.TreatmentCOD="1-3" 0.FG#c.TreatmentCOD="4-7")
graph export "$graphs/ExpenditureGrade_FG_COD_`time'.pdf", replace


coefplot , keep(*c.TreatmentCG*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(1.FG#c.TreatmentCG="1-3" 0.FG#c.TreatmentCG="4-7")
graph export "$graphs/ExpenditureGrade_FG_CG_`time'.pdf", replace

coefplot , keep(*c.TreatmentBoth* *c.TreatmentCOD* *c.TreatmentCG*) graphregion(color(white)) baselevels ci vertical yline(0) xtitle("Grade") ytitle("Effect on expenditure (TSZ)") ///
rename(1.FG#c.TreatmentBoth="Both:1-3" 0.FG#c.TreatmentBoth="Both:4-7" 1.FG#c.TreatmentCOD="COD:1-3" 0.FG#c.TreatmentCOD="COD:4-7" 1.FG#c.TreatmentCG="CG:1-3" 0.FG#c.TreatmentCG="CG:4-7")
graph export "$graphs/ExpenditureGrade_FG_`time'.pdf", replace

}
