*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*This creates the same variables for all HH we ever interview during KFI
import delim "$basein/8 Endline 2014/Reference Data/R_R6_HHData.csv", clear 
keep hhid upid v1
rename hhid HHID
rename v1 SchoolID
merge 1:1 SchoolID HHID using "$basein/8 Endline 2014/Final Data/Household/R6HHData_noPII.dta" 
drop if _merge==1
drop _merge
keep if consnt==1
rename ctcbyn ltcbyn
rename crtctb_1 lstctb_1
rename crtctb_2 lstctb_2
rename crtctb_3 lstctb_3
rename tutcrt tutrng
keep upid DistID HHID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2
*merge 1:m  HHID SchoolID using  "$basein/8 Endline 2014/ID Linkage/R6_Student_HH_ID_Linkage_noPII", keepus(upid)
drop if HHID==.
*drop if _merge==2
*drop _merge
drop upid_student
rename upid upid_student

gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/Consolidated/Household_Endline2014.dta", replace


use "$basein/8 Endline 2014/Final Data/Household/R6HHExpenditure_noPII.dta", clear
rename expn13 expn14
replace expn14=. if R6HHMemberID!=1 
collapse (sum) expn14, by(HHID) 

merge 1:1 HHID using "$base_out/Consolidated/Household_Endline2014.dta"
drop _merge
compress
save "$base_out/Consolidated/Household_Endline2014.dta", replace


use "$basein/8 Endline 2014/Final Data/Household/R6HHMember_noPII.dta", clear
collapse (count) HHSize=R6HHMemberID, by(HHID)
merge 1:1 HHID using "$base_out/Consolidated/Household_Endline2014.dta"
drop _merge
save "$base_out/Consolidated/Household_Endline2014.dta", replace



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID HHSize DistID SchoolID HHSize expn* upidst KnowsTeachers IndexEngagement IndexPoverty IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2
drop prdyyn- tutrng
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
drop _merge
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/Consolidated/Household_Endline2014.dta", replace




****************************************************
****************************************************
****************************************************

use "$basein/6 Baseline 2014/Data/household/R4HHData_noPII.dta", clear 
keep if consnt==1

keep HHID DistID SchoolID ltcbyn upid_student wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof rdngsw rdngen mathqn_1 mathqn_2
gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid_student upidst
compress
save "$base_out/Consolidated/Household_Baseline2014.dta", replace


use "$basein/6 Baseline 2014/Data/household/R4HHExpenditure_noPII.dta", clear

replace expn13=. if R4HHMemberID!=1 
replace expn14=. if R4HHMemberID!=1 
recode 	expn13 expn14 (-99=.) (-98=.)
collapse  (sum) expn13 expn14, by(HHID) 
merge 1:1 HHID using "$base_out/Consolidated/Household_Baseline2014.dta"
drop _merge
compress
save "$base_out/Consolidated/Household_Baseline2014.dta", replace


use "$basein/6 Baseline 2014/Data/household/R4hhMember_noPII.dta", clear
collapse (count) HHSize=R4HHMemberID, by(HHID)
merge 1:1 HHID using "$base_out/Consolidated/Household_Baseline2014.dta"
drop _merge
save "$base_out/Consolidated/Household_Baseline2014.dta", replace

/*
use "$basein/6 Baseline 2014/Data/household/R4HHExpenditure_noPII.dta", clear
merge m:1 SchoolID HHID R4HHMemberID using "$basein/6 Baseline 2014/Data/household/R4hhMember_noPII.dta", keepus(memage inschl lvledu smschl)
drop _merge
recode 	expn13 expn14 (-99=.) (-98=.)
collapse (mean) expn13 expn14
*/

pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID HHSize  DistID SchoolID HHSize expn* upidst KnowsTeachers IndexEngagement IndexPoverty IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
drop _merge
drop prdyyn- tutrng
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/Consolidated/Household_Baseline2014.dta", replace



****************************************************
****************************************************
****************************************************

use "$basein/3 Endline/Household/EHHData_noPII.dta", clear 
keep if consnt==1
rename workyn wrokyn
rename ctcbyn ltcbyn
rename crtctb_1 lstctb_1
rename crtctb_2 lstctb_2
rename crtctb_3 lstctb_3
rename tutcrt tutrng
keep HHID DistID SchoolID ltcbyn  wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst  twknyn twpgsu twpgcg twpgdw twpgsf twpgcd codelt_1 codelt_2 codelt_3 codelt_4 swprof

merge 1:m SchoolID HHID using "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", keepus(upid)
drop if _merge!=3
drop _merge


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}


recode brkfst (3=0) (2=1)


rename upid upidst
compress
save "$base_out/Consolidated/Household_Endline2013.dta", replace


use "$basein/3 Endline/Household/EHHExpenditure_noPII.dta", clear

replace expn13=. if HHMemberID!=1 

by HHID HHMemberID, sort: gen nvals = _n == 1 
by HHID: replace nvals = sum(nvals)
by HHID: replace nvals = nvals[_N] 

collapse  (sum) expn13, by(HHID) 

merge 1:1 HHID using "$base_out/Consolidated/Household_Endline2013.dta"
drop _merge
compress
save "$base_out/Consolidated/Household_Endline2013.dta", replace


use "$basein/3 Endline/Household/EHHMember_noPII.dta", clear
collapse (count) HHSize=EHHMemberID, by(HHID)
merge 1:1 HHID using "$base_out/Consolidated/Household_Endline2013.dta"
drop _merge
save "$base_out/Consolidated/Household_Endline2013.dta", replace



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score


keep HHID HHSize DistID SchoolID expn*  upidst KnowsTeachers IndexEngagement IndexPoverty  wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng

merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
drop _merge
drop prdyyn- tutrng
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/Consolidated/Household_Endline2013.dta", replace





****************************************************
****************************************************
****************************************************

use "$basein/1 Baseline/Household/HHData_noPII.dta", clear 
keep if consnt==1
rename workyn wrokyn
keep HHID  ltcbyn  wrokyn matwll matflr matrof srcwtr srclgh srccok tlttyp tltshr asset_1- asset_8 mobilno bnkacc hsowsh rmsnum rmsslp lndown lndqnt tchrno prdyyn prdylt  lstctb_1 lstctb_2 lstctb_3 shlcev adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng tutprc brkfst swprof rdngsw rdngen mathqn_1 mathqn_2
merge 1:m HHID using "$basein/1 Baseline/Supplementing/StudentIDLinkage_noPII.dta", keepus(SchoolID upid DistrictID)
rename DistrictID DistID
drop if _merge!=3
drop _merge


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"

gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7 | matrof==8
label variable 	roof_durable "Roof made out of a durable material"

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)

gen KnowsTeachers=!(tchrno==-99)


foreach var in ltcbyn prdyyn  wrokyn asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8  bnkacc lndown shlcev adtprt lrntlk tutrng hmwcmp hrshmw hmwhlp_1 hmwhlp_2 hmwhlp_3{
recode `var' (2=0) (-99=.) (3=0) (4=0) (-98=.) (-97=.) (-96=.) (-95=.)
}
foreach var in  mathqn_1 mathqn_2{
recode `var' (2=0) (-99=0) (3=0) (4=0) (-98=0) (-97=0) (-96=0) (-95=0)
}
recode brkfst (3=0) (2=1)

recode rdngsw rdngen (1=1) (2/4=0)


rename upid upidst
compress
save "$base_out/Consolidated/Household_Baseline2013.dta", replace

use "$basein/1 Baseline/Household/HHExpenditure_noPII.dta", clear
collapse (sum) expn12 expn13, by(HHID) 
merge 1:1 HHID using "$base_out/Consolidated/Household_Baseline2013.dta"
drop _merge
save "$base_out/Consolidated/Household_Baseline2013.dta", replace


use "$basein/1 Baseline/Household/HHMember_noPII.dta", clear
collapse (count) HHSize=HHMemberID, by(HHID)
merge 1:1 HHID using "$base_out/Consolidated/Household_Baseline2013.dta"
drop _merge



pca wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 
predict IndexPoverty, score
pca  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng
predict IndexEngagement, score
pca rdngsw rdngen mathqn_1 mathqn_2
predict IndexKowledge, score
keep HHID HHSize DistID SchoolID expn* upidst KnowsTeachers IndexEngagement IndexPoverty IndexKowledge wrokyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8  prdyyn ltcbyn ltcbyn shlcev adtprt hrshmw hmwcmp lrntlk tutrng rdngsw rdngen mathqn_1 mathqn_2

merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatment treatarm)
drop if _merge!=3
drop _merge
drop prdyyn- tutrng
replace upidst="R1"+upidst if substr(upidst,1,3)=="STU"
save "$base_out/Consolidated/Household_Baseline2013.dta", replace



*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
*********************************************************************************************************************************************************************
















*******************************************************
*************BASE LINE
********************************************************



use "$basein/1 Baseline/Household/HHData_noPII.dta", clear
rename dstrct DistID 
drop if consnt!=1
save "$base_out/1 Baseline/Household/Household.dta", replace




use "$basein/1 Baseline/Household/HHExpenditure_noPII.dta", clear
recode expn12 expn13 (-99/-1=.)
reshape wide expn12 expn13, i(HHID) j(HHExpenditureID)


label variable expn121 "Expenditure in 2012 on school fees"
label variable expn122 "Expenditure in 2012 on textbooks"
label variable expn123 "Expenditure in 2012 on other books"
label variable expn124 "Expenditure in 2012 on supplies"
label variable expn125 "Expenditure in 2012 on uniforms"
label variable expn126 "Expenditure in 2012 on tutoring"
label variable expn127 "Expenditure in 2012 on transport"
label variable expn128 "Expenditure in 2012 on food"
label variable expn129 "Expenditure in 2012 on others"

label variable expn131 "Expenditure in 2013 on school fees"
label variable expn132 "Expenditure in 2013 on textbooks"
label variable expn133 "Expenditure in 2013 on other books"
label variable expn134 "Expenditure in 2013 on supplies"
label variable expn135 "Expenditure in 2013 on uniforms"
label variable expn136 "Expenditure in 2013 on tutoring"
label variable expn137 "Expenditure in 2013 on transport"
label variable expn138 "Expenditure in 2013 on food"
label variable expn139 "Expenditure in 2013 on others"
egen Expenditure_FC_2012=rowtotal(expn121 expn122 expn123 expn124 expn125 expn126 expn127 expn128 expn129), missing
egen Expenditure_FC_2013=rowtotal(expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139), missing
label variable 	Expenditure_FC_2012 "FC education expenditure on 2012"
label variable 	Expenditure_FC_2013 "FC education expenditure on 2013"
merge 1:1 HHID using "$base_out/1 Baseline/Household/Household.dta"
drop _merge
compress
save "$base_out/1 Baseline/Household/Household.dta", replace

use "$basein/1 Baseline/Household/HHMember_noPII.dta", clear

collapse (count)  HHMemberID , by(HHID)
rename HHMemberID NumHHMembers
label variable 	NumHHMembers "Household size"

merge 1:1 HHID using "$base_out/1 Baseline/Household/Household.dta"
drop _merge

save "$base_out/1 Baseline/Household/Household.dta", replace

use "$basein/1 Baseline/Household/HHMember_noPII.dta", clear
replace hstedu=. if hstedu==-95
replace hstedu=. if hstedu==-99

gen LevelEdu=0 if (hstedu==1 | hstedu==2)
replace LevelEdu=1 if (hstedu<=16 & hstedu>=11) 
replace LevelEdu=2 if hstedu==17 | hstedu==18 | hstedu==19
replace LevelEdu=3 if (hstedu<=22 & hstedu>=20)
replace LevelEdu=4 if (hstedu==23 | hstedu==24)
replace LevelEdu=5 if (hstedu==25 | hstedu==26 | hstedu==27)
replace LevelEdu=6 if (hsted>=28)

label define education_groups 0 "No Primary" 1 "Incomplete Primary" 2 "Complete Primary" 3 "Incomplete Secondary" 4 "Complete Ordinary Secondary" 5 "Advance Secondary" 6 "Post-Secondary Education"
label values LevelEdu education_groups
drop if relchd!=2 & relchd!=3 & relchd!=4 & relchd!=5 & relchd!=6  & relchd!=7 & relchd!=14 & relchd!=18 & relchd!=19
collapse (max) LevelEdu, by(HHID)
label values LevelEdu education_groups
merge 1:1 HHID using "$base_out/1 Baseline/Household/Household.dta"
drop _merge
save "$base_out/1 Baseline/Household/Household.dta", replace

  use "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", clear
keep HHID SchoolID

merge 1:1 HHID using "$base_out/1 Baseline/Household/Household.dta"
drop _merge
save "$base_out/1 Baseline/Household/Household.dta", replace



use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:m SchoolID using "$base_out/1 Baseline/Household/Household.dta"
drop _merge
save "$base_out/1 Baseline/Household/Household.dta", replace


gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"


gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7
label variable 	roof_durable "Roof made out of a durable material"


replace asset_1=0 if asset_1==2
replace asset_2=0 if asset_2==2
replace asset_3=0 if asset_3==2
replace asset_4=0 if asset_4==2
replace asset_5=0 if asset_5==2
replace asset_6=0 if asset_6==2
replace asset_7=0 if asset_7==2
replace asset_8=0 if asset_8==2
replace bnkacc=0 if bnkacc==2
replace bnkacc=0 if cptgrt==2
gen breakfast=.
replace breakfast=0 if brkfst==3
replace breakfast=1 if brkfst==1 | brkfst==2
label variable 	breakfast "FC usually has breakfast before class"
gen crowding=rmsnum/NumHHMembers
replace workyn=0 if workyn==2
gen HHOwner=(hsowsh==1) & !missing(hsowsh)

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)
replace prdyyn=0 if prdyyn==2
replace ltcbyn=0 if ltcbyn==2

gen HH_Kis=(rdngsw==1)
gen HH_Kin=(rdngen==1)
gen HH_His_2=(mathqn_1==1 & mathqn_2==1)
gen HH_His_1=cond(mathqn_1+mathqn_1==1, 1, 0)
gen HH_His_0=(mathqn_1+mathqn_2==0)

drop if HHID==.

tab LevelEdu, gen(LevelEdu_D)
gen IncompletePrimary= (LevelEdu_D2==1 | LevelEdu_D1==1)
gen CompletePrimary=(LevelEdu_D3==1)
gen SomeSecondary= (LevelEdu_D4==1 | LevelEdu_D5==1 | LevelEdu_D6==1)
gen PostSecondary= (LevelEdu_D7==1)


pca workyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 mobilno bnkacc hsowsh crowding
predict IndexPoverty, score
pca tchrno prdyyn ltcbyn ltcbyn pttrmt ctcbyn shlcev  cptgrt adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng mrktst_1 mrktst_2 mrktst_3
predict IndexEngagement, score
pca HH_Kis HH_Kin mathqn_1 mathqn_2
predict IndexKowledge, score

save "$base_out/1 Baseline/Household/Household.dta", replace


keep  Index* SchoolID treatment treatarm DistID HHID NumHHMembers Expenditure_FC_2012 Expenditure_FC_2013 wall_mud floor_mud roof_durable  asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8 IncompletePrimary CompletePrimary SomeSecondary PostSecondary crowding workyn improveWater improveSanitation HHElectricty prdyyn ltcbyn breakfast HH_Kis HH_Kin HH_His_2 HH_His_1 bnkacc
 

rename * =_T1
rename HHID_T1 HHID
rename SchoolID_T1 SchoolID
rename treatment_T1 treatment
rename treatarm_T1 treatarm
rename DistID_T1 DistID

    
save "$base_out/Consolidated/Household.dta", replace



***************************
***********************
******************END LINE
****************************
******************************
use "$basein/3 Endline/Household/EHHData_noPII.dta", clear 
drop if consnt!=1
save "$base_out/3 Endline/Household/Household.dta", replace

gen MoreBooks=.
replace MoreBooks=1 if pvchtb==1
replace MoreBooks=0 if pvchtb==2 |pvchtb==3 |pvchtb==-99


gen KnowsTWProgram=.
replace KnowsTWProgram=1 if twknyn==1
replace KnowsTWProgram=0 if twknyn==2 | twknyn==3
label variable 	KnowsTWProgram "Knows child participating in Twaweza program" 

gen TWGrant=.
replace TWGrant=1 if twpgcg==1
replace TWGrant=0 if twpgcg==2 | twpgcg==3
label variable 	TWGrant "Thinks Twaweza gives grants" 

gen TWBonus=.
replace TWBonus=1 if twpgcd==1
replace TWBonus=0 if twpgcd==2 | twpgcd==3
label variable 	TWBonus "Thinks Twaweza gives bonuses"

gen CorrectBonuses=1 if codelt_1==1 
replace CorrectBonuses=CorrectBonuses+1 if codelt_2==1 
replace CorrectBonuses=CorrectBonuses+1 if codelt_3==2 
replace CorrectBonuses=CorrectBonuses+1 if codelt_4==2 
label variable 	CorrectBonuses "Correct answers on who gets bonus out of 4"

gen KnowsBonusSize=1 if codamt==5000
replace KnowsBonusSize=0 if codamt!=5000 & codamt!=.
label variable 	KnowsBonusSize "Knows bonus size"

gen KnowsGrantSize=1 if pkgamt==10000
replace KnowsGrantSize=0 if pkgamt!=10000 & pkgamt!=.
label variable 	KnowsBonusSize "Knows grant size"


gen CorrectGrant=1 if cgtxbk==1 
replace CorrectGrant=CorrectGrant+1 if cgthsl==2
replace CorrectGrant=CorrectGrant+1 if cgshsp==2
label variable 	CorrectGrant "Correct answers on what grant can be spent out of 3"


gen AdultAttendsSchoolsMeeting=.
replace AdultAttendsSchoolsMeeting=1 if prdyyn==1
replace AdultAttendsSchoolsMeeting=0 if prdyyn==2
label variable 	AdultAttendsSchoolsMeeting "Parents have attended school meetings" 

gen AdultMeetsTeacher=.
replace AdultMeetsTeacher=1 if cttryn==1
replace AdultMeetsTeacher=0 if cttryn==2
label variable 	AdultMeetsTeacher "Parents have meet with FC teacher" 
	 
gen AdultGivesSchool=.
replace AdultGivesSchool=1 if ctcbyn==1
replace AdultGivesSchool=0 if ctcbyn==2
label variable 	AdultGivesSchool "Hosehold donates to school"
	 
gen AdultAtHome=.
replace AdultAtHome=1 if adtprt==1
replace AdultAtHome=0 if adtprt==2
label variable 	AdultAtHome "Someone is at home when FC returns from school"
  
gen TalkAboutSchool=.
replace TalkAboutSchool=1 if lrntlk==1
replace TalkAboutSchool=0 if lrntlk==2
label variable 	TalkAboutSchool "FC talked with someone at home about school"

gen FCTutoring=.
replace FCTutoring=1 if tutcrt==1
replace FCTutoring=0 if tutcrt==2

label variable 	FCTutoring "FC had tutoring"

  gen AskToWorkbyTeacher=.
replace AskToWorkbyTeacher=1 if taskyn==1
replace AskToWorkbyTeacher=0 if taskyn==2
label variable 	AskToWorkbyTeacher "FC asked to bring things to teacher or work for him"


gen breakfast=.
replace breakfast=0 if brkfst==3
replace breakfast=1 if brkfst==1 | brkfst==2
label variable 	breakfast "FC usually has breakfast before class"   
compress
save "$base_out/3 Endline/Household/Household.dta", replace

use "$basein/3 Endline/Household/EHHExpenditure_noPII.dta", clear

drop if  HHMemberID!=1
recode expn13 (-99/-1=.)
recode expn13 (.=0)
reshape wide expn13 , i(HHID) j(EHHExpenditureID)

label variable expn131 "Expenditure in 2013 on school fees"
label variable expn132 "Expenditure in 2013 on textbooks"
label variable expn133 "Expenditure in 2013 on other books"
label variable expn134 "Expenditure in 2013 on supplies"
label variable expn135 "Expenditure in 2013 on uniforms"
label variable expn136 "Expenditure in 2013 on tutoring"
label variable expn137 "Expenditure in 2013 on transport"
label variable expn138 "Expenditure in 2013 on food"
label variable expn139 "Expenditure in 2013 on others"
egen Expenditure_FC=rowtotal(expn131 expn132 expn133 expn134 expn135 expn136 expn137  expn138 expn139), missing
label variable 	Expenditure_FC "FC education expenditure on 2013"

merge 1:1 HHID using "$base_out/3 Endline/Household/Household.dta"
drop _merge
compress
save "$base_out/3 Endline/Household/Household.dta", replace


use "$basein/3 Endline/Household/EHHExpenditure_noPII.dta", clear
recode expn13 (-99/-1=.)
*recode expn13 (.=0)
rename HHMemberID EHHMemberID
merge m:1 HHID EHHMemberID using "$basein/3 Endline/Household/EHHMember_noPII.dta", keepus(inschl lvledu smschl)
drop if _merge==2
drop _merge
drop if EHHMemberID==1
collapse (sum) expn13, by( HHID EHHMemberID inschl lvledu smschl)
collapse (mean) expn13, by(lvledu HHID smschl inschl)
save "$base_out/3 Endline/Household/Household_Expenditure.dta", replace


use "$basein/3 Endline/Household/EHHMember_noPII.dta", clear

collapse (count)  EHHMemberID , by(HHID)
rename EHHMemberID NumHHMembers
label variable 	NumHHMembers "Household size"



merge 1:1 HHID using "$base_out/3 Endline/Household/Household.dta"
drop _merge
compress
save "$base_out/3 Endline/Household/Household.dta", replace

  use "$basein/3 Endline/Supplementing/R_EL_rHHData_noPII.dta", clear
keep HHID SchoolID

merge 1:1 HHID using "$base_out/3 Endline/Household/Household.dta"
drop _merge
compress
save "$base_out/3 Endline/Household/Household.dta", replace



use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:m SchoolID using "$base_out/3 Endline/Household/Household.dta"
drop _merge
drop if HHID==.
save "$base_out/3 Endline/Household/Household.dta", replace

rename * =_T3
rename HHID_T3 HHID
rename treatment_T3 treatment
rename treatarm_T3 treatarm
rename DistID_T3 DistID
rename SchoolID_T3 SchoolID
merge 1:1 HHID using "$base_out/Consolidated/Household.dta"

compress
drop _merge
save "$base_out/Consolidated/Household.dta", replace

*******************************************************
*************BASE LINE 2014
********************************************************

use "$basein/6 Baseline 2014/Data/household/R4HHData_noPII.dta", clear
drop if HHID==.
drop if consnt!=1
save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace
use "$basein/6 Baseline 2014/Supplementing/UPID Linkage Tables/R4_Student_HH_ID_Linkage_noPII.dta", clear
drop if HHID==.
keep HHID GradeID DistrictID
rename DistrictID DistID
merge 1:1 HHID using "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta"
drop if consnt!=1
drop if _merge==1
drop _merge
drop if GradeID!=1
save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace


use "$basein/6 Baseline 2014/Data/household/R4HHExpenditure_noPII.dta", clear
drop if  R4HHMemberID!=1
recode expn13 expn14 (-99/-1=0)

reshape wide expn13 expn14, i(HHID) j(R4HHExpenditureID)


label variable expn131 "Expenditure in 2013 on school fees"
label variable expn132 "Expenditure in 2013 on textbooks"
label variable expn133 "Expenditure in 2013 on other books"
label variable expn134 "Expenditure in 2013 on supplies"
label variable expn135 "Expenditure in 2013 on uniforms"
label variable expn136 "Expenditure in 2013 on tutoring"
label variable expn137 "Expenditure in 2013 on transport"
label variable expn138 "Expenditure in 2013 on food"
label variable expn139 "Expenditure in 2013 on others"


label variable expn141 "Expenditure in 2014 on school fees"
label variable expn142 "Expenditure in 2014 on textbooks"
label variable expn143 "Expenditure in 2014 on other books"
label variable expn144 "Expenditure in 2014 on supplies"
label variable expn145 "Expenditure in 2014 on uniforms"
label variable expn146 "Expenditure in 2014 on tutoring"
label variable expn147 "Expenditure in 2014 on transport"
label variable expn148 "Expenditure in 2014 on food"
label variable expn149 "Expenditure in 2014 on others"


egen Expenditure_FC_2013=rowtotal(expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139), missing
egen Expenditure_FC_2014=rowtotal(expn141 expn142 expn143 expn144 expn145 expn146 expn147 expn148 expn149), missing

drop if Expenditure_FC_2013==. & Expenditure_FC_2014==.
recode expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139 expn141 expn142 expn143 expn144 expn145 expn146 expn147 expn148 expn149 (.=0)


label variable 	Expenditure_FC_2013 "FC education expenditure on 2013"
label variable 	Expenditure_FC_2014 "FC education expenditure on 2014"



merge 1:1 HHID using "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta"
drop if consnt!=1
drop _merge
compress
save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace

use "$basein/6 Baseline 2014/Data/household/R4hhMember_noPII.dta", clear

collapse (count)  R4HHMemberID , by(HHID)
rename R4HHMemberID NumHHMembers
label variable 	NumHHMembers "Household size"

merge 1:1 HHID using "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta"
drop _merge

save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace

use "$basein/6 Baseline 2014/Data/household/R4hhMember_noPII.dta", clear
replace hstedu=. if hstedu==-95
replace hstedu=. if hstedu==-99

gen LevelEdu=0 if (hstedu==1 | hstedu==2)
replace LevelEdu=1 if (hstedu<=16 & hstedu>=11) 
replace LevelEdu=2 if hstedu==17 | hstedu==18 | hstedu==19
replace LevelEdu=3 if (hstedu<=22 & hstedu>=20)
replace LevelEdu=4 if (hstedu==23 | hstedu==24)
replace LevelEdu=5 if (hstedu==25 | hstedu==26 | hstedu==27)
replace LevelEdu=6 if (hsted>=28)

label define education_groups 0 "No Primary" 1 "Incomplete Primary" 2 "Complete Primary" 3 "Incomplete Secondary" 4 "Complete Ordinary Secondary" 5 "Advance Secondary" 6 "Post-Secondary Education"
label values LevelEdu education_groups
drop if relchd!=2 & relchd!=3 & relchd!=4 & relchd!=5 & relchd!=6  & relchd!=7 & relchd!=14 & relchd!=18 & relchd!=19
collapse (max) LevelEdu, by(HHID)
label values LevelEdu education_groups
merge 1:1 HHID using "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta"
drop if _merge==2
drop _merge
save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace

gen wall_mud=0
replace wall_mud=1 if matwll==1
label variable 	wall_mud "Walls made out of earth/mud"


gen floor_mud=0
replace floor_mud=1 if matflr==1 
label variable 	floor_mud "Floor made out of earth/mud"


gen roof_durable=0
replace roof_durable=1 if matrof==4 | matrof==5 | matrof==6 | matrof==7
label variable 	roof_durable "Roof made out of a durable material"


replace asset_1=0 if asset_1==2
replace asset_2=0 if asset_2==2
replace asset_3=0 if asset_3==2
replace asset_4=0 if asset_4==2
replace asset_5=0 if asset_5==2
replace asset_6=0 if asset_6==2
replace asset_7=0 if asset_7==2
replace asset_8=0 if asset_8==2
replace bnkacc=0 if bnkacc==2
replace bnkacc=0 if cptgrt==2
gen breakfast=.
replace breakfast=0 if brkfst==3
replace breakfast=1 if brkfst==1 | brkfst==2
label variable 	breakfast "FC usually has breakfast before class"
gen crowding=rmsnum/NumHHMembers
rename wrokyn workyn
replace workyn=0 if workyn==2

gen improveWater=((srcwtr>=1 & srcwtr<=6) | srcwtr==12)
gen improveSanitation=(tlttyp>=1 & tlttyp<=3)
gen HHElectricty=(srclgh>=1 & srclgh<=3)
replace prdyyn=0 if prdyyn==2
replace ltcbyn=0 if ltcbyn==2

gen HH_Kis=(rdngsw==1)
gen HH_Kin=(rdngen==1)
gen HH_His_2=(mathqn_1==1 & mathqn_2==1)
gen HH_His_1=cond(mathqn_1+mathqn_1==1, 1, 0)
gen HH_His_0=(mathqn_1+mathqn_2==0)


tab LevelEdu, gen(LevelEdu_D)
gen IncompletePrimary= (LevelEdu_D2==1 | LevelEdu_D1==1)
gen CompletePrimary=(LevelEdu_D3==1)
gen SomeSecondary= (LevelEdu_D4==1 | LevelEdu_D5==1 | LevelEdu_D6==1)
gen PostSecondary= (LevelEdu_D7==1)

gen ctcbyn=(lstctb_2==1 | lstctb_3==1)
replace ltcbyn=(lstctb_1==1)


pca workyn wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1- asset_8 mobilno bnkacc hsowsh crowding
predict IndexPoverty, score
pca tchrno prdyyn ltcbyn ltcbyn pttrmt ctcbyn shlcev cptgrt adtprt hmwhlp_1 hmwhlp_2 hmwhlp_3 hrshmw hmwcmp lrntlk tutrng mrktst_1 mrktst_2 mrktst_3
predict IndexEngagement, score
pca HH_Kis HH_Kin mathqn_1 mathqn_2
predict IndexKowledge, score

save "$base_out/6 Baseline 2014/household/R4HHData_noPII.dta", replace

keep Index* DistID SchoolID  DistID HHID NumHHMembers Expenditure_FC_2013 Expenditure_FC_2014 wall_mud floor_mud roof_durable  asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8 IncompletePrimary CompletePrimary SomeSecondary PostSecondary crowding workyn improveWater improveSanitation HHElectricty prdyyn ltcbyn breakfast HH_Kis HH_Kin HH_His_2 HH_His_1 bnkacc
 

rename * =_T5
rename HHID_T5 HHID
rename DistID_T5 DistID
rename SchoolID_T5 SchoolID
merge 1:1 HHID using "$base_out/Consolidated/Household.dta"

compress
drop _merge
save "$base_out/Consolidated/Household.dta", replace

*******************************************************
*************END LINE 2014
********************************************************



use "$basein/8 Endline 2014/Final Data/Household/R6HHData_noPII.dta", clear 
drop if consnt!=1
gen StudentBringsBookHome=.
replace StudentBringsBookHome=1 if brgbok==1
replace StudentBringsBookHome=0 if brgbok==2
label variable 	StudentBringsBookHome "Child brings books home" 

gen BingsBooks3DaysAWeek=.
replace BingsBooks3DaysAWeek=1 if bokoft<=2 & !missing(bokoft)
replace BingsBooks3DaysAWeek=0 if bokoft>=3 & !missing(bokoft)
label variable 	BingsBooks3DaysAWeek "Brings book more than 3x/week" 


gen MoreBooks=.
replace MoreBooks=1 if pvchtb==1
replace MoreBooks=0 if pvchtb==2 |pvchtb==3 |pvchtb==-99


gen KnowsTWProgram=.
replace KnowsTWProgram=1 if twknyn==1
replace KnowsTWProgram=0 if twknyn==2 | twknyn==3
label variable 	KnowsTWProgram "Knows child participating in Twaweza program" 

gen TWGrant=.
replace TWGrant=1 if twpgcg==1
replace TWGrant=0 if twpgcg==2 | twpgcg==3
label variable 	TWGrant "Thinks Twaweza gives grants" 

gen TWBonus=.
replace TWBonus=1 if twpgcd==1
replace TWBonus=0 if twpgcd==2 | twpgcd==3
label variable 	TWBonus "Thinks Twaweza gives bonuses"

gen CorrectBonuses=1 if codelt_1==1 
replace CorrectBonuses=CorrectBonuses+1 if codelt_2==1 
replace CorrectBonuses=CorrectBonuses+1 if codelt_3==2 
replace CorrectBonuses=CorrectBonuses+1 if codelt_4==2 
label variable 	CorrectBonuses "Correct answers on who gets bonus out of 4"

gen KnowsBonusSize=1 if codamt==5000
replace KnowsBonusSize=0 if codamt!=5000 & codamt!=.
label variable 	KnowsBonusSize "Knows bonus size"

gen KnowsGrantSize=1 if pkgamt==10000
replace KnowsGrantSize=0 if pkgamt!=10000 & pkgamt!=.
label variable 	KnowsBonusSize "Knows grant size"


gen CorrectGrant=1 if cgtxbk==1 
replace CorrectGrant=CorrectGrant+1 if cgthsl==2
replace CorrectGrant=CorrectGrant+1 if cgshsp==2
label variable 	CorrectGrant "Correct answers on what grant can be spent out of 3"


gen AdultAttendsSchoolsMeeting=.
replace AdultAttendsSchoolsMeeting=1 if prdyyn==1
replace AdultAttendsSchoolsMeeting=0 if prdyyn==2
label variable 	AdultAttendsSchoolsMeeting "Parents have attended school meetings" 

gen AdultMeetsTeacher=.
replace AdultMeetsTeacher=1 if cttryn==1
replace AdultMeetsTeacher=0 if cttryn==2
label variable 	AdultMeetsTeacher "Parents have meet with FC teacher" 
	 
gen AdultGivesSchool=.
replace AdultGivesSchool=1 if ctcbyn==1
replace AdultGivesSchool=0 if ctcbyn==2
label variable 	AdultGivesSchool "Hosehold donates to school"
	 
gen AdultAtHome=.
replace AdultAtHome=1 if adtprt==1
replace AdultAtHome=0 if adtprt==2
label variable 	AdultAtHome "Someone is at home when FC returns from school"
  
gen TalkAboutSchool=.
replace TalkAboutSchool=1 if lrntlk==1
replace TalkAboutSchool=0 if lrntlk==2
label variable 	TalkAboutSchool "FC talked with someone at home about school"

gen FCTutoring=.
replace FCTutoring=1 if tutcrt==1
replace FCTutoring=0 if tutcrt==2

label variable 	FCTutoring "FC had tutoring"

  gen AskToWorkbyTeacher=.
replace AskToWorkbyTeacher=1 if taskyn==1
replace AskToWorkbyTeacher=0 if taskyn==2
label variable 	AskToWorkbyTeacher "FC asked to bring things to teacher or work for him"


gen breakfast=.
replace breakfast=0 if brkfst==3
replace breakfast=1 if brkfst==1 | brkfst==2
label variable 	breakfast "FC usually has breakfast before class"   
compress
save "$base_out/8 Endline 2014/Household/Household.dta", replace
use "$basein/8 Endline 2014/Final Data/Household/R6HHExpenditure_noPII.dta", clear

drop if  R6HHMemberID!=1
drop expn13_ik expn13_ik expn13_ik2
recode expn13 (-99/-1=.)

reshape wide expn13 , i(HHID) j(R6HHExpenditureID)

label variable expn131 "Expenditure in 2014 on school fees"
label variable expn132 "Expenditure in 2014 on textbooks"
label variable expn133 "Expenditure in 2014 on other books"
label variable expn134 "Expenditure in 2014 on supplies"
label variable expn135 "Expenditure in 2014 on uniforms"
label variable expn136 "Expenditure in 2014 on tutoring"
label variable expn137 "Expenditure in 2014 on transport"
label variable expn138 "Expenditure in 2014 on food"
label variable expn139 "Expenditure in 2014 on others"
egen Expenditure_FC=rowtotal(expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139), missing
drop if Expenditure_FC==.
recode expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139 (.=0)
label variable 	Expenditure_FC "FC education expenditure on 2014"

merge 1:1 HHID using "$base_out/8 Endline 2014/Household/Household.dta"
drop if _merge==2
drop _merge
compress
save "$base_out/8 Endline 2014/Household/Household.dta", replace



use "$basein/8 Endline 2014/Final Data/Household/R6HHExpenditure_noPII.dta", clear
recode expn13 (-99/-1=.)
mmerge HHID R6HHMemberID using "$basein/8 Endline 2014/Final Data/Household/R6HHMember_noPII.dta", ukeep(inschl lvledu smschl)
drop if _merge!=3
drop _merge
drop if R6HHMemberID==1
collapse (sum) expn13, by(SchoolID HHID R6HHMemberID inschl lvledu smschl)
collapse (mean) expn13, by(lvledu HHID SchoolID smschl inschl)
save "$base_out/8 Endline 2014/Household/Household_Expenditure.dta", replace


use "$basein/8 Endline 2014/Final Data/Household/R6HHMember_noPII.dta", clear

collapse (count)  R6HHMemberID , by(HHID)
rename R6HHMemberID NumHHMembers
label variable 	NumHHMembers "Household size"



merge 1:1 HHID using "$base_out/8 Endline 2014/Household/Household.dta"
drop if _merge==1
drop _merge
compress
save "$base_out/8 Endline 2014/Household/Household.dta", replace

/*
  use "$basein/8 Endline 2014/ID Linkage/R6_Student_HH_ID_Linkage_noPII.dta", clear
keep SchoolID DistrictID HHID
rename DistrictID DistID
drop if HHID==.
merge 1:1 HHID using "$base_out/8 Endline 2014/Household/Household.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/Household/Household.dta", replace
*/


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:m SchoolID using "$base_out/8 Endline 2014/Household/Household.dta"
drop _merge
drop if HHID==.
save "$base_out/8 Endline 2014/Household/Household.dta", replace

rename * =_T7
rename HHID_T7 HHID
rename treatment_T7 treatment
rename treatarm_T7 treatarm
rename DistID_T7 DistID
rename SchoolID_T7 SchoolID
merge 1:1 HHID using "$base_out/Consolidated/Household.dta"

compress
drop _merge
save "$base_out/Consolidated/Household.dta", replace
