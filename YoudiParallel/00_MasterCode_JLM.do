*This is Jessica's .do file, saving MR's files in Stata 13

clear all

* Central Path
 global mipath "D:\Box Sync\01_KiuFunza"
 *global mipath "C:\Users\Mauricio\Box Sync\01_KiuFunza\"
 *global mipath "C:\Users\Mauricio\Box Sync\KiuFunza"
 *global mipath "Z:\Box Sync\KiuFunza"
* Path Tree
  global basein 	"$mipath\RawData"
  global base_out   "$mipath\CreatedData\z_JLM_Attempts"
  global dir_do     "$mipath\Old\StataCode\z_JLM_Attempts"
  global results    "$mipath\Results\z_JLM_Attempts"
  global exceltables  "$mipath\Results\ExcelTables\z_JLM_Attempts"
  global graphs     "$mipath\Results\Graphs\z_JLM_Attempts"
    global latexcodes     "$mipath\Results\LatexCodes\z_JLM_Attempts"
	
	
* Create Student level Data Base	// this uses only 2013 base-endline, plus 2013 intervention
do "$dir_do\01_CreateStudentData"
* Create School Data Base
do "$dir_do\02_CreateSchoolData"
* Create Household level Data Base
do "$dir_do\03_CreateHousehold"
* Create Teacher level Data Base	// this uses only 2013 base-endline
do "$dir_do\04_CreateTeacherData"
* Create Student level Data Base
do "$dir_do\05_MidLineData"
* Create Student level Data Base
do "$dir_do\06_BaseLineSummary"
* Create Student level Data Base
*do "$dir_do\07_FirstAnalysis"		
* Create Student level Data Base
do "$dir_do\08_CreateConsolidatedData"	

	
* Create Student level Data Base
*do "$dir_do\09_OLSPassing"		
* Create Student level Data Base
do "$dir_do\10_OLSZscores"		
* Create Student level Data Base
do "$dir_do\11_OLSOthers"		
* Create Student level Data Base
*do "$dir_do\12_Figures"		
* Create Student level Data Base
do "$dir_do\13_Heterogeneity"	
* Create Student level Data Base
do "$dir_do\14_NonParametric_Bootstrap"	
* Create Student level Data Base
do "$dir_do\14_NonParametric_Bootstrap_CG"	
* Create Student level Data Base
do "$dir_do\14_NonParametric_Bootstrap_Combo"	


