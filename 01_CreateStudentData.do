*******************************************************
*************BASE LINE
********************************************************
use "$basein/1 Baseline/Student/Sample_noPII.dta", clear
drop _merge
compress
save "$base_out/1 Baseline/Student/Student.dta", replace

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID using "$base_out/1 Baseline/Student/Student.dta"
drop if _merge==1
drop _merge

drop  Stream_ID StreamID totSampleStreams_ID streamSize_ID StudentID
*child did not answer the base test
drop if consentChild==.
*child refused to answer finish base test
*drop if result==2 

gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance
drop  consentChild result childAvailable timeEndTest timeStartTest

/*
recode kiswahili_3- kiswahili_6 kiingereza_3- kiingereza_6 (2=0)
irt (2pl kiswahili_3- kiswahili_6) (pcm kiswahili_1 kiswahili_2), intpoints(50)  
predict IRT_kiswahili, latent
irt (2pl kiingereza_3- kiingereza_6) (pcm kiingereza_1 kiingereza_2), intpoints(100) 
predict IRT_kiingereza, latent 
irt  (pcm  hisabati_*) ,   intpoints(50) difficult 
predict IRT_hisabati, latent 
*/ 

*Now if put the children in different levels depending on how far they went in the Test
gen KiswahiliLevel=.
replace KiswahiliLevel=4 if kiswahili_4==1 & kiswahili_4!=.
replace KiswahiliLevel=3 if kiswahili_3==1 & kiswahili_3!=. & KiswahiliLevel==.
replace KiswahiliLevel=2 if kiswahili_2>=6 & kiswahili_2!=. & KiswahiliLevel==.
replace KiswahiliLevel=1 if kiswahili_1>=4 & kiswahili_1!=. & KiswahiliLevel==.
replace KiswahiliLevel=0 if KiswahiliLevel==.

label define reading 0 "Nothing"  1 "Syllable" 2 "Words"       3 "Paragraph" 4 "Read"
label values KiswahiliLevel reading
label variable KiswahiliLevel "Kiswahili reading level"
	   
gen EnglishLevel=.
replace EnglishLevel=4 if   kiingereza_4==1 & kiingereza_4!=.
replace EnglishLevel=3 if   kiingereza_3==1 & kiingereza_3!=. & EnglishLevel==.
replace EnglishLevel=2 if   kiingereza_2>=6 & kiingereza_2!=. & EnglishLevel==.
replace EnglishLevel=1 if   kiingereza_1>=4 & kiingereza_1!=. & EnglishLevel==.
replace EnglishLevel=0 if EnglishLevel==.

label values EnglishLevel reading
label variable EnglishLevel "English reading level"
		   
gen MathLevel=.
replace MathLevel=6 if  hisabati_6>=4 & hisabati_6!=.
replace MathLevel=5 if  hisabati_5>=4 & hisabati_5!=. &  MathLevel==.
replace MathLevel=4 if  hisabati_4>=4 & hisabati_4!=. &  MathLevel==.
replace MathLevel=3 if  hisabati_3>=4 & hisabati_3!=. &  MathLevel==.
replace MathLevel=2 if  hisabati_2>=4 & hisabati_2!=. &  MathLevel==.
replace MathLevel=1 if  hisabati_1>=4 & hisabati_1!=. & MathLevel==. 
replace MathLevel=0 if  MathLevel==. 

label define math 0 "Nothing"  1 "Counting" 2 "Numbers"  3 "Values" 4 "Addition" 5 "Subtraction" 6 "Multiplication"
label values MathLevel  math
label variable MathLevel "Math level"


********************************8
******************************** THIS SECTION CALCULATES THE Z-SCORE

replace kiswahili_3=0 if kiswahili_3==2
replace kiswahili_4=0 if kiswahili_4==2
replace kiswahili_5=0 if kiswahili_5==2
replace kiswahili_6=0 if kiswahili_6==2
replace kiingereza_3=0 if kiingereza_3==2
replace kiingereza_4=0 if kiingereza_4==2
replace kiingereza_5=0 if kiingereza_5==2
replace kiingereza_6=0 if kiingereza_6==2
recode bonasi_1 bonasi_2 bonasi_3 (2=0)

*For grade 1 is the easiest as students started in the easier level and stopped when they couldnt go any further. Thus its "safe" to assume they would have score zero in the next sections
*Thus the first step is to replace the missing values for zeros (since they did not get to it, it must be because they couldn't answer easier stuff so we assume they score zero)
foreach var of varlist kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
replace `var'=0 if `var'==. & GradeID==1 & attendance==1
}


*For grade  2 and 3 is the same in math... sooo... first we replace with zero the levels the student did not get to answer: 
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==2 & attendance==1
}
foreach var of varlist hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7{
replace `var'=0 if `var'==. & GradeID==3 & attendance==1
}


*For grade  2 and 3 is harder in lenguages: It started in question 3, if the student go it, then he moved on, if he didn't it moved to question 2 and if he scored zero then it moved to question 1. 
*So first, if the student got it, then lets assume he could read syllabus and letters, i.e. that he could achieve perfect score.
replace kiingereza_1 =5 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_1==. & attendance==1
replace kiingereza_2 =8 if kiingereza_3==1 & (GradeID==3 | GradeID==2) & kiingereza_2==. & attendance==1
replace kiswahili_1 =5 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_1==. & attendance==1
replace kiswahili_2 =8 if kiswahili_3==1 & (GradeID==3 | GradeID==2) & kiswahili_2==. & attendance==1
*If the student did not get question 4 (after he got question 3), then assume he would have gotten zero in questions 5 and 6
replace kiingereza_5 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==1 & kiingereza_4==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==1 & kiswahili_4==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, then lets assume he couldnt read the story or answer the questions i.e. that he could achieve perfect score. 
replace kiingereza_4 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_4==. & attendance==1
replace kiingereza_5 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_5==. & attendance==1
replace kiingereza_6 =0 if kiingereza_3==0 & (GradeID==3 | GradeID==2) & kiingereza_6==. & attendance==1
replace kiswahili_4 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_4==. & attendance==1
replace kiswahili_5 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_5==. & attendance==1
replace kiswahili_6 =0 if kiswahili_3==0 & (GradeID==3 | GradeID==2) & kiswahili_6==. & attendance==1
*If the student did not get it, and got over 0 in the word section, then we need to estimate the number of letters he would have gotten
* to do this we run an OLS regression using Grade1 data (they all answer these two questions) 
poisson kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0 & attendance==1
*reg kiswahili_1 kiswahili_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
predict input_kis1 if attendance==1, n 
*reg kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID if GradeID==1 & kiswahili_2>0
poisson kiingereza_1 kiingereza_2 i.Gender  Age c.Age#c.Age i.DistrictID i.SchoolID if GradeID==1 & kiswahili_2>0 & attendance==1
predict input_kiin1 if attendance==1, n 

replace kiswahili_1=max(round(input_kis1),5) if kiswahili_1==. & kiswahili_2>0 & kiswahili_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiingereza_1=max(round(input_kiin1),5)  if kiingereza_1==. & kiingereza_2>0 & kiingereza_3==0 & (GradeID==3 | GradeID==2) & attendance==1
replace kiswahili_1=5 if kiswahili_1>5 & attendance==1
replace kiingereza_1=5 if kiingereza_1>5 & attendance==1
sum kiswahili_1- hisabati_7
sum kiswahili_1- hisabati_7 if GradeID==1
sum kiswahili_1- hisabati_7 if GradeID==2
sum kiswahili_1- hisabati_7 if GradeID==3


/*
irt (2pl kiswahili_3- kiswahili_6) (grm kiswahili_1 kiswahili_2), intpoints(15) difficult intmethod(ghermite) technique(nr bhhh dfp bfgs)
predict IRT_kiswahili, latent 
irt (2pl kiingereza_3- kiingereza_6) (grm kiingereza_1 kiingereza_2), intpoints(4) difficult intmethod(ghermite)
predict IRT_kiingereza, latent 
irt  (grm  hisabati_*) ,   intpoints(4) difficult 
predict IRT_hisabati, latent 
*/ 
 
********************************
drop input_kis1 input_kiin1


*Now in order to calculate what the Z-score for the just passing is... I'm gonna create a series of variables that will be constant across students and will have the exact number needed to pass
gen kiswahili_1_pass=4 if GradeID==1 & attendance==1
gen kiswahili_2_pass=4 if GradeID==1 & attendance==1

gen kiingereza_1_pass=4 if GradeID==1 & attendance==1
gen kiingereza_2_pass=4 if GradeID==1 & attendance==1

gen hisabati_1_pass=4 if GradeID==1 & attendance==1
gen hisabati_2_pass=4 if GradeID==1 & attendance==1
gen hisabati_3_pass=4 if GradeID==1 & attendance==1
gen hisabati_4_pass=4 if GradeID==1 & attendance==1
gen hisabati_5_pass=4 if GradeID==1 & attendance==1


replace kiswahili_2_pass=4 if GradeID==2 & attendance==1
gen kiswahili_3_pass=1 if GradeID==2 & attendance==1

replace kiingereza_2_pass=4 if GradeID==2 & attendance==1
gen kiingereza_3_pass=1 if GradeID==2 & attendance==1


replace hisabati_3_pass=4 if GradeID==2 & attendance==1
replace hisabati_4_pass=4 if GradeID==2 & attendance==1
replace hisabati_5_pass=4 if GradeID==2 & attendance==1
gen hisabati_6_pass=4 if GradeID==2 & attendance==1


gen kiswahili_4_pass=1 if GradeID==3 & attendance==1
gen kiswahili_5_pass=1 if GradeID==3 & attendance==1
gen kiswahili_6_pass=1 if GradeID==3 & attendance==1

gen kiingereza_4_pass=1 if GradeID==3 & attendance==1
gen kiingereza_5_pass=1 if GradeID==3 & attendance==1
gen kiingereza_6_pass=1 if GradeID==3 & attendance==1


replace hisabati_4_pass=4 if GradeID==3 & attendance==1
replace hisabati_5_pass=4 if GradeID==3 & attendance==1
replace hisabati_6_pass=4 if GradeID==3 & attendance==1




*Now we can standarize for each question
foreach var of varlist bonasi_1 bonasi_2 bonasi_3 kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiswahili_5 kiswahili_6  kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 kiingereza_5 kiingereza_6 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 hisabati_6 hisabati_7 {
forvalues grade=1/3{
sum `var' if GradeID==`grade' & treatarm==4
capture gen Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade' & attendance==1
capture replace Z_`var'=(`var'-r(mean))/r(sd) if GradeID==`grade' & attendance==1
capture replace Z_`var'=0 if r(sd)==0 & GradeID==`grade' & attendance==1
/*
capture gen Z_`var'_pass=(`var'_pass-r(mean))/r(sd) if GradeID==`grade' & attendance==1
capture replace Z_`var'_pass=(`var'_pass-r(mean))/r(sd) if GradeID==`grade' & attendance==1
*/
}
}


gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
gen Z_bonasi=.
*****now we combine by subject
forvalues val=1/3{
	foreach subject in kiswahili kiingereza hisabati bonasi{
		pca Z_`subject'_* if GradeID==`val'
		predict Z_`subject'_temp, score
		replace Z_`subject'=  Z_`subject'_temp if GradeID==`val'
		drop Z_`subject'_temp
	}
}


*Old version, changed for PCA on March 8th, 2017
/*
egen Z_kiswahili=rowtotal(Z_kiswahili_1 Z_kiswahili_2 Z_kiswahili_3 Z_kiswahili_4 Z_kiswahili_5 Z_kiswahili_6),missing
egen Z_kiingereza=rowtotal( Z_kiingereza_1  Z_kiingereza_2 Z_kiingereza_3 Z_kiingereza_4 Z_kiingereza_5 Z_kiingereza_6),missing
egen Z_hisabati=rowtotal( Z_hisabati_1 Z_hisabati_2 Z_hisabati_3 Z_hisabati_4 Z_hisabati_5 Z_hisabati_6 Z_hisabati_7),missing
egen Z_bonasi=rowtotal( Z_bonasi_1 Z_bonasi_2 Z_bonasi_3),missing



egen Z_kiswahili_pass=rowtotal(Z_kiswahili_1_pass Z_kiswahili_2_pass Z_kiswahili_3_pass Z_kiswahili_4_pass Z_kiswahili_5_pass Z_kiswahili_6_pass),missing
egen Z_kiingereza_pass=rowtotal( Z_kiingereza_1_pass  Z_kiingereza_2_pass Z_kiingereza_3_pass Z_kiingereza_4_pass Z_kiingereza_5_pass Z_kiingereza_6_pass),missing
egen Z_hisabati_pass=rowtotal( Z_hisabati_1_pass Z_hisabati_2_pass Z_hisabati_3_pass Z_hisabati_4_pass Z_hisabati_5_pass Z_hisabati_6_pass),missing
*/


***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_bonasi{
		sum `var' if GradeID==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val' & attendance==1
	*if("`var'"!="Z_bonasi") replace `var'_pass=(`var'_pass-r(mean))/r(sd) if GradeID==`val' & attendance==1
	}
}

***Now we standarize by subject
foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_bonasi{
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
	*if("`var'"!="Z_bonasi") replace `var'_pass=(`var'_pass-r(mean))/r(sd)
}

***Now we create a few Z scores
pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

/*
gen SD_Z_kiswahili_pass=Z_kiswahili-Z_kiswahili_pass
gen SD_Z_kiingereza_pass=Z_kiingereza-Z_kiingereza_pass
gen SD_Z_hisabati_pass=Z_hisabati-Z_hisabati_pass
*/

***************************************************************
********* CALCULATE HOW FAR AWAY THEY FROM PASSING  *************
*****************************************************************
*****************************************************************

/*
*Now I generate the "PASS DISTANCE" as follows. If there are X abilities, and the student pass them all, then I add the extra points he got in all of them and call that the "superavit"
*If the student did not pass al abilities, then I count the number of answers he fall short of in the abilities he did not pass and called this the "deficit". And thats it.
capture drop PassDistance_K_S1- PassDistance_M_S3
*For first grader in kiswahili we do not have a "sentance" question, so we just use the first two abilities
gen PassDistance_K_S1=0 if GradeID==1 & attendance==1
replace PassDistance_K_S1=(kiswahili_1-4)+(kiswahili_2-4) if (kiswahili_1>=4 & kiswahili_2>=4  & GradeID==1) & attendance==1
replace PassDistance_K_S1=PassDistance_K_S1-(4-kiswahili_1) if (kiswahili_1<4 & GradeID==1) & attendance==1
replace PassDistance_K_S1=PassDistance_K_S1-(4-kiswahili_2) if (kiswahili_2<4 & GradeID==1) & attendance==1

*For first grader in english we do not have a "sentance" question, so we just use the first two abilities
gen PassDistance_E_S1=0 if GradeID==1 & attendance==1
replace PassDistance_E_S1=(kiingereza_1-4)+(kiingereza_2-4) if (kiingereza_1>=4 & kiingereza_2>=4  & GradeID==1) & attendance==1
replace PassDistance_E_S1=PassDistance_E_S1-(4-kiingereza_1) if (kiingereza_1<4 & GradeID==1) & attendance==1
replace PassDistance_E_S1=PassDistance_E_S1-(4-kiingereza_2) if (kiingereza_2<4 & GradeID==1) & attendance==1

*For first grader in math the questions are the same
gen PassDistance_M_S1=0 if GradeID==1 & attendance==1
replace PassDistance_M_S1=(hisabati_1-4)+(hisabati_2-4)+(hisabati_3-4)+(hisabati_4-4)+(hisabati_5-4) if (hisabati_1>=4 & hisabati_2>=4 & hisabati_3>=4 & hisabati_4>=4 & hisabati_5>=4  & GradeID==1) & attendance==1
replace PassDistance_M_S1=PassDistance_M_S1-(4-hisabati_1) if (hisabati_1<4 & GradeID==1) & attendance==1
replace PassDistance_M_S1=PassDistance_M_S1-(4-hisabati_2) if (hisabati_2<4 & GradeID==1) & attendance==1
replace PassDistance_M_S1=PassDistance_M_S1-(4-hisabati_3) if (hisabati_3<4 & GradeID==1) & attendance==1
replace PassDistance_M_S1=PassDistance_M_S1-(4-hisabati_4) if (hisabati_4<4 & GradeID==1)  & attendance==1
replace PassDistance_M_S1=PassDistance_M_S1-(4-hisabati_5) if (hisabati_5<4 & GradeID==1) & attendance==1

*For second grader in kiswahili we do not have a "sentance" question, so we just use the first and the third abilities
gen PassDistance_K_S2=0 if GradeID==2 & attendance==1
replace PassDistance_K_S2=(kiswahili_2-4)+(kiswahili_3-1) if (kiswahili_2>=4 & kiswahili_3==1  & GradeID==2) & attendance==1
replace PassDistance_K_S2=PassDistance_K_S2-(4-kiswahili_2) if (kiswahili_2<4 & GradeID==2) & attendance==1
replace PassDistance_K_S2=PassDistance_K_S2-(1-kiswahili_3) if (kiswahili_3==0 & GradeID==2) & attendance==1

*For second grader in english we do not have a "sentance" question, so we just use the first and the third abilities
gen PassDistance_E_S2=0 if GradeID==2 & attendance==1
replace PassDistance_E_S2=(kiingereza_2-4)+(kiingereza_3-1) if (kiingereza_2>=4 & kiingereza_3==1  & GradeID==2) & attendance==1
replace PassDistance_E_S2=PassDistance_E_S2-(4-kiingereza_2) if (kiingereza_2<4 & GradeID==2) & attendance==1
replace PassDistance_E_S2=PassDistance_E_S2-(1-kiingereza_3) if (kiingereza_3==0 & GradeID==2) & attendance==1

*For second grader in math the questions are the same
gen PassDistance_M_S2=0 if GradeID==2 & attendance==1
replace PassDistance_M_S2=(hisabati_3-4)+(hisabati_4-4)+(hisabati_5-4)+(hisabati_6-4) if (hisabati_3>=4 & hisabati_4>=4 & hisabati_5>=4 & hisabati_6>=4 & GradeID==2) & attendance==1
replace PassDistance_M_S2=PassDistance_M_S2-(4-hisabati_3) if (hisabati_3<4 & GradeID==2) & attendance==1
replace PassDistance_M_S2=PassDistance_M_S2-(4-hisabati_4) if (hisabati_4<4 & GradeID==2) & attendance==1
replace PassDistance_M_S2=PassDistance_M_S2-(4-hisabati_5) if (hisabati_5<4 & GradeID==2) & attendance==1
replace PassDistance_M_S2=PassDistance_M_S2-(4-hisabati_6) if (hisabati_6<4 & GradeID==2) & attendance==1


*For third grader in kiswahili the child needed to be able to read the story and answer at least one question

gen PassDistance_K_S3=0 if GradeID==3 & attendance==1
replace PassDistance_K_S3=kiswahili_5+kiswahili_6-1 if (kiswahili_4==1 & (kiswahili_5+kiswahili_6)>=1 & GradeID==3) & attendance==1
replace PassDistance_K_S3=PassDistance_K_S3-3 if (kiswahili_4==0 & GradeID==3) & attendance==1
replace PassDistance_K_S3=PassDistance_K_S3-2 if (kiswahili_4==1 & kiswahili_5==0 & kiswahili_6==0 & GradeID==3) & attendance==1
replace PassDistance_K_S3=PassDistance_K_S3-1 if (kiswahili_4==1 & kiswahili_5==1 & kiswahili_6==0 & GradeID==3) & attendance==1

*For third grader in english we do not have a "sentance" question, so we just use the first and the third abilities
gen PassDistance_E_S3=0 if GradeID==3 & attendance==1
replace PassDistance_E_S3=kiingereza_5+kiingereza_6-1 if (kiingereza_4==1 & (kiingereza_5+kiingereza_6)>=1 & GradeID==3) & attendance==1
replace PassDistance_E_S3=PassDistance_E_S3-3 if (kiingereza_4==0 & GradeID==3) & attendance==1
replace PassDistance_E_S3=PassDistance_E_S3-2 if (kiingereza_4==1 & kiingereza_5==0 & kiingereza_6==0 & GradeID==3) & attendance==1
replace PassDistance_E_S3=PassDistance_E_S3-1 if (kiingereza_4==1 & kiingereza_5==1 & kiingereza_6==0 & GradeID==3) & attendance==1

*For third grader in math there is no "division"
gen PassDistance_M_S3=0 if GradeID==3 & attendance==1
replace PassDistance_M_S3=(hisabati_4-4)+(hisabati_5-4)+(hisabati_6-4) if ( hisabati_4>=4 & hisabati_5>=4 & hisabati_6>=4 & GradeID==3) & attendance==1
replace PassDistance_M_S3=PassDistance_M_S3-(4-hisabati_4) if (hisabati_4<4 & GradeID==3) & attendance==1
replace PassDistance_M_S3=PassDistance_M_S3-(4-hisabati_5) if (hisabati_5<4 & GradeID==3) & attendance==1
replace PassDistance_M_S3=PassDistance_M_S3-(4-hisabati_6) if (hisabati_6<4 & GradeID==3) & attendance==1


gen PassK=((PassDistance_K_S1>=0 & !missing(PassDistance_K_S1)) | (PassDistance_K_S2>=0 & !missing(PassDistance_K_S2)) | (PassDistance_K_S3>=0 & !missing(PassDistance_K_S3)))
gen PassE=((PassDistance_E_S1>=0 & !missing(PassDistance_E_S1)) | (PassDistance_E_S2>=0 & !missing(PassDistance_E_S2)) | (PassDistance_E_S3>=0 & !missing(PassDistance_E_S3)))
gen PassM=((PassDistance_M_S1>=0 & !missing(PassDistance_M_S1)) | (PassDistance_M_S2>=0 & !missing(PassDistance_M_S2)) | (PassDistance_M_S3>=0 & !missing(PassDistance_M_S3)))


foreach var of varlist  PassDistance_K_S1 PassDistance_E_S1 PassDistance_M_S1 PassDistance_K_S2 PassDistance_E_S2 PassDistance_M_S2 PassDistance_K_S3 PassDistance_E_S3 PassDistance_M_S3{
qui sum `var'
gen `var'_STD=`var'/r(sd)
}

egen PassDistance_K_STD= rowtotal( PassDistance_K_S1_STD PassDistance_K_S2_STD PassDistance_K_S3_STD),missing
egen PassDistance_E_STD= rowtotal( PassDistance_E_S1_STD PassDistance_E_S2_STD PassDistance_E_S3_STD),missing
egen PassDistance_M_STD= rowtotal( PassDistance_M_S1_STD PassDistance_M_S2_STD PassDistance_M_S3_STD),missing



foreach var of varlist  PassDistance_K_S1 PassDistance_E_S1 PassDistance_M_S1 PassDistance_K_S2 PassDistance_E_S2 PassDistance_M_S2 PassDistance_K_S3 PassDistance_E_S3 PassDistance_M_S3{
drop `var'_STD
drop `var'
}
*/

*********Create above, below and around according to K ideas... if got nothin then below, if got something but didint pass... around. if pass then above


/*
gen Below_TH_K=0 if GradeID==1 & attendance==1
replace Below_TH_K=1 if kiswahili_1==0 & kiswahili_2==0 & GradeID==1 & attendance==1
replace Below_TH_K=0 if GradeID==2 & attendance==1
replace Below_TH_K=1 if kiswahili_2==0 & kiswahili_3==0 & GradeID==2 & attendance==1
replace Below_TH_K=0 if GradeID==3 & attendance==1
replace Below_TH_K=1 if kiswahili_4==0 & kiswahili_5==0 & kiswahili_6==0 & GradeID==3 & attendance==1

gen Above_TH_K=0 if GradeID==1 & attendance==1
replace Above_TH_K=1 if kiswahili_1>=4 & kiswahili_2>=4 & GradeID==1 & attendance==1
replace Above_TH_K=0 if GradeID==2 & attendance==1
replace Above_TH_K=1 if kiswahili_2>=4 & kiswahili_3==1 & GradeID==2 & attendance==1
replace Above_TH_K=0 if GradeID==3 & attendance==1
replace Above_TH_K=1 if kiswahili_4==1 & (kiswahili_5+kiswahili_6)>=1 & GradeID==3 & attendance==1

gen Around_TH_K=0 if GradeID==1 & attendance==1
replace Around_TH_K=1 if (kiswahili_1<4 | kiswahili_2<4) & (kiswahili_1>0 | kiswahili_2>0) & GradeID==1 & attendance==1
replace Around_TH_K=0 if GradeID==2 & attendance==1
replace Around_TH_K=1 if (kiswahili_2<4 | kiswahili_3==0) & (kiswahili_2>0 | kiswahili_3>0) & GradeID==2 & attendance==1
replace Around_TH_K=0 if GradeID==3 & attendance==1
replace Around_TH_K=1 if (kiswahili_4==0 | (kiswahili_5+kiswahili_6)==0) & (kiswahili_4!=0 | kiswahili_5!=0 | kiswahili_6!=0) & GradeID==3 & attendance==1


gen Below_TH_E=0 if GradeID==1 & attendance==1
replace Below_TH_E=1 if kiingereza_1==0 & kiingereza_2==0 & GradeID==1 & attendance==1
replace Below_TH_E=0 if GradeID==2 & attendance==1
replace Below_TH_E=1 if kiingereza_2==0 & kiingereza_3==0 & GradeID==2 & attendance==1
replace Below_TH_E=0 if GradeID==3 & attendance==1
replace Below_TH_E=1 if kiingereza_4==0 & kiingereza_5==0 & kiingereza_6==0 & GradeID==3 & attendance==1

gen Above_TH_E=0 if GradeID==1 & attendance==1
replace Above_TH_E=1 if kiingereza_1>=4 & kiingereza_2>=4 & GradeID==1 & attendance==1
replace Above_TH_E=0 if GradeID==2 & attendance==1
replace Above_TH_E=1 if kiingereza_2>=4 & kiingereza_3==1 & GradeID==2 & attendance==1
replace Above_TH_E=0 if GradeID==3 & attendance==1
replace Above_TH_E=1 if kiingereza_4==1 & (kiingereza_5+kiingereza_6)>=1 & GradeID==3 & attendance==1

gen Around_TH_E=0 if GradeID==1 & attendance==1
replace Around_TH_E=1 if (kiingereza_1<4 | kiingereza_2<4) & (kiingereza_1>0 | kiingereza_2>0) & GradeID==1 & attendance==1
replace Around_TH_E=0 if GradeID==2 & attendance==1
replace Around_TH_E=1 if (kiingereza_2<4 | kiingereza_3==0) & (kiingereza_2>0 | kiingereza_3>0)& GradeID==2 & attendance==1
replace Around_TH_E=0 if GradeID==3 & attendance==1
replace Around_TH_E=1 if (kiingereza_4==0 | (kiingereza_5+kiingereza_6)==0) & (kiingereza_4!=0 | kiingereza_5!=0 | kiingereza_6!=0) & GradeID==3 & attendance==1

gen Below_TH_M=0 if GradeID==1 & attendance==1
replace Below_TH_M=1 if hisabati_1==0 & hisabati_2==0 & hisabati_3==0 & hisabati_4==0 & hisabati_5==0  & GradeID==1 & attendance==1
replace Below_TH_M=0 if GradeID==2 & attendance==1
replace Below_TH_M=1 if hisabati_3==0 & hisabati_4==0 & hisabati_5==0 & hisabati_6==0 & GradeID==2 & attendance==1
replace Below_TH_M=0 if GradeID==3 & attendance==1
replace Below_TH_M=1 if hisabati_4==0 & hisabati_5==0 & hisabati_6==0 & GradeID==3 & attendance==1

gen Above_TH_M=0 if GradeID==1 & attendance==1
replace Above_TH_M=1 if hisabati_1>=4 & hisabati_2>=4 & hisabati_3>=4 & hisabati_4>=4 & hisabati_5>=4  & GradeID==1 & attendance==1
replace Above_TH_M=0 if GradeID==2 & attendance==1
replace Above_TH_M=1 if hisabati_3>=4 & hisabati_4>=4 & hisabati_5>=4 & hisabati_6>=4 & GradeID==2 & attendance==1
replace Above_TH_M=0 if GradeID==3 & attendance==1
replace Above_TH_M=1 if hisabati_4>=4 & hisabati_5>=4 & hisabati_6>=4 & GradeID==3 & attendance==1

gen Around_TH_M=0 if GradeID==1 & attendance==1
replace Around_TH_M=1 if (hisabati_1<4 | hisabati_2<4 | hisabati_3<4 | hisabati_4<4 | hisabati_5<4) & (hisabati_1>0 | hisabati_2>0 | hisabati_3>0 | hisabati_4>0 | hisabati_5>0)  & GradeID==1 & attendance==1
replace Around_TH_M=0 if GradeID==2 & attendance==1
replace Around_TH_M=1 if (hisabati_3<4 | hisabati_4<4 | hisabati_5<4 | hisabati_6<4) & (hisabati_3>0 | hisabati_4>0 | hisabati_5>0 | hisabati_6>0) & GradeID==2 & attendance==1
replace Around_TH_M=0 if GradeID==3 & attendance==1
replace Around_TH_M=1 if (hisabati_4<4 | hisabati_5<4 | hisabati_6<4) & (hisabati_4>0 | hisabati_5>0 | hisabati_6>0) & GradeID==2 & GradeID==3 & attendance==1
*/



compress
save "$base_out/1 Baseline/Student/Student.dta", replace


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:m SchoolID using "$base_out/1 Baseline/Student/Student.dta"
drop if _merge==1
drop _merge
compress
save "$base_out/1 Baseline/Student/Student.dta", replace


gen male=0 & attendance==1
replace male=1 if Gender==1 & attendance==1
compress
replace seenUwezoTests=0 if seenUwezoTests==2 & attendance==1
replace preSchoolYN=0 if preSchoolYN==2 & attendance==1

save "$base_out/1 Baseline/Student/Student.dta", replace

rename * =_T1
rename usid_T1 usid
rename DistID_T1 DistID
rename treatment_T1 treatment
rename treatarm_T1 treatarm
rename SchoolID_T1 SchoolID
*PassK PassE PassM SD_Z_kiswahili_pass SD_Z_kiingereza_pass SD_Z_hisabati_pass PassDistance_K_STD_T1 PassDistance_E_STD_T1 PassDistance_M_STD_T1 Below_TH_K_T1 Above_TH_K_T1 Around_TH_K_T1 Below_TH_E_T1 Above_TH_E_T1 Around_TH_E_T1 Below_TH_M_T1 Above_TH_M_T1 Around_TH_M_T1
keep treatment treatarm SchoolID DistID attendance_T1   usid GradeID_T1  preSchoolYN_T1 Age_T1 Gender_T1 male_T1 seenUwezoTests_T1  EnglishLevel_T1 MathLevel_T1 KiswahiliLevel_T1  Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1 Z_bonasi_T1 Z_ScoreKisawMath_T1 Z_ScoreFocal_T1 
compress
save "$base_out/1 Baseline/Student/Student_BaselineChar.dta", replace


save "$base_out/Consolidated/Student.dta", replace

*******************************************************
*************MID LINE
**********************************************************
use "$basein/2 Midline/MStudent_noPII.dta", clear


gen student_present=.
replace student_present=1 if  attstd==1
replace student_present=0 if  attstd==2
replace student_present=0 if  attstd==3 
replace student_present=1 if  attstd==3 & attstd2==1
replace student_present=0 if  attstd==3 & attstd2==2	 

rename upidst usid
rename student_present attendance
replace usid=subinstr(usid,"STU","",.)
drop if usid==""


keep usid attendance
rename * =_T2
rename usid_T2 usid
merge 1:m usid using "$base_out/Consolidated/Student.dta"
replace attendance_T2=0 if _merge==2
drop if _merge==1
drop _merge

save "$base_out/Consolidated/Student.dta", replace

*******************************************************
*************END LINE3
**********************************************************

use "$basein/3 Endline/Student/ESTudent_noPII.dta", clear
compress
save "$base_out/3 Endline/Student/Student.dta", replace

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID

merge 1:m SchoolID using "$base_out/3 Endline/Student/Student.dta"

drop _merge
compress
save "$base_out/3 Endline/Student/Student.dta", replace

gen grade=stdgrd
rename grade GradeID



**gen pass variables for each grade,subject using KiuFunza EL Test pass standards
gen passkiswahili_1=1 if S1_kiswahili_1>=4 & S1_kiswahili_2>=4 & S1_kiswahili_3>=4 & S1_kiswahili_1!=. & S1_kiswahili_2!=. & S1_kiswahili_3!=.
recode passkiswahili_1 .=0 if GradeID==1 & consentChild==1
gen passkiingereza_1=1 if S1_kiingereza_1>=4 & S1_kiingereza_2>=4 & S1_kiingereza_3>=4 & S1_kiingereza_1!=. & S1_kiingereza_2!=. & S1_kiingereza_3!=.
recode passkiingereza_1 .=0 if GradeID==1  & consentChild==1
gen passhisabati_1=1 if S1_hisabati_1>=4 & S1_hisabati_2>=4 & S1_hisabati_3>=4 & S1_hisabati_4>=4 & S1_hisabati_5>=4 & S1_hisabati_1!=. & S1_hisabati_2!=. & S1_hisabati_3!=. & S1_hisabati_4!=. & S1_hisabati_5!=.
recode passhisabati_1 .=0 if GradeID==1  & consentChild==1

gen passkiswahili_2=1 if S2_kiswahili_1>=4 & S2_kiswahili_2>=4 & S2_kiswahili_3==1 & S2_kiswahili_1!=. & S2_kiswahili_2!=. & S2_kiswahili_3!=.
recode passkiswahili_2 .=0 if GradeID==2  & consentChild==1
gen passkiingereza_2=1 if S2_kiingereza_1>=4 & S2_kiingereza_2>=4 & S2_kiingereza_3==1 & S2_kiingereza_1!=. & S2_kiingereza_2!=. & S2_kiingereza_3!=.
recode passkiingereza_2 .=0 if GradeID==2  & consentChild==1
gen passhisabati_2=1 if S2_hisabati_1>=4 & S2_hisabati_2>=4 & S2_hisabati_3>=4 & S2_hisabati_4>=4 & S2_hisabati_1!=. & S2_hisabati_2!=. & S2_hisabati_3!=. & S2_hisabati_4!=.
recode passhisabati_2 .=0 if GradeID==2  & consentChild==1

gen passkiswahili_3=1 if S3_kiswahili_1==1 & S3_kiswahili_2>=2 & S3_kiswahili_1!=. & S3_kiswahili_2!=.
recode passkiswahili_3 .=0 if GradeID==3  & consentChild==1
gen passkiingereza_3=1 if S3_kiingereza_1==1 & S3_kiingereza_2>=2 & S3_kiingereza_1!=. & S3_kiingereza_2!=.
recode passkiingereza_3 .=0 if GradeID==3  & consentChild==1
gen passhisabati_3=1 if S3_hisabati_1>=4 & S3_hisabati_2>=4 & S3_hisabati_3>=4 & S3_hisabati_4>=4 & S3_hisabati_1!=. & S3_hisabati_2!=. & S3_hisabati_3!=. & S3_hisabati_4!=.
recode passhisabati_3 .=0 if GradeID==3  & consentChild==1

label variable 	passkiswahili_1 "Pass Kiswahili test"
label variable 	passkiingereza_1 "Pass English test"
label variable 	passhisabati_1 "Pass Math test"
label variable 	passkiswahili_2 "Pass Kiswahili test"
label variable 	passkiingereza_2 "Pass English test"
label variable 	passhisabati_2 "Pass Math test"
label variable 	passkiswahili_3 "Pass Kiswahili test"
label variable 	passkiingereza_3 "Pass English test"
label variable 	passhisabati_3 "Pass Math test"

egen passkiswahili=rowtotal(passkiswahili_1 passkiswahili_2 passkiswahili_3),missing
egen passkiingereza=rowtotal( passkiingereza_1 passkiingereza_2  passkiingereza_3),missing
egen passhisabati=rowtotal( passhisabati_1 passhisabati_2 passhisabati_3),missing

label variable 	passkiswahili "Pass Kiswahili test"
label variable 	passkiingereza "Pass English test"
label variable 	passhisabati "Pass Math test"

rename passkiswahili passkis
rename passkiingereza passeng
rename passhisabati passmath

drop passkiswahili_1 passkiswahili_2 passkiswahili_3 passkiingereza_1 passkiingereza_2 passkiingereza_3 passhisabati_1 passhisabati_2 passhisabati_3

*Now i create standarized test scores for each question
foreach var of varlist S1_kiswahili_1 S1_kiswahili_2 S1_kiswahili_3 S1_kiswahili_4  S1_kiingereza_1 S1_kiingereza_2 S1_kiingereza_3 S1_kiingereza_4 S1_hisabati_1 S1_hisabati_2 S1_hisabati_3 S1_hisabati_4 S1_hisabati_5 S1_sayansi_1 {
	sum `var' if GradeID==1 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati {
	pca Z_S1_`subject'*
	predict Z_S1_`subject', score
}

gen Z_S1_sayansi=Z_S1_sayansi_1
/*
egen Z_S1_kiswahili=rowtotal(Z_S1_kiswahili_1 Z_S1_kiswahili_2 Z_S1_kiswahili_3 Z_S1_kiswahili_4),missing
egen Z_S1_kiingereza=rowtotal(Z_S1_kiingereza_1 Z_S1_kiingereza_2  Z_S1_kiingereza_3 Z_S1_kiingereza_4),missing
egen Z_S1_hisabati=rowtotal(Z_S1_hisabati_1 Z_S1_hisabati_2 Z_S1_hisabati_3 Z_S1_hisabati_4  Z_S1_hisabati_5),missing
egen Z_S1_sayansi=rowtotal(Z_S1_sayansi_1),missing
*/

foreach var of varlist Z_S1_kiswahili Z_S1_kiingereza Z_S1_hisabati  Z_S1_sayansi{
	sum `var' if GradeID==1 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


replace S2_kiswahili_3=0 if S2_kiswahili_3==2
replace S2_kiingereza_3=0 if S2_kiingereza_3==2

foreach var of varlist S2_kiswahili_1 S2_kiswahili_2 S2_kiswahili_3 S2_kiswahili_4 S2_kiingereza_1 S2_kiingereza_2 S2_kiingereza_3 S2_kiingereza_4 S2_hisabati_1 S2_hisabati_2 S2_hisabati_3 S2_hisabati_4 S2_sayansi_1 {
	sum `var' if GradeID==2 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati {
	pca Z_S2_`subject'*
	predict Z_S2_`subject', score
}
gen Z_S2_sayansi=Z_S2_sayansi_1

/*
egen Z_S2_kiswahili=rowtotal(Z_S2_kiswahili_1 Z_S2_kiswahili_2 Z_S2_kiswahili_3 Z_S2_kiswahili_4),missing
egen Z_S2_kiingereza=rowtotal( Z_S2_kiingereza_1 Z_S2_kiingereza_2  Z_S2_kiingereza_3 Z_S2_kiingereza_4),missing
egen Z_S2_hisabati=rowtotal( Z_S2_hisabati_1 Z_S2_hisabati_2 Z_S2_hisabati_3  Z_S2_hisabati_4),missing
egen Z_S2_sayansi=rowtotal(  Z_S2_sayansi_1),missing
*/
foreach var of varlist Z_S2_kiswahili Z_S2_kiingereza Z_S2_hisabati Z_S2_sayansi {
	sum `var' if GradeID==2 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



replace S3_kiswahili_1=0 if S3_kiswahili_1==2
replace S3_kiingereza_1=0 if S3_kiingereza_1==2

foreach var of varlist S3_kiswahili_1 S3_kiswahili_2 S3_kiswahili_3 S3_kiingereza_1 S3_kiingereza_2 S3_kiingereza_3 S3_hisabati_1 S3_hisabati_2 S3_hisabati_3 S3_hisabati_4 S3_hisabati_5 S3_sayansi_1 {
	sum `var' if GradeID==3 & treatarm==4
	gen Z_`var'=(`var'-r(mean))/r(sd)
}


foreach subject in kiswahili kiingereza hisabati {
	pca Z_S3_`subject'*
	predict Z_S3_`subject', score
}
gen Z_S3_sayansi=Z_S3_sayansi_1



/*
egen Z_S3_kiswahili=rowtotal(Z_S3_kiswahili_1 Z_S3_kiswahili_2 Z_S3_kiswahili_3),missing
egen Z_S3_kiingereza=rowtotal( Z_S3_kiingereza_1  Z_S3_kiingereza_2 Z_S3_kiingereza_3),missing
egen Z_S3_hisabati=rowtotal( Z_S3_hisabati_1 Z_S3_hisabati_2 Z_S3_hisabati_3 Z_S3_hisabati_4 Z_S3_hisabati_5),missing
egen Z_S3_sayansi=rowtotal(  Z_S3_sayansi_1),missing
*/

foreach var of varlist Z_S3_kiswahili Z_S3_kiingereza Z_S3_hisabati Z_S3_sayansi {
	sum `var' if GradeID==3 & treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


gen IRT_kiswahili=.
gen IRT_kiingereza=.
gen IRT_hisabati=.
gen IRT_sayansi=.
drop S1_*_time
drop S2_*_time
drop S3_*_time

forvalues grd = 1/3{
	capture drop IRT_kiswahili_temp
	capture drop IRT_kiingereza_temp
	capture drop IRT_hisabati_temp
	capture drop IRT_sayansi_temp
	
	irt  (grm S`grd'_kiswahili_*) if stdgrd==`grd', intpoints(4) difficult  intmethod(ghermite) listwise
	predict IRT_kiswahili_temp if  e(sample)==1, latent 
	replace IRT_kiswahili=IRT_kiswahili_temp if IRT_kiswahili==.
	
	irt (grm S`grd'_kiingereza_*) if stdgrd==`grd', intpoints(4) difficult  intmethod(ghermite) listwise
	predict IRT_kiingereza_temp if e(sample)==1, latent 
	replace IRT_kiingereza=IRT_kiingereza_temp if IRT_kiingereza==.
	
	irt  (grm  S`grd'_hisabati_*) if stdgrd==`grd',   intpoints(4) difficult  intmethod(ghermite) listwise
	predict IRT_hisabati_temp if e(sample)==1, latent 
	replace IRT_hisabati=IRT_hisabati_temp if IRT_hisabati==.
	
	irt  (grm  S`grd'_sayansi_*) if stdgrd==`grd',   intpoints(4) difficult  intmethod(ghermite) listwise
	predict IRT_sayansi_temp if e(sample)==1, latent 
	replace IRT_sayansi=IRT_sayansi_temp if IRT_sayansi==.
}

drop IRT_kiswahili_temp IRT_kiingereza_temp IRT_hisabati_temp
forvalues val=1/3{
	foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
		sum `var' if stdgrd==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}

foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

pca IRT_kiswahili IRT_hisabati
predict IRT_ScoreKisawMath, score 
pca IRT_kiswahili IRT_kiingereza IRT_hisabati
predict IRT_ScoreFocal, score 

foreach var of varlist IRT_ScoreKisawMath IRT_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

*******************Total Z score
****************************
*****************************
egen Z_kiswahili=rowtotal(Z_S1_kiswahili Z_S2_kiswahili Z_S3_kiswahili), missing
egen Z_kiingereza=rowtotal(Z_S1_kiingereza Z_S2_kiingereza Z_S3_kiingereza), missing
egen Z_hisabati=rowtotal(Z_S1_hisabati Z_S2_hisabati Z_S3_hisabati), missing
egen Z_sayansi=rowtotal(Z_S1_sayansi Z_S2_sayansi Z_S3_sayansi), missing

foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 


foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

/*
egen Z_ScoreKisawMath=rowtotal(Z_kiswahili Z_hisabati), missing
egen Z_ScoreFocal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati), missing
egen Z_ScoreTotal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi), missing

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal {
sum `var' if treatarm==4
replace `var'=(`var'-r(mean))/r(sd)
}
*/






gen unavailable=.
replace unavailable=1 if status==4 
replace unavailable=0 if status==1 | status==2 | status==3 
label variable 	unavailable "Student is unavailable"


gen change_school=.
replace change_school=1 if status==3 
replace change_school=0 if status==1 | status==2 | status==4
label variable 	change_school "Student changed schools"

*rename grade GradeID
compress
save "$base_out/3 Endline/Student/Student.dta", replace



gen usid=subinstr(upid,"R1STU","",.)
drop if usid==""
gen date=dofc(time)
format date %dN/D/Y
gen WeekIntvTest=week(date)

drop timeStartTest S1_kiswahili_1- timeEndTest
drop Z_S1_kiswahili_1- Z_S3_sayansi


gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance


rename * =_T3
rename usid_T3 usid
rename upid_T3 upid
rename SchoolID_T3 SchoolID
rename DistID_T3 DistID
rename treatment_T3 treatment
rename treatarm_T3 treatarm

merge 1:m usid using "$base_out/Consolidated/Student.dta"
replace attendance_T3=0 if _merge==2
drop _merge
compress
save "$base_out/Consolidated/Student.dta", replace

gen date=dofc(time)
format date %dN/D/Y

merge m:1 SchoolID using "$basein/4 Intervention/TwaEL_2013/TwaTestDates.dta"
drop if _merge==1
drop _merge
collapse (last)  date iddate , by(SchoolID)
gen iddate1=date( iddate,"DM19Y")
format iddate1 %dN/D/Y 

gen DiffIntvTestRestTest=iddate1-date
merge 1:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta"
drop if _merge==2
keep  SchoolID date iddate iddate1 DiffIntvTestRestTest treatment treatarm 
compress
save "$base_out/3 Endline/School/TestTiming.dta", replace

********************
********************
*************** INTERVENTION TEST
********************
********************

**The teacher was paid 5000 Tsh per student that pass a subject.  The head teacher was paid 1000 Tsh. Students are worth a total of 18,000 Tsh
*Exchange rate today (12/06/2014) was 1683.50
*PIB per capita 608.85 USD
*Daily minimum wage 3080
*average wage in 2012 was 577,070


use "$basein/4 Intervention/TwaEL_2013/TwaStudResults_StreamID.dta", clear
save "$base_out/4 Intervention/TwaEL_2013/StudentTest.dta", replace
use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID


merge 1:m SchoolID using "$base_out/4 Intervention/TwaEL_2013/StudentTest.dta"
drop if _merge==1
drop _merge
drop if kis_pass==. & eng_pass==. &  math_pass==.

replace kis_a=. if kis_a>1
replace kis_h=. if kis_h>1
replace eng_p=. if eng_p>1
replace eng_se=. if  eng_se>4
replace eng_s=. if eng_s>1
replace math_bwa=. if math_bwa>6
replace math_j=. if math_j>6
replace math_t=. if math_t>6


**gen pass variables for each grade,subject using KiuFunza EL Test pass standards
gen PassK_Int=0 if (kis_si<4 | kis_ma<4 | kis_se<4) & GradeID==1
replace PassK_Int=1 if (kis_si>=4 & kis_ma>=4 & kis_se>=4) & GradeID==1
gen PassE_Int=0 if (eng_l<4 | eng_w<4 | eng_se<4) & GradeID==1
replace PassE_Int=1 if (eng_l>=4 & eng_w>=4 & eng_se>=4) & GradeID==1
gen PassM_Int=0 if (math_id<4 | math_uta<4 | math_bwa<4 | math_j<4 | math_t<4) & GradeID==1
replace PassM_Int=1 if (math_id>=4 & math_uta>=4 & math_bwa>=4 & math_j>=4 & math_t>=4) & GradeID==1

replace PassK_Int=0 if ( kis_ma<4 | kis_se<4 | kis_a==0) & GradeID==2
replace PassK_Int=1 if (kis_ma>=4 & kis_se>=4 & kis_a==1) & GradeID==2
replace PassE_Int=0 if (eng_w<4 | eng_se<4 | eng_p==0) & GradeID==2
replace PassE_Int=1 if (eng_w>=4 & eng_se>=4 & eng_p==1) & GradeID==2 
replace PassM_Int=0 if (math_bwa<4 | math_j<4 | math_t<4 | math_z<4 ) & GradeID==2
replace PassM_Int=1 if (math_bwa>=4 & math_j>=4 & math_t>=4 & math_z>=4 ) & GradeID==2


replace PassK_Int=0 if ( kis_h ==0 | kis_m<2 ) & GradeID==3
replace PassK_Int=1 if (kis_h ==1 & kis_m>=2 ) & GradeID==3
replace PassE_Int=0 if (eng_s==0 | eng_c<2) & GradeID==3
replace PassE_Int=1 if (eng_s==1 & eng_c>=2) & GradeID==3 
replace PassM_Int=0 if (math_j<4 | math_t<4 | math_z<4 | math_g<4 ) & GradeID==3
replace PassM_Int=1 if (math_j>=4 & math_t>=4 & math_z>=4 & math_g>=4 ) & GradeID==3


foreach var of varlist kis_si kis_ma kis_se kis_a kis_h kis_m eng_l eng_w eng_se eng_p eng_s eng_c math_id math_uta math_bwa math_j math_t math_z math_g {
	forvalues val=1/3{
		forvalues testNum=1/10{
			sum `var' if GradeID==`val' & treatarm==4 & testset==`testNum'
			gen Z_`var'_S`val'_test`testNum'=(`var'-r(mean))/r(sd) 
		}
	}
}

foreach var of varlist kis_si kis_ma kis_se kis_a kis_h kis_m eng_l eng_w eng_se eng_p eng_s eng_c math_id math_uta math_bwa math_j math_t math_z math_g {
	forvalues val=1/3{
		egen Z_`var'_S`val'=rowtotal(Z_`var'_S`val'_test1 Z_`var'_S`val'_test2 Z_`var'_S`val'_test3 Z_`var'_S`val'_test4 Z_`var'_S`val'_test5 Z_`var'_S`val'_test6 Z_`var'_S`val'_test7 Z_`var'_S`val'_test8 Z_`var'_S`val'_test9 Z_`var'_S`val'_test10 ) if GradeID==`val',missing
		sum Z_`var'_S`val' if treatarm==4
		replace Z_`var'_S`val' =(Z_`var'_S`val' -r(mean))/r(sd)
	}
}




forvalues val=1/3{
	egen Z_S`val'_kiswahili=rowtotal(Z_kis_si_S`val' Z_kis_ma_S`val' Z_kis_se_S`val' Z_kis_a_S`val' Z_kis_h_S`val' Z_kis_m_S`val') if GradeID==`val',missing
	sum Z_S`val'_kiswahili if treatarm==4
	replace Z_S`val'_kiswahili =(Z_S`val'_kiswahili -r(mean))/r(sd)
	egen Z_S`val'_kiingereza=rowtotal(Z_eng_l_S`val' Z_eng_w_S`val' Z_eng_se_S`val' Z_eng_p_S`val' Z_eng_s_S`val' Z_eng_c_S`val') if GradeID==`val',missing
	sum Z_S`val'_kiswahili if treatarm==4
	replace Z_S`val'_kiswahili =(Z_S`val'_kiswahili -r(mean))/r(sd)
	egen Z_S`val'_hisabati=rowtotal( Z_math_id_S`val' Z_math_uta_S`val' Z_math_bwa_S`val' Z_math_j_S`val' Z_math_t_S`val' Z_math_z_S`val' Z_math_g_S`val') if GradeID==`val',missing
	sum Z_S`val'_kiswahili if treatarm==4
	replace Z_S`val'_kiswahili =(Z_S`val'_kiswahili -r(mean))/r(sd)
}


egen Z_kiswahili_Int=rowtotal(Z_S1_kiswahili Z_S2_kiswahili Z_S3_kiswahili), missing
egen Z_kiingereza_Int=rowtotal(Z_S1_kiingereza Z_S2_kiingereza Z_S3_kiingereza), missing
egen Z_hisabati_Int=rowtotal(Z_S1_hisabati Z_S2_hisabati Z_S3_hisabati), missing

foreach var of varlist Z_kiswahili_Int Z_kiingereza_Int Z_hisabati_Int {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



egen Z_ScoreKisawMath_Int=rowtotal(Z_kiswahili_Int Z_hisabati_Int), missing
egen Z_ScoreFocal_Int=rowtotal(Z_kiswahili_Int Z_kiingereza_Int Z_hisabati_Int), missing
foreach var of varlist Z_ScoreKisawMath_Int Z_ScoreFocal_Int {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


by treatarm, sort : summarize Z_ScoreKisawMath_Int
by treatarm, sort : summarize Z_ScoreFocal_Int
rename DistID DistrictID



merge 1:1 DistrictID SchoolID GradeID StreamID StudentID Sample_ID using  "$basein/1 Baseline/Supplementing/StudentIDLinkage_noPII.dta", keepus(upid)
drop if _merge==2
drop _merge
drop if upid==""	
rename upid upidst
compress
save "$base_out/4 Intervention/TwaEL_2013/StudentTest.dta", replace

gen usid=subinstr(upidst,"STU","",.)
drop if usid==""


keep usid Z_kiswahili_Int Z_kiingereza_Int Z_hisabati_Int Z_ScoreKisawMath_Int Z_ScoreFocal_Int GradeID
rename *_Int *_T4
rename GradeID GradeID_T4

merge 1:m usid using "$base_out/Consolidated/Student.dta"
drop _merge
compress


save "$base_out/Consolidated/Student.dta", replace




********************
********************
*************** BASELINE YEAR 2
********************
********************

use "$basein/6 Baseline 2014/Data/student/R4Student_noPII.dta",clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepusing(treatment treatarm DistID)
drop if _merge==2
drop _merge
gen GradeID=1
drop if consentChild==. | consentChild==2

gen male=0
replace male=1 if Gender==1
compress
replace seenUwezoTests=0 if seenUwezoTests==2
replace preSchoolYN=0 if preSchoolYN==2

gen student_present=0
replace student_present=1 if consentChild==1
rename student_present attendance

*Now in order to calculate what the Z-score for the just passing is... I'm gonna create a series of variables that will be constant across students and will have the exact number needed to pass
gen Akiswahili_1_pass=4
gen Akiswahili_2_pass=4 
gen Akiswahili_3_pass=4 


gen Akiingereza_1_pass=4 
gen Akiingereza_2_pass=4
gen Akiingereza_3_pass=4

gen Ahisabati_1_pass=4
gen Ahisabati_2_pass=4 
gen Ahisabati_3_pass=4
gen Ahisabati_4_pass=4 
gen Ahisabati_5_pass=4


*Now I create standarized test scores for each question
foreach var in kiswahili_1 kiswahili_2 kiswahili_3 kiswahili_4 kiingereza_1 kiingereza_2 kiingereza_3 kiingereza_4 hisabati_1 hisabati_2 hisabati_3 hisabati_4 hisabati_5 sayansi_1{
sum A`var' if treatarm==4
gen Z_`var'=(A`var'-r(mean))/r(sd)
*capture gen Z_`var'_pass=(A`var'_pass-r(mean))/r(sd)
}

foreach subject in kiswahili kiingereza hisabati{
	pca Z_`subject'_* 
	predict Z_`subject', score
}
	
/*	
egen Z_kiswahili=rowtotal(Z_kiswahili_1 Z_kiswahili_2 Z_kiswahili_3 Z_kiswahili_4),missing
egen Z_kiingereza=rowtotal(Z_kiingereza_1 Z_kiingereza_2  Z_kiingereza_3 Z_kiingereza_4),missing
egen Z_hisabati=rowtotal(Z_hisabati_1 Z_hisabati_2 Z_hisabati_3 Z_hisabati_4  Z_hisabati_5),missing
*/
egen Z_sayansi=rowtotal(Z_sayansi_1),missing

/*
egen Z_kiswahili_pass=rowtotal(Z_kiswahili_1_pass Z_kiswahili_2_pass Z_kiswahili_3_pass),missing
egen Z_kiingereza_pass=rowtotal(Z_kiingereza_1_pass Z_kiingereza_2_pass  Z_kiingereza_3_pass),missing
egen Z_hisabati_pass=rowtotal(Z_hisabati_1_pass Z_hisabati_2_pass Z_hisabati_3_pass Z_hisabati_4  Z_hisabati_5_pass),missing
*/

foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if  treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
	capture replace `var'_pass=(`var'_pass-r(mean))/r(sd)
}
/*
gen SD_Z_kiswahili_pass=Z_kiswahili-Z_kiswahili_pass
gen SD_Z_kiingereza_pass=Z_kiingereza-Z_kiingereza_pass
gen SD_Z_hisabati_pass=Z_hisabati-Z_hisabati_pass
*/


pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 

/*
egen Z_ScoreKisawMath=rowtotal(Z_kiswahili Z_hisabati), missing
egen Z_ScoreFocal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati), missing
egen Z_ScoreTotal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi), missing
*/

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

*SD_Z_kiswahili_pass SD_Z_kiingereza_pass SD_Z_hisabati_passZ_ScoreTotal
keep attendance  DistID SchoolID groupSize R4StudentID Age male preSchoolYN seenUwezoTests upid treatment treatarm GradeID Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi Z_ScoreKisawMath Z_ScoreFocal 

compress
save "$base_out/6 Baseline 2014/student/Student.dta", replace


rename * =_T5
rename SchoolID_T5 SchoolID
rename DistID_T5 DistID
rename R4StudentID_T5 R4StudentID
rename upid_T5 upid
rename treatment_T5 treatment
rename treatarm_T5 treatarm

   
append using "$base_out/Consolidated/Student.dta"
compress

decode stdstrel_T3, gen(stdstrel2_T3)
replace stdstr_T3= stdstrel2_T3 if stdstr_T3==""
save "$base_out/Consolidated/Student.dta", replace





********************
********************
*************** ENDLINE YEAR 2
********************
********************

*First determined the schools that are also tested by TWA
use "$basein/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents.dta", clear
drop SchoolID_15_s Grade_15_s USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
drop if DistrictID==11
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}
drop if treatment==.
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)
keep SchoolID
sort SchoolID
quietly by SchoolID:  gen dup = cond(_N==1,0,_n)
drop if dup>1
drop dup		
save "$base_out/Consolidated/SchoolsTWA.dta", replace

** WE NEED A BREAK TO CREATE HOME TESTED DATA
use "$basein/8 Endline 2014/Final Data/Household/R6HHData_noPII.dta",clear
drop if consentChild!=1
keep HHID consentChild- edtstm
merge 1:m HHID using "$basein/8 Endline 2014/ID Linkage/R6_Student_HH_ID_Linkage_noPII.dta",keepus(upid HHID)
drop if _merge!=3
rename upid upidst 
drop if upidst==""
drop HHID _merge
save "$base_out/8 Endline 2014/Household/HomeTests.dta", replace 
****
***
*Now lets loading the data
use "$basein/8 Endline 2014/Final Data/Student/R6Student_noPII.dta",clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop if _merge==2
drop _merge
merge 1:1 upidst using "$base_out/8 Endline 2014/Household/HomeTests.dta", update
gen TestedAtHome=(_merge==4)
*drop if _merge==5
drop _merge
*Create a dummy for whether the student wwas in a school also tested by twaweza
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsTWA.dta"
gen SchoolTWA=(_merge==3)
drop if _merge==2
drop _merge

*Create the correct grade for the student
*This doesnt work as students only get asked accoridng to stdgrd
/*
gen grade=1 if stdgrd==1 & atrgrd==.
replace grade=1 if atrgrd==1
replace grade=2 if stdgrd==2 & atrgrd==.
replace grade=2 if atrgrd==2
replace grade=3 if stdgrd==3 & atrgrd==.
replace grade=3 if atrgrd==3
rename grade GradeID
*/
drop GradeID
gen GradeID=stdgrd

gen attgrade=1 if stdgrd==1 & atrgrd==.
replace attgrade=1 if atrgrd==1
replace attgrade=2 if stdgrd==2 & atrgrd==.
replace attgrade=2 if atrgrd==2
replace attgrade=3 if stdgrd==3 & atrgrd==.
replace attgrade=3 if atrgrd==3



gen student_present=0
replace student_present=1 if  consentChild==1
rename student_present attendance

*drop if attendance==0

*drop if consentChild==2 | consentChild==.


gen date=dofc(time)
format date %dN/D/Y
gen WeekIntvTest=week(date)


foreach var of varlist kissyl1_1 -othsub1_8{
	recode `var' (.=0) if GradeID==1 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl2_1- othsub2_15{
	recode `var' (.=0) if GradeID==2 & consentChild!=2 & consentChild!=.
}
foreach var of varlist kissyl3_1- othsub3_24{
	recode `var' (.=0) if GradeID==3 & consentChild!=2 & consentChild!=.
}

foreach var of varlist kissyl1_1-othsub3_24 {
	recode `var' (2=0) (3=0) if consentChild!=2 & consentChild!=.
}



gen IRT_kiswahili=.
gen IRT_kiingereza=.
gen IRT_hisabati=.
gen IRT_sayansi=.

forvalues grd = 1/3{
	capture drop IRT_kiswahili_temp
	capture drop IRT_kiingereza_temp
	capture drop IRT_hisabati_temp
	capture drop IRT_sayansi_temp
	
	irt  (2pl kis*`grd'_*) if stdgrd==`grd', intpoints(9) difficult  intmethod(ghermite) listwise
	predict IRT_kiswahili_temp if e(sample)==1, latent 
	replace IRT_kiswahili=IRT_kiswahili_temp if IRT_kiswahili==.
	
	irt (2pl eng*`grd'_*) if stdgrd==`grd', intpoints(9) difficult  intmethod(ghermite) listwise
	predict IRT_kiingereza_temp if e(sample)==1, latent 
	replace IRT_kiingereza=IRT_kiingereza_temp if IRT_kiingereza==.
	
	irt  (2pl  his*`grd'_*) if stdgrd==`grd',   intpoints(9) difficult  intmethod(ghermite) listwise
	predict IRT_hisabati_temp if e(sample)==1, latent 
	replace IRT_hisabati=IRT_hisabati_temp if IRT_hisabati==.
	
	irt  (2pl  othsub*`grd'_*) if stdgrd==`grd',   intpoints(9) difficult  intmethod(ghermite) listwise
	predict IRT_sayansi_temp if e(sample)==1, latent 
	replace IRT_sayansi=IRT_sayansi_temp if IRT_sayansi==.
	
}
drop IRT_kiswahili_temp IRT_kiingereza_temp IRT_hisabati_temp IRT_sayansi_temp

forvalues val=1/3{
	foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
		sum `var' if stdgrd==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}




foreach var of varlist IRT_kiswahili IRT_kiingereza IRT_hisabati IRT_sayansi {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


pca IRT_kiswahili IRT_hisabati
predict IRT_ScoreKisawMath, score 
pca IRT_kiswahili IRT_kiingereza IRT_hisabati
predict IRT_ScoreFocal, score 

foreach var of varlist IRT_ScoreKisawMath IRT_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}

********************
********************
*************** ENDLINE YEAR 2 - Version A
********************
********************

gen Z_kiswahili=.
gen Z_kiingereza=.
gen Z_hisabati=.
gen Z_sayansi=.

forvalues grd = 1/3{
	pca kis*`grd'_* if stdgrd==`grd'
	predict Z_kiswahili_temp, score
	replace Z_kiswahili=Z_kiswahili_temp  if stdgrd==`grd'
	
	pca eng*`grd'_* if stdgrd==`grd'
	predict Z_kiingereza_temp, score
	replace Z_kiingereza=Z_kiingereza_temp  if stdgrd==`grd'
	
	pca his*`grd'_* if stdgrd==`grd'
	predict Z_hisabati_temp, score
	replace Z_hisabati=Z_hisabati_temp  if stdgrd==`grd'
	
	pca othsub*`grd'_* if stdgrd==`grd'
	predict Z_sayansi_temp, score
	replace Z_sayansi=Z_sayansi_temp  if stdgrd==`grd'
	
	drop Z_hisabati_temp  Z_kiingereza_temp Z_kiswahili_temp Z_sayansi_temp
}


***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
		sum `var' if GradeID==`val' & treatarm==4
		replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi {
	sum `var' if  treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}


pca Z_kiswahili Z_hisabati
predict Z_ScoreKisawMath, score 
pca Z_kiswahili Z_kiingereza Z_hisabati
predict Z_ScoreFocal, score 

/*

egen Z_ScoreKisawMath=rowtotal(Z_kiswahili Z_hisabati), missing
egen Z_ScoreFocal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati), missing
egen Z_ScoreTotal=rowtotal(Z_kiswahili Z_kiingereza Z_hisabati Z_sayansi), missing
*/

foreach var of varlist Z_ScoreKisawMath Z_ScoreFocal  {
	sum `var' if treatarm==4
	replace `var'=(`var'-r(mean))/r(sd)
}



********************
********************
*************** ENDLINE YEAR 2 - Version C /* Comptatible with TWA test*/
********************
********************

*Grade 1
egen C_kisLtr1= rowtotal( kissyl1_1 - kissyl1_5),missing
egen C_kisWrd1= rowtotal( kiswrd1_1- kiswrd1_5),missing /*this should be 1-5*/
egen C_kisSen1= rowtotal( kissen1_1 -kissen1_5),missing /*this should be 1-5*/
egen C_engLtr1= rowtotal( englet1_1 - englet1_5),missing
egen C_engWrd1= rowtotal( engwrd1_1- engwrd1_5),missing /*this should be 1-5*/
egen C_engSen1= rowtotal( engsen1_1 -engsen1_5),missing /*this should be 1-5*/
egen C_mathCount1= rowtotal( hiscou1_1 -hiscou1_3),missing
egen C_mathNumber1= rowtotal( hisidn1_1- hisidn1_3),missing
egen C_mathIneq1= rowtotal( hisinq1_1 hisinq1_2 hisinq1_4),missing /*exclude hisinq1_3*/
egen C_mathAdd1= rowtotal( hisadd1_1- hisadd1_3),missing /*this should be 1-3*/
egen C_mathSub1= rowtotal(hissub1_1- hissub1_3),missing /*this should be 1-3*/



*Grade 2
egen C_kisWrd2= rowtotal( kiswrd2_5- kiswrd2_9),missing /*this should be 5-9*/
egen C_kisSen2= rowtotal( kissen2_5- kissen2_9),missing /*this shoudl be 5-9*/
gen C_kisPara2=kispar2_1
egen C_engWrd2= rowtotal( engwrd2_5 -engwrd2_9),missing /*this shoudl be 5-9*/
egen C_engSen2= rowtotal( engsen2_5- engsen2_9),missing /*this shoudl be 5-9*/
gen C_engPara2=engpar2_1
egen C_mathIneq2= rowtotal( hisinq2_2 -hisinq2_4),missing /*this should be 2-4*/
egen C_mathAdd2= rowtotal( hisadd2_3- hisadd2_5),missing /*this should be 3-5*/
egen C_mathSub2= rowtotal(hissub2_3- hissub2_5),missing /*this should be 3-5*/
egen C_mathMult2= rowtotal(hismul2_1- hismul2_3),missing /*this should be 1-3*/

*Grade 3
gen C_kisRead3=kissto3_1 /*Twaweza scores the story as 1=successfully read, 0=not read*/
egen C_kisCom3= rowtotal( kiscom3_1 -kiscom3_3),missing /*then scores the comprehension out of the three questions asked, but it only takes 2 to pass*/
gen C_engRead3=engsto3_1
egen C_engCom3=rowtotal( engcom3_1 -engcom3_3),missing /*scored same as Kiswahili*/
egen C_mathAdd3= rowtotal( hisadd3_5- hisadd3_7),missing /*this should be 5-7*/
egen C_mathSub3= rowtotal(hissub3_5- hissub3_7),missing /*this should be 5-7*/
egen C_mathMult3= rowtotal(hismul3_3- hismul3_5),missing /*this should be 3-5*/
egen C_mathDivi3= rowtotal(hisdiv3_1- hisdiv3_3),missing


foreach var of varlist C_kisLtr1- C_mathDivi3 {
sum `var' if treatarm==4
gen Z_`var'= (`var'-r(mean))/r(sd)
}


egen Z_kiswahili_C=rowtotal(Z_C_kis*),missing
egen Z_kiingereza_C=rowtotal(Z_C_eng*),missing
egen Z_hisabati_C=rowtotal(Z_C_math*),missing

/*
egen Z_kiswahili_C=rowtotal(Z_C_kis*)
replace Z_kiswahili_C=. if Z_kiswahili_B==.
egen Z_kiingereza_C=rowtotal(Z_C_eng*)
replace Z_kiingereza_C=. if Z_kiingereza_B==.
egen Z_hisabati_C=rowtotal(Z_C_math*)
replace Z_hisabati_C=. if Z_hisabati_B==.
*/

***Now we standarize by grade subject
forvalues val=1/3{
foreach var of varlist Z_kiswahili_C Z_kiingereza_C Z_hisabati_C {
sum `var' if GradeID==`val' & treatarm==4
replace `var'=(`var'-r(mean))/r(sd) if GradeID==`val'
}
}


foreach var of varlist Z_kiswahili_C Z_kiingereza_C Z_hisabati_C {
sum `var' if  treatarm==4
replace `var'=(`var'-r(mean))/r(sd)
}

drop C_kisLtr1- Z_C_mathDivi3
********************
********************
*************** ENDLINE YEAR 2 - Pass test as if TWA
********************
********************
*grade 1
foreach subskil in kissyl englet {
	gen `subskil'1=`subskil'1_1
	foreach val of numlist 2/5 {
		replace `subskil'1=`subskil'1+`subskil'1_`val'
	}
}
foreach subskil in engsen engwrd kissen kiswrd {
	gen `subskil'1=`subskil'1_1
	foreach val of numlist 2/5 {
		replace `subskil'1=`subskil'1+`subskil'1_`val'
	}
}
foreach subskil in hiscou hisidn hisadd hissub {
	gen `subskil'1=`subskil'1_1
	foreach val of numlist 2/3 {
		replace `subskil'1=`subskil'1+`subskil'1_`val'
	}
}
foreach subskil in hisinq {
	gen `subskil'1=`subskil'1_1
	foreach val of numlist 2 4 {
		replace `subskil'1=`subskil'1+`subskil'1_`val'
	}
}



*grade 2
foreach subskil in hisinq {
	gen `subskil'2=`subskil'2_2
	foreach val of numlist 3/4 {
		replace `subskil'2=`subskil'2+`subskil'2_`val'
	}
}
foreach subskil in hismul {
	gen `subskil'2=`subskil'2_1
	foreach val of numlist 2/3 {
		replace `subskil'2=`subskil'2+`subskil'2_`val'
	}
}
foreach subskil in hisadd hissub {
	gen `subskil'2=`subskil'2_3
	foreach val of numlist 4/5 {
		replace `subskil'2=`subskil'2+`subskil'2_`val'
	}
}
foreach subskil in kiswrd kissen engwrd engsen {
	gen `subskil'2=`subskil'2_5
	foreach val of numlist 6/9 {
		replace `subskil'2=`subskil'2+`subskil'2_`val'
	}
}
gen kispar2=kispar2_1
gen engpar2=engpar2_1


*grade 3
foreach subskil in kiscom engcom hisdiv {
	gen `subskil'3=`subskil'3_1
	foreach val of numlist 2/3 {
		replace `subskil'3=`subskil'3+`subskil'3_`val'
	}
}
foreach subskil in hismul {
	gen `subskil'3=`subskil'3_3
	foreach val of numlist 4/5 {
		replace `subskil'3=`subskil'3+`subskil'3_`val'
	}
}
foreach subskil in hisadd hissub {
	gen `subskil'3=`subskil'3_5
	foreach val of numlist 6/7 {
		replace `subskil'3=`subskil'3+`subskil'3_`val'
	}
}
gen kispar3=kispar3_1
gen kissto3=kissto3_1
gen engpar3=engpar3_1
gen engsto3=engsto3_1
**calculate passes based on Twaweza Pass rules
gen passkis_stud=0 if consentChild==1
replace passkis_stud=1 if kissyl1>=4 & kissyl1!=. & kiswrd1>=4 & kiswrd1!=. & kissen1>=4 & kissen1!=. & GradeID==1 & consentChild==1
replace passkis_stud=1 if kiswrd2>=4 & kiswrd2!=. & kissen2>=4 & kissen2!=. & kispar2==1 & GradeID==2 & consentChild==1
replace passkis_stud=1 if kissto3==1 & kiscom3>=2 & kiscom3!=. & GradeID==3 & consentChild==1
gen passeng_stud=0 if consentChild==1
replace passeng_stud=1 if englet1>=4 & englet1!=. & engwrd1>=4 & engwrd1!=. & engsen1>=4 & engsen1!=. & GradeID==1 & consentChild==1
replace passeng_stud=1 if engwrd2>=4 & engwrd2!=. & engsen2>=4 & engsen2!=. & engpar2==1 & GradeID==2 & consentChild==1
replace passeng_stud=1 if engsto3==1 & engcom3>=2 & engcom3!=. & GradeID==3 & consentChild==1
gen passmath_stud=0 if consentChild==1
replace passmath_stud=1 if hiscou1>=3 & hisidn1>=3 & hisinq1>=3 & hisadd1>=3 & hissub1>=3 & GradeID==1 & consentChild==1
replace passmath_stud=1 if hisinq2>=3 & hisadd2>=3 & hissub2>=3 & hismul2>=3 & GradeID==2 & consentChild==1
replace passmath_stud=1 if hisadd3>=3 & hissub3>=3 & hismul3>=3 & hisdiv3>=3 & GradeID==3 & consentChild==1

rename passkis_stud passkis
rename passeng_stud passeng
rename passmath_stud passmath

label variable 	passkis "Pass Kiswahili test"
label variable 	passeng "Pass English test"
label variable 	passmath "Pass Math test"

keep IRT* passkis passeng passmath SchoolID R6StudentID upidst stdgrp stdage stdsex time revisit attstd atrgrd stdstrel attstd2 timeStartTest consentChild treatment treatarm TestedAtHome SchoolTWA GradeID attendance date WeekIntvTest Z_kiswahili* Z_kiingereza* Z_hisabati* Z_sayansi* Z_ScoreKisawMath* Z_ScoreFocal* attgrade stdgrd


rename * =_T7
rename upidst_T7 upid
rename SchoolID_T7 SchoolID

rename R6StudentID_T7 R6StudentID
merge 1:m upid using "$base_out/Consolidated/Student.dta"
replace attendance_T7=0 if GradeID_T1<=2 & attendance_T7==.
drop _merge
compress

save "$base_out/Consolidated/Student.dta", replace

********************
********************
*************** INTERVENTION TEST 2014
********************
********************

use "$basein/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents.dta", clear
drop SchoolID_15_s Grade_15_s USchoolID UGrade UStuID StuName_s StuName_sound UStuName_sound uniqid2 dupmergscore uniqid1 studnum studnum_s Darasa_s SchoolID_s SchGrd SchGrd_s SchGrd_group v10 StuID_el schgrdnum schgrdnum_15 schgrdnum_15_s StuID  number stuid_dup
foreach var in DistrictID SchoolID Grade Stream{
replace `var'_el=`var' if missing(`var'_el)
}
gen NoInfoBL=(DistrictID==.)
drop DistrictID SchoolID Grade Stream
foreach var in DistrictID SchoolID Grade Stream date StuTest Kis_SI Kis_MA Kis_SE Kis_A Kis_H Kis_M Kis_Pass Eng_L Eng_W Eng_SE Eng_P Eng_S Eng_C Eng_Pass Math_ID Math_UTA Math_BWA Math_J Math_T Math_Z Math_G Math_Pass VolName start end time_session stud_session time_pertest treatment{
rename `var'_el `var'
}

drop if treatment==.
drop if DistrictID==11
gen TestedEL=!(Kis_Pass==. & Eng_Pass==. & Math_Pass==.)


*First we need to clean the data a bit
 replace Kis_SI=. if Kis_SI==8
 replace Kis_MA=. if Kis_MA==6
 replace Kis_SE=. if Kis_SE==6
 replace Kis_A=. if Kis_A>1
 replace Kis_H=. if Kis_H>1
 replace Eng_SE=. if Eng_SE==6
 replace Eng_P=. if Eng_P>1
 replace Eng_S=. if Eng_S>1
 replace Math_J=. if Math_J==14
 replace Math_T=. if Math_T==6
 replace Math_G=. if Math_G==6

 
 foreach var of varlist Kis_SI- Kis_M Eng_L- Eng_C Math_ID- Math_G{
 replace `var'=. if Kis_Pass==. | Eng_Pass==. | Math_Pass==.
 }
 
 *Now we generate LEVELS DATA!!
gen Pass_Syll_Kis=(Kis_SI==4) & !missing(Kis_SI)
gen Pass_Word_Kis=(Kis_MA==4) & !missing(Kis_MA)
gen Pass_Sent_Kis=(Kis_SE==4) & !missing(Kis_SE)
gen Pass_Para_Kis=(Kis_A==1) & !missing(Kis_A)
gen Pass_Read_Kis=(Kis_H==1) & !missing(Kis_H)
gen Pass_Compre_Kis=(Kis_M==2) & !missing(Kis_M)


gen Pass_Syll_Eng=(Eng_L==4) & !missing(Eng_L)
gen Pass_Word_Eng=(Eng_W==4) & !missing(Eng_W)
gen Pass_Sent_Eng=(Eng_SE==4) & !missing(Eng_SE)
gen Pass_Para_Eng=(Eng_P==1) & !missing(Eng_P)
gen Pass_Read_Eng=(Eng_S==1) & !missing(Eng_S)
gen Pass_Compre_Eng=(Eng_C==2) & !missing(Eng_C)
   
       
gen Pass_Count_Math=(Math_ID==4) & !missing(Math_ID)
gen Pass_Numbers_Math=(Math_UTA==4) & !missing(Math_UTA)
gen Pass_Inequal_Math=(Math_BWA==4) & !missing(Math_BWA)
gen Pass_Add_Math=(Math_J==4) & !missing(Math_J)
gen Pass_Sub_Math=(Math_T==4) & !missing(Math_T)
gen Pass_Mult_Math=(Math_Z==4) & !missing(Math_Z)
gen Pass_Div_Math=(Math_G==4) & !missing(Math_G)   


gen Kis_Pass2=(Pass_Syll_Kis==1 & Pass_Word_Kis==1 & Pass_Sent_Kis==1 ) if Grade==1 /* & TestedEL==1 */
replace Kis_Pass2=(Pass_Word_Kis==1 & Pass_Sent_Kis==1 & Pass_Para_Kis==1) if Grade==2 /* & TestedEL==1 */
replace Kis_Pass2=( Pass_Read_Kis==1 & Pass_Compre_Kis==1) if  Grade==3 /* & TestedEL==1 */


gen Eng_Pass2=(Pass_Syll_Eng==1 & Pass_Word_Eng==1 & Pass_Sent_Eng==1 ) if Grade==1 /* & TestedEL==1 */
replace Eng_Pass2=(Pass_Word_Eng==1 & Pass_Sent_Eng==1 & Pass_Para_Eng==1) if Grade==2 /* & TestedEL==1 */
replace Eng_Pass2=(Pass_Read_Eng==1 & Pass_Compre_Eng==1) if  Grade==3 /* & TestedEL==1 */


gen Math_Pass2=(Pass_Count_Math==1 & Pass_Numbers_Math==1 & Pass_Inequal_Math==1 & Pass_Add_Math==1 & Pass_Sub_Math==1 ) if Grade==1 /* & TestedEL==1 */
replace Math_Pass2=(Pass_Inequal_Math==1 & Pass_Add_Math==1 & Pass_Sub_Math==1 & Pass_Mult_Math==1) if Grade==2 /* & TestedEL==1 */
replace Math_Pass2=(Pass_Add_Math==1 & Pass_Sub_Math==1 & Pass_Mult_Math==1 & Pass_Div_Math==1) if  Grade==3 /* & TestedEL==1 */

replace Kis_Pass=Kis_Pass2 
replace Eng_Pass= Eng_Pass2 
replace Math_Pass= Math_Pass2 



 
 
 
forvalues grd=1/3{
	foreach var of varlist Kis_SI- Kis_M Eng_L- Eng_C Math_ID- Math_G{
		sum `var' if Grade==`grd' & treatment==4
		capture gen Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd' & !missing(`var')
		capture replace Z_`var'=(`var'-r(mean))/r(sd) if Grade==`grd' & !missing(`var')
	}
}



egen Z_kiswahili=rowtotal(Z_Kis*), missing
egen Z_kiingereza=rowtotal(Z_Eng*), missing
egen Z_hisabati=rowtotal(Z_Math*), missing

***Now we standarize by grade subject
forvalues val=1/3{
	foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
		sum `var' if Grade==`val' & treatment==4
		replace `var'=(`var'-r(mean))/r(sd) if Grade==`val' & !missing(`var')
	}
}


foreach var of varlist Z_kiswahili Z_kiingereza Z_hisabati {
sum `var' if  treatment==4
replace `var'=(`var'-r(mean))/r(sd) if !missing(`var')
}

rename Grade GradeID
rename DistrictID DistID
rename Kis_Pass passkis
rename Eng_Pass passeng
rename Math_Pass passmath

gen dateTWA= date
format dateTWA %td

keep SchoolID GradeID Z_kiswahili Z_kiingereza Z_hisabati date passkis passeng passmath dateTWA StuID_15 DistID treatment



merge 1:1 StuID_15 using "$base_out/4 Intervention/TwaEL_2014/EDITwaweza_idlink4.dta"
drop if _merge==2
drop _merge

rename * =_T8
rename upidst_T8 upid
rename DistID_T8 DistID
rename SchoolID_T8 SchoolID
drop StuID_15_T8
*drop if upid=="" /*drop students without merging id*/
merge m:1 SchoolID upid using "$base_out/Consolidated/Student.dta"
*drop if upid=="" /*drop students without merging id*/
*drop if _merge==1
drop _merge
compress

saveold "$base_out/Consolidated/Student.dta", replace version(12)

preserve
egen dateTWA= mode(date_T8),by(SchoolID) maxmode
egen dateEDI= mode(date_T7),by(SchoolID) maxmode
keep dateTWA dateEDI SchoolID
collapse (mean) dateTWA dateEDI, by(SchoolID)
save "$base_out/Consolidated/SchoolDatesTest.dta", replace
restore

preserve
gen LagGrade=GradeID_T7
replace LagGrade=4 if GradeID_T3==3
replace LagGrade=GradeID_T8 if LagGrade==.
foreach var in  Z_hisabati Z_kiswahili Z_kiingereza Z_ScoreFocal seenUwezoTests preSchoolYN male{
gen Lag`var'=.
replace Lag`var'=`var'_T1 if !missing(`var'_T1)
replace Lag`var'=`var'_T5 if !missing(`var'_T5)
}

collapse (mean) LagZ_kiswahili LagZ_kiingereza LagZ_hisabati LagZ_ScoreFocal Z_kiswahili* Z_kiingereza* Z_hisabati*, by(LagGrade SchoolID)
foreach x of var * { 
	rename `x' MeanGrade_`x' 
} 
rename MeanGrade_LagGrade LagGrade
rename MeanGrade_SchoolID SchoolID
keep SchoolID LagGrade MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati 
save "$base_out/Consolidated/SchoolsGradeAverageScores.dta", replace
restore
foreach var in  Z_hisabati Z_kiswahili Z_kiingereza Z_ScoreFocal seenUwezoTests preSchoolYN male{
gen Lag`var'=.
replace Lag`var'=`var'_T1 if !missing(`var'_T1)
replace Lag`var'=`var'_T5 if !missing(`var'_T5)
}
collapse (mean) LagZ_kiswahili LagZ_kiingereza LagZ_hisabati LagZ_ScoreFocal Z_kiswahili* Z_kiingereza* Z_hisabati*, by(SchoolID)
foreach x of var * { 
	rename `x' MeanSchool_`x' 
} 
rename MeanSchool_SchoolID SchoolID
keep SchoolID MeanSchool_LagZ_kiswahili MeanSchool_LagZ_kiingereza MeanSchool_LagZ_hisabati 
save "$base_out/Consolidated/SchoolsAverageScores.dta", replace


********************
********************
*************** LETS CALCULCATE EXPECTED PAYOFF
********************
********************
/*
use  "$base_out/Consolidated/Student.dta", clear
/*

merge 1:1 upid using "$base_out/Consolidated/StudentExpectedPay.dta"
drop _merge
*/
foreach time in T3 T7{
foreach var in Z_hisabati Z_kiswahili Z_kiingereza  Z_ScoreFocal{
qui sum `var'_`time',d
replace `var'_`time'=r(p99) if `var'_`time'>r(p99) & !missing(`var'_`time')
replace `var'_`time'=r(p1) if `var'_`time'<r(p1) & !missing(`var'_`time')
}
}
saveold "$base_out/Consolidated/Student.dta", replace version(12)
