use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear

rename passmath_T8 passZ_hisabati_T8
rename passeng_T8 passZ_kiingereza_T8
rename passkis_T8 passZ_kiswahili_T8
  
tab GradeID_T8, gen(Grade_T)
tab DistID, gen(DistID_T)


foreach time in  T7{
	foreach var in Z_hisabati Z_kiswahili Z_kiingereza{
		foreach grade in 1 2 3{
			sum pass`var'_T8 if TreatmentBoth==0 & TreatmentCOD==0 & GradeID_`time'==`grade'
			scalar define media=10-`r(mean)'*10
			local xlabel ""                             // for x-axis labels                                // counter for quantiles   
			local models ""   
			local j=1              
			forvalues q = 10(10)90{
						scalar define a=`q'/100
						qui eststo mod_`q': qreg2 `var'_`time' ${treatmentlist} DistID_T1-DistID_T10  LagseenUwezoTests LagpreSchoolYN Lagmale LagAge ///
						LagZ_kiswahili LagZ_hisabati LagZ_kiingereza PTR_T1  SingleShift_T1 IndexDistancia_T1 InfrastructureIndex_T1 IndexFacilities_T1 s108_T1 ///
						if GradeID_`time'==`grade', cluster(SchoolID)  quantile(`=scalar(a)')
						estimates store mod_`q'
						local models `"`models' mod_`q' || "'
						local xlabel `"`xlabel' `j++' "Q{sub:`q'}""'
					}

			coefplot `models', keep(TreatmentCOD) graphregion(color(white)) ci vertical bycoefs ciopts(recast(rcap)) ///
			ylab(, ang(h) nogrid labsize(large)) xlab(none,labsize(large))   yline(0) ytitle("Incentives") xlabel(`xlabel', add) ///
			xline(`=scalar(media)', lcolor(gs10))
			graph export "$graphs/QuantileLowCOD_`grade'_`var'.pdf", replace
			
			coefplot `models', keep(TreatmentBoth) graphregion(color(white)) ci vertical bycoefs ciopts(recast(rcap)) ///
			ylab(, ang(h) nogrid labsize(large)) xlab(none,labsize(large))   yline(0) ytitle("Incentives") xlabel(`xlabel', add) ///
			xline(`=scalar(media)', lcolor(gs10))
			graph export "$graphs/QuantileLowCombo_`grade'_`var'.pdf", replace
			
			coefplot `models', keep(TreatmentCG) graphregion(color(white)) ci vertical bycoefs ciopts(recast(rcap)) ///
			ylab(, ang(h) nogrid labsize(large)) xlab(none,labsize(large))   yline(0) ytitle("Incentives") xlabel(`xlabel', add) ///
			xline(`=scalar(media)', lcolor(gs10))
			graph export "$graphs/QuantileLowCG_`grade'_`var'.pdf", replace
		}
	}
}
				 
	
