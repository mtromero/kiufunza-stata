   clear mata
   clear all
   set maxvar 20000
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
   gen Z_ScoreFocal_BL_Xtry=Z_ScoreFocal_BL
   keep Z_ScoreFocal_BL_Xtry
   replace Z_ScoreFocal_BL_Xtry=round(Z_ScoreFocal_BL_Xtry,0.01)
   duplicates drop
   drop if Z_ScoreFocal_BL_Xtry==.
   save "$base_out\3 Endline\Student\bootstrapLpoly", replace
   save "$base_out\3 Endline\Student\Z_ScoreFocal_BL_Xtry", replace
   set seed 1234
   local it = 1000
quietly forvalues j = 1(1)`it' {
   clear
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear

   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   bsample, cluster(SchoolID)
   append using "$base_out\3 Endline\Student\Z_ScoreFocal_BL_Xtry" 
keep Z_ScoreFocal Z_ScoreFocal_BL  TreatmentCG  Z_ScoreFocal_BL_Xtry
lpoly Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==0, nosc degree(0)  at(Z_ScoreFocal_BL_Xtry) generate(Y_0_`j') nograph
lpoly Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==1, nosc degree(0)  at(Z_ScoreFocal_BL_Xtry) generate(Y_1_`j') nograph
gen Diff_`j'=Y_1_`j'-Y_0_`j' 
drop if Z_ScoreFocal_BL_Xtry==.
keep Diff_`j' Z_ScoreFocal_BL_Xtry
merge 1:1 Z_ScoreFocal_BL_Xtry using "$base_out\3 Endline\Student\bootstrapLpoly"
drop _merge
save "$base_out\3 Endline\Student\bootstrapLpoly" , replace

}


use "$base_out\3 Endline\Student\bootstrapLpoly",clear
keep Diff* Z_ScoreFocal_BL_Xtry
egen ci_L=rowpctile(Diff*), p(2.5)
egen ci_H=rowpctile(Diff*), p(97.5)
keep ci_L ci_H Z_ScoreFocal_BL_Xtry
save "$base_out\3 Endline\Student\CI_lpoly",replace

use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear

   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   
lpoly Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==0, nosc degree(1)  at(Z_ScoreFocal_BL) generate(Y_0) nograph
lpoly Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==1, nosc degree(1)  at(Z_ScoreFocal_BL) generate(Y_1) nograph
gen Diff=Y_1-Y_0
keep  Diff Y_1 Y_0 Z_ScoreFocal_BL
append using "$base_out\3 Endline\Student\CI_lpoly"

drop if Y_1==. & Y_0==. & Diff==. & ci_H==. &  ci_L==.
twoway (rarea ci_L ci_H Z_ScoreFocal_BL_Xtry, sort fintensity(50)) ///
 (line Diff Z_ScoreFocal_BL, sort) (line Y_1 Z_ScoreFocal_BL, sort lcolor(blue)) (line Y_0 Z_ScoreFocal_BL, sort lcolor(red)), yline(0) ///
 legend(label(1 "95% CI") label(2 "Difference") label(3 "Treatment") label(4 "Control")) ///
ytitle("1st year score") xtitle("Basline score")
graph export "$results\Graphs\LpolyZScore_CG.pdf", as(pdf) replace
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  *******************************************************************************
  
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
   keep Z_ScoreFocal_BL
   replace Z_ScoreFocal_BL=round(Z_ScoreFocal_BL,0.01)
   drop if Z_ScoreFocal_BL==.
   duplicates drop
   save "$base_out\3 Endline\Student\bootstrapLowess", replace
   save "$base_out\3 Endline\Student\Z_ScoreFocal_BL", replace
      set seed 1234
   local it = 1000
quietly forvalues j = 1(1)`it' {
   clear
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
   replace Z_ScoreFocal_BL=round(Z_ScoreFocal_BL,0.01)
   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   bsample, cluster(SchoolID)
keep Z_ScoreFocal Z_ScoreFocal_BL  TreatmentCG 
lowess Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==0, generate(Low0) nog
lowess Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==1, generate(Low1) nog
gen YLow=Low0 if TreatmentCG==0
replace YLow=Low1 if TreatmentCG==1
collapse (mean) YLow, by(TreatmentCG Z_ScoreFocal_BL) 
reshape wide YLow, i(Z_ScoreFocal_BL) j(TreatmentCG)
gen Diff_`j'=YLow1-YLow0
keep  Diff_`j' Z_ScoreFocal_BL

merge 1:1 Z_ScoreFocal_BL using "$base_out\3 Endline\Student\bootstrapLowess"
drop if Z_ScoreFocal_BL==.
drop _merge
save "$base_out\3 Endline\Student\bootstrapLowess" , replace

}

use "$base_out\3 Endline\Student\bootstrapLowess",clear
keep Diff* Z_ScoreFocal_BL
egen ci_L=rowpctile(Diff*), p(2.5)
egen ci_H=rowpctile(Diff*), p(97.5)
keep ci_L ci_H Z_ScoreFocal_BL
save "$base_out\3 Endline\Student\CI_Lowess",replace



use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
replace Z_ScoreFocal_BL=round(Z_ScoreFocal_BL,0.01)
   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   
lowess Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==0, generate(Low0) nog
lowess Z_ScoreFocal Z_ScoreFocal_BL if TreatmentCG==1, generate(Low1) nog
gen YLow=Low0 if TreatmentCG==0
replace YLow=Low1 if TreatmentCG==1
collapse (mean) YLow, by(TreatmentCG Z_ScoreFocal_BL) 
reshape wide YLow, i(Z_ScoreFocal_BL) j(TreatmentCG)
gen Diff=YLow1-YLow0
keep YLow1 YLow0 Diff Z_ScoreFocal_BL

append using "$base_out\3 Endline\Student\CI_Lowess"

drop if YLow1==. & YLow0==. & Diff==. & ci_H==. &  ci_L==.
twoway (rarea ci_L ci_H Z_ScoreFocal_BL, sort fintensity(50)) (line Diff Z_ScoreFocal_BL, sort) ///
(line YLow1 Z_ScoreFocal_BL, sort lcolor(blue)) (line YLow0 Z_ScoreFocal_BL, sort lcolor(red))  , yline(0) ///
legend(label(1 "95% CI") label(2 "Difference") label(3 "Treatment") label(4 "Control")) ///
ytitle("1st year score") xtitle("Basline score")
graph export "$results\Graphs\LowessZScore_CG.pdf", as(pdf) replace
   
   
