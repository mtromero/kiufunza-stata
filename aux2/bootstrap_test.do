     **********************
  **************Student
  ************************
   set matsize 10000
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
   
   gen studentGrade=StudentsGr1 if stdgrd==1
   replace studentGrade=StudentsGr2 if stdgrd==2
   replace studentGrade=StudentsGr3 if stdgrd==3

	 
   
   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"

   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"

   
eststo clear   
 eststo: regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth, vce(cluster SchoolID)
 eststo: regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth, vce(bootstrap, cluster(SchoolID) reps(1000))
 
 
 

