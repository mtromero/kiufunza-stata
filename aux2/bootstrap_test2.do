     **********************
  **************Student
  ************************
  log using "$results\Logs\Boot.smcl", replace
     local NBoot=1000
	    set seed 1234
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear
   
   gen studentGrade=StudentsGr1 if stdgrd==1
   replace studentGrade=StudentsGr2 if stdgrd==2
   replace studentGrade=StudentsGr3 if stdgrd==3

	 
   
   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"

   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   gen SchoolGradeID= string(SchoolID)+ string(stdgrd)
   

 eststo clear  
 eststo: regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth
 eststo: regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth, vce(cluster SchoolID)
 eststo: regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth, vce(bootstrap, cluster(SchoolID) reps(`NBoot'))
 eststo: bootstrap, reps(`NBoot') strata(SchoolGradeID) : regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth
 eststo: bootstrap, reps(`NBoot') cluster(SchoolID) : regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth
 eststo: bootstrap, reps(`NBoot') strata(SchoolID) : regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth
 eststo: bootstrap, reps(`NBoot') cluster(SchoolGradeID) : regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth 
 eststo: bootstrap, reps(`NBoot') strata(SchoolID) cluster(SchoolGradeID) : regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth 
 esttab, se ar2 label  replace  b(a3) se(a3) nocon nomtitles  keep(TreatmentCG TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01)
	

   local it = `NBoot'
   matrix mat1 = J(`it',3,.)
quietly forvalues j = 1(1)`it' {
   clear
   use "$base_out\3 Endline\Student\StudentConsolidated.dta", clear

   gen TreatmentCG=0 
   replace TreatmentCG=1 if treatment=="CG" | treatment=="Both"
   label var TreatmentCG "CG"
   gen TreatmentCOD=0 
   replace TreatmentCOD=1 if treatment=="COD" | treatment=="Both"
   label var TreatmentCOD "COD"
   gen TreatmentBoth=0 
   replace TreatmentBoth=1 if treatment=="Both"
   label var TreatmentBoth "CG*COD"
   gen SchoolGradeID= string(SchoolID)+ string(stdgrd)
   bsample, cluster(SchoolID)
  regress Z_ScoreFocal TreatmentCG TreatmentCOD TreatmentBoth
  matrix mat1[`j',1] = _b[TreatmentCG]
  matrix mat1[`j',2] = _b[TreatmentCOD]
  matrix mat1[`j',3] = _b[TreatmentBoth]
  }
  clear
  svmat mat1
  rowsd(varlist)
  sum mat11 mat12 mat13
  
log close

 

