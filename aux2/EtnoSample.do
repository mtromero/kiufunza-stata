use "$base_out\Consolidated\Student.dta", replace
   
   
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

reg Z_ScoreFocal_T3 Z_ScoreFocal_T1 i.DistID preSchoolYN_T1 Age_T1 i.Gender_T1 i.stdgrd_T3 
predict resid, residuals

*twoway (kdensity  resid if treatarm==1) (kdensity  resid if treatarm==2) (kdensity  resid if treatarm==3) (kdensity  resid if treatarm==4) ///
*,legend( lab(1 "Both")  lab(2 "CG") lab(3 "COD") lab(4 "Control"))   title("Distribution of teacher value-added") ytitle("Density")

sum Z_ScoreFocal_T1 Z_ScoreFocal_T3 resid if treatarm==1
sum Z_ScoreFocal_T1 Z_ScoreFocal_T3 resid if treatarm==2
sum Z_ScoreFocal_T1 Z_ScoreFocal_T3 resid if treatarm==3
sum Z_ScoreFocal_T1 Z_ScoreFocal_T3 resid if treatarm==4


collapse (mean) resid treatarm DistID, by(SchoolID)
preserve
drop if (treatarm!=1 & treatarm!=3)
sort resid 
egen Order=seq()
save "$base_out\Consolidated\EtnoSampleCODCOMBO.dta", replace
restore
drop if (treatarm!=2)
sort resid 
egen Order=seq()
save "$base_out\Consolidated\EtnoSampleCG.dta", replace



use "$base_out\Consolidated\Student.dta", replace
   

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "CG"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "COD"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

collapse (mean) Z_ScoreFocal_T3 treatarm DistID, by(SchoolID)
preserve
drop if (treatarm!=1 & treatarm!=3)
sort Z_ScoreFocal_T3 
egen Order=seq()
save "$base_out\Consolidated\EtnoSampleCODCOMBO_Levels.dta", replace
restore
drop if (treatarm!=2)
sort Z_ScoreFocal_T3 
egen Order=seq()
save "$base_out\Consolidated\EtnoSampleCG_Levels.dta", replace

/*
*This needs to be changed!
drop if (DistID!=1 & DistID!=5 & DistID!=10)
preserve 

*First COD and COMBO High
drop if (treatarm!=1 & treatarm!=3)


_pctile resid, p(45, 55, 85)
drop if resid< r(r3)

sample 1, count by(DistID)
gen Tipo="High COD/COMBO"
save "$base_out\Consolidated\EtnoSample.dta", replace

restore
preserve

*First COD and COMBO Medium
drop if (treatarm!=1 & treatarm!=3)
_pctile resid, p(45, 55, 85)

drop if (resid< r(r1) | resid> r(r2))

sample 1, count by(DistID)
gen Tipo="Median COD/COMBO"
append using "$base_out\Consolidated\EtnoSample.dta"
save "$base_out\Consolidated\EtnoSample.dta",replace


restore

drop if (treatarm!=2)
xtile Pctile=resid,  n(3)
sample 1, count by(Pctile)
gen Tipo="CG"
append using "$base_out\Consolidated\EtnoSample.dta"
save "$base_out\Consolidated\EtnoSample.dta",replace
