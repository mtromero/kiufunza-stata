clear all

* Central Path
 *global mipath "E:\Box Sync\01_KiuFunza"
  global mipath "C:\Users\Mauricio\Box Sync\01_KiuFunza\"
 *global mipath "C:\Users\Mauricio\Box Sync\KiuFunza"
 *global mipath "Z:\Box Sync\KiuFunza"
* Path Tree
  global basein 	"$mipath\RawData"
  global base_out   "$mipath\CreatedData"
  global dir_do     "$mipath\StataCode"
  global results    "$mipath\Results"
  global exceltables  "$mipath\Results\ExcelTables"
  global graphs     "$mipath\Results\Graphs"
    global latexcodes     "$mipath\Results\LatexCodes"


use "$basein\3 Endline\Teacher\ETGrdSub_noPII.dta", clear  
	
 keep gsbhrs ETTeacherID ETGradeID ETGrdSubID
 reshape wide gsbhrs, i(ETTeacherID ETGradeID) j( ETGrdSubID )
 
 merge m:1 ETTeacherID using "$basein\3 Endline\Teacher\ETTeacher_noPII.dta", keepus(SchoolID)
 drop _merge

  collapse (sum) gsbhrs1 gsbhrs2 gsbhrs3 gsbhrs4 gsbhrs5 gsbhrs6 gsbhrs7 gsbhrs8 gsbhrs9 gsbhrs10 gsbhrs11, by(SchoolID ETGradeID)

 /*
 rename gsbhrs1 HrsMath
 rename gsbhrs2 HrsSwahili
 rename gsbhrs3 HrsEnglish

 */
 
  drop gsbhrs4-gsbhrs11
 rename ETGradeID GradeID
 drop if GradeID>=4

 
qui merge 1:1 SchoolID GradeID using "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\1 Baseline\School\GradesHead_noPII.dta", keepus(DistrictID SchoolID hrsWk_eng hrsWk_maths hrsWk_kisw hrsWk_voc hrsWk_ict hrsWk_geog hrsWk_science hrsWk_sportPers)
drop _merge
drop if GradeID>=4

gen DiffEng= hrsWk_eng- gsbhrs3
gen DiffMath=  hrsWk_maths- gsbhrs1
gen DiffKisw=  hrsWk_kisw- gsbhrs2



merge m:1 SchoolID using "$base_out\3 Endline\School\School.dta", keepus(treatment treatarm)
drop _merge
tab treatment, gen(TD) 
 corr gsbhrs1 hrsWk_maths
  corr gsbhrs3 hrsWk_eng
   corr gsbhrs2 hrsWk_kisw
reg DiffEng i.GradeID i.DistrictID TD1 TD2 TD3, vce(cluster SchoolID)
reg DiffKisw i.GradeID i.DistrictID TD1 TD2 TD3, vce(cluster SchoolID)
reg DiffMath i.GradeID i.DistrictID TD1 TD2 TD3, vce(cluster SchoolID)
