*******************************************************
*************BASE LINE 2013
********************************************************

use "$basein/1 Baseline/School/GradesHead_noPII.dta", clear
gen Enroll= s184+ s185
collapse (sum) Enroll, by (SchoolID)

merge 1:m SchoolID using "$basein/1 Baseline/School/Expenses_noPII.dta"
drop _merge


gen ExpenseGroup=ExpenseID
replace  ExpenseGroup=1 if ExpenseID==1 | ExpenseID==2 | ExpenseID==3 | ExpenseID==4 | ExpenseID==5
replace  ExpenseGroup=2 if ExpenseID==7 | ExpenseID==8
replace  ExpenseGroup=3 if ExpenseID==9 | ExpenseID==11 | ExpenseID==12
replace  ExpenseGroup=4 if ExpenseID==13 | ExpenseID==14 | ExpenseID==15
replace  ExpenseGroup=5 if ExpenseID==6
replace  ExpenseGroup=6 if ExpenseID==10
recode s163 (-99/-1=.)



label define expensegroup 1 "Administrative" 2 "Student Aid" 3 "Teaching Aid" 4 "Teachers" 5 "Construction" 6 "Textbooks"
label values ExpenseGroup  expensegroup
label variable 	ExpenseGroup "Expense Category"

collapse (mean) Enroll  (sum)  s163 , by(ExpenseGroup SchoolID)
/*
drop if ExpenseGroup==4
collapse (mean) Enroll (sum) s163, by( SchoolID)
gen SpendPC=s163/Enroll
sum SpendPC
*/
reshape wide s163, i(SchoolID) j(ExpenseGroup)


rename s1631 administrative_expenses
rename s1632 student_expenses
rename s1633 teaching_aid_expenses
rename s1634 Teacher_expenses
rename s1635 Construction_expenses
rename s1636 textbook_expenses


label variable 	administrative_expenses "Administrative Expenses"
label variable 	student_expenses "Student Aid Expenses"
label variable 	teaching_aid_expenses "Teaching Aid Expenses"
label variable 	Teacher_expenses "Teachers Expenses"
label variable 	Construction_expenses "Construction Expenses"
label variable 	textbook_expenses "Textbook Expenses"

foreach var of varlist administrative_expenses- Construction_expenses{
replace `var'=0 if `var'<0
gen `var'PS=`var'/Enroll
}
egen TotalExpensesPS=rowtotal(administrative_expensesPS- Construction_expensesPS), missing

merge 1:1 SchoolID using "$basein/1 Baseline/School/School_noPII.dta"

drop _merge

save "$base_out/1 Baseline/School/School.dta", replace


use "$basein/1 Baseline/School/Facilities_noPII.dta", clear

drop totStreamsGrade1
drop totStreamsGrade2
drop totStreamsGrade3
drop s144
reshape wide s142 s143, i(SchoolID) j(FacilityID)


label variable s1421 "Km District education office"
label variable s1422 "Km Closest bank"
label variable s1423 "Km Closest post office"
label variable s1424 "Km Closest high school"
label variable s1425 "Km Closest govt primary school"

label variable s1431 "Min. District education office"
label variable s1432 "Min. Closest bank"
label variable s1433 "Min. Closest post office"
label variable s1434 "Min. Closest high school"
label variable s1435 "Min. Closest govt primary school"

pca s1421- s1425
predict IndexDistancia, score

pca s1431- s1435
predict IndexTiempo, score

merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge
compress

rename s33 SizeSchoolCommittee
gen PropCommitteeFemale=s34/SizeSchoolCommittee
gen PropCommitteeTeachers=committeeTeachers/SizeSchoolCommittee
gen PropCommitteeParents=committeeParents/SizeSchoolCommittee
rename s36 TimesCommitteeMet2012
rename noticeboard noticeboard
rename s172 KeepRecords

pca SizeSchoolCommittee  PropCommitteeFemale PropCommitteeTeachers PropCommitteeParents  TimesCommitteeMet2012 s42
predict IndexCommittee, score

save "$base_out/1 Baseline/School/School.dta", replace




use "$basein/1 Baseline/School/Funding_noPII.dta", clear

drop    s158 s159 s160 s161 category2 category3 category4 category5 category6 category7 category8 category8_other
rename s155 s155
recode s155 (-99/-1=.)
reshape wide s155, i(SchoolID) j(FundingID)

label variable s1551 "Recieved fund from national government capitation funds"
label variable s1552 "Recieved fund from other national government programs"
label variable s1553 "Recieved fund from local government support"
label variable s1554 "Recieved fund from PTA fees"
label variable s1555 "Recieved fund from churches/NGOs/donors"
label variable s1556 "Recieved fund from fundraising"
label variable s1557 "Recieved fund from other sources"

pca s1551- s1557
predict IndexSupport, score

merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace


use "$basein/1 Baseline/School/GradesHead_noPII.dta", clear
drop  s182 s183 s184 s185 hrsWk_eng hrsWk_maths hrsWk_kisw hrsWk_voc hrsWk_ict hrsWk_geog hrsWk_science hrsWk_sportPers
rename s13 s13
reshape wide s13, i(SchoolID) j(GradeID)


label variable s131 "Head of school teaching 1st grade"
label variable s132 "Head of school teaching 2nd grade"
label variable s133 "Head of school teaching 3rd grade"
label variable s134 "Head of school teaching 4th grade"
label variable s135 "Head of school teaching 5th grade" 
label variable s136 "Head of school teaching 6th grade"
label variable s137 "Head of school teaching 7th grade"

merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace


use "$basein/1 Baseline/School/GradesHead_noPII.dta", clear
gen StudentsGr= s184+s185
keep StudentsGr SchoolID GradeID
reshape wide StudentsGr, i(SchoolID) j(GradeID)


merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace



use "$basein/1 Baseline/School/SchoolFacilities_noPII.dta", clear

drop s146 s147
rename s145 s145
reshape wide s145, i(SchoolID) j(SchoolFacilityID)


label variable s1451 "School has Kitchen"
label variable s1452 "School has Library"
label variable s1453 "School has Playground"
label variable s1454 "School has Staff room"
label variable s1455 "School has Outer wall or fence"
label variable s1456 "School has Receive a newspaper"
recode s1451 s1452 s1453 s1454 s1455 s1456  (2=0)
pca s1451 s1452 s1453 s1454 s1455 s1456
predict InfrastructureIndex,score

merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop if _merge!=3
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop if _merge!=3
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace


recode s108 s118 s120 computersYN s191 s188 s175 s200  (2=0)


egen StudentsTotal=rowtotal(StudentsGr1-StudentsGr7), missing

gen ToiletsStudents=s126/StudentsTotal
gen ClassRoomsStudents=s110/StudentsTotal
gen TeacherStudents=s68/StudentsTotal

gen PipedWater=0
replace PipedWater=1 if s124==5
gen NoWater=0
replace NoWater=1 if s124==1
gen SingleShift=0
replace SingleShift=1 if s198==1



label variable totStreamsGrade1 "No. of streams in grade 1"
label variable totStreamsGrade2 "No. of streams in grade 2"
label variable totStreamsGrade3 "No. of streams in grade 3"
label variable PipedWater "Piped drinking water"
label variable NoWater "No drinking water"
label variable SingleShift "Single Shift"

recode noticeboard KeepRecords  (2=0)


pca s1451- s1456 s108 computersYN   s120  s175  s200 NoWater s191 s188 s118 ToiletsStudents ClassRoomsStudents TeacherStudents SingleShift PipedWater noticeboard KeepRecords s128 s108
predict IndexFacilities, score 

gen PTR=Enroll/s68


compress
save "$base_out/1 Baseline/School/School.dta", replace

*******************************************************
*************Skills questions Baseline Y2... im gonna add them to Y1
********************************************************
use "$basein/6 Baseline 2014/Data/school/R4School_noPII.dta", clear

recode aim1 -aim12 (2=0)
pca aim1 -aim12


recode dgtrc3- dgtrc11 (2=0)
forvalues i=4/11{
recode dgtrc`i' (.=0) if dgtrc`=scalar(`i'-1)'==0
}



keep dgtrc3- dgtrc11 aim1 -aim12   SchoolID
merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace

use "$basein/7 Monitoring 2014/Data/School and Teacher/R5School_noPII.dta", clear
recode aim1 -aim12 (2=0)


recode dgtrc3- dgtrc11 (2=0)
forvalues i=4/11{
recode dgtrc`i' (.=0) if dgtrc`=scalar(`i'-1)'==0
}


keep dgtrc3- dgtrc11 aim1 -aim12   SchoolID
merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta", update
drop _merge
save "$base_out/1 Baseline/School/School.dta", replace

use "$basein/8 Endline 2014/Final Data/School/R6School_noPII.dta", clear
recode aim1 -aim12 (2=0)

recode dgtrc3- dgtrc11 (2=0)
forvalues i=4/11{
recode dgtrc`i' (.=0) if dgtrc`=scalar(`i'-1)'==0
}
keep dgtrc3- dgtrc11 aim1 -aim12   SchoolID 
merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta", update
drop _merge
pca dgtrc3- dgtrc11
save "$base_out/1 Baseline/School/School.dta", replace

use "$basein/9 Baseline 2015/Final Data/School/R7School_noPII.dta", clear
recode aim1 -aim12 (2=0)

recode dgtrc3- dgtrc11 (2=0)
forvalues i=4/11{
recode dgtrc`i' (.=0) if dgtrc`=scalar(`i'-1)'==0
}
keep dgtrc3- dgtrc11 aim1 -aim12   SchoolID 
merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta", update
drop _merge
pca dgtrc3- dgtrc11
save "$base_out/1 Baseline/School/School.dta", replace

pca dgtrc3- dgtrc11
predict IndexRecall, score
pca aim1 -aim12
predict IndexManagerial, score
save "$base_out/1 Baseline/School/School.dta", replace




*******************************************************
*************FUNDING DATA - Baseline Y1
********************************************************
use "$basein/1 Baseline/School/Funding_noPII.dta", clear
replace s155=0 if s155==2
keep s155 SchoolID FundingID
reshape wide s155 , i(SchoolID) j(FundingID)
replace s1556=s1554+s1556
replace s1556=1 if s1556==2
rename s1551 GovermentCG_D
rename s1552 GovermentOther_D
rename s1553 LocalGoverment_D
rename s1555 NGO_D
rename s1556 Parents_D
rename s1557 Other_D
save "$base_out/1 Baseline/School/FundingLong.dta", replace


merge 1:1 SchoolID using "$base_out/1 Baseline/School/School.dta"
drop _merge


keep TotalExpensesPS s128 PTR *Index* SchoolID GovermentCG_D GovermentOther_D LocalGoverment_D s1554 NGO_D Parents_D Other_D StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7 s1451 s1452 s1453 s1454 s1455 s1456 s1431 s1432 s1433 s1434 s1435 computersYN s120 s118 PipedWater NoWater SingleShift ToiletsStudents ClassRoomsStudents TeacherStudents s188 s175 s200 s108 SizeSchoolCommittee KeepRecords noticeboard PropCommitteeFemale PropCommitteeTeachers PropCommitteeParents StudentsTotal TimesCommitteeMet2012
rename * =_T1
rename SchoolID_T1 SchoolID
label var IndexFacilities_T1 "Facilities (PCA)"
label var IndexSupport_T1  "Support (PCA)"
label var IndexCommittee_T1 "Committee (PCA)" 
label var IndexTiempo_T1 "Remote (mins) (PCA)" 
label var IndexDistancia_T1  "Remote (KM) (PCA)"
label var IndexRecall_T1  "Recall (PCA)"
label var IndexManagerial_T1 "Managerial (PCA)"
save "$base_out/Consolidated/School.dta", replace


*******************************************************
*************MID LINE 2013
********************************************************

use "$basein/2 Midline/MSchool_noPII.dta", clear


compress
save "$base_out/2 Midline/School.dta", replace



keep SchoolID ifnbyn ifscyn ifpmyn ifotyn
rename * =_T2
rename SchoolID_T2 SchoolID

merge 1:1 SchoolID using "$base_out/Consolidated/School.dta"
drop if _merge==2
drop _merge
save "$base_out/Consolidated/School.dta", replace

*******************************
******************End line 2013
**********************

use "$basein/3 Endline/School/ESchool_noPII.dta", clear
save "$base_out/3 Endline/School/School.dta", replace

use "$basein/3 Endline/School/EGrade_noPII.dta", clear

keep SchoolID EGradeID  s184 s185 gdrctb
replace gdrctb=. if gdrctb==-99
replace gdrctb=0 if gdrctb==.
reshape wide s184 s185 gdrctb, i(SchoolID) j(EGradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

label variable gdrctb1 "Expenditure in textbooks grade 1"
label variable gdrctb2 "Expenditure in textbooks grade 2"
label variable gdrctb3 "Expenditure in textbooks grade 3"
label variable gdrctb4 "Expenditure in textbooks grade 4"
label variable gdrctb5 "Expenditure in textbooks grade 5"
label variable gdrctb6 "Expenditure in textbooks grade 6"
label variable gdrctb7 "Expenditure in textbooks grade 7"


gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"


merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
save "$base_out/3 Endline/School/School.dta", replace


use "$basein/3 Endline/School/EExpenses_noPII.dta", clear 

replace s168=0 if s168==.
replace cgspct=0 if cgspct==.
recode s168 cgspct (-99/-1=.)
gen ExpenseGroup=.
replace  ExpenseGroup=1 if EExpenseID==1 | EExpenseID==2 | EExpenseID==3 | EExpenseID==4 | EExpenseID==5 
replace  ExpenseGroup=2 if EExpenseID==7 | EExpenseID==8 | EExpenseID==16
replace  ExpenseGroup=3 if EExpenseID==9  | EExpenseID==11 | EExpenseID==12 
replace  ExpenseGroup=4 if EExpenseID==13 | EExpenseID==14 | EExpenseID==15
replace  ExpenseGroup=5 if EExpenseID==6
replace  ExpenseGroup=6 if EExpenseID==10

label define expensegroup 1 "Administrative" 2 "Student Aid" 3 "Teaching Aid" 4 "Teachers" 5 "Construction" 6 "Textbooks"
label values ExpenseGroup  expensegroup
label variable 	ExpenseGroup "Expense Category"


collapse (sum) s168 cgspct, by(ExpenseGroup SchoolID)

reshape wide s168 cgspct, i(SchoolID) j(ExpenseGroup)

rename s1681 administrative_expenses
rename s1682 student_expenses
rename s1683 teaching_aid_expenses
rename s1684 Teacher_expenses
rename s1685 Construction_expenses
rename s1686 textbook_expenses

label variable 	administrative_expenses "Administrative Expenses"
label variable 	student_expenses "Student Aid Expenses"
label variable 	teaching_aid_expenses "Teaching Aid Expenses"
label variable 	Teacher_expenses "Teachers Expenses"
label variable 	Construction_expenses "Construction Expenses"
label variable 	textbook_expenses "Textbook Expenses"

rename cgspct1 administrative_twaweza
rename cgspct2 student_twaweza
rename cgspct3 teaching_aid_twaweza
rename cgspct4 Teacher_twaweza
rename cgspct5 Construction_twaweza
rename cgspct6 textbook_twaweza


label variable 	administrative_twaweza "Administrative Twaweza"
label variable 	student_twaweza "Student Aid Twaweza"
label variable 	teaching_aid_twaweza "Teaching Aid Twaweza"
label variable 	Teacher_twaweza "Teachers Twaweza"
label variable 	Construction_twaweza "Construction Twaweza"
label variable 	textbook_twaweza "Textbook Twaweza"


merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
save "$base_out/3 Endline/School/School.dta", replace

use "$basein/3 Endline/School/ESFacility_noPII.dta", clear

keep  SchoolID ESFacilityID factyn
drop if ESFacilityID<8
replace factyn=0 if factyn==2

reshape wide factyn, i(SchoolID) j(ESFacilityID)


label variable factyn8 "Kitchen"
label variable factyn9 "Library"
label variable factyn10 "Assembly Hall"
label variable factyn11 "Playground"
label variable factyn12 "Water Source"
label variable factyn13 "Outer Wall/Fence"
merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
save "$base_out/3 Endline/School/School.dta", replace

use "$basein/3 Endline/School/EVolunteer_noPII.dta", clear 

collapse (count)  EVolunteerID , by(SchoolID) 
rename EVolunteerID NumberOfVolunteers
label variable 	NumberOfVolunteers "Number of volunteers at school"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
save "$base_out/3 Endline/School/School.dta", replace

use "$basein/3 Endline/School/ETeacher_noPII.dta", clear 

collapse (count)  ETeacherID , by(SchoolID) 
rename ETeacherID NumberOfTeachers
label variable 	NumberOfTeachers "Number of teachers at school"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
save "$base_out/3 Endline/School/School.dta", replace


use "$basein/3 Endline/Teacher/ETGrade_noPII.dta", clear 

save "$base_out/3 Endline/School/TeacherGradeTemp.dta", replace 

use "$basein/3 Endline/Supplementing/IDLinkTeachers_R3_noPII.dta", clear 

keep SchoolID ETTeacherID

merge 1:m ETTeacherID using "$base_out/3 Endline/School/TeacherGradeTemp.dta"

save "$base_out/3 Endline/School/TeacherGradeTemp.dta", replace 

drop if ETGradeID!=1

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S1
label variable 	NumberOfTeachers "Number of teachers in S1"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace	 

use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=2

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S2
label variable 	NumberOfTeachers "Number of teachers in S2"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace

use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=3

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S3
label variable 	NumberOfTeachers "Number of teachers in S3"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace

use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=4

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S4
label variable 	NumberOfTeachers "Number of teachers in S4"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace

use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=5

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S5
label variable 	NumberOfTeachers "Number of teachers in S5"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace

use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=6

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S6
label variable 	NumberOfTeachers "Number of teachers in S6"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge 
save "$base_out/3 Endline/School/School.dta", replace


use "$base_out/3 Endline/School/TeacherGradeTemp.dta", clear     
drop if ETGradeID!=7

collapse (sum) grdeyn , by(SchoolID) 

rename grdeyn NumberOfTeachers_S7
label variable 	NumberOfTeachers "Number of teachers in S7"

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
compress
save "$base_out/3 Endline/School/School.dta", replace


gen PTR=(StudentsGr1+StudentsGr2+StudentsGr3+StudentsGr4+StudentsGr5+StudentsGr6+StudentsGr7)/s68

gen studentsTeacherRatio=(StudentsGr1+StudentsGr2+StudentsGr3+StudentsGr4+StudentsGr5+StudentsGr6+StudentsGr7)/NumberOfTeachers
gen studentsVolunteerRatio=(StudentsGr1+StudentsGr2+StudentsGr3+StudentsGr4+StudentsGr5+StudentsGr6+StudentsGr7)/NumberOfVolunteers
gen studentsTeacherRatio_S1=StudentsGr1/NumberOfTeachers_S1
gen studentsTeacherRatio_S2=StudentsGr2/NumberOfTeachers_S2
gen studentsTeacherRatio_S3=StudentsGr3/NumberOfTeachers_S3
gen studentsTeacherRatio_S4=StudentsGr4/NumberOfTeachers_S4
gen studentsTeacherRatio_S5=StudentsGr5/NumberOfTeachers_S5
gen studentsTeacherRatio_S6=StudentsGr6/NumberOfTeachers_S6
gen studentsTeacherRatio_S7=StudentsGr7/NumberOfTeachers_S7

label variable 	studentsTeacherRatio "Overall student teacher ratio"
label variable 	studentsVolunteerRatio "Overall volunteer teacher ratio"
label variable 	studentsTeacherRatio_S1 "student teacher ratio S1"
label variable 	studentsTeacherRatio_S2 "student teacher ratio S2"
label variable 	studentsTeacherRatio_S3 "student teacher ratio S3"
label variable 	studentsTeacherRatio_S4 "student teacher ratio S4"
label variable 	studentsTeacherRatio_S5 "student teacher ratio S5"
label variable 	studentsTeacherRatio_S6 "student teacher ratio S6"
label variable 	studentsTeacherRatio_S7 "student teacher ratio S7"

egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7), missing

compress

save "$base_out/3 Endline/School/School.dta", replace

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge

gen strategicly_change_teachers=0 
replace strategicly_change_teachers=1 if iptcst_c==1
label variable 	strategicly_change_teachers "HT changed teachers strategically"  


  gen strategicly_change_students=0 
replace strategicly_change_students=1 if iptcst_b==1
label variable 	strategicly_change_students "HT changed students strategically" 
compress
save "$base_out/3 Endline/School/School.dta", replace

********How many hours teach each subject in each grade

use "$basein/3 Endline/Teacher/ETGrdSub_noPII.dta", clear  

keep gsbhrs ETTeacherID ETGradeID ETGrdSubID
reshape wide gsbhrs, i(ETTeacherID ETGradeID) j( ETGrdSubID )

merge m:1 ETTeacherID using "$basein/3 Endline/Teacher/ETTeacher_noPII.dta", keepus(SchoolID)



collapse (sum) gsbhrs1 gsbhrs2 gsbhrs3 gsbhrs4 gsbhrs5 gsbhrs6 gsbhrs7 gsbhrs8 gsbhrs9 gsbhrs10 gsbhrs11, by(SchoolID ETGradeID)
drop if ETGradeID==4
drop if ETGradeID==5
drop if ETGradeID==6
drop if ETGradeID==7
drop if ETGradeID==.
rename gsbhrs1 HrsMath
rename gsbhrs2 HrsSwahili
rename gsbhrs3 HrsEnglish
drop gsbhrs4-gsbhrs11
gen HrsFocal=HrsMath+HrsSwahili+HrsEnglish
reshape wide HrsMath HrsSwahili HrsEnglish HrsFocal, i(SchoolID) j(ETGradeID)
forvalues i=1/3{
replace HrsMath`i'=0 if HrsMath`i'==.
replace HrsSwahili`i'=0 if HrsSwahili`i'==.
replace HrsEnglish`i'=0 if HrsEnglish`i'==.
replace HrsFocal`i'=0 if HrsFocal`i'==.
}

merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
compress
save "$base_out/3 Endline/School/School.dta", replace	 



rename nocmmt TimesCommitteeMet2013



foreach var of varlist  textbook_twaweza textbook_expenses administrative_expenses student_expenses teaching_aid_expenses Teacher_expenses Construction_expenses administrative_twaweza student_twaweza teaching_aid_twaweza Teacher_twaweza Construction_twaweza{
gen `var'PS=`var'/TotalNumberStudents
*replace `var'PS=. if `var'PS>100000
}

forval i=1/7  {
replace gdrctb`i'=0 if gdrctb`i'==.
replace StudentsGr`i'=0 if StudentsGr`i'==.
gen gdrctb`i'PS=gdrctb`i'/StudentsGr`i'
}

gen ExpenditureBooksFocalPS=(gdrctb1+gdrctb2+gdrctb3)/(StudentsGr1+StudentsGr2+StudentsGr3)
gen ExpenditureBooksNonFocalPS=(gdrctb4+gdrctb5+gdrctb6+gdrctb7)/(StudentsGr4+StudentsGr5+StudentsGr6+StudentsGr7)
label var ExpenditureBooksFocalPS "\\$ Textbooks/Student in focal grades"
label var ExpenditureBooksNonFocalPS "\\$ Textbooks/Student in non-focal grades"

pca httmus_a- httmus_h
predict IndexTimeSpend, score



compress
save "$base_out/3 Endline/School/School.dta", replace	 

   
*******************************************************
*************FUNDING DATA - Endline 2013
********************************************************

 
use "$basein/3 Endline/School/EFunding_noPII.dta", clear
replace inkdyn=0 if inkdyn==2
keep s155 SchoolID EFundingID inkdyn
recode s155 inkdyn (-99/-1=.)
reshape wide s155 inkdyn, i(SchoolID) j(EFundingID)

label var s1551 "Government CG"
label var s1552 "Government Other"
label var s1553 "Local Government"
label var s1555 "NGOs"
label var s1556 "Parents"
label var s1557 "Other"

label var inkdyn1 "Government CG"
label var inkdyn2 "Government Other"
label var inkdyn3 "Local Government"
label var inkdyn5 "NGOs"
label var inkdyn6 "Parents"
label var inkdyn7 "Other"

rename inkdyn1 GovermentCG_D
rename inkdyn2 GovermentOther_D
rename inkdyn3 LocalGoverment_D
rename inkdyn5 NGO_D
rename inkdyn6 Parents_D
rename inkdyn7 Other_D

rename s1551 GovermentCG_M
rename s1552 GovermentOther_M
rename s1553 LocalGoverment_M
rename s1555 NGO_M
rename s1556 Parents_M
rename s1557 Other_M


merge 1:1 SchoolID using "$base_out/3 Endline/School/School.dta"
drop _merge
  
   foreach var of varlist GovermentCG_M GovermentOther_M LocalGoverment_M NGO_M Parents_M Other_M{
   replace `var'=`var'/TotalNumberStudents
   }

compress
save "$base_out/3 Endline/School/School.dta", replace	


rename * =_T3
rename SchoolID_T3 SchoolID
rename DistrictID_T3 DistrictID
rename treatment_T3 treatment
rename treatarm_T3 treatarm

merge 1:1 SchoolID using "$base_out/Consolidated/School.dta"
drop if _merge!=3
drop _merge


foreach var in ifnbyn ifscyn ifpmyn ifotyn{
egen `var'_Since_T1=rowtotal(`var'_T2 `var'_T3), missing
replace `var'_Since_T1=1 if `var'_Since_T1==2
}

save "$base_out/Consolidated/School.dta", replace


   
   


   
*******************************************************
*************INTERVENTION DATA
********************************************************
use "$basein/4 Intervention/TwaEL_2013/TwaTestData.dta", clear


foreach name in "k" "e" "m"{
forval i=1/3  {
gen PR_`i'_`name'=Passed_`i'`name'/NrTests_`i'
if "`name'"=="k" label var PR_`i'_`name' "K S`i'"
if "`name'"=="e" label var PR_`i'_`name' "E S`i'"
if "`name'"=="m" label var PR_`i'_`name' "M S`i'"
}
}




save "$base_out/4 Intervention/TwaEL_2013/School.dta", replace
drop DistrictID
rename * =_T4
rename SchoolID_T4 SchoolID

merge 1:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
compress

*****Estoi se hace en la student data*********
/*
gen IDDate2_T4 = date(IDDate_T4, "DMY")
format IDDate2_T4 %td
*format sttme1_T3 %td
gen IDDate_T3=dofc(sttme1_T3)
format IDDate_T3 %dN/D/Y
format IDDate2_T4 %dN/D/Y
gen DiffIntvTestRestTest=IDDate2_T4-IDDate_T3

*/

save "$base_out/Consolidated/School.dta", replace



***********************************
***********************************************************
***********************************************************
******** Find main teacher per grade, group, subject *************************
**********************************************************
use "$basein/3 Endline/School/EGroup_noPII.dta", clear
keep SchoolID EGradeID EGroupID gpmten gpmtsw gpmtmt
replace gpmten=. if gpmten==-98
replace gpmtsw=. if gpmtsw==-98
replace gpmtmt=. if gpmtmt==-98
label drop L_GroupAbility
label drop L_Shifts
label drop L_groupTeacher
label drop grpstr_a
label drop grpstr_b
label drop grpstr_c
label drop grpstr_d
label drop grpstr_e
label drop grpstr_f
label drop grpstr_g
rename EGradeID stdgrd_T3
rename EGroupID stdgrp_T3
save "$base_out/3 Endline/School/TeacherGradeSubjectStream.dta", replace
  
use "$basein/3 Endline/School/EGroup_noPII.dta", clear  
keep SchoolID EGradeID EGroupID grpstr_a grpstr_b grpstr_c grpstr_d grpstr_e grpstr_f grpstr_g
reshape  long grpstr_, i(SchoolID EGradeID EGroupID) j(Stream) string
replace Stream = upper(Stream)
rename EGradeID stdgrd_T3
rename Stream stdstr_T3
drop if grpstr_==0
qui unique SchoolID stdgrd_T3 stdstr_T3 , by(SchoolID stdgrd_T3 stdstr_T3) generate(id)
drop if id==.
save "$base_out/3 Endline/School/StramsGroups.dta", replace


***********************************
***********************************************************
***********************************************************
******** HT ID *************************
**********************************************************


use "$basein/3 Endline/School/ESchool_noPII", clear
keep respondent headteacher
drop if headteacher==3
drop if respondent<0
gen TeacherHT=1
rename respondent ETTeacherID
save "$base_out/3 Endline/School/HT.dta", replace



*******************************************************
*************END LINE 2014
********************************************************

use "$basein/8 Endline 2014/Final Data/Teacher/R6Grade_noPII.dta" , clear

keep SchoolID R6GradeID  s184 s185
recode s184 s185 (-99/-1=.)
reshape wide s184 s185, i(SchoolID) j(R6GradeID)

label variable s1841 "Boys Grade 1"
label variable s1842 "Boys Grade 2"
label variable s1843 "Boys Grade 3"
label variable s1844 "Boys Grade 4"
label variable s1845 "Boys Grade 5"
label variable s1846 "Boys Grade 6"
label variable s1847 "Boys Grade 7"

label variable s1851 "Girls Grade 1"
label variable s1852 "Girls Grade 2"
label variable s1853 "Girls Grade 3"
label variable s1854 "Girls Grade 4"
label variable s1855 "Girls Grade 5"
label variable s1856 "Girls Grade 6"
label variable s1857 "Girls Grade 7"

gen StudentsGr1=s1841+s1851
gen StudentsGr2=s1842+s1852
gen StudentsGr3=s1843+s1853
gen StudentsGr4=s1844+s1854
gen StudentsGr5=s1845+s1855
gen StudentsGr6=s1846+s1856
gen StudentsGr7=s1847+s1857


label variable StudentsGr1 "Students Grade 1"
label variable StudentsGr2 "Students Grade 2"
label variable StudentsGr3 "Students Grade 3"
label variable StudentsGr4 "Students Grade 4"
label variable StudentsGr5 "Students Grade 5"
label variable StudentsGr6 "Students Grade 6"
label variable StudentsGr7 "Students Grade 7"

egen TotalNumberStudents=rowtotal(StudentsGr1 StudentsGr2 StudentsGr3 StudentsGr4 StudentsGr5 StudentsGr6 StudentsGr7), missing

compress
save "$base_out/8 Endline 2014/School/School.dta", replace	



use "$basein/8 Endline 2014/Final Data/School/R6Expenses_noPII.dta", clear

drop if R6ExpenseID==-95
rename R6ExpenseID ExpenseID
replace s168=0 if s168==.
replace cgspct=0 if cgspct==.
gen ExpenseGroup=.
recode s168 cgspct (-99/-1=.)
replace  ExpenseGroup=1 if ExpenseID==1 | ExpenseID==2 | ExpenseID==3 | ExpenseID==4 | ExpenseID==5
replace  ExpenseGroup=2 if ExpenseID==7 | ExpenseID==8 | ExpenseID==16
replace  ExpenseGroup=3 if ExpenseID==9  | ExpenseID==11 | ExpenseID==12
replace  ExpenseGroup=4 if ExpenseID==13 | ExpenseID==14 | ExpenseID==15
replace  ExpenseGroup=5 if ExpenseID==6
replace  ExpenseGroup=6 if ExpenseID==17
replace  ExpenseGroup=7 if ExpenseID==10



label define expensegroup 1 "Administrative" 2 "Student Aid" 3 "Teaching Aid" 4 "Teachers" 5 "Construction" 6 "Savings" 7 "Textbooks"
label values ExpenseGroup  expensegroup
label variable 	ExpenseGroup "Expense Category"

collapse (sum)  s168 cgspct, by(ExpenseGroup SchoolID)

reshape wide s168 cgspct, i(SchoolID) j(ExpenseGroup)

rename s1681 administrative_expenses
rename s1682 student_expenses
rename s1683 teaching_aid_expenses
rename s1684 Teacher_expenses
rename s1685 Construction_expenses
rename s1686 Savings_expenses
rename s1687 textbook_expenses

rename cgspct1 administrative_twaweza
rename cgspct2 student_twaweza
rename cgspct3 teaching_aid_twaweza
rename cgspct4 Teacher_twaweza
rename cgspct5 Construction_twaweza
rename cgspct6 Savings_twaweza
rename cgspct7 textbook_twaweza


label variable 	administrative_expenses "Administrative Expenses"
label variable 	student_expenses "Student Aid Expenses"
label variable 	teaching_aid_expenses "Teaching Aid Expenses"
label variable 	Teacher_expenses "Teachers Expenses"
label variable 	Construction_expenses "Construction Expenses"
label variable 	Savings_expenses "Savings"
label variable 	textbook_expenses "Textbooks expenses"

label variable 	administrative_twaweza "Administrative Twaweza"
label variable 	student_twaweza "Student Aid Twaweza"
label variable 	teaching_aid_twaweza "Teaching Aid Twaweza"
label variable 	Teacher_twaweza "Teachers Twaweza"
label variable 	Construction_twaweza "Construction Twaweza"
label variable 	Savings_twaweza "Savings Twaweza"
label variable 	textbook_twaweza "Textbooks Twaweza"


compress
merge 1:1 SchoolID using  "$base_out/8 Endline 2014/School/School.dta"
drop _merge	

   foreach var of varlist textbook_expenses textbook_twaweza administrative_expenses student_expenses teaching_aid_expenses Teacher_expenses Construction_expenses Savings_expenses administrative_twaweza student_twaweza teaching_aid_twaweza Teacher_twaweza Construction_twaweza Savings_twaweza{
   gen `var'PS=`var'/TotalNumberStudents
   replace `var'PS=. if SchoolID==1030
   *replace `var'PS=. if `var'PS>100000
   }
   


compress
save "$base_out/8 Endline 2014/School/School.dta", replace	

********How many hours teach each subject in each grade

import delimited "$basein/7 Monitoring 2014/Data/R5GrdSub.csv", clear 
keep gradeid subjectid subject egrdsubid
rename egrdsubid fgradesubjectid
merge 1:m fgradesubjectid using "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSub3_noPII.dta"
drop if _merge!=3
drop _merge

keep SchoolID R6TeacherID gradeid subjectid gsbhrs
reshape wide gsbhrs, i(SchoolID R6TeacherID  gradeid) j( subjectid  )


collapse (sum) gsbhrs1 gsbhrs2 gsbhrs3, by(SchoolID gradeid)
drop if gradeid==.
rename gsbhrs1 HrsMath
rename gsbhrs2 HrsSwahili
rename gsbhrs3 HrsEnglish

gen HrsFocal=HrsMath+HrsSwahili+HrsEnglish
reshape wide HrsMath HrsSwahili HrsEnglish HrsFocal, i(SchoolID) j(gradeid)
forvalues i=1/3{
replace HrsMath`i'=0 if HrsMath`i'==.
replace HrsSwahili`i'=0 if HrsSwahili`i'==.
replace HrsEnglish`i'=0 if HrsEnglish`i'==.
replace HrsFocal`i'=0 if HrsFocal`i'==.
}

merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	 



use "$basein/8 Endline 2014/Final Data/School/R6Funding_noPII.dta", clear
replace inkdyn=0 if inkdyn==2
keep s155 SchoolID R6FundingID inkdyn
recode s155 SchoolID (-99/-1=.)
reshape wide s155 inkdyn, i(SchoolID) j(R6FundingID)

label var s1551 "Government CG"
label var s1552 "Government Other"
label var s1553 "Local Government"
label var s1555 "NGOs"
label var s1556 "Parents"
label var s1557 "Other"

label var inkdyn1 "Government CG"
label var inkdyn2 "Government Other"
label var inkdyn3 "Local Government"
label var inkdyn5 "NGOs"
label var inkdyn6 "Parents"
label var inkdyn7 "Other"

rename inkdyn1 GovermentCG_D
rename inkdyn2 GovermentOther_D
rename inkdyn3 LocalGoverment_D
rename inkdyn5 NGO_D
rename inkdyn6 Parents_D
rename inkdyn7 Other_D

rename s1551 GovermentCG_M
rename s1552 GovermentOther_M
rename s1553 LocalGoverment_M
rename s1555 NGO_M
rename s1556 Parents_M
rename s1557 Other_M


merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
  
   foreach var of varlist GovermentCG_M GovermentOther_M LocalGoverment_M NGO_M Parents_M Other_M{
   replace `var'=`var'/TotalNumberStudents
   }
   
   

compress
save "$base_out/8 Endline 2014/School/School.dta", replace	

use "$basein/8 Endline 2014/Final Data/School/R6Grade_noPII.dta" , clear
replace gdrctb=. if gdrctb==-99
replace gdrctb=0 if gdrctb==.


keep SchoolID R6GradeID tbgdno_math tbgdno_swahili tbgdno_english tbgdno_other gdrctb 
reshape wide  tbgdno_math tbgdno_swahili tbgdno_english tbgdno_other gdrctb, i(SchoolID) j(R6GradeID)


label variable gdrctb1 "Expenditure in textbooks grade 1"
label variable gdrctb2 "Expenditure in textbooks grade 2"
label variable gdrctb3 "Expenditure in textbooks grade 3"
label variable gdrctb4 "Expenditure in textbooks grade 4"
label variable gdrctb5 "Expenditure in textbooks grade 5"
label variable gdrctb6 "Expenditure in textbooks grade 6"
label variable gdrctb7 "Expenditure in textbooks grade 7"


label variable tbgdno_math1 "Math textbooks grade 1"
label variable tbgdno_math2 "Math textbooks grade 2"
label variable tbgdno_math3 "Math textbooks grade 3"
label variable tbgdno_math4 "Math textbooks grade 4"
label variable tbgdno_math5 "Math textbooks grade 5"
label variable tbgdno_math6 "Math textbooks grade 6"
label variable tbgdno_math7 "Math textbooks grade 7"

label variable tbgdno_swahili1 "Swahili textbooks grade 1"
label variable tbgdno_swahili2 "Swahili textbooks grade 2"
label variable tbgdno_swahili3 "Swahili textbooks grade 3"
label variable tbgdno_swahili4 "Swahili textbooks grade 4"
label variable tbgdno_swahili5 "Swahili textbooks grade 5"
label variable tbgdno_swahili6 "Swahili textbooks grade 6"
label variable tbgdno_swahili7 "Swahili textbooks grade 7"

label variable tbgdno_english1 "English textbooks grade 1"
label variable tbgdno_english2 "English textbooks grade 2"
label variable tbgdno_english3 "English textbooks grade 3"
label variable tbgdno_english4 "English textbooks grade 4"
label variable tbgdno_english5 "English textbooks grade 5"
label variable tbgdno_english6 "English textbooks grade 6"
label variable tbgdno_english7 "English textbooks grade 7"

label variable tbgdno_other1 "Other textbooks grade 1"
label variable tbgdno_other2 "Other textbooks grade 2"
label variable tbgdno_other3 "Other textbooks grade 3"
label variable tbgdno_other4 "Other textbooks grade 4"
label variable tbgdno_other5 "Other textbooks grade 5"
label variable tbgdno_other6 "Other textbooks grade 6"
label variable tbgdno_other7 "Other textbooks grade 7"

merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge

forval i=1/7  {
replace gdrctb`i'=0 if gdrctb`i'==.
replace StudentsGr`i'=0 if StudentsGr`i'==.
gen gdrctb`i'PS=gdrctb`i'/StudentsGr`i'
gen tbgdno_math`i'PS=tbgdno_math`i'/StudentsGr`i'
gen tbgdno_swahili`i'PS=tbgdno_swahili`i'/StudentsGr`i'
gen tbgdno_english`i'PS=tbgdno_english`i'/StudentsGr`i'
gen tbgdno_other`i'PS=tbgdno_other`i'/StudentsGr`i'
}

gen ExpenditureBooksFocalPS=(gdrctb1+gdrctb2+gdrctb3)/(StudentsGr1+StudentsGr2+StudentsGr3)
gen ExpenditureBooksNonFocalPS=(gdrctb4+gdrctb5+gdrctb6+gdrctb7)/(StudentsGr4+StudentsGr5+StudentsGr6+StudentsGr7)

label var ExpenditureBooksFocalPS "\\$ Textbooks/Student in focal grades"
label var ExpenditureBooksNonFocalPS "\\$ Textbooks/Student in non-focal grades"


compress
save "$base_out/8 Endline 2014/School/School.dta", replace	

use "$basein/8 Endline 2014/Final Data/School/R6SFacility_noPII.dta", clear

keep  SchoolID R6SFacilityID factyn
drop if R6SFacilityID<8
replace factyn=0 if factyn==2

reshape wide factyn, i(SchoolID) j(R6SFacilityID)


label variable factyn8 "Kitchen"
label variable factyn9 "Library"
label variable factyn10 "Assembly Hall"
label variable factyn11 "Playground"
label variable factyn12 "Water Source"
label variable factyn13 "Outer Wall/Fence"
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	


use "$basein/8 Endline 2014/Final Data/School/R6Volunteer_noPII.dta", clear 

collapse (count)  R6VolunteerID , by(SchoolID) 
rename R6VolunteerID NumberOfVolunteers
label variable 	NumberOfVolunteers "Number of volunteers at school"
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
replace NumberOfVolunteers=0 if NumberOfVolunteers==.
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	


forvalues i=1/7{
use "$basein/8 Endline 2014/Final Data/Teacher/R6TGrade_noPII.dta", clear 
replace grdeyn=0 if grdeyn==2
drop if R6TGradeID!=`i'
collapse (sum) grdeyn , by(SchoolID) 
rename grdeyn NumberOfTeachers_S`i'
label variable 	NumberOfTeachers_S`i' "Number of teachers in S`i'"
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace		
}

use "$basein/8 Endline 2014/Final Data/Teacher/R6TGrade_noPII.dta", clear 
bys SchoolID R6TeacherID: gen n=_n
drop if n>1
collapse (count) R6TeacherID, by(SchoolID)
rename R6TeacherID NumberOfTeachers
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	



gen studentsTeacherRatio=(TotalNumberStudents)/NumberOfTeachers
gen studentsVolunteerRatio=(TotalNumberStudents)/NumberOfVolunteers
forvalues i=1/7{
gen studentsTeacherRatio_S`i'=StudentsGr`i'/NumberOfTeachers_S`i'
}

label variable 	studentsTeacherRatio "Overall student teacher ratio"
label variable 	studentsVolunteerRatio "Overall volunteer teacher ratio"
label variable 	studentsTeacherRatio_S1 "student teacher ratio S1"
label variable 	studentsTeacherRatio_S2 "student teacher ratio S2"
label variable 	studentsTeacherRatio_S3 "student teacher ratio S3"
label variable 	studentsTeacherRatio_S4 "student teacher ratio S4"
label variable 	studentsTeacherRatio_S5 "student teacher ratio S5"
label variable 	studentsTeacherRatio_S6 "student teacher ratio S6"
label variable 	studentsTeacherRatio_S7 "student teacher ratio S7"


save "$base_out/8 Endline 2014/School/School.dta", replace	

use "$basein/8 Endline 2014/Final Data/School/R6School_noPII.dta", clear 

pca httmus_a- httmus_h
predict IndexTimeSpend, score

label define L_expsource 1 "School's ledger book" 2 "receipt from supplier" 3 "school committee/HT budget records" 4 "memory/no written record" -96 "other(specify)" -95 "no other source"
label values expsrc_1 expsrc_2 expsrc_3 L_expsource

gen strategicly_change_teachers=0 
replace strategicly_change_teachers=1 if iptcst_c==1
label variable 	strategicly_change_teachers "HT changed teachers strategically"  


 gen strategicly_change_students=0 
replace strategicly_change_students=1 if iptcst_b==1
label variable 	strategicly_change_students "HT changed students strategically" 
compress

keep strategicly_change_students strategicly_change_teachers  SchoolID IndexTimeSpend
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear


keep SchoolID treatment treatarm
merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"

drop _merge
compress
save "$base_out/8 Endline 2014/School/School.dta", replace	

*******************************************************
*************FUNDING DATA - Endline 2014
********************************************************

 
use "$basein/8 Endline 2014/Final Data/School/R6Funding_noPII.dta", clear
replace inkdyn=0 if inkdyn==2
keep s155 SchoolID  R6FundingID inkdyn
recode s155 SchoolID (-99/-1=.)
reshape wide s155 inkdyn, i(SchoolID) j( R6FundingID)

label var s1551 "Government CG"
label var s1552 "Government Other"
label var s1553 "Local Government"
label var s1555 "NGOs"
label var s1556 "Parents"
label var s1557 "Other"

label var inkdyn1 "Government CG"
label var inkdyn2 "Government Other"
label var inkdyn3 "Local Government"
label var inkdyn5 "NGOs"
label var inkdyn6 "Parents"
label var inkdyn7 "Other"

rename inkdyn1 GovermentCG_D
rename inkdyn2 GovermentOther_D
rename inkdyn3 LocalGoverment_D
rename inkdyn5 NGO_D
rename inkdyn6 Parents_D
rename inkdyn7 Other_D

rename s1551 GovermentCG_M
rename s1552 GovermentOther_M
rename s1553 LocalGoverment_M
rename s1555 NGO_M
rename s1556 Parents_M
rename s1557 Other_M


merge 1:1 SchoolID using "$base_out/8 Endline 2014/School/School.dta"
drop _merge
  
   foreach var of varlist GovermentCG_M GovermentOther_M LocalGoverment_M NGO_M Parents_M Other_M{
   replace `var'=`var'/TotalNumberStudents
   }

compress


rename * =_T7
rename SchoolID_T7 SchoolID
rename treatment_T7 treatment
rename treatarm_T7 treatarm

merge 1:1 SchoolID using "$base_out/Consolidated/School.dta"
drop if _merge!=3
drop _merge


save "$base_out/Consolidated/School.dta", replace
/*
merge 1:1 SchoolID using "$base_out/Consolidated/TAttendace.dta"
drop if _merge!=3
drop _merge


save "$base_out/Consolidated/School.dta", replace

  */ 
