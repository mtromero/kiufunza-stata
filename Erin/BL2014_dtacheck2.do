/* BL2014 (R4) Data Checks

Erin Litzow
1 April 2014

Mauricio Check's
22 April 2014
*/

log using "E:\Box Sync\01_KiuFunza\Results\Logs\Baseline 2014 Data Check", replace

set more off

display "SCHOOL DATA"
*use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4School.dta, clear
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4School_noPII.dta", clear

display "Unique SchoolID"
duplicates examples SchoolID
display "No refusals at school level:"
tab consnt
tab numvis
list SchoolID if numvis==2, nolabel
display "Types of school interview respondents:"
tab headteacher
display "Head Teacher Teaching a class...HTs teaching a class has gone up 1.7 percentage points from EL2013"
tab s12 /*26 schools report HT not teaching a class, 26*7=182 missing from R4Grade data set*/
tab respondent
display "No COD payments received before end of field data collection"
tab codhtpay
display "Receipt of gov't textbooks"
tab txbkyn
list SchoolID if txbkyn==2 /*school 504*/
tab txbksh
display "BRN: received 'School Improvement Toolkit'"
tab brntkph
tab brntktr
display "AIM asked to all HT, 9 out of 12 statements were answered same by at least 70% of respondents"
tab aim1
tab aim4
display "Digit recall asked to all HT, only 4 got to 11 digits"
tab dgtrc3
tab dgtrc6
tab dgtrc11
tab dgtrc9

*test things by treatment
drop intbeg
merge 1:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop _merge
display "Balance of volunteers at school"
tabstat vlnoto, by(treatment) statistics( mean semean ) 
display "Balance of HT attendance, last schooling day"
tab htatyd treatment
tabstat htatyd, by(treatment) statistics( mean semean ) 
display "Balance of HT teaching a class"
tab s12 treatment
tabstat s12, by(treatment) statistics( mean semean ) 
display "Balance of full-time teachers at school"
tabstat s68, by(treatment) statistics( mean semean ) 
display "BRN: Grade 2 students tested"
tab brn3rex treatment
tabstat brn3rex, by(treatment) statistics( mean semean ) 

*check distribution
display "Variables with one distinct value (all are OK)"
foreach var of varlist _all {
	quietly tabulate `var'
	if r(r) == 1 {
		display "`var' has only one distinct value."
		describe `var'
	}
}

display "Displaying percent DON'T KNOW..."
foreach var of varlist brntkph txbkep brntktr brn3rex brn3rrs {
	capture confirm numeric variable `var'
	if _rc == 0 {
		scalar miss = .
		scalar dk = 3
	}
	else {
		scalar miss = ""
		scalar dk = "DON'T KNOW"
	}
	
	quietly count if `var' != miss
	local nonmiss = r(N)
	quietly count if `var' == dk
	local dkn = r(N)
	local dkrate = `dkn' / `nonmiss'
	
	bysort intvwr: egen totdk = total(inlist(`var', dk))
	quietly count if totdk >= 3
	
	if `dkrate' >= 0.025 | r(N) > 0 {
		describe `var'
		display "DK:  " string(100 * `dkrate', "%5.1f") "%"
		tabulate intvwr `var' if inlist(`var', dk) == 1, missing
	}
	
	drop totdk
}


clear
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Grade_noPII.dta",clear
*use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Grade.dta
*

*enrollment data(there are no grades missing data)
list SchoolID R4GradeID if s184==. | s185==. | grpnum==.
tab grpnum
tab staldt
list SchoolID staldt_other if staldt_other!=""
list SchoolID R4GradeID if grpnum==0

*government textbooks received
list SchoolID R4GradeID if tbgdno_math==. & tbgdno_swahili==. & tbgdno_english==. & tbgdno_other==. /*school 504, same school as reported not receiving any textbooks from the government*/
tabstat tbgdno_math, by(R4GradeID)  statistics( mean semean ) 
tabstat tbgdno_english, by(R4GradeID) statistics( mean semean ) 
tabstat tbgdno_swahili, by(R4GradeID) statistics( mean semean ) 
tabstat tbgdno_other, by(R4GradeID) statistics( mean semean ) 

merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

tab staldt treatment

display "head teacher teaching subjects, 0=notselected 1=selected"
tabstat s14_eng, by(treatment) statistics( mean semean ) 
tabstat s14_maths, by(treatment) statistics( mean semean ) 
tabstat s14_swahili, by(treatment) statistics( mean semean ) 
*weird...looks like CG has the highest report of HTs teaching focal grade/subject

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Group_noPII.dta",clear
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

tab R4GradeID R4GroupID

display "avg num of students per group"
sum numstu
tabstat numstu, by(treatment) statistics( mean semean )
tab treatment, gen(TD) 
reg numstu TD1 TD2 TD3


display "ability level of group, 1=strong 2=weaker 3=average"
tab grpabl
tab grpabl treatment

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4GroupSubject_noPII.dta"
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

display "subjects with no teacher reported"
list SchoolID R4GradeID R4GroupID R4GroupSubjectID treatment if gpmtch==.
tab gpmtch
*69 grade, group, subjects not taught; all but 2 are english
list SchoolID R4GradeID R4GroupSubjectID if gpmtch==-98

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Volunteer_noPII.dta"
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

tab vobgyr
sum volhrs
tab voledmo
tab voltme treatment

display "total volunteer hours worked, treatment v. control"
preserve
replace volhrs=7 if voltme==1
collapse (sum) volhrs, by(SchoolID)
rename volhrs totvolhrs
merge 1:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
tabstat totvolhrs, by(treatment) statistics( mean semean ) 
tempfile totvolhrs
save "`totvolhrs'"
restore

display "amount of volunteer stipend, treatment v. control"
preserve
tab volstp
tab volunt
replace volstp=volstp*4 if volunt==1
tabstat volstp, by(treatment) statistics( mean semean ) 
tab volstsr
restore

clear

display "TEACHER DATA"
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Teacher_noPII.dta"
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

duplicates examples upid
tabstat tcatls, by(treatment) statistics( mean semean ) 
tab tchwrk treatment
tab atttch treatment
tabstat atttch, by(treatment) statistics( mean semean ) 
display "teacher availability as reported by HT"
tab tchavl

display "tchr unavailability balance before followup calls"
recode tchavl 2=0
tabstat tchavl, by(treatment) statistics( mean semean ) 
tab treatment, gen(TD)
regress tchavl TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
drop TD1 TD2 TD3 TD4

display "teacher unavailability after follow up calls"
gen interviewed=0 if tchavl==0
replace interviewed=1 if tchavl==1
replace interviewed=1 if tchavl2==1 
tabstat interviewed, by(treatment) statistics( mean semean ) 
tab treatment, gen(TD)
regress interviewed TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal

tab tcnsnt
tab tcodhtpay
tabstat missedClasses, by(treatment) statistics( mean semean ) 
tabstat committeeMember, by(treatment) statistics( mean semean ) 
tab tdgtrc4
tab tdgtrc6
tab tdgtrc7
tab tdgtrc9
tab tdgtrc11

clear

display "new teacher balance"
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage_noPII.dta" 
generate R4newtchr=1 if ETTeacherID==. & R4TeacherID!=.
replace R4newtchr=0 if ETTeacherID!=. & R4TeacherID!=.
tab R4newtchr
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
tabstat R4newtchr, by(treatment) statistics( mean semean ) 

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4TGrade_noPII.dta"
tab R4TGradeID
tab grdeyn if R4TGradeID==1
tab grdeyn if R4TGradeID==2
tab grdeyn if R4TGradeID==3

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4TGrdSubExit_noPII.dta" 
tab extini
tab extrsn
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
preserve
collapse (count) R4TeacherID, by(SchoolID)
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
display "number of teachers who have exited at least one focal grade, subject per school"
tabstat R4TeacherID, by(treatment) statistics( mean semean ) 
restore

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4TGrdSub_noPII.dta"
tab entini
tab entrsn
preserve
collapse (count) R4TeacherID, by(SchoolID)
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
display "number of teachers who have entered at least one focal grade, subject per school"
tabstat R4TeacherID, by(treatment) statistics( mean semean ) 
restore

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4TGrdSub2_noPII.dta" 
tab R4TGradeID
tab R4TGrdSub2ID

display "STUDENT DATA"
display "attendance of 2013 students from 2013"
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\attendance\R4Student_noPII.dta"
duplicates examples upidst
tab attstd
tab attstd2
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

display "checking for differential student attrition (stud is attrited if they are coded as 'moved school,' 'not in this school anymore,' or 'cannot find student')"
gen atrstud=0
replace atrstud=1 if atrdet==2 | atrdet==3 | atrdet==4
tabstat atrstud, by(treatment) statistics( mean semean ) 
tab treatment, gen(TD)
regress atrstud TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])

display "checking attendance by grade and treatment"
tab atrgrd, m
gen currentgrd=stdgrd
replace currentgrd=atrgrd if atrgrd!=.
gen completeatt=1 if attstd==1
replace completeatt=0 if attstd==2
replace completeatt=1 if attstd2==1
replace completeatt=0 if attstd2==2
tab completeatt currentgrd

tabstat completeatt, by(currentgrd) statistics( mean semean ) 
tabstat completeatt, by(treatment) statistics( mean semean ) 

tabstat completeatt if currentgrd==2, by(treatment) statistics( mean semean ) 
tabstat completeatt if currentgrd==3, by(treatment) statistics( mean semean ) 
tabstat completeatt if currentgrd==4, by(treatment) statistics( mean semean ) 

display "female student who has apparently moved from Grade 4 to Grade 7"
list SchoolID R4StudentID stdgrd  stdsex stdage if currentgrd==7
display "students who were moved back down to pre-primary"
list SchoolID stdgrd elgrde if atrgrd==0

clear



display "Grade 1 Test Results"
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\student\R4Student_noPII.dta",clear
display "2013 Grade 1 Student Age average: 7.234"
sum Age
display "2013 Grade 1 Student Gender average: 1.499"
tab Gender
tab consentChild

sum Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4
sum Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4
display "counting items score is very similar between BL2014 and EL2013"
sum Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5
display "BL2014 Grade 1 science results are better than EL2013 Grade 1 science results" /*...pick a better 'other' subject for 2014*/
sum Asayansi_1

display "check for balance across treatment groups"
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

tab treatment, gen(TD)
foreach var in Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4 Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4 Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5 Asayansi_1 {

tabstat `var', by(treatment) statistics( mean semean ) 
regress `var' TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
}

eststo clear
foreach var in Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4 Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4 Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5 Asayansi_1 {
eststo: regress `var' TD1 TD2 TD3
}
 esttab using "E:\Box Sync\01_KiuFunza\Results\ExcelTables\RegBlance_1stgrade_year2.csv", se ar2 label replace
eststo clear
foreach var in Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4 Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4 Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5 Asayansi_1 {
eststo: regress `var' TD1 TD2 TD3, vce(cluster SchoolID) 
}
 esttab using "E:\Box Sync\01_KiuFunza\Results\ExcelTables\RegBlance_1stgrade_year2_cluster.csv", se ar2 label replace

 merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\1 Baseline\School\School_noPII.dta", keepus(s175)
 
 eststo clear
foreach var in Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4 Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4 Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5 Asayansi_1 {
by s175:  eststo: regress `var' TD1 TD2 TD3
}
 esttab using "E:\Box Sync\01_KiuFunza\Results\ExcelTables\RegBlance_1stgrade_year2_group.csv", se ar2 label replace

 gen kindergarden=1 if s175==1
 replace kindergarden=0 if s175==2
 eststo clear
foreach var in Akiswahili_1 Akiswahili_2 Akiswahili_3 Akiswahili_4 Akiingereza_1 Akiingereza_2 Akiingereza_3 Akiingereza_4 Ahisabati_1 Ahisabati_2 Ahisabati_3 Ahisabati_4 Ahisabati_5 Asayansi_1 {
eststo: regress `var' TD1##kindergarden TD2##kindergarden TD3##kindergarden
}
 esttab using "E:\Box Sync\01_KiuFunza\Results\ExcelTables\RegBlance_1stgrade_year2_group2.csv", se ar2 label replace


display "HOUSEHOLD DATA"
use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4hhMember_noPII.dta",clear
duplicates examples upid

/*
use "E:\Box Sync\01_KiuFunza\RawData\1 Baseline\Household\HHMember_noPII.dta" 
collapse (count) HHMemberID, by(HHID)
merge 1:1 HHID using "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Supplementing\UPID Linkage Tables\R4_Student_HH_ID_Linkage_noPII.dta", keepusing(GradeID)

drop if _merge==2
drop _merge
tab GradeID
keep if GradeID==2
merge 1:1 HHID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\supplementing\R_EL_rHHData_noPII.dta", keepusing(SchoolID)
drop if _merge==2
drop _merge
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
tabstat HHMemberID, by(treatment)
tab treatment, gen(TD)
regress HHMemberID TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
*/

collapse (count) R4HHMemberID, by(HHID)
sum R4HHMemberID
display "average # of members/HH for Grade 2 HHs in 2013 was 6.3"
merge 1:1 HHID using "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4HHData_noPII.dta", keepusing(SchoolID)
drop _merge
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
tabstat R4HHMemberID, by(treatment) statistics( mean semean ) 
tab treatment, gen(TD)
regress R4HHMemberID TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
tempfile HHmembers
save "`HHmembers'"

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4hhMember_noPII.dta"
sum memage
tab memsex
tab smschl
tab hstedu

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4HHData_noPII.dta"
*265 sampled HHs had to be replaced
sum replacement
gen interviewed=0
tab intrs1
tab intrs2
replace interviewed=1 if intrs1==1
replace interviewed=1 if intrs2==1
preserve
drop if interviewed==0
collapse (count) HHID, by(SchoolID)
*5 HHs interviewed/school
sum HHID
restore

tab wrokyn
tab jobsec
tab matwll
tab srcwtr
tab asset_8
tab mobilno
tab bnkacc
tab hsowsh
sum rmsnum
tab prdyyn
tab prdyto_1
tab cptgrt
tab recknw
tab hmwhlp_1
tab hmwhlp_2
tab hmwhlp_3
tab perknw
tab perrep
sum rnkchl

merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge
tab twknyn treatment

tab rdngsw
tab rdngen
tab mathqn_1
tab mathqn_2

tab lnguge
tab intprt

clear

use "E:\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4HHExpenditure_noPII.dta"
tab R4HHExpenditureID
merge m:1 SchoolID using "E:\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatment)
drop if _merge==2
drop _merge

display "textbook (2) and tutoring (6) expenditure by treatment group in 2013 and 2014"
tabstat expn13 if R4HHExpenditureID==2, by(treatment) statistics( mean semean ) 
tabstat expn14 if R4HHExpenditureID==2, by(treatment) statistics( mean semean ) 
tabstat expn13 if R4HHExpenditureID==6, by(treatment) statistics( mean semean ) 
tabstat expn14 if R4HHExpenditureID==6, by(treatment) statistics( mean semean ) 

clear

log close
