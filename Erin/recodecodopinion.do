/*Recode COD opinion variables, EL Teacher data

Erin Litzow
Feb. 20, 2014*/

clear
use "ETTeacher_noPII.dta"

foreach var of varlist codotn codowe codort codops codogt codoif codopt {
recode `var' 1=6
recode `var' 2=7
recode `var' 3=-99
recode `var' 4=8
recode `var' 5=8
}
