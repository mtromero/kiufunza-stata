use "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\household\R4HHData_noPII.dta" 
tab tchrno
recode tchrno -2=2

use "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData\6 Baseline 2014\Data\school\R4Grade_noPII.dta" 
list SchoolID R4GradeID if tbgdno_math==-95 | tbgdno_swahili==-95 | tbgdno_english==-95 | tbgdno_other==-95
recode tbgdno_math -95=0
recode tbgdno_swahili -95=0
recode tbgdno_english -95=0
recode tbgdno_other -95=0
