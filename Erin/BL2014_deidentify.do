/*De-Identify BL2014 Data

Erin Litzow
April 11, 2014*/

use O:\Data\attendance\COMMENTS.dta 
replace Comment=" " if Comment!="the name is repeated twice"
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\attendance\COMMENTS_noPII.dta"

clear

use O:\Data\attendance\R4School.dta 
label drop L_districts
label drop L_schools
drop s141
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\attendance\R4School_noPII.dta"

clear

use O:\Data\attendance\R4Student.dta 
drop stdnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\attendance\R4Student_noPII.dta"

clear

use O:\Data\attendance\TRANSLATIONS.dta
*drop comments with student names in them
drop if SchoolID==101 & R4StudentID==11
drop if SchoolID==231 & R4StudentID==19
drop if SchoolID==306 & R4StudentID==24
drop if SchoolID==306 & R4StudentID==26
drop if SchoolID==306 & R4StudentID==28
drop if SchoolID==310 & R4StudentID==16
drop if SchoolID==310 & R4StudentID==17
drop if SchoolID==316 & R4StudentID==1
drop if SchoolID==322 & R4StudentID==23
drop if SchoolID==322 & R4StudentID==26
drop if SchoolID==322 & R4StudentID==29
drop if SchoolID==331 & R4StudentID==11
drop if SchoolID==331 & R4StudentID==12
drop if SchoolID==331 & R4StudentID==14
drop if SchoolID==331 & R4StudentID==28
drop if SchoolID==331 & R4StudentID==30
drop if SchoolID==529 & R4StudentID==15
drop if SchoolID==529 & R4StudentID==19
drop if SchoolID==529 & R4StudentID==23
drop if SchoolID==602 & R4StudentID==7
drop if SchoolID==607 & R4StudentID==3
drop if SchoolID==617 & R4StudentID==26
drop if SchoolID==709 & R4StudentID==.
drop if SchoolID==906 & R4StudentID==1
drop if SchoolID==906 & R4StudentID==8
drop if SchoolID==911 & R4StudentID==5
drop if SchoolID==911 & R4StudentID==10
drop if SchoolID==914 & R4StudentID==8
drop if SchoolID==929 & R4StudentID==4
drop if SchoolID==1001 & R4StudentID==.
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\attendance\TRANSLATIONS_noPII.dta"

clear

use O:\Data\household\Stata12\COMMENTS.dta 
*drop comments with names in them
drop if HHID==100411 & R4HHMemberID==1
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\household\COMMENTS_noPII.dta"

clear

use O:\Data\household\Stata12\R4HHData.dta 
drop hhhnme mobil1 mobil1_other mobil2 mobil2_other
drop gpsstr gpsstr2 gpsstr3
label drop L_hh
label drop L_districts
label drop L_schools
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\household\R4HHData_noPII.dta"

clear

use O:\Data\household\Stata12\R4HHExpenditure.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\household\R4HHExpenditure_noPII.dta"

clear

use O:\Data\household\Stata12\R4HHMember.dta
drop memnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\household\R4HHMember_noPII.dta"

clear

use O:\Data\household\Stata12\TRANSLATIONS.dta
*drop comments with names in them
drop if HHID==60513 & R4HHMemberID==1
drop if HHID==70314 & R4HHMemberID==.
drop if HHID==71312 & R4HHMemberID==.
drop if HHID==71313 & R4HHMemberID==.
drop if HHID==71812 & R4HHMemberID==.
drop if HHID==73512 & R4HHMemberID==.
drop if HHID==73514 & R4HHMemberID==.
drop if HHID==100411 & R4HHMemberID==1 & varlabel=="Age (in completed years)"
*drop swahili comment b/c a lot of them contain names
drop originalEntry
*drop if HHID==101110 & R4HHMemberID==. & varlabel=="Mobile phone number 1"
*drop if HHID==101113 & R4HHMemberID==. & varlabel=="Mobile phone number 1"
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\household\TRANSLATIONS_noPII.dta"

clear

use O:\Data\school\Stata12\COMMENTS.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\COMMENTS_noPII.dta"

clear

use O:\Data\school\Stata12\R4Grade.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4Grade_noPII.dta"

clear

use O:\Data\school\Stata12\R4Group.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4Group_noPII.dta"

clear

use O:\Data\school\Stata12\R4GroupSubject.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4GroupSubject_noPII.dta"

clear

use O:\Data\school\Stata12\R4School.dta
label drop L_schools
label drop L_districts
drop s1 s2
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4School_noPII.dta"

clear

use O:\Data\school\Stata12\R4Teacher.dta 
drop tchnme_pp tchnme tcontNum ttchnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4Teacher_noPII.dta"

clear

use O:\Data\school\Stata12\R4TGrade.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4TGrade_noPII.dta"

clear

use O:\Data\school\Stata12\R4TGrdSub.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4TGrdSub_noPII.dta"

clear

use O:\Data\school\Stata12\R4TGrdSub2.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4TGrdSub2_noPII.dta"

clear

use O:\Data\school\Stata12\R4TGrdSubExit.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4TGrdSubExit_noPII.dta"

clear

use O:\Data\school\Stata12\R4Volunteer.dta 
drop volnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\R4Volunteer_noPII.dta"

clear

use O:\Data\school\Stata12\TRANSLATIONS.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\school\TRANSLATIONS_noPII.dta"

clear

use O:\Data\student\Stata12\COMMENTS.dta 
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\student\COMMENTS_noPII.dta"

clear

use O:\Data\student\Stata12\R4School.dta 
label drop L_schools
label drop L_districts
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\student\R4School_noPII.dta"

clear

use O:\Data\student\Stata12\R4SSchool.dta 
label drop L_schools
label drop L_districts
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\student\R4SSchool_noPII.dta"

clear

use O:\Data\student\Stata12\R4Student.dta 
drop Name
drop hhHeadName hhDetails hhPhone
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\student\R4Student_noPII.dta"

clear

use O:\Data\student\Stata12\TRANSLATIONS.dta
*drop directions to household 
drop if varlabel=="Any other household contact details available (e.g. address)"
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Data\student\TRANSLATIONS_noPII.dta"

clear

use "O:\Supplementing\UPID Linkage Tables\R4_Student_HH_ID_Linkage.dta" 
drop Name
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Supplementing\R4_Student_HH_ID_Linkage_noPII.dta"

clear

use "O:\Supplementing\UPID Linkage Tables\R4_Teacher_ID_Linkage.dta" 
drop tchnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Supplementing\R4_Teacher_ID_Linkage_noPII.dta"

clear

use "O:\Supplementing\UPID Linkage Tables\R4_Volunteer_ID_Linkage.dta" 
drop volnme
saveold "C:\Users\ErinLitzow\Desktop\BL2014_noPII\Supplementing\R4_Volunteer_ID_Linkage.dta"

clear
