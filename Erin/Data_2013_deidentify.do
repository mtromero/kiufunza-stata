truecrypt Data_2013_PII.tc, drive(T)

**deidentify baseline data
use "T:\1 Baseline\Household\HHData.dta"
drop gpsstr school ward vllage ktngji ngbrhd hhhnme mobil1 mobil1_other mobil2 mobil2_other
label drop L_districts
saveold HHData_noPII.dta

use "T:\1 Baseline\Household\HHMember.dta" 
drop memnme
saveold HHMember_noPII.dta

use "T:\1 Baseline\School\School.dta"
drop SchoolName s1 s2 s141
saveold School_noPII.dta

use "T:\1 Baseline\Student\Sample.dta"
drop Sample_ID Name hhHeadName hhPhone hhDetails
label drop L_sampleDistricts
saveold Sample_noPII.dta

use "T:\1 Baseline\Teacher\Teacher.dta" 
drop teacherName t03
label drop L_sampleDistricts
saveold Teacher_noPII.dta

use "T:\1 Baseline\Student\Stream.dta"
label drop L_sampleDistricts
saveold Stream_noPII.dta

**deidentify midline data
use "T:\2 Midline\MNewTeacher.dta"
drop ntcnme
saveold MNewTeacher_noPII.dta

use "T:\2 Midline\MSchool.dta"
drop gpsstr s1 s2
label drop L_districts L_schools
saveold MSchool_noPII.dta

use "T:\2 Midline\MStudent.dta"
drop stdnme stdnme_correct
saveold MStudent_noPII.dta

use "T:\2 Midline\MTeacher.dta"
drop tchnme crtnme tcontNum
saveold MTeacher_noPII.dta

use "T:\2 Midline\MVolunteer.dta" 
drop volnme
saveold MVolunteer_noPII.dta

use "T:\2 Midline\Supplementing\R_MO_teacher.dta"
drop s_teacherName
saveold R_MO_teacher_noPII.dta

use "T:\2 Midline\Supplementing\IDLinkTeachers.dta" 
drop s_teacherName
saveold IDLinkTeachers_noPII.dta

**deidentify endline data
use "T:\3 Endline\Household\EHHData.dta"
label drop L_districts
label drop L_schools
drop mobil1 mobil1_other mobil2 mobil2_other
saveold EHHData_noPII.dta

use "T:\3 Endline\Household\EHHMember.dta" 
drop blmemnme memnme
saveold EHHMember_noPII.dta

use "T:\3 Endline\School\ESchool.dta" 
label drop  L_districts
drop gpsstr s1 s2 s141
saveold ESchool_noPII.dta

use "T:\3 Endline\School\ETeacher.dta"
drop tchname_pp tchnme
saveold ETeacher_noPII.dta

use "T:\3 Endline\School\EVolunteer.dta"
drop volnme_pp volnme
saveold EVolunteer_noPII.dta

use "T:\3 Endline\Student\EStudent.dta" 
drop stdnme
saveold ESTudent_noPII.dta

use "T:\3 Endline\Teacher\ETTeacher.dta"
label drop L_districts
label drop L_schools
drop tcontNum tchnme
saveold ETTeacher_noPII.dta

use "T:\3 Endline\Supplementing\ClusterSummary_R3.dta" 
label drop L_schools
drop District School
saveold ClusterSummary_R3_noPII.dta

use "T:\3 Endline\Supplementing\IDLinkTeachers_R3.dta" 
drop tchnme
saveold IDLinkTeachers_R3_noPII.dta

use "T:\3 Endline\Supplementing\R_EL_HHMembers.dta" 
drop name
saveold R_EL_HHMembers_noPII.dta

use "T:\3 Endline\Supplementing\R_EL_rHHData.dta" 
label drop L_schools
drop Name hhHeadName
saveold R_EL_rHHData_noPII.dta

use "T:\3 Endline\Supplementing\R_EL_schools.dta" 
drop District School repnme
saveold R_EL_schools_noPII.dta

use "T:\3 Endline\Supplementing\R_EL_student.dta" 
drop Name
saveold R_EL_student_noPII.dta

use "T:\3 Endline\Supplementing\R_EL_teacher.dta"
drop s_teacherName
saveold R_EL_teacher_noPII.dta

truecrypt, dismount drive(T)
