use "EStudent.dta"

log using "EstTwaPassRates1"	
**generating a gradeID for current grade for all students
tab atrgrd
tab stdgrd
gen grade=1 if stdgrd==1 & atrgrd==.
replace grade=1 if atrgrd==1
replace grade=2 if stdgrd==2 & atrgrd==.
replace grade=2 if atrgrd==2
replace grade=3 if stdgrd==3 & atrgrd==.
replace grade=3 if atrgrd==3
tab grade

**gen pass variables for each grade,subject using KiuFunza EL Test pass standards
gen passkiswahili_1=1 if S1_kiswahili_1>=4 & S1_kiswahili_2>=4 & S1_kiswahili_3>=4 & S1_kiswahili_1!=. & S1_kiswahili_2!=. & S1_kiswahili_3!=.
recode passkiswahili_1 .=0 if grade==1
gen passkiingereza_1=1 if S1_kiingereza_1>=4 & S1_kiingereza_2>=4 & S1_kiingereza_3>=4 & S1_kiingereza_1!=. & S1_kiingereza_2!=. & S1_kiingereza_3!=.
recode passkiingereza_1 .=0 if grade==1
gen passhisabati_1=1 if S1_hisabati_1>=4 & S1_hisabati_2>=4 & S1_hisabati_3>=4 & S1_hisabati_4>=4 & S1_hisabati_5>=4 & S1_hisabati_1!=. & S1_hisabati_2!=. & S1_hisabati_3!=. & S1_hisabati_4!=. & S1_hisabati_5!=.
recode passhisabati_1 .=0 if grade==1

gen passkiswahili_2=1 if S2_kiswahili_1>=4 & S2_kiswahili_2>=4 & S2_kiswahili_3==1 & S2_kiswahili_1!=. & S2_kiswahili_2!=. & S2_kiswahili_3!=.
recode passkiswahili_2 .=0 if grade==2
gen passkiingereza_2=1 if S2_kiingereza_1>=4 & S2_kiingereza_2>=4 & S2_kiingereza_3==1 & S2_kiingereza_1!=. & S2_kiingereza_2!=. & S2_kiingereza_3!=.
recode passkiingereza_2 .=0 if grade==2
gen passhisabati_2=1 if S2_hisabati_1>=4 & S2_hisabati_2>=4 & S2_hisabati_3>=4 & S2_hisabati_4>=4 & S2_hisabati_1!=. & S2_hisabati_2!=. & S2_hisabati_3!=. & S2_hisabati_4!=.
recode passhisabati_2 .=0 if grade==2

gen passkiswahili_3=1 if S3_kiswahili_1==1 & S3_kiswahili_2>=1 & S3_kiswahili_1!=. & S3_kiswahili_2!=.
recode passkiswahili_3 .=0 if grade==3
gen passkiingereza_3=1 if S3_kiingereza_1==1 & S3_kiingereza_2>=1 & S3_kiingereza_1!=. & S3_kiingereza_2!=.
recode passkiingereza_3 .=0 if grade==3
gen passhisabati_3=1 if S3_hisabati_1>=4 & S3_hisabati_2>=4 & S3_hisabati_3>=4 & S3_hisabati_4>=4 & S3_hisabati_1!=. & S3_hisabati_2!=. & S3_hisabati_3!=. & S3_hisabati_4!=.
recode passhisabati_3 .=0 if grade==3

sum passkiswahili_1 passkiingereza_1 passhisabati_1 passkiswahili_2 passkiingereza_2 passhisabati_2 passkiswahili_3 passkiingereza_3 passhisabati_3
saveold "EStudent_passrates1.dta"
log close
