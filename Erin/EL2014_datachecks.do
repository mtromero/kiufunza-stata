/*EL2014/R6 Data Checks for EDI Payment
Erin Litzow
12.15.2014

Modified by:
Mauricio Romero
12.15.2014
*/

clear

*global rawinput "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData\8 Endline 2014\Final Data"
*global output "C:\Users\ErinLitzow\Box Sync\01_KiuFunza Analysis_Box\6 Data Collection\2014\EL2014 Monitoring\Data Checks"
*global treatment "C:\Users\ErinLitzow\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing" 

global rawinput "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\8 Endline 2014\Final Data"
global output "C:\Users\Mauricio\Box Sync\01_KiuFunza\Results\Logs"
global treatment "C:\Users\Mauricio\Box Sync\01_KiuFunza\RawData\3 Endline\Supplementing" 

log using "$output\EL2014_datachecks", replace

************************
*****Teachers*****
************************
*check for balance
use "$rawinput\R6_ClusterReport.dta"
gen tch_tobeint=(tch_prepop+ tch_new)- tch_left
move tch_tobeint tch_intv
gen tch_percentint= (tch_intv/ tch_tobeint)*100
move tch_percentint tch_unav
tab treatarm, gen(TD)
regress tch_percentint TD*, nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
drop TD1 TD2 TD3 TD4
display "TEACHER SAMPLE BALANCED"
clear

use "$rawinput\Teacher\R6Teacher_noPII.dta"
tab tchwrk, missing
display "91.7% of prepopulated teachers are reported to still be teaching at the school"
display "added 79 new teachers to sample this round"
tab tcnsnt /*not asked to teachers who are not currently teaching FGS or were not teaching FGS during R5*/
display "only one teacher refusal"

tab tqcobyn
display "9.2% of teacher interviews partly observed by supervisor"
tab unavrn
tab unavrn_other
display "no mention of teachers being 'unavailable' b/c of unhappiness with KiuFunza program"

egen SchTchID=concat(SchoolID R6TeacherID), punct(_)
tempfile missingTime
save "`missingTime'"

*checking sample of other questions for obvious errors
tab missedClasses
tab mchtch1
tab mchtch7
clear
use "$rawinput\Teacher\R6TGrdSub_noPII.dta" 
tab grbgyn
tab entini
tab entrsn
clear
use "$rawinput\Teacher\R6TGrdSubExit_noPII.dta"
tab grstyn
tab extini
tab extrsn
display "only two teacher assignment changes attributed to KiuFunza"
clear
use "$rawinput\Teacher\R6TGrdSub3_noPII.dta"
egen SchTchID=concat(SchoolID R6TeacherID), punct(_)
collapse (count) fgradesubjectid, by(SchTchID)
display "focal grade/subject questions asked for everyone currently teaching FGS"
sum fgradesubjectid
display "average teacher teaches 1.7 focal grade/subject combinations"
clear
use "$rawinput\Teacher\R6TGrdSub4_noPII.dta" 
tab anotest
sum anotest
tab attutyn
tab attutyn
tab atremyn
egen SchTchID=concat(SchoolID R6TeacherID), punct(_)
collapse (count) nfgradesubjectid, by(SchTchID)
display "average FGS teacher also teaches 2.9 non-focal grade/subject combos"
clear

use "$rawinput\Teacher\R6GroupSubject_noPII.dta"
*browse if gpmtch==.
display "21 (0.6%) focal grade/subject combinations have no teacher assigned, according to HT"
clear

use "$rawinput\Teacher\R6Time_noPII.dta"
egen SchTchID=concat(SchoolID R6TeacherID), punct(_)
collapse (first) t27, by(SchTchID)
tab t27, missing
merge 1:1 SchTchID using "`missingTime'"
*browse if t27==.
display "Time use asked for everyone currently teaching FGS"
clear

************************
*******Schools*********
*************************
use "$rawinput\R6_ClusterReport.dta"
format schl_visitdate %tc
tab treatarm, gen(TD)
regress schl_visitdate TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
drop TD1 TD2 TD3 TD4
display "SCHOOL VISIT DATES BALANCED ACROSS TREATMENTS"
clear

use "$rawinput\School\R6School_noPII.dta"
tab consnt
display "no school refusals"
tab headteacher
tab httmas
tab respondent headteacher
tab aim1
tab dgtrc3

merge 1:1 SchoolID using "$treatment\R_EL_schools_noPII.dta", keepusing(treatarm)
drop if _merge==2
drop _merge
tab headteacher treatarm
display "104 head teacher respondents in COD and Combo schools"
tab treatarm, gen(TD)
gen HT=(headteacher==1) & !missing(headteacher)
reg HT TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
display "HEAD TEACHER ANSWERED INTERVIEW BALANCED"

tab tbshyn
tab atbshyn

tab numvis
gen ONEVISIT=(numvis==1) & !missing(numvis)
reg ONEVISIT TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
display "85% of school interviews completed in one visit and is balanced"

tab qcobyn
gen overview=(qcobyn==1) & !missing(qcobyn)
reg overview TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
display "14% of school interviews were at least partly observed by supervisor and is balanced"
clear

use "$rawinput\School\R6SFacility_noPII.dta" 
tab SchoolID if SchoolID>=200 & SchoolID<=300
list if SchoolID==212
display "no facilities observations for School 212"
clear

use "$rawinput\School\R6ExamGrd_noPII.dta"
tab rnkavl
display "we found 23% of the rankings we missed at midline"
clear

use "$rawinput\School\R6Funding_noPII.dta" 
list if SchoolID==212
display "funding data missing for 212"
clear

use "$rawinput\School\R6Expenses_noPII.dta" 
tab SchoolID
tab R6ExpenseID
clear



************************
*****Students********
************************
use "$rawinput\R6_ClusterReport.dta"
gen stu_testanywhere= stu_testtkn+stu_hh
move stu_testanywhere hh_intv
tab treatarm, gen(TD)
regress stu_testanywhere TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
drop TD1 TD2 TD3 TD4
display "STUDENT TEST RATES BALANCED ACROSS TREATMENTS"
clear

use "$rawinput\Student\R6School_noPII.dta" 
tab intrs1
list intrs1_other if intrs1_other!=""
clear

use "$rawinput\Student\R6Student_noPII.dta"
tab result
tab consentChild
list result_other if result==2
display "9,101 tested at school, 557 tested at home, for a total of 92% of students interviewed"

tab stdgrd
tab consentChild stdgrd
display "at school: 88.5% of grade 1 tested, 86.1% of grade 2 tested, 85.6% of grade 3 tested"

*check that at least the first question in each sub was correctly asked to each grade
foreach var in kissyl1_1 englet1_1 hiscou1_1 othsub1_1 {
	list SchoolID R6StudentID if `var'==. & stdgrd==1 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==2 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==3 & consentChild==1
}
foreach var in kissyl2_1 englet2_1 hiscou2_1 othsub2_1 {
	list SchoolID R6StudentID if `var'==. & stdgrd==2 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==1 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==3 & consentChild==1
}
foreach var in kissyl3_1 englet3_1 hiscou3_1 othsub3_1 {
	list SchoolID R6StudentID if `var'==. & stdgrd==3 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==1 & consentChild==1
	list SchoolID R6StudentID if `var'!=. & stdgrd==2 & consentChild==1
}

clear

************************
*******Households*********
************************
use "$rawinput\R6_ClusterReport.dta"
gen hh_notint=hh_ref+hh_nontracked+hh_unav
tab hh_notint treatment
gen hh_perint=(hh_int/15)*100
sum hh_perint
tab treatarm, gen(TD)
regress hh_perint TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
drop TD1 TD2 TD3 TD4
display "HH not balanced Combo/Control, but balanced joint"
clear

use "$rawinput\Household\R6HHData_noPII.dta"
merge m:1 SchoolID using "$treatment\R_EL_schools_noPII.dta", keepusing(treatarm)
drop if _merge==2
drop _merge
tab consnt
display "six HH refusals"

tab fchome
tab schtst
tab consentChild


tab treatarm, gen(TD)
gen TestHome=(consentChild==1) & !missing(consentChild)
regress TestHome TD*, vce(cluster SchoolID) nocons
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal
test (_b[TD1] == _b[TD2]== _b[TD3]== _b[TD4])
drop TD1 TD2 TD3 TD4
display "557 students tested at home and balanced"

rename upid_student upid
drop if upid==""
merge 1:1 upid using "$treatment\R_EL_student_noPII.dta", keepusing(GradeID)
drop if _merge==2
drop _merge
tab intrs1 GradeID
tab intrs2 GradeID
display "tracked 96.9% of grade 2 HHs and 94.7% of grade 3 HHs"

tab hhhead respnd
display "47.1% of interviews conducted with HoH"
clear

use "$rawinput\Household\R6HHExpenditure_noPII.dta" 
collapse (count) expn13, by(HHID)
gen schagemem=expn13/9
sum schagemem
display "average of 1.79 primary age children in each HH, asked about expenditures"
clear

use "$rawinput\Household\R6HHMember_noPII.dta"
tab smschl
collapse (count) R6HHMemberID, by(HHID)
sum R6HHMemberID
display "average of 6.4 members per HH"
clear

log close
