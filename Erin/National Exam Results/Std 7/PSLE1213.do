/*TZ PSLE Results 2013

Erin Litzow
17 March 2014*/

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Geita.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Geita.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Kahama.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Kahama.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Karagwe.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Karagwe.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Kinondoni.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Kinondoni.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Kondoa.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Kondoa.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Korogwe.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Korogwe.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Lushoto.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Lushoto.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Mbinga.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Mbinga.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Mbozi.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Mbozi.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2013_Sumbawanga.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Sumbawanga.dta"
clear

use "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013_Geita.dta"
append using "PSLE2013_Kahama.dta"
append using "PSLE2013_Karagwe.dta"
append using "PSLE2013_Kinondoni.dta"
append using "PSLE2013_Kondoa.dta"
append using "PSLE2013_Korogwe.dta"
append using "PSLE2013_Lushoto.dta"
append using "PSLE2013_Mbinga.dta"
append using "PSLE2013_Mbozi.dta"
append using "PSLE2013_Sumbawanga.dta"

*renaming 2013 variables
rename tzschlcode tzschlcode13
rename tzschlname tzschlname13
rename tzdistname tzdistname13
rename tzregname tzregname13
*drop 2012 variables from 2013 data set (will add these again from 2012 data set which is more complete)
drop avgtot12
drop bandschl12
drop rankschl12

saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013.dta"

clear

*import .csv files of 2012 PSLE data
import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Geita.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Geita.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Kahama.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Kahama.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Karagwe.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Karagwe.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Kinondoni.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Kinondoni.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Korogwe.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Korogwe.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Lushoto.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Lushoto.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Mbinga.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Mbinga.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Kondoa.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Kondoa.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Mbozi.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Mbozi.dta"
clear

import delimited "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\csv files\2012_Sumbawanga.csv", case(preserve) 
saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Sumbawanga.dta"
clear

use "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012_Geita.dta" 
append using "PSLE2012_Kahama.dta"
append using "PSLE2012_Karagwe.dta"
append using "PSLE2012_Kondoa.dta"
append using "PSLE2012_Kinondoni.dta"
append using "PSLE2012_Korogwe.dta"
append using "PSLE2012_Lushoto.dta"
append using "PSLE2012_Mbinga.dta"
append using "PSLE2012_Mbozi.dta"
append using "PSLE2012_Sumbawanga.dta"

saveold "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2012.dta" 

clear

*merge 2012 and 2013 data sets
use "C:\Users\ErinLitzow\Box Sync\KiuFunza Analysis_Box\7 Data\Natl Exam Results\Standard 7, PSLE\dta files\PSLE2013.dta"
merge 1:1 SchoolID using "PSLE2012.dta", keepusing(tzschlcode12 tzschlname12 tzdistname12 candidates12 numpass12 avgtot12 avgtot11 chgavg1112 bandschl12 bandschl11 rankschl12 rankschl11)
saveold "PSLE1213.dta"

drop _merge

*label variables
label var DistID "District ID"
label var SchoolID "School ID"
label var tzschlcode13 "Government School Code, 2013"
label var tzschlname13 "Government School Name, 2013"
label var tzdistname13 "District Name, 2013"
label var tzregname13 "Region Name, 2013"
label var candidates13 "Total Students Tested, 2013"
label var numpass13 "Number of students passed, 2013"
label var avgtot13 "Average total score of all students tested, 2013"
label var chgavg1213 "change in average total score between 2012 and 2013"
label var bandschl13 "Band, 2013"
label var rankschl13 "Rank, 2013"
label var tzschlcode12 "Government School Code, 2012"
label var tzschlname12 "Government School Name, 2012"
label var tzdistname12 "District Name, 2012"
label var candidates12 "Total Students Tested, 2012"
label var numpass12 "Number of students passed, 2012"
label var avgtot12 "Average total score of all students tested, 2012"
label var avgtot11 "Average total score of all students tested, 2011"
label var chgavg1112 "change in average total score between 2011 and 2012"
label var bandschl12 "Band, 2012"
label var bandschl11 "Band, 2011"
label var rankschl12 "Rank, 2012"
label var rankschl11 "Rank, 2011"

merge 1:1 SchoolID using "C:\Users\ErinLitzow\Box Sync\KiuFunza\RawData\3 Endline\Supplementing\R_EL_schools_noPII.dta", keepusing(treatarm)
drop if _merge==2
drop _merge

log using missresultsbalance, name(missresultslog) smcl
log using missresultsbalance, name(missresultstxt) text

gen missing13=1
list candidates13 if SchoolID==211 | SchoolID==215 | SchoolID==331 | SchoolID==511 | SchoolID==521 | SchoolID==601 | SchoolID==717 | SchoolID==923 | SchoolID==927 | SchoolID==1018 | SchoolID==1034
replace missing13=0 if candidates13==.
tab missing13

gen missing12=1
list candidates12 if SchoolID==111 | SchoolID==116 | SchoolID==331 | SchoolID==511 | SchoolID==521 | SchoolID==601 | SchoolID==717 | SchoolID==923 | SchoolID==927 | SchoolID==1018 | SchoolID==1034
replace missing12=0 if candidates12==.
tab missing12

gen missing11=1
list avgtot11 if SchoolID==111 | SchoolID==116 | SchoolID==331 | SchoolID==511 | SchoolID==521 | SchoolID==601 | SchoolID==717 | SchoolID==923 | SchoolID==927 | SchoolID==1018 | SchoolID==1034
replace missing11=0 if avgtot11==.
tab missing11

label var missing13 "Missing Results, 2013"
label var missing12 "Missing Results, 2012"
label var missing11 "Missing Results, 2011"

tabstat missing13, by(treatarm)
tabstat missing12, by(treatarm)
tabstat missing11, by(treatarm)

**test for balance of missing test results
tab treatarm, gen(TD)

regress missing13 TD*, nocons

*pairwise tests
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal

*simultaneous parwise of treatments against control
test TD1=TD4
test TD2=TD4, accum
test TD3=TD4, accum

regress missing12 TD*, nocons

*pairwise tests
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal

*simultaneous parwise of treatments against control
test TD1=TD4
test TD2=TD4, accum
test TD3=TD4, accum

regress missing11 TD*, nocons

*pairwise tests
testparm TD1 TD2, equal
testparm TD2 TD3, equal
testparm TD3 TD4, equal
testparm TD1 TD3, equal
testparm TD1 TD4, equal
testparm TD2 TD4, equal

*simultaneous parwise of treatments against control
test TD1=TD4
test TD2=TD4, accum
test TD3=TD4, accum

log close _all

saveold "PSLE1213.dta", replace
