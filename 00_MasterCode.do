*ssc install unique
*ssc install estout
*ssc install reghdfe


clear all
set more off
******************************
*** SET  PREFERENCES ***
******************************
	
	*** Mauricio's laptop
	if "`c(username)'" == "Mauricio"  {
		global mipath "C:/Users/Mauricio/Box Sync/01_KiuFunza"
		global dir_do     "C:/Users/Mauricio/Documents/git/kiufunza-stata"
		global latexcodesfinals     "C:/Users/Mauricio/Dropbox/KF_Draft/KFI/LaTeX/tables"
		global graphs      "C:/Users/Mauricio/Dropbox/KF_Draft/KFI/LaTeX/graphs"
		global radar      "C:/Users/Mauricio/Dropbox/Research/TZ_Radar/"
	}
	*** Mauricio's desktop
	if "`c(username)'" == "mauricio"  {
		global mipath "/media/mauricio/TeraHDD2/Box Sync/01_KiuFunza"
		global dir_do     "/media/mauricio/TeraHDD2/git/kiufunza-stata"
		global latexcodesfinals     "/media/mauricio/TeraHDD2/Dropbox/KF_Draft/KFI/LaTeX/tables"
		global graphs      "/media/mauricio/TeraHDD2/Dropbox/KF_Draft/KFI/LaTeX/graphs"
		global radar      "/media/mauricio/TeraHDD2/Dropbox/Research/TZ_Radar/"
	}
	
		*** Mauricio's ITAM desktop
	if "`c(username)'" == "MROMEROLO"  {
		global mipath "C:/Users/mromerolo/Box Sync/01_KiuFunza"
		global dir_do     "C:/Users/mromerolo/Documents/git/kiufunza-stata"
		global latexcodesfinals     "C:/Users/mromerolo/Dropbox/KF_Draft/KFI/LaTeX/tables"
		global graphs      "C:/Users/mromerolo/Dropbox/KF_Draft/KFI/LaTeX/graphs"
		global radar      "C:/Users/mromerolo/Dropbox/Research/TZ_Radar/"
	}
	
	
	
	

*** Path tree
	global basein 	"$mipath/RawData"
	global base_out   "$mipath/CreatedData"
	global results    "$mipath/Results"
	global exceltables  "$mipath/Results/ExcelTables"
	/*
	global graphs     "$mipath/Results/Graphs"
	global latexcodes     "$mipath/Results/LatexCodes"
	*/
	
	
*** Globals controls and outcomes
	global AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
	global AggregateDep_Karthik 	Z_hisabati Z_kiswahili Z_kiingereza  Z_ScoreFocal  /*this should be added in the future...*/
	global AggregateDep_int 	Z_hisabati Z_kiswahili Z_kiingereza
	global subjects 	hisabati kiswahili kiingereza ScoreFocal
	global treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
	global treatmentlist_int TreatmentCOD TreatmentBoth
	global week_T3  i.WeekIntvTest_T3 
	global week_T7  i.WeekIntvTest_T7 
	global teachercontrol  male higher_degree t05 t07 t08 t10 t21 t23
	global school_average MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati
	global schoolcontrol  PTR_T1  SingleShift_T1 IndexDistancia_T1 InfrastructureIndex_T1 IndexFacilities_T1 s108_T1 
	global studentcontrol LagseenUwezoTests LagpreSchoolYN Lagmale LagAge i.LagGrade LagGrade#(c.LagZ_kiswahili##c.LagZ_kiswahili) LagGrade#(c.LagZ_hisabati##c.LagZ_hisabati)  LagGrade#(c.LagZ_kiingereza##c.LagZ_kiingereza)
	global HHcontrol c.HHSize#c.MissingHHSize c.IndexPoverty#c.MissingIndexPoverty c.IndexPoverty c.IndexEngagement#c.MissingIndexEngagement c.IndexEngagement  c.LagExpenditure#c.MissingLagExpenditure c.LagExpenditure  
	
	exit
*** Actual do 
	do "$dir_do/01_CreateStudentData"
	do "$dir_do/01_CreateStudentData_InterventionEDI"
	do "$dir_do/02_CreateSchoolData"
	do "$dir_do/03_CreateHousehold"
	do "$dir_do/04_CreateTeacherData"
	do "$dir_do/06_CreateConsolidatedData"
	do "$dir_do/05_BaseLineSummary"
	do "$dir_do/99_SlidesFiguresTables_V3"	


