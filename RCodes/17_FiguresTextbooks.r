
#setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Reg")
setwd("E:/Box Sync/01_KiuFunza/Results/Reg")



Coefs=read.csv("Total_books.csv")
Coefs[3:4,]=sqrt(Coefs[3:4,])

Error=Coefs[3:4,]
Coefs=Coefs[1:2,]

if(periodo=="T3") year="yr 1"
if(periodo!="T3") year="yr 2"



Coefs=as.matrix(Coefs)
Error=as.matrix(Error)

colnames(Coefs)=c("CG","COD","Combo","Control")
rownames(Coefs)= c("Grades 1-3","Grades 4-7")

Coefs=Coefs[,c(4,2,1,3)]
Error=Error[,c(4,2,1,3)]

library(gplots)
Coefs2=Coefs[1,]
Error2=Error[1,]
CI=0.975

pdf("TextbookFocal.pdf")
barplot2(Coefs2,names.arg=names(Coefs2),main="Textbook expenditure in grades 1-3",
              xlab="Treatment",ylab="Mean expenditure",plot.ci=TRUE,
              ci.u=Coefs2+qnorm(CI)*Error2,ci.l=Coefs2-qnorm(CI)*Error2,beside=T,col=gray.colors(nrow(Coefs2)))
              abline(h=0,lwd=2,col=1)
dev.off()

Coefs2=Coefs[2,]
Error2=Error[2,]

pdf("TextbookNonFocal.pdf")
barplot2(Coefs2,names.arg=names(Coefs2),main="Textbook expenditure in grades 4-7",
              xlab="Treatment",ylab="Mean expenditure",plot.ci=TRUE,
              ci.u=Coefs2+qnorm(CI)*Error2,ci.l=Coefs2-qnorm(CI)*Error2,beside=T,col=gray.colors(nrow(Coefs2)))
              abline(h=0,lwd=2,col=1)
              dev.off()

pdf("TextbookTotal.pdf")
barplot2(Coefs,names.arg=colnames(Coefs),main="Textbook expenditure",
              xlab="Treatment",ylab="Mean expenditure",plot.ci=TRUE,
              ci.u=Coefs+qnorm(CI)*Error,ci.l=Coefs-qnorm(CI)*Error,beside=T,col=heat.colors(nrow(Coefs)))
              abline(h=0,lwd=2,col=1)
              legend("topleft", rownames(Coefs), fill=heat.colors(nrow(Coefs)),ncol=2)
              dev.off()