setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/RawData/SpatialData/")
#setwd("E:/Box Sync/01_KiuFunza/Results/Reg/RawData/SpatialData/")
library(sp)
library(maptools)
library(rgdal)
library(dismo)
library(raster)  # grids, rasters
library(rasterVis)  # raster visualisation
library(rgeos)
library(RgoogleMaps)
library(choroplethr)
library(choroplethrMaps)
library(ggplot2)
library(mapproj)
library(gridExtra)
library(GISTools)
library(foreign)
library(maps)

AfricanCountries=readOGR("AfricanCountries","AfricanCountires")
AfricanCountries=spTransform(AfricanCountries, CRS("+proj=lcc +lat_1=20 +lat_2=-23 +lat_0=0 +lon_0=25 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs")) 
pdf("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/Africa.pdf")
par(mar=c(1,1,1,1))
plot(AfricanCountries)
plot(AfricanCountries[AfricanCountries$COUNTRYAFF=="Tanzania",],add=T,col=rgb(.8,0,0))
GISTools::map.scale(extent(AfricanCountries)@xmin+250000*12,extent(AfricanCountries)@ymin+50000*25,len=1000*5000,units="KM",ndivs=5,subdiv=1000,tcol='black',sfcol='red')          
north.arrow(extent(AfricanCountries)@xmin+80000*28,extent(AfricanCountries)@ymin+170000*20 ,len=1000*200, lab="N")
dev.off()


GPS_schools=read.dta("C:/Users/Mauricio/Box Sync/GPS/clean_schoolgeodata.dta")
schoolid=data.frame(GPS_schools$schoolid)
colnames(schoolid)="SchoolID"
GPS_schools = SpatialPoints(GPS_schools[,c(3,2)],proj4string=CRS(as.character("+init=epsg:4326")))
GPS_schools = SpatialPointsDataFrame(GPS_schools,schoolid)
GPS_schools = spTransform(GPS_schools, CRS(" +proj=utm +zone=37 +ellps=WGS84 +datum=WGS84 +units=m +no_defs +towgs84=0,0,0")) 


Tanzania=readRDS("TZA_adm2.rds")
plot(Tanzania)
summary(Tanzania@data)
Tanzania@data$RCT="white"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Kahama"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Kondoa"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Geita"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Karagwe"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Korogwe"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Lushoto"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Mbinga"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Kinondoni"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Mbozi"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Sumbawanga Rural"]="darkorange"
#Tanzania@data$RCT[Tanzania@data$NAME_2=="Kigoma Rural"]="darkorange"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Lake Tanganyika"]="dodgerblue3"
Tanzania@data$RCT[Tanzania@data$NAME_2=="Lake Victoria"]="dodgerblue3"
Tanzania=spTransform(Tanzania,CRS(" +proj=utm +zone=37 +ellps=WGS84 +datum=WGS84 +units=m +no_defs +towgs84=0,0,0"))  

pdf("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/map_rct_1.pdf")
plot(Tanzania,col=Tanzania@data$RCT)
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Kinondoni"),])+c(1.5,0), labels=as.character(Tanzania$NAME_2[which(Tanzania@data#$NAME_2=="Kinondoni")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Korogwe"),])+c(-0.5,-0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Korogwe")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Lushoto"),])+c(0.8,0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Lushoto")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$RCT=="darkorange")[2:9],]), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$RCT=="darkorange")[2:9]]), cex=1))
#plot(GPS_schools,add=T,pch=19,cex=0.1)
map.scale(extent(Tanzania)@xmin+250000,extent(Tanzania)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='black',sfcol='red')          
north.arrow(extent(Tanzania)@xmin+80000,extent(Tanzania)@ymin+170000 ,len=1000*30, lab="N")
dev.off()

pdf("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/map_rct_1_bw.pdf")
Tanzania@data$RCT[Tanzania@data$RCT=="dodgerblue3"]="white"
plot(Tanzania,col=Tanzania@data$RCT)
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Kinondoni"),])+c(1.5,0), labels=as.character(Tanzania$NAME_2[which(Tanzania@data#$NAME_2=="Kinondoni")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Korogwe"),])+c(-0.5,-0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Korogwe")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Lushoto"),])+c(0.8,0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Lushoto")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$RCT=="darkorange")[2:9],]), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$RCT=="darkorange")[2:9]]), cex=1))
#plot(GPS_schools,add=T,pch=19,cex=0.1)
map.scale(extent(Tanzania)@xmin+250000,extent(Tanzania)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='black',sfcol='red')          
north.arrow(extent(Tanzania)@xmin+80000,extent(Tanzania)@ymin+170000 ,len=1000*30, lab="N")
dev.off()


pdf("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/map_rct_schools.pdf")
plot(Tanzania,col=Tanzania@data$RCT)
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Kinondoni"),])+c(1.5,0), labels=as.character(Tanzania$NAME_2[which(Tanzania@data#$NAME_2=="Kinondoni")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Korogwe"),])+c(-0.5,-0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Korogwe")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Lushoto"),])+c(0.8,0.5), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Lushoto")]), cex=1))
#invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$RCT=="darkorange")[2:9],]), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$RCT=="darkorange")[2:9]]), cex=1))
plot(GPS_schools,add=T,pch=19,cex=0.1)
map.scale(extent(Tanzania)@xmin+250000,extent(Tanzania)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='black',sfcol='red')          
north.arrow(extent(Tanzania)@xmin+80000,extent(Tanzania)@ymin+170000 ,len=1000*30, lab="N")
dev.off()

Tanzania@data$RCT[Tanzania@data$NAME_2=="Kigoma Rural"]="darkorange"

pdf("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/map_intervention.pdf")
plot(Tanzania,col=Tanzania@data$RCT,border="darkgray")
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Kinondoni"),])+c(1.2*100000,0), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Kinondoni")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Korogwe"),])+c(-0.5,-0.7*100000), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Korogwe")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Lushoto"),])+c(0.5,0.7*100000), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Lushoto")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$RCT=="darkorange")[2:9],]), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$RCT=="darkorange")[2:9]]), cex=0.7,font=2))
#plot(GPS_schools,add=T,pch=19,cex=0.1)
map.scale(extent(Tanzania)@xmin+250000,extent(Tanzania)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='darkgray',sfcol='red')          
north.arrow(extent(Tanzania)@xmin+80000,extent(Tanzania)@ymin+170000 ,len=1000*30, lab="N",col="darkgray")
dev.off()


jpeg("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Graphs/map_intervention.jpeg",quality =100)
plot(Tanzania,col=Tanzania@data$RCT,border="darkgray")
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Kinondoni"),])+c(1.2*100000,0), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Kinondoni")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Korogwe"),])+c(-0.5,-0.7*100000), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Korogwe")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$NAME_2=="Lushoto"),])+c(0.5,0.7*100000), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$NAME_2=="Lushoto")]), cex=0.7,font=2))
invisible(text(getSpPPolygonsLabptSlots(Tanzania[which(Tanzania@data$RCT=="darkorange")[2:9],]), labels=as.character(Tanzania$NAME_2[which(Tanzania@data$RCT=="darkorange")[2:9]]), cex=0.7,font=2))
#plot(GPS_schools,add=T,pch=19,cex=0.1)
map.scale(extent(Tanzania)@xmin+250000,extent(Tanzania)@ymin+50000,len=1000*500,units="KM",ndivs=5,subdiv=100,tcol='darkgray',sfcol='red')          
north.arrow(extent(Tanzania)@xmin+80000,extent(Tanzania)@ymin+170000 ,len=1000*30, lab="N",col="darkgray")
dev.off()

# 
#Tanzania2002<-readOGR("Tanzania_District_EA_2002_region","Tanzania_District_EA_2002_region")
#
#Tanzania2002@data$RCT=0
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Kahama"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Kondoa"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Geita"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Karagwe"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Korogwe"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Lushoto"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Mbinga"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Kinondoni"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Mbozi"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Sumbawanga Rural"]=1
#Tanzania2002@data$RCT[Tanzania2002@data$DISTNAME=="Kigoma Rural"]=1
#
#plot(Tanzania2002,col=Tanzania2002@data$RCT*2)
#print(proj4string(Tanzania2002))
#
#
# Tanzania2002_df <- fortify(Tanzania2002)
# 
# ggplot(Tanzania2002_df, aes(long,lat, group=group)) + 
#  geom_polygon() + 
#  labs(title="World map (longlat)") + 
#  coord_equal() + 
#  theme_opts
#          