rm(list=ls())

load("/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/LibroSchool.RData")

require(RCurl)
require(XML)
curl = getCurlHandle()
BasePublisher=NULL
BasePublisher=data.frame(BasePublisher)
namesPub=c("Educational Books Publishers L","Ben and Company","Jadida Bookhouse (T) Ltd","E & D Vision Publishing Ltd",
"Best Deal Publishers","Mture Educational Publishers L","Mkuki na Nyota Publishers","Longhorn Publishers (T) Ltd","Ujuzi Books",
"Aidan Publishers Limited")

for(id_pub in c(1,2,4,5,6,8,9,10,11,12)){
  for(sc_id in unique(Base$ID_Gov)){ 
    appURL <- getURL(paste0("http://www.pesptz.org/index.php/publisher/bysubject?lang=english&s_id=",sc_id,"&p_id=",id_pub), ssl.verifypeer = FALSE,curl=curl)
    while((inherits(appURL, "try-error"))){
	Sys.sleep(1)
	try(appURL <- getURL(paste0("http://www.pesptz.org/index.php/publisher/bysubject?lang=english&s_id=",sc_id,"&p_id=",id_pub), ssl.verifypeer = FALSE,curl=curl))
      }

    Libros=readHTMLTable(appURL,header=F,which=1,stringsAsFactors=FALSE,skip.rows=1)
    
    if(is.null(Libros)==T) next
    
    Libros=Libros[,-c(1,9)]
    colnames(Libros)=c("BookName","Subject","Planned","PlannedDate","Delivered","DeliveredDate","PropDelivered")
    Libros$ID_Gov=sc_id
    Libros$ID_Dist=id_pub
    BasePublisher=rbind(BasePublisher,Libros)
    print(paste0("School ",sc_id," with Publisher ",id_pub))
  }
}


save(BasePublisher,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/BasePublisher.RData")
library(foreign)
BasePublisher$PlannedDate[which(BasePublisher$PlannedDate=="")]="NA"
BasePublisher$DeliveredDate[which(BasePublisher$DeliveredDate=="")]="NA"

write.dta(BasePublisher,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/BasePublisher.dta")
  
