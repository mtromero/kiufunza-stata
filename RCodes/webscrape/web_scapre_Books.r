﻿rm(list=ls())


require(RCurl)
require(XML)
curl = getCurlHandle()
Base=NULL
Base=data.frame(Base)
for(sc_id in 1:16000){ 
  print(sc_id)
  appURL <- getURL(paste0("http://www.pesptz.org/index.php/book_delivery/bystandard?lang=english&s_id=",sc_id), ssl.verifypeer = FALSE,curl=curl)
  SchoolInfo=htmlTreeParse(appURL)[[1]][[2]][[1]][[3]][[9]]
  if(length(SchoolInfo[[2]])==0) next
  SchoolName=SchoolInfo[[2]][[1]]$value
  SchoolDistrict=SchoolInfo[[4]][[1]]$value
  SchoolRegion=SchoolInfo[[6]][[1]]$value

  BooksInfo=htmlTreeParse(appURL)[[1]][[2]][[1]][[3]][[12]][[1]]

  Libros=NULL

  for(i in 2:length(BooksInfo)){
    LibTemp=NULL
    for(j in c(1,3,4,5,6)){
      ent=try(as.numeric(BooksInfo[[i]][[j]][[1]]$value),silent=T)
      if(inherits(ent, "try-error")) ent=NA
      LibTemp=c(LibTemp,ent)
    }
    Libros=rbind(Libros,LibTemp)   
  }
  Libros=data.frame(Libros)
  colnames(Libros)=c("Grade","Pupils","Planned","Delivered","PropDelivered")
  Libros$ID_Gov=sc_id
  Libros$School=SchoolName
  Libros$District=SchoolDistrict
  Libros$Region=SchoolRegion
  Base=rbind(Base,Libros)
}

save(Base,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/LibroSchool.RData")
library(foreign)
write.dta(Base,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/LibroSchool.dta")


# remDr$close()
# pJS$stop()
#

