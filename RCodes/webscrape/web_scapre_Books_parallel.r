﻿rm(list=ls())

fun_hace_todo=function(appURL){
  SchoolInfo=htmlTreeParse(appURL)[[1]][[2]][[1]][[3]][[9]]
  if(length(SchoolInfo[[2]])==0) return(NULL)
  SchoolName=SchoolInfo[[2]][[1]]$value
  SchoolDistrict=SchoolInfo[[4]][[1]]$value
  SchoolRegion=SchoolInfo[[6]][[1]]$value

  BooksInfo=htmlTreeParse(appURL)[[1]][[2]][[1]][[3]][[12]][[1]]

  Libros=NULL

  for(i in 2:length(BooksInfo)){
    Libros=rbind(Libros,
    c(as.numeric(BooksInfo[[i]][[1]][[1]]$value),as.numeric(BooksInfo[[i]][[3]][[1]]$value),
    as.numeric(BooksInfo[[i]][[4]][[1]]$value),as.numeric(BooksInfo[[i]][[5]][[1]]$value),
    as.numeric(BooksInfo[[i]][[6]][[1]]$value)))
  }
  Libros=data.frame(Libros)
  colnames(Libros)=c("Grade","Pupils","Planned","Delivered","PropDelivered")
  Libros$ID_Gov=as.numeric(substring(xmlAttrs(htmlTreeParse(appURL)[[1]][[2]][[1]][[3]][[3]][[1]][[1]][[1]][[1]]), 81))
  Libros$School=SchoolName
  Libros$District=SchoolDistrict
  Libros$Region=SchoolRegion
  
  return(Libros)
  }

require(RCurl)
require(XML)
curl = getCurlHandle()
urls=paste0("http://www.pesptz.org/index.php/book_delivery/bystandard?lang=english&s_id=",1:15600)
txt <- getURIAsynchronous(urls[1:2],curl=curl)
Base <- lapply(txt,fun_hace_todo)
Base=do.call(rbind,Base)
  
