rm(list=ls())
require(RCurl)
require(XML)
curl = getCurlHandle()
trim <- function (x) gsub("^\\s+|\\s+$", "", x)

NivelSuperios <- getURL("http://maktaba.tetea.org/exam-results/PSLE2013/psle.htm", ssl.verifypeer = FALSE,curl=curl)
doc <- htmlParse(NivelSuperios)
links_region <- xpathSApply(doc, "//a/@href")
regions_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))


ResultadosEstudiantesTotal=NULL
ResultadosSchoolTotal=NULL
ResultadosEstudiantesTotal=data.frame(ResultadosEstudiantesTotal)
ResultadosSchoolTotal=data.frame(ResultadosSchoolTotal)


for(reg in 2:length(links_region)){

  try(NivelRegion <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  while((inherits(NivelRegion, "try-error"))){
    Sys.sleep(1)
    try(NivelRegion <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  }
  if(length(grep("ERROR 404",NivelRegion))!=0 | length(grep("Forbidden",NivelRegion))!=0) next
  doc <- htmlParse(NivelRegion)
  links_distrcits <- xpathSApply(doc, "//a/@href")
  districts_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))

  for(dist in 2:length(links_distrcits)){
    NivelDistrcit <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl)
    while((inherits(NivelRegion, "try-error"))){
      Sys.sleep(1)
      try(NivelDistrcit <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl))
    }
  
    if(length(grep("ERROR 404",NivelDistrcit))!=0 | length(grep("Forbidden",NivelDistrcit))!=0) next
    doc <- htmlParse(NivelDistrcit)
    links_schools <- xpathSApply(doc, "//a/@href")
    schools_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))
    
    for(school in 1:length(links_schools)){
      NivelSchool <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_schools[[school]]), ssl.verifypeer = FALSE,curl=curl)
      while((inherits(NivelRegion, "try-error"))){
	Sys.sleep(1)
	try(NivelSchool <- getURL(paste0("http://maktaba.tetea.org/exam-results/PSLE2013/",links_schools[[school]]), ssl.verifypeer = FALSE,curl=curl))
      }
      if(length(grep("ERROR 404",NivelSchool))!=0 | length(grep("Forbidden",NivelSchool))!=0 ) next
      doc <- htmlTreeParse(NivelSchool)
      ResultadosSchool=NULL
      ResultadosSchool=data.frame(SchoolID=c(NA))

		
      ResultadosEstudiantes=readHTMLTable(NivelSchool,header=T,which=1,stringsAsFactors=FALSE)
      ResultadosEstudiantes[,4]=gsub(",","",ResultadosEstudiantes[,4])
      if(dim(ResultadosEstudiantes)[2]==4){
	if(sum(grepl("Avarage Grade",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'Avarage Grade -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("average grade",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'average grade -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("AVERAGE GRADE",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'AVERAGE GRADE-'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
      }
      if(dim(ResultadosEstudiantes)[2]==4) stop("Fucked up... still average wrong")
      ResultadosEstudiantes$Absent=0
      ResultadosEstudiantes$Irregularities=0
      ResultadosEstudiantes$W=0
      ResultadosEstudiantes$S=0
      
      if(length(doc[[1]])>1) tipo=1
      if(length(doc[[1]])==1) tipo=2
      
      if(tipo==1) ResultadosEstudiantes$SchoolID=doc[[1]][[2]][[1]][[5]][[1]]$value
      if(tipo==2) ResultadosEstudiantes$SchoolID=doc[[1]][[1]][[5]][[5]][[1]]$value
      
      ResultadosEstudiantes$school_name=gsub("\r","",gsub("\n","",schools_name[school]))
      ResultadosEstudiantes$district_name=gsub("\r","",gsub("\n","",districts_name[dist]))
      ResultadosEstudiantes$region_name=gsub("\r","",gsub("\n","",regions_name[reg]))
      ResultadosEstudiantes$SUBJECTS=tolower(ResultadosEstudiantes$SUBJECTS)
      if( sum(grepl("absent",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Absent[grepl("absent",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("absent",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
	
      }
      if(sum(grepl("irregularities",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Irregularities[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      if(sum(grepl("\\*w",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$W[grepl("\\*w",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("\\*w",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      if(sum(grepl("\\*s",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$S[grepl("\\*s",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("\\*s",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      
      ResultadosEstudiantes$SUBJECTS=gsub("kiswahili","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("english","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("maarifa","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("hisabati","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("science","",ResultadosEstudiantes$SUBJECTS)
      MatrizSubjects=toupper(t(sapply(strsplit(ResultadosEstudiantes$SUBJECTS,'-'),trim))[,-1])
      if(length(MatrizSubjects)==5) ResultadosEstudiantes[,c("kiswahili","english","maarifa","hisabati","sayansi")]=cbind(MatrizSubjects)
      else if(dim(MatrizSubjects)[2]==5)     ResultadosEstudiantes[,c("kiswahili","english","maarifa","hisabati","sayansi")]=cbind(MatrizSubjects)
      else {  stop("Something is wrong!")}
      ResultadosEstudiantes=subset(ResultadosEstudiantes,select=-SUBJECTS)
      ResultadosEstudiantesTotal=rbind(ResultadosEstudiantesTotal,ResultadosEstudiantes)
    

      
      if(tipo==1){
	ResultadosSchool$SchoolID=doc[[1]][[2]][[1]][[5]][[1]]$value
	ResultadosSchool$Students=as.numeric(trim(gsub(":","",gsub("WALIOFANYA MTIHANI","",doc[[1]][[2]][[1]][[6]][[1]]$value))))
	ResultadosSchool$Average=as.numeric(trim(gsub(":","",gsub("WASTANI WA SHULE","",doc[[1]][[2]][[1]][[6]][[3]]$value))))
	ResultadosSchool$RankingDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIWILAYA","",doc[[1]][[2]][[1]][[6]][[5]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIWILAYA","",doc[[1]][[2]][[1]][[6]][[5]]$value))), "kati ya")[[1]][[2]])
	ResultadosSchool$RankingRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIMKOA","",doc[[1]][[2]][[1]][[6]][[7]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIMKOA","",doc[[1]][[2]][[1]][[6]][[7]]$value))), "kati ya")[[1]][[2]])
	ResultadosSchool$RankingNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KITAIFA","",doc[[1]][[2]][[1]][[6]][[9]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KITAIFA","",doc[[1]][[2]][[1]][[6]][[9]]$value))), "kati ya")[[1]][[2]])
      }
      
      if(tipo==2){
	ResultadosSchool$SchoolID=doc[[1]][[1]][[5]][[5]][[1]]$value
      	ResultadosSchool$Students=as.numeric(trim(gsub(":","",gsub("WALIOFANYA MTIHANI","",doc[[1]][[1]][[5]][[6]][[1]]$value))))
	ResultadosSchool$Average=as.numeric(trim(gsub(":","",gsub("WASTANI WA SHULE","",doc[[1]][[1]][[5]][[6]][[3]]$value))))
	ResultadosSchool$RankingDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIWILAYA","",doc[[1]][[1]][[5]][[6]][[5]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIWILAYA","",doc[[1]][[1]][[5]][[6]][[5]]$value))), "kati ya")[[1]][[2]])
	ResultadosSchool$RankingRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIMKOA","",doc[[1]][[1]][[5]][[6]][[7]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KIMKOA","",doc[[1]][[1]][[5]][[6]][[7]]$value))), "kati ya")[[1]][[2]])
	ResultadosSchool$RankingNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KITAIFA","",doc[[1]][[1]][[5]][[6]][[9]]$value))), "kati ya")[[1]][[1]])
	ResultadosSchool$TotalNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KITAIFA","",doc[[1]][[1]][[5]][[6]][[9]]$value))), "kati ya")[[1]][[2]])
      
      }
      ResultadosSchool$school_name=gsub("\r","",gsub("\n","",schools_name[school]))
      ResultadosSchool$district_name=gsub("\r","",gsub("\n","",districts_name[dist]))
      ResultadosSchool$region_name=gsub("\r","",gsub("\n","",regions_name[reg]))
      ResultadosSchoolTotal=rbind(ResultadosSchoolTotal,ResultadosSchool)
      print(paste0("School ",school," in District ",dist," in Region ",reg))
    }
  }
}

save(ResultadosSchoolTotal,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/ResultadosSchoolTotal_PSLE_2013.Rdata")
save(ResultadosEstudiantesTotal,file="/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData/ResultadosEstudiantesTotal_PSLE_2013.RData")



##############################################
##############################################
##############################################
##############################################

   