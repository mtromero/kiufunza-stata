setwd("C:/Users/Mauricio/Dropbox/Research/TZ_Radar/RawData")
#setwd("/media/mauricio/TeraHDD1/Dropbox/Research/TZ_Radar/RawData")
trim <- function (x) gsub("^\\s+|\\s+$", "", x)

library(foreign)
library(stringr)


load("ResultadosEstudiantesTotal_SFNA_2015.RData")
load("ResultadosSchoolTotal_SFNA_2015.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codeSFNA=str_extract(ResultadosEstudiantesTotal$SchoolID, "PS[0123456789]+")
ResultadosSchoolTotal$school_codeSFNA=str_extract(ResultadosSchoolTotal$SchoolID, "PS[0123456789]+")

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_SFNA_2015.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_SFNA_2015.dta")


load("ResultadosEstudiantesTotal_SFNA_2016.RData")
load("ResultadosSchoolTotal_SFNA_2016.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codeSFNA=str_extract(ResultadosEstudiantesTotal$SchoolID, "PS[0123456789]+")
ResultadosSchoolTotal$school_codeSFNA=str_extract(ResultadosSchoolTotal$SchoolID, "PS[0123456789]+")

ResultadosEstudiantesTotal=ResultadosEstudiantesTotal[which(ResultadosEstudiantesTotal$`CANDIDATE NAME`!=""),]
write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_SFNA_2016.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_SFNA_2016.dta")

load("ResultadosEstudiantesTotal_SFNA_2017.RData")
load("ResultadosSchoolTotal_SFNA_2017.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codeSFNA=str_extract(ResultadosEstudiantesTotal$SchoolID, "PS[0123456789]+")
ResultadosSchoolTotal$school_codeSFNA=str_extract(ResultadosSchoolTotal$SchoolID, "PS[0123456789]+")

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_SFNA_2017.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_SFNA_2017.dta")


load("ResultadosEstudiantesTotal_PSLE_2013.RData")
load("ResultadosSchoolTotal_PSLE_2013.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codePSLE=str_extract(ResultadosEstudiantesTotal$school_name, "P[0123456789]+")
ResultadosEstudiantesTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosEstudiantesTotal$school_name))
ResultadosSchoolTotal$school_codePSLE=str_extract(ResultadosSchoolTotal$school_name, "P[0123456789]+")
ResultadosSchoolTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosSchoolTotal$school_name))

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_PSLE_2013.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_PSLE_2013.dta")


load("ResultadosEstudiantesTotal_PSLE_2014.RData")
load("ResultadosSchoolTotal_PSLE_2014.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codePSLE=str_extract(ResultadosEstudiantesTotal$school_name, "P[0123456789]+")
ResultadosEstudiantesTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosEstudiantesTotal$school_name))
ResultadosSchoolTotal$school_codePSLE=str_extract(ResultadosSchoolTotal$school_name, "P[0123456789]+")
ResultadosSchoolTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosSchoolTotal$school_name))

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_PSLE_2014.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_PSLE_2014.dta")


load("ResultadosEstudiantesTotal_PSLE_2015.RData")
load("ResultadosSchoolTotal_PSLE_2015.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codePSLE=str_extract(ResultadosEstudiantesTotal$school_name, "P[0123456789]+")
ResultadosEstudiantesTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosEstudiantesTotal$school_name))
ResultadosSchoolTotal$school_codePSLE=str_extract(ResultadosSchoolTotal$school_name, "P[0123456789]+")
ResultadosSchoolTotal$school_name=trim(gsub(" - P[0123456789]+","", ResultadosSchoolTotal$school_name))

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_PSLE_2015.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_PSLE_2015.dta")



load("ResultadosEstudiantesTotal_PSLE_2016.RData")
load("ResultadosSchoolTotal_PSLE_2016.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codePSLE=str_extract(ResultadosEstudiantesTotal$school_name, "PS[0123456789]+")
ResultadosEstudiantesTotal$school_name=trim(gsub(" - PS[0123456789]+","", ResultadosEstudiantesTotal$school_name))
ResultadosSchoolTotal$school_codePSLE=str_extract(ResultadosSchoolTotal$school_name, "PS[0123456789]+")
ResultadosSchoolTotal$school_name=trim(gsub(" - PS[0123456789]+","", ResultadosSchoolTotal$school_name))

ResultadosEstudiantesTotal=ResultadosEstudiantesTotal[which(nchar(ResultadosEstudiantesTotal[,4])<=2),]

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_PSLE_2016.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_PSLE_2016.dta")


load("ResultadosEstudiantesTotal_PSLE_2017.RData")
load("ResultadosSchoolTotal_PSLE_2017.RData")
colnames(ResultadosEstudiantesTotal)[1]="CAND"
ResultadosEstudiantesTotal$school_codePSLE=str_extract(ResultadosEstudiantesTotal$school_name, "PS[0123456789]+")
ResultadosEstudiantesTotal$school_name=trim(gsub(" - PS[0123456789]+","", ResultadosEstudiantesTotal$school_name))
ResultadosSchoolTotal$school_codePSLE=str_extract(ResultadosSchoolTotal$school_name, "PS[0123456789]+")
ResultadosSchoolTotal$school_name=trim(gsub(" - PS[0123456789]+","", ResultadosSchoolTotal$school_name))

write.dta(ResultadosEstudiantesTotal,file="ResultadosEstudiantesTotal_PSLE_2017.dta")
write.dta(ResultadosSchoolTotal,file="ResultadosSchoolTotal_PSLE_2017.dta")


load("LibroSchool.RData")
write.dta(Base,file="LibroSchool.dta")


load("BasePublisher.RData")
BasePublisher$DeliveredDate=as.Date(BasePublisher$DeliveredDate,format="%d, %B ,%Y")
BasePublisher=subset(BasePublisher,select=-PlannedDate)
BasePublisher$BookName=gsub("English","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Haiba na Michezo","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Hisabati","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Historia","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Jiografia","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Kiswahili","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Sayansi","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Stadi za kazi","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Tehama","",BasePublisher$BookName)
BasePublisher$BookName=gsub("Uraia","",BasePublisher$BookName)
BasePublisher$BookName=trim(BasePublisher$BookName)
BasePublisher$BookName=as.numeric(BasePublisher$BookName)
BasePublisher$Planned=as.numeric(BasePublisher$Planned)
BasePublisher$Delivered=as.numeric(BasePublisher$Delivered)
BasePublisher$PropDelivered=BasePublisher$Delivered/BasePublisher$Planned
colnames(BasePublisher)[1]="Grade"
write.dta(BasePublisher,file="BasePublisher.dta")

