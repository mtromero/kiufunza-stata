rm(list=ls())
require(RCurl)
require(XML)
curl = getCurlHandle()
trim <- function (x) gsub("^\\s+|\\s+$", "", x)



NivelSuperios <- getURL("http://maktaba.tetea.org/exam-results/SFNA2016//index.htm", ssl.verifypeer = FALSE,curl=curl)
doc <- htmlParse(NivelSuperios)
links_region <- xpathSApply(doc, "//a/@href")
regions_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))


ResultadosEstudiantesTotal=NULL
ResultadosSchoolTotal=NULL
ResultadosEstudiantesTotal=data.frame(ResultadosEstudiantesTotal)
ResultadosSchoolTotal=data.frame(ResultadosSchoolTotal)

regions_name=regions_name[grep("reg",links_region)]
links_region=links_region[grep("reg",links_region)]

for(reg in 1:length(links_region)){

  try(NivelRegion <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  while((inherits(NivelRegion, "try-error"))){
    Sys.sleep(1)
    try(NivelRegion <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  }
  if(length(grep("ERROR 404",NivelRegion))!=0 | length(grep("Forbidden",NivelRegion))!=0) next
  doc <- htmlParse(NivelRegion)
  links_distrcits <- xpathSApply(doc, "//a/@href")
  districts_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))
  
  districts_name=districts_name[grep("distr",links_distrcits)]
  links_distrcits=links_distrcits[grep("distr",links_distrcits)]

  for(dist in 1:length(links_distrcits)){
    NivelDistrcit <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl)
    while((inherits(NivelRegion, "try-error"))){
      Sys.sleep(1)
      try(NivelDistrcit <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl))
    }
  
    if(length(grep("ERROR 404",NivelDistrcit))!=0 | length(grep("Forbidden",NivelDistrcit))!=0) next
    doc <- htmlParse(NivelDistrcit)
    links_schools <- xpathSApply(doc, "//a/@href")
    schools_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))
    
    schools_name=schools_name[grep("ps",links_schools)]
    links_schools=links_schools[grep("ps",links_schools)]
    
    for(school in 1:length(links_schools)){
      NivelSchool <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_schools[[school]]), ssl.verifypeer = FALSE,curl=curl)
      while((inherits(NivelRegion, "try-error"))){
	Sys.sleep(1)
	try(NivelSchool <- getURL(paste0("http://maktaba.tetea.org/exam-results/SFNA2016/",links_schools[[school]]), ssl.verifypeer = FALSE,curl=curl))
      }
      if(length(grep("ERROR 404",NivelSchool))!=0 | length(grep("Forbidden",NivelSchool))!=0 ) next
      doc <- htmlTreeParse(NivelSchool)
      ResultadosSchool=NULL
      ResultadosSchool=data.frame(SchoolID=c(NA))

		
      ResultadosEstudiantes=readHTMLTable(NivelSchool,header=T,which=1,stringsAsFactors=FALSE)
      if(dim(ResultadosEstudiantes)[2]==4){
	if(sum(grepl("Average",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'Average -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("average",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'average -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("AVERAGE",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'AVERAGE -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
      }
      if(dim(ResultadosEstudiantes)[2]==4) {warning("Fucked up... still average wrong");next}
      ResultadosEstudiantes$Absent=0
      ResultadosEstudiantes$Irregularities=0

      ResultadosEstudiantes$SchoolID=doc[[1]][[3]][[5]][[1]]$value
      ResultadosEstudiantes$school_name=gsub("\r\n","",schools_name[school])
      ResultadosEstudiantes$district_name=gsub("\r\n","",districts_name[dist])
      ResultadosEstudiantes$region_name=gsub("\r\n","",regions_name[reg])
      ResultadosEstudiantes$SUBJECTS=tolower(ResultadosEstudiantes$SUBJECTS)
      if(sum(grepl("tehama",ResultadosEstudiantes$SUBJECTS))>0 & sum(grepl("absent",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Absent[grepl("absent",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("absent",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa ya jamii - NA hisabati - NA sayansi - NA stadi za kazi, haiba na michezo - NA tehama - NA"
	
      }
      if(sum(grepl("tehama",ResultadosEstudiantes$SUBJECTS))==0 & sum(grepl("absent",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Absent[grepl("absent",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("absent",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa ya jamii - NA hisabati - NA sayansi - NA stadi za kazi, haiba na michezo - NA"
      }
      if(sum(grepl("tehama",ResultadosEstudiantes$SUBJECTS))>0 & sum(grepl("irregularities",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Irregularities[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa ya jamii - NA hisabati - NA sayansi - NA stadi za kazi, haiba na michezo - NA tehama - NA"
	
      }
      if(sum(grepl("tehama",ResultadosEstudiantes$SUBJECTS))==0 & sum(grepl("irregularities",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Irregularities[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa ya jamii - NA hisabati - NA sayansi - NA stadi za kazi, haiba na michezo - NA"
      }
      
      ResultadosEstudiantes$SUBJECTS=gsub("kiswahili","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("english","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("maarifa ya jamii","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("hisabati","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("sayansi","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("stadi za kazi, haiba na michezo","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("tehama","",ResultadosEstudiantes$SUBJECTS)
      MatrizSubjects=toupper(t(sapply(strsplit(ResultadosEstudiantes$SUBJECTS,'-'),trim))[,-1])
      if(length(MatrizSubjects)==7) ResultadosEstudiantes[,c("kiswahili","english","jamii","hisabati","sayansi","stadi","tehama")]=MatrizSubjects
      else if(length(MatrizSubjects)==6) ResultadosEstudiantes[,c("kiswahili","english","jamii","hisabati","sayansi","stadi","tehama")]=c(MatrizSubjects,NA)
      else if(dim(MatrizSubjects)[2]==6)     ResultadosEstudiantes[,c("kiswahili","english","jamii","hisabati","sayansi","stadi","tehama")]=cbind(MatrizSubjects,NA)
      else if(dim(MatrizSubjects)[2]==7)     ResultadosEstudiantes[,c("kiswahili","english","jamii","hisabati","sayansi","stadi","tehama")]=MatrizSubjects
      else{stop("Something is wrong!")}
      ResultadosEstudiantes=subset(ResultadosEstudiantes,select=-SUBJECTS)
      ResultadosEstudiantesTotal=rbind(ResultadosEstudiantesTotal,ResultadosEstudiantes)
    
      
      ResultadosSchool$SchoolID=doc[[1]][[3]][[5]][[1]]$value
      ResultadosSchool$school_name=gsub("\r\n","",schools_name[school])
      ResultadosSchool$district_name=gsub("\r\n","",districts_name[dist])
      ResultadosSchool$region_name=gsub("\r\n","",regions_name[reg])
      ResultadosSchool$Registered=as.numeric(trim(gsub(":","",gsub("WALIOSAJILIWA","",doc[[1]][[3]][[6]][[1]]$value))))
      ResultadosSchool$Students=as.numeric(trim(gsub(":","",gsub("WALIOFANYA MTIHANI","",doc[[1]][[3]][[6]][[3]]$value))))
      ResultadosSchool$Average=as.numeric(trim(gsub(":","",gsub("WASTANI WA SHULE","",doc[[1]][[3]][[6]][[5]]$value))))
      ResultadosSchool$GroupSchools=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[3]][[6]][[7]]$value))))
      
      ResultadosSchool$RankingDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KATIKA HALMASHAURI/MANISPAA","",doc[[1]][[3]][[6]][[9]]$value))), "kati ya")[[1]][[1]])
      ResultadosSchool$TotalDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KATIKA HALMASHAURI/MANISPAA","",doc[[1]][[3]][[6]][[9]]$value))), "kati ya")[[1]][[2]])
      ResultadosSchool$RankingRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[3]][[6]][[11]]$value))), "kati ya")[[1]][[1]])
      ResultadosSchool$TotalRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[3]][[6]][[11]]$value))), "kati ya")[[1]][[2]])
      ResultadosSchool$RankingNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[3]][[6]][[13]]$value))), "kati ya")[[1]][[1]])
      ResultadosSchool$TotalNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[3]][[6]][[13]]$value))), "kati ya")[[1]][[2]])
      ResultadosSchoolTotal=rbind(ResultadosSchoolTotal,ResultadosSchool)
      print(paste0("School ",school," in District ",dist," in Region ",reg))
    }
  }
}
setwd("C:/Users/Mauricio/Dropbox/")
save(ResultadosSchoolTotal,file="Research/TZ_Radar/RawData/ResultadosSchoolTotal_SFNA_2016.Rdata")
save(ResultadosEstudiantesTotal,file="Research/TZ_Radar/RawData/ResultadosEstudiantesTotal_SFNA_2016.RData")






##############################################
##############################################
##############################################
##############################################

   