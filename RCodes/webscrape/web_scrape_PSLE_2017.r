rm(list=ls())
require(RCurl)
require(XML)
library(httr)
#require("devtools")
#devtools::install_github("hadley/httr")

curl = getCurlHandle()
trim <- function (x) gsub("^\\s+|\\s+$", "", x)

NivelSuperios <- getURL("http://www.necta.go.tz/results/2017/psle/psle.htm", ssl.verifypeer = FALSE,curl=curl)
doc <- htmlParse(NivelSuperios)
links_region <- xpathSApply(doc, "//a/@href")
regions_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))


ResultadosEstudiantesTotal=NULL
ResultadosSchoolTotal=NULL
ResultadosEstudiantesTotal=data.frame(ResultadosEstudiantesTotal)
ResultadosSchoolTotal=data.frame(ResultadosSchoolTotal)
regions_name=regions_name[grep("reg",links_region)]
links_region=links_region[grep("reg",links_region)]
links_region=gsub("\\\\","/",links_region)

for(reg in 13:length(links_region)){

  try(NivelRegion <- getURL(paste0("http://www.necta.go.tz/results/2017/psle/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  while((inherits(NivelRegion, "try-error"))){
    Sys.sleep(1)
    try(NivelRegion <- getURL(paste0("http://www.necta.go.tz/results/2017/psle/",links_region[[reg]]), ssl.verifypeer = FALSE,curl=curl))
  }
  if(length(grep("ERROR 404",NivelRegion))!=0 | length(grep("Forbidden",NivelRegion))!=0) next
  doc <- htmlParse(NivelRegion)
  links_distrcits <- xpathSApply(doc, "//a/@href")
  districts_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))
  districts_name=districts_name[grep("distr",links_distrcits)]
  links_distrcits=links_distrcits[grep("distr",links_distrcits)]
  
  
  for(dist in 1:length(links_distrcits)){
    NivelDistrcit <- getURL(paste0("http://www.necta.go.tz/results/2017/psle/results/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl)
    while((inherits(NivelRegion, "try-error"))){
      Sys.sleep(1)
      try(NivelDistrcit <- getURL(paste0("http://www.necta.go.tz/results/2017/psle/results/",links_distrcits[[dist]]), ssl.verifypeer = FALSE,curl=curl))
    }
  
    if(length(grep("ERROR 404",NivelDistrcit))!=0 | length(grep("Forbidden",NivelDistrcit))!=0) next
    doc <- htmlParse(NivelDistrcit)
    links_schools <- xpathSApply(doc, "//a/@href")
    schools_name=sapply(FUN=getChildrenStrings,xpathSApply(doc, "//a"))
    schools_name=schools_name[grep("shl",links_schools)]
    links_schools=links_schools[grep("shl",links_schools)]
    
    
    for(school in 1:length(links_schools)){
      Sys.sleep(5*runif(1))
      #Mauro of the future: Note this is a really weird fix I had to apply since the webpage had NULL things... fix was found at https://www.quora.com/R-Question-How-to-fix-this-RCurl-error
      htmlRaw <- getURLContent(paste0("http://www.necta.go.tz/results/2017/psle/results/",links_schools[[school]]), ssl.verifypeer = FALSE,curl= getCurlHandle(),binary=T)
      htmlAsc <- htmlRaw
      # find where the NULLs are
      htmlNul <- htmlRaw == as.raw(0)
      # modify the new vector NULLs to SPACEs
      htmlAsc[htmlNul] <- as.raw(20)
      # you can now convert these to Char
      NivelSchool <- rawToChar(htmlAsc)
      while((inherits(NivelRegion, "try-error"))){
	Sys.sleep(1)
	try(NivelSchool <- getURL(paste0("http://www.necta.go.tz/results/2017/psle/results/",links_schools[[school]]), ssl.verifypeer = FALSE,curl=curl))
      }
      if(length(grep("ERROR 404",NivelSchool))!=0 | length(grep("Forbidden",NivelSchool))!=0 ) next
      doc <- htmlTreeParse(NivelSchool)
      ResultadosSchool=NULL
      ResultadosSchool=data.frame(SchoolID=c(NA))

		
      ResultadosEstudiantes=readHTMLTable(NivelSchool,header=T,which=1,stringsAsFactors=FALSE)
      ResultadosEstudiantes[,4]=gsub(",","",ResultadosEstudiantes[,4])
      ResultadosEstudiantes=ResultadosEstudiantes[which(ResultadosEstudiantes[,4]!=""),] #removing empty strings
      if(dim(ResultadosEstudiantes)[2]==4){
	if(sum(grepl("Average Grade",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'Average Grade -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("average grade",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'average grade -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
	else if(sum(grepl("AVERAGE GRADE",ResultadosEstudiantes[,4]))>0){
	  Correcto=do.call(rbind,sapply(strsplit(ResultadosEstudiantes[,4],'AVERAGE GRADE -'),trim,simplify=F))
	  ResultadosEstudiantes[,4]=Correcto[,1]
	  ResultadosEstudiantes[,5]=Correcto[,2]
	  colnames(ResultadosEstudiantes)=c("CAND. NO","SEX","CANDIDATE NAME","SUBJECTS","AVERAGE GRADE")
	}
      }
      if(dim(ResultadosEstudiantes)[2]==4) {warning(paste0("Fucked up... still average wrong")); next}
      ResultadosEstudiantes$Absent=0
      ResultadosEstudiantes$Irregularities=0
      ResultadosEstudiantes$W=0
      ResultadosEstudiantes$S=0
      
      if(length(doc[[1]][[2]])>1) tipo=1
      if(length(doc[[1]][[2]])==1) tipo=2
      #getNodeSet(doc, "//h3//p")
      if(tipo==1) ResultadosEstudiantes$SchoolID=doc[[1]][[2]][[2]][[5]][[1]]$value
      if(tipo==2) ResultadosEstudiantes$SchoolID=doc[[1]][[2]][[1]][[5]][[1]]$value
      
      ResultadosEstudiantes$school_name=gsub("\r","",gsub("\n","",schools_name[school]))
      ResultadosEstudiantes$district_name=gsub("\r","",gsub("\n","",districts_name[dist]))
      ResultadosEstudiantes$region_name=gsub("\r","",gsub("\n","",regions_name[reg]))
      ResultadosEstudiantes$SUBJECTS=tolower(ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes=ResultadosEstudiantes[which(!(is.na(ResultadosEstudiantes$`CANDIDATE NAME`) & ResultadosEstudiantes$SEX=="")),]
      if( sum(grepl("absent",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Absent[grepl("absent",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("absent",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
	
      }
      if(sum(grepl("irregularities",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$Irregularities[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("irregularities",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      if(sum(grepl("\\*w",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$W[grepl("\\*w",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("\\*w",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      if(sum(grepl("\\*s",ResultadosEstudiantes$SUBJECTS))>0){
	ResultadosEstudiantes$S[grepl("\\*s",ResultadosEstudiantes$SUBJECTS)]=1
	ResultadosEstudiantes$SUBJECTS[grepl("\\*s",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      if(sum(!grepl("kiswahili",ResultadosEstudiantes$SUBJECTS))>0){
        ResultadosEstudiantes$SUBJECTS[!grepl("kiswahili",ResultadosEstudiantes$SUBJECTS)]="kiswahili - NA english - NA maarifa - NA hisabati - NA science - NA"
      }
      
      ResultadosEstudiantes$SUBJECTS=gsub("kiswahili","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("english","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("maarifa","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("hisabati","",ResultadosEstudiantes$SUBJECTS)
      ResultadosEstudiantes$SUBJECTS=gsub("science","",ResultadosEstudiantes$SUBJECTS)
      MatrizSubjects=toupper(t(sapply(strsplit(ResultadosEstudiantes$SUBJECTS,'-'),trim))[,-1])
      if(length(MatrizSubjects)==5) ResultadosEstudiantes[,c("kiswahili","english","maarifa","hisabati","sayansi")]=cbind(MatrizSubjects)
      else if(dim(MatrizSubjects)[2]==5)     ResultadosEstudiantes[,c("kiswahili","english","maarifa","hisabati","sayansi")]=cbind(MatrizSubjects)
      else {  stop("Something is wrong!")}
      ResultadosEstudiantes=subset(ResultadosEstudiantes,select=-SUBJECTS)
      ResultadosEstudiantesTotal=rbind(ResultadosEstudiantesTotal,ResultadosEstudiantes)
    

      
      if(tipo==1){
        	ResultadosSchool$SchoolID=doc[[1]][[2]][[2]][[5]][[1]]$value
        	ResultadosSchool$Registered=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[2]][[6]][[1]]$value))))
        	ResultadosSchool$Students=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[2]][[6]][[3]]$value))))
        	ResultadosSchool$Average=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[2]][[6]][[5]]$value))))
        	ResultadosSchool$GroupSchools=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[2]][[6]][[7]]$value))))
        	ResultadosSchool$RankingDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIWILAYA","",doc[[1]][[2]][[2]][[6]][[9]]$value))), "kati ya")[[1]][[1]])
        	ResultadosSchool$TotalDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIWILAYA","",doc[[1]][[2]][[2]][[6]][[9]]$value))), "kati ya")[[1]][[2]])
        	ResultadosSchool$RankingRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[2]][[2]][[6]][[11]]$value))), "kati ya")[[1]][[1]])
        	ResultadosSchool$TotalRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[2]][[2]][[6]][[11]]$value))), "kati ya")[[1]][[2]])
        	ResultadosSchool$RankingNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[2]][[2]][[6]][[13]]$value))), "kati ya")[[1]][[1]])
        	ResultadosSchool$TotalNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[2]][[2]][[6]][[13]]$value))), "kati ya")[[1]][[2]])
      }
      
      if(tipo==2){
        ResultadosSchool$SchoolID=doc[[1]][[2]][[1]][[5]][[1]]$value
        ResultadosSchool$Registered=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[1]][[6]][[1]]$value))))
        ResultadosSchool$Students=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[1]][[6]][[3]]$value))))
        ResultadosSchool$Average=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[1]][[6]][[5]]$value))))
        ResultadosSchool$GroupSchools=as.numeric(trim(gsub(":","",gsub("[[:alpha:]]*[[:space:]]*[[:punct:]]*","",doc[[1]][[2]][[1]][[6]][[7]]$value))))
        ResultadosSchool$RankingDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIWILAYA","",doc[[1]][[2]][[1]][[6]][[9]]$value))), "kati ya")[[1]][[1]])
        ResultadosSchool$TotalDistrict=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIWILAYA","",doc[[1]][[2]][[1]][[6]][[9]]$value))), "kati ya")[[1]][[2]])
        ResultadosSchool$RankingRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[2]][[1]][[6]][[11]]$value))), "kati ya")[[1]][[1]])
        ResultadosSchool$TotalRegion=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KIMKOA","",doc[[1]][[2]][[1]][[6]][[11]]$value))), "kati ya")[[1]][[2]])
        ResultadosSchool$RankingNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[2]][[1]][[6]][[13]]$value))), "kati ya")[[1]][[1]])
        ResultadosSchool$TotalNation=as.numeric(strsplit(trim(gsub(":","",gsub("NAFASI YA SHULE KWENYE KUNDI LAKE KITAIFA","",doc[[1]][[2]][[1]][[6]][[13]]$value))), "kati ya")[[1]][[2]])
        
      }
      ResultadosSchool$school_name=gsub("\r","",gsub("\n","",schools_name[school]))
      ResultadosSchool$district_name=gsub("\r","",gsub("\n","",districts_name[dist]))
      ResultadosSchool$region_name=gsub("\r","",gsub("\n","",regions_name[reg]))
      ResultadosSchoolTotal=rbind(ResultadosSchoolTotal,ResultadosSchool)
      print(paste0("School ",school," in District ",dist," in Region ",reg))
    }
  }
}

setwd("C:/Users/Mauricio/Dropbox/")
ResultadosSchoolTotal=ResultadosSchoolTotal[!duplicated(ResultadosSchoolTotal),]
ResultadosEstudiantesTotal=ResultadosEstudiantesTotal[!duplicated(ResultadosEstudiantesTotal),]

save(ResultadosSchoolTotal,file="Research/TZ_Radar/RawData/ResultadosSchoolTotal_PSLE_2017.Rdata")
save(ResultadosEstudiantesTotal,file="Research/TZ_Radar/RawData/ResultadosEstudiantesTotal_PSLE_2017.RData")
   