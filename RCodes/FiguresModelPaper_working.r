setwd("/media/mauricio/TeraHDD1/git/kiufunza-stata")

a=-1
c=-1
b=170
d=170
f=10
print(paste("e must be below -",sqrt(4*a*c)," or be greater than ",sqrt(4*a*c)))
e=-3
 
print("Check conditions")
2*a*10+b+e*0>0
2*a*0+b+e*10>0
2*c*10+d+e*0>0
2*c*0+d+e*10>0
2*a<0
2*b<0
4*a*c-e^2<0
t=0.5
lambda=0.5


funProduccion=function(eff,Inp) a*eff^2+b*eff+c*Inp^2+d*Inp+e*eff*Inp+f
FunDerivada_e=function(eff,Inp) 2*a*eff+b+e*Inp
FunDerivada_I=function(eff,Inp) 2*c*Inp+d+e*eff
CostFun=function(eff,A) A*eff^2
DerivadaCosto=function(eff,A) 2*A*eff

FunBeneficioMarginal=function(eff,Inp) (t+lambda)*FunDerivada_e(eff,Inp)

eVec=seq(0,100,0.01)
e=-3
print("Check conditions")
2*a*10+b+e*0>0
2*a*0+b+e*10>0
2*c*10+d+e*0>0
2*c*0+d+e*10>0
2*a<0
2*b<0
4*a*c-e^2<0
lambda=0.5
pdf("LaTeX/fig_sub_increaset.pdf")
t=0.5
plot(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),type="l",col=1,lty=1,xlab="Effort",ylab="Marginal benefit/Marginal cost",main="Substitutes",ylim=c(0,150),xlim=c(0,70),xaxt="n",yaxt="n")
x_1=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_1=2*1*x_1
t=1
lines(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),col=2,lty=1)
lines(eVec,sapply(eVec,DerivadaCosto,A=1),type="l",col=1,lty=1)
x_2=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_2=2*1*x_2
segments(x_1, -10, x1 = x_1, y1 = y_1,col = par("fg"), lty = 2, lwd = par("lwd"))
segments(x_2, -10, x1 = x_2, y1 = y_2,col = par("fg"), lty = 2, lwd = par("lwd"))
text(10,125,expression((lambda+t[0])*f[e](e,I[0])))
text(45,140,expression((lambda+t[1])*f[e](e,I[0])),col=2)
text(7,2,expression(c[e](e)))
axis(1, at = c(x_1,x_2), labels =c(expression(e[1]^"*"),expression(e[2]^"*")))
dev.off()



e=3
print("Check conditions")
2*a*10+b+e*0>0
2*a*0+b+e*10>0
2*c*10+d+e*0>0
2*c*0+d+e*10>0
2*a<0
2*b<0
4*a*c-e^2<0
lambda=0.5
pdf("LaTeX/fig_com_increaset.pdf")
t=0.5
plot(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),type="l",col=1,lty=1,xlab="Effort",ylab="Marginal benefit/Marginal cost",main="Complements",ylim=c(0,150),xlim=c(0,70),xaxt="n",yaxt="n")

x_1=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_1=2*1*x_1
t=1
lines(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),col=2,lty=1)
lines(eVec,sapply(eVec,DerivadaCosto,A=1),type="l",col=1,lty=1)
x_2=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_2=2*1*x_2
segments(x_1, -10, x1 = x_1, y1 = y_1,col = par("fg"), lty = 2, lwd = par("lwd"))
segments(x_2, -10, x1 = x_2, y1 = y_2,col = par("fg"), lty = 2, lwd = par("lwd"))
text(18,120,expression((lambda+t[0])*f[e](e,I[0])))
text(50,135,expression((lambda+t[1])*f[e](e,I[0])),col=2)
text(7,2,expression(c[e](e)))
axis(1, at = c(x_1,x_2), labels =c(expression(e[1]^"*"),expression(e[2]^"*")))
dev.off()



####################################
eVec=seq(0,100,0.01)
e=-3
print("Check conditions")
2*a*10+b+e*0>0
2*a*0+b+e*10>0
2*c*10+d+e*0>0
2*c*0+d+e*10>0
2*a<0
2*b<0
4*a*c-e^2<0
t=0.5
lambda=0.5
pdf("LaTeX/fig_sub.pdf")
plot(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),type="l",col=1,lty=1,xlab="Effort",ylab="Marginal benefit/Marginal cost",main="Substitutes",ylim=c(0,150),xaxt="n",yaxt="n")
lines(eVec,sapply(eVec,FunBeneficioMarginal,Inp=20),col=2,lty=1)
lines(eVec,sapply(eVec,DerivadaCosto,A=1),type="l",col=1,lty=1)
x_1=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_1=2*1*x_1
x_2=(b+e*20)*(t+lambda)/(2*1-2*a*(t+lambda))
y_2=2*1*x_2
segments(x_1, -10, x1 = x_1, y1 = y_1,col = par("fg"), lty = 2, lwd = par("lwd"))
segments(x_2, -10, x1 = x_2, y1 = y_2,col = par("fg"), lty = 2, lwd = par("lwd"))
text(85,20,expression((lambda+t[0])*f[e](e,I[0])))
text(65,5,expression((lambda+t[0])*f[e](e,I[1])),col=2)
text(7,2,expression(c[e](e)))
axis(1, at = c(x_1,x_2), labels =c(expression(e[1]^"*"),expression(e[2]^"*")))
dev.off()




e=3
print("Check conditions")
2*a*10+b+e*0>0
2*a*0+b+e*10>0
2*c*10+d+e*0>0
2*c*0+d+e*10>0
2*a<0
2*b<0
4*a*c-e^2<0
t=0.5
lambda=0.5
pdf("LaTeX/fig_com.pdf")
plot(eVec,sapply(eVec,FunBeneficioMarginal,Inp=1),type="l",col=1,lty=1,xlab="Effort",ylab="Marginal benefit/Marginal cost",main="Complements",ylim=c(0,150),xaxt="n",yaxt="n")
lines(eVec,sapply(eVec,FunBeneficioMarginal,Inp=20),col=2,lty=1)
lines(eVec,sapply(eVec,DerivadaCosto,A=1),type="l",col=1,lty=1)
x_1=(b+e*1)*(t+lambda)/(2*1-2*a*(t+lambda))
y_1=2*1*x_1
x_2=(b+e*20)*(t+lambda)/(2*1-2*a*(t+lambda))
y_2=2*1*x_2
segments(x_1, -10, x1 = x_1, y1 = y_1,col = par("fg"), lty = 2, lwd = par("lwd"))
segments(x_2, -10, x1 = x_2, y1 = y_2,col = par("fg"), lty = 2, lwd = par("lwd"))
text(72,2,expression((lambda+t[0])*f[e](e,I[0])))
text(79,50,expression((lambda+t[0])*f[e](e,I[1])),col=2)
text(7,2,expression(c[e](e)))
axis(1, at = c(x_1,x_2), labels =c(expression(e[1]^"*"),expression(e[2]^"*")))
dev.off()
