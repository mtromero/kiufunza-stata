setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza")
library(foreign)
opt.cut = function(perf, pred){
    cut.ind = mapply(FUN=function(x, y, p){
        d = (x - 0)^2 + (y-1)^2
        ind = which(d == min(d))
        c(sensitivity = y[[ind]], specificity = 1-x[[ind]], 
            cutoff = p[[ind]])
    }, perf@x.values, perf@y.values, pred@cutoffs)
}

Student=read.dta("CreatedData/Consolidated/Student.dta")
Student=Student[Student$upid!="",]

Student[[paste0("passhisabati_T7")]]=Student[[paste0("passmath_T7")]]
Student[[paste0("passkiswahili_T7")]]=Student[[paste0("passkis_T7")]]
Student[[paste0("passkiingereza_T7")]]=Student[[paste0("passeng_T7")]] 

for(subject in  c("hisabati","kiswahili","kiingereza")){
Student[[paste0("Lag",subject)]]=rowSums(cbind(Student[[paste0("Z_",subject,"_T1")]],Student[[paste0("Z_",subject,"_T5")]]),na.rm=T)* ifelse(rowSums(is.na(cbind(Student[[paste0("Z_",subject,"_T1")]],Student[[paste0("Z_",subject,"_T5")]]))) == ncol(cbind(Student[[paste0("Z_",subject,"_T1")]],Student[[paste0("Z_",subject,"_T5")]])), NA, 1) 
}
Student$DistID=factor(Student$DistID)
Student$Laghisabati2=Student$Laghisabati^2
Student$Laghisabati3=Student$Laghisabati^3

Student$Lagkiswahili2=Student$Lagkiswahili^2
Student$Lagkiswahili3=Student$Lagkiswahili^3


Student$Lagkiingereza2=Student$Lagkiingereza^2
Student$Lagkiingereza3=Student$Lagkiingereza^3


for(subject in  c("hisabati","kiswahili","kiingereza")){

reg1=lm(formula(paste0("Z_",subject,"_T7~Laghisabati+Lagkiswahili+Lagkiingereza+stdage_T7+factor(stdsex_T7)+factor(GradeID_T7)+DistID")),data=Student,subset=Student$treatment=="Control")  
normal_increase=predict(reg1,newdata=Student,subset=!is.na(Student$GradeID_T7))
normal_increase[which(normal_increase<quantile(normal_increase,probs=0.001,na.rm=T))]=quantile(normal_increase,probs=0.001,na.rm=T)
normal_increase[which(normal_increase>quantile(normal_increase,probs=0.999,na.rm=T))]=quantile(normal_increase,probs=0.999,na.rm=T)

reg2=loess(formula(paste0("pass",subject,"_T7~Z_",subject,"_T7")),data=Student,control =loess.control(surface = "direct"),degree=0,span=0.1)

pred <- prediction(Student[,paste0("Z_",subject,"_T7")], Student[,paste0("pass",subject,"_T7")])
perf <- performance(pred, measure = "tpr", x.measure = "fpr") 
plot(perf, col=rainbow(10))
EP01=pmax(predict(reg2,normal_increase+0.1)-predict(reg2,normal_increase),0)
EP02=pmax(predict(reg2,normal_increase+0.2)-predict(reg2,normal_increase),0)
DistancePassCut=normal_increase-opt.cut(perf, pred)[3,]
 
Student[[paste0("ExpPay01",subject,"_T7")]]=EP01
Student[[paste0("ExpPay02",subject,"_T7")]]=EP02
Student[[paste0("DistancePassCut",subject,"_T7")]]=DistancePassCut



reg1=lm(formula(paste0("Z_",subject,"_T3~Laghisabati+Lagkiswahili+Lagkiingereza++male_T1+Age_T1+factor(GradeID_T3)+DistID")),data=Student,subset=Student$treatment=="Control")
normal_increase=predict(reg1,newdata=Student,subset=!is.na(Student$GradeID_T3))
normal_increase[which(normal_increase<quantile(normal_increase,probs=0.001,na.rm=T))]=quantile(normal_increase,probs=0.001,na.rm=T)
normal_increase[which(normal_increase>quantile(normal_increase,probs=0.999,na.rm=T))]=quantile(normal_increase,probs=0.999,na.rm=T)

reg2=loess(formula(paste0("pass",subject,"_T3~Z_",subject,"_T3")),data=Student,control =loess.control(surface = "direct"),degree=0,span=0.4)

pred <- prediction(Student[,paste0("Z_",subject,"_T3")], Student[,paste0("pass",subject,"_T3")])
perf <- performance(pred, measure = "tpr", x.measure = "fpr") 
perf <- performance(pred, measure = "tpr", x.measure = "fpr") 
plot(perf, col=rainbow(10))

EP01=pmax(predict(reg2,normal_increase+0.1)-predict(reg2,normal_increase),0)
EP02=pmax(predict(reg2,normal_increase+0.2)-predict(reg2,normal_increase),0)
DistancePassCut=normal_increase-opt.cut(perf, pred)[3,]

Student[[paste0("ExpPay01",subject,"_T3")]]=EP01
Student[[paste0("ExpPay02",subject,"_T3")]]=EP02
Student[[paste0("DistancePassCut",subject,"_T3")]]=DistancePassCut

}


Student=Student[,c("upid",colnames(Student)[c(grep("ExpPay",colnames(Student)), grep("DistancePassCut",colnames(Student)))])]



write.dta(Student,"CreatedData/Consolidated/StudentExpectedPay.dta")


