rm(list=ls())
library(foreign)
library(VGAM)
library(MASS)
library(lfe)
library(fBasics)
library(expm)
library(doParallel)
library(foreach)
REPETICIONES=1000
#source("/media/mauricio/TeraHDD1/git/kiufunza-stata/RCodes/McCloskeyFunction.r")
#dir="C:/Users/Mauricio/Documents/git/"
dir="/media/mauricio/TeraHDD2/git/"

source(paste0(dir,"kiufunza-stata/RCodes/McCloskeyFunction.r"))
source(paste0(dir,"kiufunza-stata/RCodes/McCloskeyFunction_Conservative.r"))

s#etwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/") 
setwd("/media/mauricio/TeraHDD2/Box Sync/01_KiuFunza/")


for(time in c("T3","T7")){
  for(varOut in c("Z_hisabati","Z_kiswahili","Z_kiingereza","Z_ScoreFocal")){
  
    Data=read.dta("CreatedData/Consolidated/RegFinales_McClosky.dta")
    Data$SchoolID=as.factor(Data$SchoolID)



    
    varInterest="TreatCOD_Total_resid"
    
    
   
    varIncluded=c()


    varControl=c("TreatCGCOD_Total_resid")


    varCluster="SchoolID"



    Results=BF_McCloskey(data=Data,varInterest=varInterest,varIncluded=varIncluded,varControl=varControl,varOutput=paste0(varOut,"_",time,"_resid"),varCluster=varCluster,alpha=0.05,epsilon=0.001,Reps=REPETICIONES)
    save(Results,file=paste0("McCloskey/Results/",varOut,"_",time,"_COD_05.RData"))
    
    Results=BF_McCloskey_Conserv(data=Data,varInterest=varInterest,varIncluded=varIncluded,varControl=varControl,varOutput=paste0(varOut,"_",time,"_resid"),varCluster=varCluster,alpha=0.05,Reps=REPETICIONES)
    save(Results,file=paste0("McCloskey/Results/",varOut,"_",time,"_COD_conserv_05.RData"))
    
  }
}


for(time in c("T3","T7")){
  for(varOut in c("Z_hisabati","Z_kiswahili","Z_kiingereza","Z_ScoreFocal")){
    Data=read.dta("CreatedData/Consolidated/RegFinales_McClosky.dta")
    Data$SchoolID=as.factor(Data$SchoolID)


    
    varInterest="TreatCG_Total_resid"

    varIncluded=c()


    varControl=c("TreatCGCOD_Total_resid")


    varCluster="SchoolID"                 


    Results=BF_McCloskey(data=Data,varInterest=varInterest,varIncluded=varIncluded,varControl=varControl,varOutput=paste0(varOut,"_",time,"_resid"),varCluster=varCluster,alpha=0.05,epsilon=0.001,Reps=REPETICIONES)
    save(Results,file=paste0("McCloskey/Results/",varOut,"_",time,"_CG_05.RData"))
    
    Results=BF_McCloskey_Conserv(data=Data,varInterest=varInterest,varIncluded=varIncluded,varControl=varControl,varOutput=paste0(varOut,"_",time,"_resid"),varCluster=varCluster,alpha=0.05,Reps=REPETICIONES)
    save(Results,file=paste0("McCloskey/Results/",varOut,"_",time,"_CG_conserv_05.RData"))

  }
}
