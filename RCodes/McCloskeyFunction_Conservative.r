
BF_McCloskey_Conserv=function(data=NULL,varInterest=NULL,varIncluded=NULL,varControl=NULL,varOutput=NULL,varCluster=NULL,alpha=0.05,Reps=10000,seed=12345){

##This functions implements the algorithim "Algorithm Bonf-Adj Post-Sel" in McCloskey (June 2015 Version) "Bonferroni-Based Size-Correction for Nonstandard Testing Problems".
##In short, its calculates an adjusted critical value for the variable "varInterest", after taking into account controls that are always included (varIncluded), and model selection over "varControl"
##data= is a matrix that has the variable of intereste, the variables to be included in the model, the control variables, and the output variables
##varInterest= is the variable on which we want to calculcate the adjusted critical values
##varIncluded= is a set of variables that are always included in the model (e.g., a constant)
##varControl= is the set of variables over which model selection will be done
##varOutput= is the LHS variable
##varCluster== for adjusting the original standard errors. Robust standard errors are used by default anyway
##alpha= is the type-I error rate
##beta= is XXXXX
##epsilon= Corresponds to the value epsilon in page 37. Seldomy adjusted
##Reps=Number of Reps for the simulation. Seldomy adjusted
##seed= Its a seed for the random number generation

require(VGAM)
require(MASS)
require(lfe)
require(fBasics)
require(expm)
require(doParallel)
require(foreach)
require(mvtnorm)

if(is.null(data)) stop("You NEED to input a data set")
if(is.null(varInterest)) stop("You NEED to input a variable of interest")
if(is.null(varControl)) stop("You NEED to input variables over which the model selection will be done")
if(is.null(varOutput)) stop("You NEED to input a LHS variable")
if(alpha>1 | alpha<0) stop("alpha needs to be between 0 and 1")
if(Reps<0) stop("Reps needs to be positive")
if(sum(!(varInterest %in% colnames(data) ))>0) stop("The variable of interested is no in the data")
if(sum(!(varIncluded %in% colnames(data) ))>0) stop("Some of the variables to be included are not in the data")
if(sum(!(varControl %in% colnames(data) ))>0) stop("Some of the variables over which selection is to be done are not in the data")
if(sum(!(varOutput %in% colnames(data) ))>0) stop("The variable on the LHS is no in the data")
if(length(varControl)>1) stop("This function only takes one possible variable for model selection")

##First lets keep only what we need/works.. so the columns that we want, plus rows with all non-missing values
if(!is.null(varCluster)) data=data[,c(varInterest,varIncluded,varControl,varOutput,varCluster)]
if(is.null(varCluster)) data=data[,c(varInterest,varIncluded,varControl,varOutput)]
data=data[complete.cases(data),]
dataCentered=data

##Now we are going to project the data on varInterest,varControl and varOutput on the variables in varIncluded
if(!is.null(varIncluded)){
for(var in c(varInterest,varControl,varOutput)){
  reg=lm(formula(paste0(var,"~-1+",paste(varIncluded,collapse="+",sep=""))),data=dataCentered,model=F)
  dataCentered[,var]=reg$resid
}
}


##We dont need the varIncluded anymore, so lets get rid of it
if(!is.null(varCluster))  dataCentered=dataCentered[,c(varInterest,varControl,varOutput,varCluster)]
if(is.null(varCluster))  dataCentered=dataCentered[,c(varInterest,varControl,varOutput)]

##Number of observations
n=dim(dataCentered)[1]

NumExtra=length(varControl) ##This is "v" in the paper. number of additional vars
p=2^NumExtra ##number of possible subsets

##Need to calculcate what happens if we use all the models in varControl+varInterest
OriginalRegUseful=lm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1")),data=dataCentered)
if(is.null(varCluster)) OriginalReg=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1")),data=dataCentered)
if(!is.null(varCluster)) OriginalReg=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1 | 0 | 0 | ",varCluster)),data=dataCentered)



#####################
#####################
##the t-values of the full model
t_values=coef(summary(OriginalReg,robust=TRUE))[-1,3]
c_n=qt(1-alpha/2,df=n-length(t_values))


original_coefs=coef(summary(OriginalReg,robust=TRUE))[-1,1]
##if we just do some model selection


##the estimated model that we get from doing this
if(is.null(varCluster)) Reg2=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl[which(abs(t_values)>c_n)],collapse="+",sep=""),"-1")),data=dataCentered)
if(!is.null(varCluster)) Reg2=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl[which(abs(t_values)>c_n)],collapse="+",sep=""),"-1 | 0 | 0 |",varCluster)),data=dataCentered)
theta_mn=coefficients(summary(Reg2,robust=TRUE))[1,1]
Tn0=abs(theta_mn*sqrt(n))
CVSimple=abs(coefficients(summary(Reg2,robust=TRUE))[1,2]*sqrt(n)*qnorm(1-alpha/2))

TnFull=abs(coefficients(summary(OriginalRegUseful,robust=TRUE))[1,1]*sqrt(n))
CVFull=abs(coefficients(summary(OriginalReg,robust=TRUE))[1,2]*sqrt(n)*qnorm(1-alpha/2))
#####################
#####################


##Now onto the real algorithim!!!

############################
###########  STEP 1 ########
############################
##H
H=model.matrix(OriginalRegUseful)
##The variance-covariance matrix
if(is.null(varCluster))  OMEGA_N=OriginalReg[["robustvcv"]]*n
if(!is.null(varCluster)) OMEGA_N=OriginalReg[["clustervcv"]]*n

rho= OMEGA_N[1,2]/sqrt(OMEGA_N[1,1]*OMEGA_N[2,2])   ##h_22
sigma_X= sqrt(OMEGA_N[1,1])
h_n1= sqrt(n)*original_coefs/sqrt(OMEGA_N[2,2]) 





###########  STEP 3 D ii  ####
##Grid_2
Grid_2=seq(from=0.005 , by=0.005, to=alpha)



#########
##This loops does steps iv-vi
##########
set.seed(seed)
Z_h=mvrnorm(n = Reps, mu=c(0,0,0), Sigma=rbind(c(1,sqrt(1-rho^2),rho),c(sqrt(1-rho^2),1,0),c(rho,0,1)))



SequenceHs=seq(from=-(max(abs(h_n1),abs(c_n)))*10,to=max(abs(h_n1),abs(c_n))*10,length.out=10000)
MatrixQuantiles=matrix(0,nrow=length(SequenceHs),ncol=length(Grid_2))
Grid_1=seq(from=-(max(abs(h_n1),abs(c_n)))*2,to=max(abs(h_n1),abs(c_n))*2,length.out=500)

##This is to do things in paralle... future iterations should fix this to work across plataforms. This only works in Linux right now
availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
## Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
##set seed for reproducible results
clusterSetRNGStream(cl,seed)

MatrixQuantiles=foreach(i=1:length(SequenceHs),.combine=rbind) %dopar% {
      as.numeric(quantile(abs(sigma_X*Z_h[,1])*as.numeric(abs(SequenceHs[i]+Z_h[,3])>c_n)+ abs(-SequenceHs[i]*sigma_X*rho+sigma_X*sqrt(1-rho^2)*Z_h[,2])*as.numeric(abs(SequenceHs[i]+Z_h[,3])<c_n),1-Grid_2))
}

#pTNDist=function(x,h_1,c_n) pfoldnorm(x,mean=0,sd=sigma_X)*(1-pfoldnorm(c_n,mean=h_1,sd=1))+
#pfoldnorm(x,mean=-h_1*sigma_X*rho,sd=sigma_X*sqrt(1-rho^2))*(pfoldnorm(c_n,mean=h_1,sd=1))
#
#
#
#pTNDist2=function(x,h_1,c_n)  pmvnorm(lower=c(-x/sigma_X,-Inf,c_n), upper=c(x/sigma_X,Inf,Inf), mean=c(0,-h_1*sigma_X*rho,h_1), sigma=rbind(c(1,sqrt(1-rho^2),rho),c(sqrt(1-rho^2),1,0),c(rho,0,1)))[1]+
#pmvnorm(lower=c(-x/sigma_X,-Inf,-Inf), upper=c(x/sigma_X,Inf,-c_n), mean=c(0,-h_1*sigma_X*rho,h_1), sigma=rbind(c(1,sqrt(1-rho^2),rho),c(sqrt(1-rho^2),1,0),c(rho,0,1)))[1]+
#pmvnorm(lower=c(-Inf,-x/(sigma_X*sqrt(1-rho^2)),-c_n), upper=c(Inf,x/(sigma_X*sqrt(1-rho^2)),c_n), mean=c(0,-h_1*sigma_X*rho,h_1), sigma=rbind(c(1,sqrt(1-rho^2),rho),c(sqrt(1-rho^2),1,0),c(rho,0,1)))[1]
#
#
#
#
#    
#inverse = function (f, lower = 0.0001, upper = 1000,...) {
#   function (y) uniroot((function (x) f(x,...) - y), lower = lower, upper = upper)[1]$root
#}
#
#
#
#
#MatrixQuantiles2=matrix(0,nrow=length(SequenceHs),ncol=length(Grid_2))
#
#for(h_1 in SequenceHs){
#  for(j in Grid_2){
#      MatrixQuantiles2[which(SequenceHs==h_1),which(Grid_2==j)]=inverse(pTNDist,h_1=h_1,c_n=c_n)(1-j)
#  }
#}
#
#MatrixQuantiles3=matrix(0,nrow=length(SequenceHs),ncol=length(Grid_2))
#
#for(h_1 in SequenceHs){
#  print(h_1)
#  for(j in Grid_2){
#      MatrixQuantiles3[which(SequenceHs==h_1),which(Grid_2==j)]=inverse(pTNDist2,h_1=h_1,c_n=c_n)(1-j)
#  }
#}




for(beta in seq(1,0.1,-0.1)){
z_int=qnorm(1-beta/2)
ApproxPower=foreach(i=1:length(Grid_1),.packages=c("VGAM"),.combine=cbind) %dopar% {
      #t0=Sys.time()
      print(i)
      W_h =abs(sigma_X*Z_h[,1])*as.numeric(abs(Grid_1[i]+Z_h[,3])>c_n)+ abs(-Grid_1[i]*sigma_X*rho+sigma_X*sqrt(1-rho^2)*Z_h[,2])*as.numeric(abs(Grid_1[i]+Z_h[,3])<=c_n)
      htild1 = Grid_1[i]+Z_h[,3]
      Min=sapply(htild1,function(x) min(which(SequenceHs>=(x-z_int))))
      Max=sapply(htild1,function(x) max(which(SequenceHs<=(x+z_int))))
      A=mapply(function(x,y) apply(MatrixQuantiles[c(x:y),],2,max),Min,Max) 
      A=t(A)
      apply(W_h>A,2,mean)
      #print(Sys.time()-t0)
}##Close the for that goes through the whole grid Grid_1

MaxGrid2=apply(ApproxPower,1,max,na.rm=T)



############################
###########  STEP 3 E ####
############################

WhichMenor=which(MaxGrid2<=alpha)
if(length(WhichMenor)==0){
#print(paste0("With beta ",beta, "You are not controlling size!!"))
next
}
Keep_index=max(WhichMenor)
abar = Grid_2[Keep_index]
if(abar >=(alpha-alpha/10)) break
}

stopCluster(cl)
stopImplicitCluster()

if(beta==1) warning("You were not able to control size")
z_int=qnorm(1-beta/2)
MaxQuantile=max(MatrixQuantiles[which((SequenceHs>(h_n1-z_int)) & (SequenceHs<(h_n1+z_int))),which(Grid_2==abar)])


return(list(abar=abar,CV_Bonferroni=MaxQuantile,beta=beta,Tn0=Tn0,CVSimple=CVSimple,TnFull=CVFull))


}##Close the function








