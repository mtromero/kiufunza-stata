
BF_McCloskey=function(data=NULL,varInterest=NULL,varIncluded=NULL,varControl=NULL,varOutput=NULL,varCluster=NULL,alpha=0.05,epsilon=0.001,Reps=100000,seed=12345){

##This functions implements the algorithim "Algorithm Bonf-Adj Post-Sel" in McCloskey (June 2015 Version) "Bonferroni-Based Size-Correction for Nonstandard Testing Problems". 
##In short, its calculates an adjusted critical value for the variable "varInterest", after taking into account controls that are always included (varIncluded), and model selection over "varControl"
##data= is a matrix that has the variable of intereste, the variables to be included in the model, the control variables, and the output variables
##varInterest= is the variable on which we want to calculcate the adjusted critical values
##varIncluded= is a set of variables that are always included in the model (e.g., a constant)
##varControl= is the set of variables over which model selection will be done
##varOutput= is the LHS variable
##varCluster== for adjusting the original standard errors. Robust standard errors are used by default anyway
##alpha= is the type-I error rate
##beta= is XXXXX
##epsilon= Corresponds to the value epsilon in page 37. Seldomy adjusted
##Reps=Number of Reps for the simulation. Seldomy adjusted
##seed= Its a seed for the random number generation

require(VGAM)
require(MASS)
require(lfe)
require(fBasics)
require(expm)
require(doParallel)
require(foreach)

if(is.null(data)) stop("You NEED to input a data set")
if(is.null(varInterest)) stop("You NEED to input a variable of interest")
if(is.null(varControl)) stop("You NEED to input variables over which the model selection will be done")
if(is.null(varOutput)) stop("You NEED to input a LHS variable")
if(alpha>1 | alpha<0) stop("alpha needs to be between 0 and 1")
if(epsilon<0) stop("epsilon needs to be positive")
if(Reps<0) stop("Reps needs to be positive")
if(sum(!(varInterest %in% colnames(data) ))>0) stop("The variable of interested is no in the data")
if(sum(!(varIncluded %in% colnames(data) ))>0) stop("Some of the variables to be included are not in the data")
if(sum(!(varControl %in% colnames(data) ))>0) stop("Some of the variables over which selection is to be done are not in the data")
if(sum(!(varOutput %in% colnames(data) ))>0) stop("The variable on the LHS is no in the data")


##First lets keep only what we need/works.. so the columns that we want, plus rows with all non-missing values
if(!is.null(varCluster)) data=data[,c(varInterest,varIncluded,varControl,varOutput,varCluster)]
if(is.null(varCluster)) data=data[,c(varInterest,varIncluded,varControl,varOutput)]
data=data[complete.cases(data),]
dataCentered=data

##Now we are going to project the data on varInterest,varControl and varOutput on the variables in varIncluded

if(!is.null(varIncluded)){
  for(var in c(varInterest,varControl,varOutput)){
    reg=lm(formula(paste0(var,"~-1+",paste(varIncluded,collapse="+",sep=""))),data=dataCentered,model=F)
    dataCentered[,var]=reg$resid
  }
}

##We dont need the varIncluded anymore, so lets get rid of it
if(!is.null(varCluster))  dataCentered=dataCentered[,c(varInterest,varControl,varOutput,varCluster)]
if(is.null(varCluster))  dataCentered=dataCentered[,c(varInterest,varControl,varOutput)]

##Number of observations
n=dim(dataCentered)[1]

##Calculcate the number of observations and the consistent selection threshold

c_n=sqrt(log(n))



NumExtra=length(varControl) ##This is "v" in the paper. number of additional vars
p=2^NumExtra ##number of possible subsets

##Need to calculcate what happens if we use all the models in varControl+varInterest
OriginalRegUseful=lm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1")),data=dataCentered)
if(is.null(varCluster)) OriginalReg=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1")),data=dataCentered)
if(!is.null(varCluster)) OriginalReg=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl,collapse="+",sep=""),"-1 | 0 | 0 | ",varCluster)),data=dataCentered)



#####################
#####################
##the t-values of the full model
t_values=coef(summary(OriginalReg,robust=TRUE))[-1,3]
##the coefficients of the full model
original_coefs=coef(summary(OriginalReg,robust=TRUE))[-1,1]
##if we just do some model selection consistently
Pi_m_original=matrix(0,ncol=NumExtra,nrow=sum(abs(t_values)>c_n))
#if(dim(Pi_m_original)[1]==0) warning("Nothing should be selected... ask adam what to do!")

if(dim(Pi_m_original)[1]>0){
  for(i in 1:sum(abs(t_values)>c_n)){
  Pi_m_original[i,sort(abs(t_values),T,index.return=T)$ix[i]]=1
  }
}
##the estimated model that we get from doing this
if(is.null(varCluster)) Reg2=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl[which(abs(t_values)>c_n)],collapse="+",sep=""),"-1")),data=dataCentered)
if(!is.null(varCluster)) Reg2=felm(formula(paste0(varOutput,"~",varInterest,"+",paste(varControl[which(abs(t_values)>c_n)],collapse="+",sep=""),"-1 | 0 | 0 |",varCluster)),data=dataCentered)
theta_mn=coefficients(summary(Reg2,robust=TRUE))[1,1]
Tn0=abs(theta_mn*sqrt(n))
CVSimple=abs(coefficients(summary(Reg2,robust=TRUE))[1,2]*sqrt(n)*qnorm(1-alpha/2))

TnFull=abs(coefficients(summary(OriginalRegUseful,robust=TRUE))[1,1]*sqrt(n))
CVFull=abs(coefficients(summary(OriginalReg,robust=TRUE))[1,2]*sqrt(n)*qnorm(1-alpha/2))
#####################
#####################


##Now onto the real algorithim!!!

############################
###########  STEP 1 ########
############################
zeta_n=t_values/c_n
##H
H=model.matrix(OriginalRegUseful)
##The variance-covariance matrix
if(is.null(varCluster))  OMEGA_N=(t(H)%*%H)%*%OriginalReg[["robustvcv"]]%*%(t(H)%*%H)/n
if(!is.null(varCluster)) OMEGA_N=(t(H)%*%H)%*%OriginalReg[["clustervcv"]]%*%(t(H)%*%H)/n
##estimador de Q... es decir \hat{Q}
Q=(1/n)*(t(H)%*%H)
##Q_XX
Q_XX=Q[1,1]
##Q_ZZ
Q_ZZ=Q[-1,-1]
##Q_XZ
Q_XZ=matrix(Q[1,-1],nrow=1)
##Q_ZX
Q_ZX=t(Q_XZ)
##S_0
S_0=rbind(matrix(0,ncol=NumExtra,nrow=1),diag(NumExtra))
##h_{n,2}
h_n2=t(cbind(OMEGA_N[1,1],t(vec(OMEGA_N[1,-1])),t(matrix(vech(OMEGA_N[-1,-1]),ncol=1)),Q_XX,t(vec(Q_XZ)),t(matrix(vech(Q_ZZ),ncol=1))))
 



############################
###########  STEP 2 ########
############################

SuperIndex=findInterval(zeta_n, c(-10e10,-1-epsilon,-1+epsilon,1-epsilon,1+epsilon,10e10 ))
IndexUnique=unique(SuperIndex)
############################
###########  STEP 3 A ######
############################

PossiblesSubsets=lapply(0:NumExtra, function(x) combn(NumExtra,x))
LIST_M=list()
cont=1
for(i in 0:NumExtra){
  Comb=PossiblesSubsets[[i+1]]
  if(dim(Comb)[1]>0){
    for(col in 1:dim(Comb)[2]){
      indexIn=Comb[,col]
      PisubM=matrix(0,ncol=NumExtra,nrow=length(indexIn))
      for(pos in 1:length(indexIn)){
        PisubM[pos,indexIn[pos]]=1
      }  ##This is what is done just after equation 20
      index_i=which(diag(t(PisubM)%*%PisubM)==1) ## This is M_m
      index_j=which(diag(diag(NumExtra)- t(PisubM)%*%PisubM)==1)  ## This is M_m^c
      if(!(3 %in% SuperIndex[index_i]) & !(1 %in% SuperIndex[index_j] | 5 %in% SuperIndex[index_j])){  
        LIST_M[[cont]]=indexIn
        cont=cont+1
      }
    }##This is 
    
  }##this closes the if in case the size is greater than zero
  
  
  if(dim(Comb)[1]==0){
  index_i=NULL
  index_j=1:NumExtra
      if(!(3 %in% SuperIndex[index_i]) & !(1 %in% SuperIndex[index_j] | 5 %in% SuperIndex[index_j])){  
        LIST_M[[cont]]=NULL
        cont=cont+1
      }
  }##This is an if in case the size is 0, i.e., lets not include anything!
  
}##This goes through the possible sizes of subsets 

if(length(LIST_M)>1) strop("more than one model selected selected - code is not ready to take on this contigency!!")

if(length(LIST_M)==1){
Mestrella=LIST_M[[1]]
##Primero \Pi_m
Pi_M=matrix(0,ncol=NumExtra,nrow=length(Mestrella))
      if(length(Mestrella)>=1){
      	for(pos in 1:length(Mestrella)){
	  Pi_M[pos,Mestrella[pos]]=1
	 }
      }
}

if(length(LIST_M)==0){
  Mestrella=0
  Pi_M=matrix(0,ncol=NumExtra,nrow=length(NumExtra))
}

      
############################
###########  STEP 3 B ######
############################

##S_m
if(length(LIST_M)==1) S_m=rbind(cbind(matrix(1,ncol=1,nrow=1),matrix(0,nrow=1,ncol=length(Mestrella))),cbind(matrix(0,ncol=1,nrow=NumExtra),t(Pi_M)))
if(length(LIST_M)==0) S_m=rbind(matrix(1,ncol=1,nrow=1),matrix(0,ncol=1,nrow=NumExtra))

##Estimador de Q_m, es decir \hat{Q_m}
Q_m=t(S_m)%*%Q%*%S_m

##A_{m,h_{n,2,2}}
A_mhn22=qr.solve(Q_m)%*%t(S_m)%*%Q%*%S_0%*%(diag(NumExtra)- t(Pi_M)%*%Pi_M)
##h_{n,1,m}
h_n1m=sqrt(n)*(A_mhn22%*%original_coefs)[1]
##\sigma_{h_2}
sigma_hn2=A_mhn22[1,] %*%(qr.solve(Q)%*% (OMEGA_N)  %*%qr.solve(Q))[-1,-1]%*% t(matrix(A_mhn22[1,],nrow=1)) ##Correct, but lets use Adam's for comparison
##sigma_hn2=A_mhn22[1,] %*%(qr.solve(Q)%*% (Omega_hat_A$Omega.hat)  %*%qr.solve(Q))[-1,-1]%*% t(matrix(A_mhn22[1,],nrow=1))

if(dim(sigma_hn2)[1]>1) stop("dim more than 1")
##\omega{h_2}
Omega_h2=(sigma_hn2^(-0.5))%*%sigma_hn2%*%(sigma_hn2^(-0.5))


############################
###########  STEP 3 C ######
############################

###Grid_1#I create a grid that takes values from -50 to 50 around h_n1m
#OrderMagnitud=ceiling(log10(abs(h_n1m)))
#if(OrderMagnitud==0 | OrderMagnitud==-Inf){
#  Grid_1=c(seq(from=-10^(OrderMagnitud+2),by=10^(OrderMagnitud),to=abs(h_n1m)-10^(OrderMagnitud+1)),
#  seq(from=abs(h_n1m)-10^(OrderMagnitud+1),by=10^(OrderMagnitud-1),to=abs(h_n1m)+10^(OrderMagnitud+1)),
#  seq(from=abs(h_n1m)+10^(OrderMagnitud+1),by=10^(OrderMagnitud),to=10^(OrderMagnitud+2)))
#}
#
#if(OrderMagnitud==1 | OrderMagnitud==2){
#  Grid_1=c(seq(from=-10^(OrderMagnitud+2),by=10^(OrderMagnitud),to=-10^(OrderMagnitud+1)),
#  seq(from=-10^(OrderMagnitud+1),by=10^(OrderMagnitud-1),to=-abs(h_n1m)),
#  seq(from=-abs(h_n1m),by=10^(OrderMagnitud-2),to=abs(h_n1m)),
#  seq(from=abs(h_n1m),by=10^(OrderMagnitud-1),to=10^(OrderMagnitud)),
#  seq(from=10^(OrderMagnitud),by=10^(OrderMagnitud-1),to=10^(OrderMagnitud+1)),
#  seq(from=10^(OrderMagnitud+1),by=10^(OrderMagnitud+2),to=10^(OrderMagnitud+2)))
#}
#
#if(OrderMagnitud>2) stop("h_n1m is big... code needs to be adjusted to account for this")

SequenceHs=seq(from=0,to=max(h_n1m,c_n)*10,length.out=10000)
Grid_1=seq(from=0,to=max(c_n,h_n1m)*2,length.out=500)



############################
###########  STEP 3 D ####
############################


###########  STEP 3 D i  ####


##distributions_{1,m}
set.seed(seed)
ZRand=mvrnorm(n =Reps ,mu=rep(0,NumExtra+1),  Sigma=diag(nrow=NumExtra+1))
Sample_wm=((qr.solve(Q_m)%*%t(S_m)%*%sqrtm(OMEGA_N)) %*% t(ZRand))[1,]
Sample_h1m=as.vector(A_mhn22[1,]%*%((qr.solve(Q)%*% sqrtm(OMEGA_N)) %*% t(ZRand))[-1,])

###########  STEP 3 D ii  ####
##Grid_2
Grid_2=seq(from=0.005 , by=0.005, to=alpha)


##The SD of W_h
SDWm=sqrt(sum(((qr.solve(Q_m)%*%t(S_m)%*%sqrtm(OMEGA_N)))[1,]^2))

##This is to do things in paralle... future iterations should fix this to work across plataforms. This only works in Linux right now
availableCores<-detectCores()-1
no_cores <- max(1, availableCores)
## Initiate cluster
cl <- makeCluster(no_cores)
registerDoParallel(cl)
##set seed for reproducible results
clusterSetRNGStream(cl,seed)

MatrixQuantiles=foreach(i=1:length(SequenceHs),.packages=c("VGAM"),.combine=rbind) %dopar% {
      qfoldnorm(1-Grid_2, mean=SequenceHs[i], sd=SDWm)
}




#########
##This loops does steps iv-vi
########## 
for(beta in seq(1,0.1,-0.1)){ 
z_int=sqrt(sigma_hn2)*qnorm(1-beta/2)

ApproxPower=foreach(i=1:length(Grid_1),.combine=cbind) %dopar% {
      print(i)
      W_h = abs(Grid_1[i]+Sample_wm)
      htild1 = Grid_1[i]+Sample_h1m
      Max=sapply(htild1,function(x) max(which(SequenceHs<as.numeric(abs(x)+z_int))))
      A=MatrixQuantiles[Max,] 
      apply(W_h>A,2,mean)
}##Close the for that goes through the whole grid Grid_1



MaxGrid2=apply(ApproxPower,1,max,na.rm=T)



############################
###########  STEP 3 E ####
############################
WhichMenor=which(MaxGrid2<=alpha)
if(length(WhichMenor)==0){
#print(paste0("With beta ",beta, "You are not controlling size!!"))
next
}
Keep_index=max(WhichMenor)
abar = Grid_2[Keep_index]
if(abar >=(alpha-alpha/10)) break
}

stopCluster(cl)
stopImplicitCluster()

if(beta==0.1) warning("You were not able to control size")
z_int=sqrt(sigma_hn2)*qnorm(1-beta/2)
MaxQuantile=qfoldnorm(1-abar, mean=abs(h_n1m)+z_int, sd=SDWm)


return(list(abar=abar,CV_Bonferroni=MaxQuantile,beta=beta,Pi_m_original=Pi_m_original,Pi_M=Pi_M,Tn0=Tn0,CVSimple=CVSimple,TnFull=CVFull))


}##Close the function








