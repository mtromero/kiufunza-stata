
ScoreStudentsTotal=NULL
for(k in 1:1000){
NSchools=70
NStudents=80*70
Budget=8333.333
StudentsData=NULL
StudentsData$Ability=rnorm(NStudents)
StudentsData=data.frame(StudentsData)
StudentsData$School=rep(1:70,80)
StudentsData$decile=cut(StudentsData$Ability, quantile(StudentsData$Ability, (0:10)/10), labels=1:10,include.lowest=T)
StudentsData$decile=as.numeric(as.character(StudentsData$decile))


StudentsData$EndScore=NA
StudentsData$EndRanking=NA
for(dec in 1:10){
StudentsData$EndScore[which(StudentsData$decile==dec)]=rnorm(length(which(StudentsData$decile==dec)))
StudentsData$EndRanking[which(StudentsData$decile==dec)]=sort.int(StudentsData$EndScore[which(StudentsData$decile==dec)],index.return=T)$ix
}

ScoreStudents=rep(0,NSchools)
for(scho in 1:NSchools){
ScoreStudents[scho]=ScoreStudents[scho]+sum(StudentsData$EndRanking[which(StudentsData$School==scho)])*(Budget/10)/sum(1:(NStudents/10))
}
ScoreStudentsTotal=c(ScoreStudents,ScoreStudentsTotal)
}

setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/Latex/Current")

pdf("SimPayments.pdf")
hist(ScoreStudentsTotal,xlab="payments",ylab="Density",freq=F,col=4,main="Histogram of payments")
dev.off()
