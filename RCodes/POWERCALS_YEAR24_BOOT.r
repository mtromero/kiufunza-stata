 rm(list=ls())
 ols <- function(form, data, robust=FALSE, cluster=NULL,digits=3){
  r1 <- lm(form, data)
  if(length(cluster)!=0){
    data <- na.omit(data[,c(colnames(r1$model),cluster)])
    r1 <- lm(form, data)
  }
  X <- model.matrix(r1)
  n <- dim(X)[1]
  k <- dim(X)[2]
  if(robust==FALSE & length(cluster)==0){
    se <- sqrt(diag(solve(crossprod(X)) * as.numeric(crossprod(resid(r1))/(n-k))))
    res <- cbind(coef(r1),se)
  }
  if(robust==TRUE){
    u <- matrix(resid(r1))
    meat1 <- t(X) %*% diag(diag(crossprod(t(u)))) %*% X
    dfc <- n/(n-k)
    se <- sqrt(dfc*diag(solve(crossprod(X)) %*% meat1 %*% solve(crossprod(X))))
    res <- cbind(coef(r1),se)
    }
  if(length(cluster)!=0){
    clus <- cbind(X,data[,cluster],resid(r1))
    colnames(clus)[(dim(clus)[2]-1):dim(clus)[2]] <- c(cluster,"resid")
    m <- dim(table(clus[,cluster]))
    dfc <- (m/(m-1))*((n-1)/(n-k))
    uclust  <- apply(resid(r1)*X,2, function(x) tapply(x, clus[,cluster], sum))
    se <- sqrt(diag(solve(crossprod(X)) %*% (t(uclust) %*% uclust) %*% solve(crossprod(X)))*dfc)
    res <- cbind(coef(r1),se)
  }
  res <- cbind(res,res[,1]/res[,2],(1-pnorm(res[,1]/res[,2]))*2)
  res1 <- matrix(as.numeric(sprintf(paste("%.",paste(digits,"f",sep=""),sep=""),res)),nrow=dim(res)[1])
  rownames(res1) <- rownames(res)
  colnames(res1) <- c("Estimate","Std. Error","t value","Pr(>|t|)")
  return(res1)
}




 library(MASS)
 library(Matrix)
 library(plyr)
 


COD1_COD2=40
COD1_GAINS2=20
COD1_CONT=10

COMBO1_COD2=10
COMBO1_GAINS2=30
COMBO1_CONT=30

CONTROL1_COD2=10
CONTROL1_GAINS2=10
CONTROL1_CONT=20

KIDS_PER_SCHOOL=30

TOTAL_SCHOOLS=COD1_COD2+COD1_GAINS2+COD1_CONT+COMBO1_COD2+ COMBO1_GAINS2+ COMBO1_CONT+ CONTROL1_COD2+CONTROL1_GAINS2+CONTROL1_CONT
NTOTAL=TOTAL_SCHOOLS*KIDS_PER_SCHOOL
ICC=0.3


X=matrix(0,ncol=6,nrow=NTOTAL)
X[,1]=1
for(i in 1:TOTAL_SCHOOLS){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),6]=i
if(i>=1 & i<=COD1_COD2){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),2]=1
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),4]=1
}
if(i>=(COD1_COD2+1) & i<=(COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),3]=1
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),4]=1
}
if(i>=(COD1_COD2+COD1_GAINS2+1) & i<=(COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),4]=1
}

if(i>=(COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),2]=1
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),5]=1
}
if(i>=(COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),3]=1
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),5]=1
}
if(i>=(COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),5]=1
}

if(i>=(COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(CONTROL1_COD2+COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),2]=1
}
if(i>=(CONTROL1_COD2+COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(CONTROL1_GAINS2+CONTROL1_COD2+COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
X[((i-1)*KIDS_PER_SCHOOL+1):(i*KIDS_PER_SCHOOL),3]=1
}
if(i>=(CONTROL1_GAINS2+CONTROL1_COD2+COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2+1) & i<=(CONTROL1_CONT+CONTROL1_GAINS2+CONTROL1_COD2+COMBO1_CONT+COMBO1_GAINS2+COMBO1_COD2+COD1_CONT+COD1_COD2+COD1_GAINS2)){
}
}

 Replications=10000
 VarinceBeta=matrix(NA,ncol=5,nrow=Replications)
 Beta=matrix(NA,ncol=5,nrow=Replications)
 for(rep in 1:Replications){
ErrorCov=matrix(ICC,nrow=KIDS_PER_SCHOOL,ncol=KIDS_PER_SCHOOL)
diag(ErrorCov)=1
diag(ErrorCov)=.6208977
ErrorTerm=as.vector(replicate(TOTAL_SCHOOLS, diag(mvrnorm(n = KIDS_PER_SCHOOL, mu=rep(0,KIDS_PER_SCHOOL), Sigma=ErrorCov))))
 Data=data.frame(cbind(ErrorTerm,X))
 colnames(Data)=c("Y","CONS","COD2","GAINS2","COD1","COMBO1","SCHOOL")
A=ols(Y~ CONS+COD2+GAINS2+COD1+COMBO1-1,data=Data,cluster="SCHOOL")
VarinceBeta[rep,]=A[,2]
Beta[rep,]=A[,1]
}
