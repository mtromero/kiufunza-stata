rm(list=ls())
learning=function(e,I){
  log(e)+log(I)+e*I
}
costo_benefit=function(e,I,t,lambda){
  (t+lambda)*(1/e+I)-2*e
}


e_min=uniroot(learning,c(0,100),I=1)$root
e_min2=uniroot(learning,c(0,100),I=1.2)$root


e_opt_seq_0=NULL
for(extra_t in seq(0.001,1,0.001)){
  e_opt=uniroot(costo_benefit,c(0,100),t=extra_t,I=1,lambda=0)$root
  e_opt_seq_0=c(e_opt_seq_0,e_opt)
}


e_opt_seq_2=NULL
for(extra_t in seq(0.001,1,0.001)){
  e_opt=uniroot(costo_benefit,c(0,100),t=extra_t,I=1.2,lambda=0)$root
  e_opt_seq_2=c(e_opt_seq_2,e_opt)
}


setwd("C:/Users/Mauricio/Dropbox/KF_Draft/KFI/LaTeX/graphs")
pdf("Optimal_effort_motivation_movI.pdf")
par(mgp=c(1,0,0),oma=c(0,0,0,0),cex.lab=1.5,mar=c(2.5,2.5,2.5,2))
plot(seq(0.001,1,0.001),pmax(e_min,e_opt_seq_0),xaxt='n',yaxt='n',type="l",lwd=3,ylab="Effort",xlab=expression(t),ylim=c(0,1),main="Optimal effort, inputs, and incentives")
lines(seq(0.001,1,0.001),pmax(e_min2,e_opt_seq_2),lwd=3,col=2,lty=2)
legend("bottomright",c(expression(I[0]),expression(I[1])),lty=1:2,lwd=3,col=1:2)
dev.off()

pdf("Optimal_learning_motivation_movI.pdf")
par(mgp=c(1,0,0),oma=c(0,0,0,0),cex.lab=1.5,mar=c(2.5,2.5,2.5,2))
plot(seq(0.001,1,0.001),learning(pmax(e_min,e_opt_seq_0),1),xaxt='n',yaxt='n',type="l",lwd=3,ylab="Learning",xlab=expression(t),ylim=c(0,1),main="Learning and motivation")
lines(seq(0.001,1,0.001),learning(pmax(e_min2,e_opt_seq_2),1.2),lwd=3,col=2,lty=2)
legend("bottomright",c(expression(I[0]),expression(I[1])),lty=1:2,lwd=3,col=1:2)
dev.off()

