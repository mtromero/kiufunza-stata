﻿rm(list=ls())
library(R.matlab)
library(foreign)
library(VGAM)
library(MASS)
library(lfe)
library(fBasics)
library(expm)
#Data=read.dta("C:/Users/Mauricio/Downloads/caschool.dta")
Data=read.dta("/home/mauricio/Downloads/caschool.dta")
alpha=0.05
n=dim(Data)[1]
c_n=sqrt(log(n))
epsilon=0.001
Reps=100
##Ni idea como se selecciona
beta=0.4

Vars=c("meal_pct","calw_pct","log(avginc)","el_pct","el_pct:str")
NumExtra=length(Vars) #This is "v" in the paper. number of additional vars
p=2^NumExtra #number of possible subsets

OriginalRegUseful=lm(formula(paste0("testscr~str+",paste(Vars,collapse="+",sep=""))),data=Data)
OriginalReg=felm(formula(paste0("testscr~str+",paste(Vars,collapse="+",sep=""))),data=Data)

OMEGA_N=OriginalReg[["robustvcv"]]
OMEGA_N=OMEGA_N[-1,-1]

t_values=coef(summary(OriginalReg,robust=TRUE))[3:(3+NumExtra-1),3]
original_coefs=coef(summary(OriginalReg,robust=TRUE))[3:(3+NumExtra-1),1]
Pi_m_original=matrix(0,ncol=NumExtra,nrow=sum(abs(t_values)>c_n))
for(i in 1:sum(abs(t_values)>c_n)){
Pi_m_original[i,sort(abs(t_values),T,index.return=T)$ix[i]]=1
}

#####################
#####################
Reg2=felm(formula(paste0("testscr~str+",paste(Vars[which(abs(t_values)>c_n)],collapse="+",sep=""))),data=Data)
summary(Reg2,robust=TRUE)
theta_mn=coefficients(summary(Reg2,robust=TRUE))[2,1]
Tn0=abs(theta_mn*sqrt(n))
#####################
#####################

############################
###########  STEP 1 ########
############################
zeta_n=t_values/c_n # sqrt(n) hat{\chi}/(\sigma_{\chi} c_n) where c_n=log(n) for bic case.names
##H
H=model.matrix(OriginalRegUseful)[,-1]
##estimador de Q... es decir \hat{Q}
Q=(1/n)*(t(H)%*%H)
##Q_XX
Q_XX=Q[1,1]
##Q_ZZ
Q_ZZ=Q[-1,-1]
##Q_XZ
Q_XZ=matrix(Q[1,-1],nrow=1)
##Q_ZX
Q_ZX=t(Q_XZ)
##S_0
S_0=rbind(matrix(0,ncol=NumExtra,nrow=1),diag(NumExtra))
##h_{n,2}
h_n2=t(cbind(Q_XX,t(vec(Q_XZ)),t(matrix(vech(Q_ZZ),ncol=1)),(1/n)*(t(H[,1])%*%H[,1]),t(vec((1/n)*(t(H[,1])%*%H[,-1]))),t(matrix(vech((1/n)*(t(H[,-1])%*%H[,-1])),ncol=1))))



############################
###########  STEP 2 ########
############################

SuperIndex=findInterval(zeta_n, c(-10e10,-1-epsilon,-1+epsilon,1-epsilon,1+epsilon,10e10 ))

############################
###########  STEP 3 A ######
############################

PossiblesSubsets=lapply(0:NumExtra, function(x) combn(NumExtra,x))
LIST_M=list()
cont=1
for(i in 0:NumExtra){
  Comb=PossiblesSubsets[[i+1]]
  if(dim(Comb)[1]>0){
    for(col in 1:dim(Comb)[2]){
      indexIn=Comb[,col]
      PisubM=matrix(0,ncol=5,nrow=length(indexIn))
      for(pos in 1:length(indexIn)){
        PisubM[pos,indexIn[pos]]=1
      } #este es for que crea la matrix pi_M (ver abajo de la ecuacion 20)
      index_i=which(diag(t(PisubM)%*%PisubM)==1) #esto son los M_m
      index_j=which(diag(diag(NumExtra)- t(PisubM)%*%PisubM)==1)  #esto son los M_m^c
      if(!(3 %in% SuperIndex[index_i]) & !(1 %in% SuperIndex[index_j] | 5 %in% SuperIndex[index_j])){  #no me sirve
        LIST_M[[cont]]=indexIn
        cont=cont+1
      }
    }#Este es for de todos los posibles modelos de este tamano
  } #if de que solo siga en caso de que los modelos tengan mas de una cosa
  
  if(dim(Comb)[1]==0){
  index_i=NULL
  index_j=1:NumExtra
      if(!(3 %in% SuperIndex[index_i]) & !(1 %in% SuperIndex[index_j] | 5 %in% SuperIndex[index_j])){  #no me sirve
        LIST_M[[cont]]=indexIn
        cont=cont+1
      }
  } #if de que el modelo es vacio,    
}#for the tamano del submodelo

if(length(LIST_M)>1) strop("more than one selected")


Mestrella=LIST_M[[1]]

##Primero \Pi_m
Pi_M=matrix(0,ncol=5,nrow=length(Mestrella))
      for(pos in 1:length(Mestrella)){
        Pi_M[pos,Mestrella[pos]]=1
} #este es for que crea la matrix pi_M (ver abajo de la ecuacion 20)
      
############################
###########  STEP 3 B ######
############################

##S_m
S_m=rbind(cbind(matrix(1,ncol=1,nrow=1),matrix(0,nrow=1,ncol=length(Mestrella))),
cbind(matrix(0,ncol=1,nrow=NumExtra),t(Pi_M)))
##Estimador de Q_m, es decir \hat{Q_m}
Q_m=t(S_m)%*%Q%*%S_m

##A_{m,h_{n,2,2}}
A_mhn22=qr.solve(Q_m)%*%t(S_m)%*%Q%*%S_0%*%(diag(NumExtra)- t(Pi_M)%*%Pi_M)
##h_{n,1,m}
h_n1m=sqrt(n)*(A_mhn22%*%original_coefs)[1]
##\sigma_{h_2}
sigma_hn2=A_mhn22[1,] %*%(qr.solve(Q)%*% OMEGA_N  %*%qr.solve(Q))[-1,-1]%*% t(matrix(A_mhn22[1,],nrow=1))
if(dim(sigma_hn2)[1]>1) stop("dim more than 1")
##\omega{h_2}
Omega_h2=(sigma_hn2^(-0.5))%*%sigma_hn2%*%(sigma_hn2^(-0.5))
z_int=sqrt(sigma_hn2)*qnorm(1-beta/2)

############################
###########  STEP 3 C ######
############################

##Grid_1
Grid_1=seq(from=h_n1m-3,by=0.1,to=h_n1m+3)
Grid_1=seq(from=-10,by=0.1,to=10)



############################
###########  STEP 3 D ####
############################


###########  STEP 3 D i  ####

##distributions_{1,m}
ZRand=mvrnorm(n =Reps ,mu=rep(0,NumExtra+1),  Sigma=diag(nrow=NumExtra+1))
Sample_wm=((qr.solve(Q_m)%*%t(S_m)%*%sqrtm(OMEGA_N)) %*% t(ZRand))[1,]
Sample_h1m=as.vector(A_mhn22[1,]%*%((qr.solve(Q)%*% sqrtm(OMEGA_N)) %*% t(ZRand))[-1,])

###########  STEP 3 D ii  ####
##Grid_2
Grid_2=seq(from=0.005 , by=0.005, to=alpha)

SDWm=sqrt(sum(((qr.solve(Q_m)%*%t(S_m)%*%sqrtm(OMEGA_N)))[1,]^2))
ApproxPower=matrix(0,nrow=length(Grid_2),ncol=length(Grid_1))

for(i in 1:length(Grid_1)){
      print(i)
      W_h = abs(Grid_1[i]+Sample_wm)
      htild1 = Grid_1[i]+Sample_h1m
      A=sapply(1-Grid_2, function(x) qfoldnorm(x, mean=abs(htild1)+z_int, sd=SDWm))
      ApproxPower[,i]=apply(W_h>A,2,mean)
}


MaxGrid2=apply(ApproxPower,1,max)
IndexMaxGrid2=apply(ApproxPower,1,which.max)
MaxGrid1=Grid_1[IndexMaxGrid2]



############################
###########  STEP 3 E ####
############################

Keep_index=min(max(which(MaxGrid2<0.05)),10)
abar = Grid_2[Keep_index]
h_1_abar = MaxGrid1[Keep_index]

MaxQuantiles=qfoldnorm(1-abar, mean=abs(h_1_abar)+z_int, sd=SDWm)



