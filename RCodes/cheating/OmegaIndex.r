OmegaIndex=function(NumQuestions,NumOptions,AnswersSource,AnswersCopy,MatrixProbabilitiesSource,MatrixProbabilitiesCopy){
#This function calculates the omega index between two individuals.
#Inputs:
#NumQuestions = Number of questions to compare
# NumOptions = Number of options in each question
# AnswersSource = The answers of the source as a vector
# AnswersCopy = The answers of the one who copies as a vector
# MatrixProbabilitiesCopy = Probability of Answers for the question the source actually answered

#Output:
#The omega index. uncodnitional and conditional. Standarized and non-standarized. Critical values.


 #Number of answers that are the same
 M=sum(AnswersSource==AnswersCopy,na.rm=T)
 
  #The probability that someone with the ability of the copier answer as r as the source did.
  #i.e. Conditional probability
 Prob_Conditional=as.vector(na.omit(MatrixProbabilitiesCopy[cbind(1:NumQuestions,AnswersSource)]))
 
 #UnConditional probability
 Prob_UnConditional=rowSums(MatrixProbabilitiesCopy*MatrixProbabilitiesSource,na.rm=T)

 #the standarized test p-value
 w_1_standarized=pnorm((M-sum(Prob_UnConditional))/sqrt(sum(Prob_UnConditional*(1-Prob_UnConditional))))
 w_2_standarized=pnorm((M-sum(Prob_Conditional))/sqrt(sum(Prob_Conditional*(1-Prob_Conditional))))

 
 #the p-value for the non-standarizes tests
 CriticalValue_Conditional= ppoibin(M, pp=Prob_Conditional)
 CriticalValue_UnConditional= ppoibin(M, pp=Prob_UnConditional)
 
return(c(M,CriticalValue_UnConditional,CriticalValue_Conditional,w_1_standarized,w_2_standarized))
}


