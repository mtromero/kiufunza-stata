
rm(list=ls())
setwd("Z:/CopyIndex")
CRANMirror="http://cran.cnr.Berkeley.edu"
set.seed(1986)  

library(rirt)
library(MiscPsycho)


source("RCode/OmegaIndex/OmegaIndex.r")
source("RCode/KappaIndex/KappaIndexMatrixNoPreRecode.r")
source("RCode/KappaIndex/ValorA.r")
source("RCode/WesolowskyIndex/AbilityIndividual.r")
source("RCode/WesolowskyIndex/WesolowskyIndex.r")

 
FORMATOS=c("PBA9042F2","PBA9043F1","PBA9043F2","PBA5041F1","PBA5041F2","PBA5042F1","PBA5042F2","PBA5043F1","PBA5043F2","PBA9041F1","PBA9041F2","PBA9042F1") 
for (Formato in  FORMATOS){
tinicial=Sys.time()

#LOAD answers
DataAnswers=read.delim( paste("Data/Raw/Colombia/",Formato,".4copy.raw",sep=""),sep="\t")
DataAnswers[,2]=as.character(DataAnswers[,2])
DataAnswers[,3]=as.character(DataAnswers[,3])
#LOAD answerkeys
AnswerKey=read.delim( paste("Data/Raw/Colombia/",Formato,".key.raw",sep=""),sep="\t",header=F)
AnswerKey=unlist(strsplit(as.character(AnswerKey[1,1]),character(0)))
#Get the number of questions from this test
NumQuestions=length(AnswerKey)
#copy levels
nivel_copia=c(0,1,seq(5,NumQuestions,5))

#We create a matrix with the answers for all persons
AnswerMatrix=matrix( unlist(strsplit(as.character(DataAnswers$String),character(0))),ncol=NumQuestions,byrow=TRUE)
#Relabel missings
AnswerMatrix[which(AnswerMatrix=="O")]=NA
AnswerMatrix[which(AnswerMatrix=="M")]=NA
#Transform options into numbers
AnswerMatrix=matrix(match(AnswerMatrix,LETTERS),ncol=NumQuestions,byrow=F)
AnswerKey=match(AnswerKey,LETTERS)
#Remove answer string since we dont need it anymore
DataAnswers=subset (DataAnswers,select=-String)


#We sample one student from each room!
funsample=function(x){ 
if(length(x)>1){return(sample(x,1))} 
if(length(x)==1) {return(x)}
}
SNP_Independent=aggregate(DataAnswers$SNP, by=list(DataAnswers$Sitio), FUN=funsample )
#This are the samples students... this is to calibrate the model using one student per room
INDEX_INDEP=which(DataAnswers$SNP %in% SNP_Independent[,2])


#We estimate the parameters of the nominal response model.
modelo=fitirt(AnswerMatrix[INDEX_INDEP,],model="NOMINAL",key=AnswerKey,max.nr.iter=1000,max.em.iter=1000)



#We calculate the latent ability estimated by the model
habilidad_media=predict(modelo, type="Z",data=AnswerMatrix)
#the intercept 
intercepto_medio=matrix(predict(modelo, type="COEFFICIENTS")$intercept,nrow=NumQuestions,byrow=T)
#the slope
pendiente_medio=matrix(predict(modelo, type="COEFFICIENTS")$a,nrow=NumQuestions,byrow=T)

save( modelo,habilidad_media,intercepto_medio,pendiente_medio,file=paste("Data/Created/Colombia/",Formato,"_IRTModelo.RData",sep=""),row.names=F)


} #for exams











