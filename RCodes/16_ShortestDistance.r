

library(foreign)
#setwd("E:/Box Sync/01_KiuFunza/")
setwd("C:/Users/Mauricio/Box Sync/01_KiuFunza/Results/Reg")
Tabla=read.dta("CreatedData/1 Baseline/School/DistanceData.dta")

VecIDS=unique(Tabla$SchoolID_1)
VecDistancia=NULL
for(i in 1:length(VecIDS)){
id=VecIDS[i]
TablaID=Tabla[which(Tabla$SchoolID_1==id),]
TablaID=TablaID[which(TablaID$treatarm==1 |   TablaID$treatarm==2),]
DistanciaID=min(TablaID$distance)
VecDistancia=c(VecDistancia,DistanciaID)
}

TablaOut=cbind(VecIDS,VecDistancia)
colnames(TablaOut)=c("SchoolID","DistanceCG")
TablaOut=data.frame(TablaOut)
write.dta(TablaOut,"CreatedData/1 Baseline/School/DistanceDataCG.dta")