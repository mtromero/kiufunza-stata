
**************************************************************
************************* TABLE 1 - SUMMARY STATISTICS *******
**************************************************************

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4 d_p d_p2
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2 TD3 TD4 `if', nocons vce(cluster `clus_id')
 test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_1' = nullmat(`mu_1'), A[1,1]
 mat `mu_2' = nullmat(`mu_2'), A[1,2]
 mat `mu_3' = nullmat(`mu_3'), A[1,3]
 mat `mu_4' = nullmat(`mu_4'), A[1,4]
 mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
 mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
 mat `se_3' = nullmat(`se_3'), sqrt(B[3,3])
 mat `se_4' = nullmat(`se_4'), sqrt(B[4,4])
 reg `var' TD1 TD2 TD3 TD4  i.`strat_id' `if', nocons vce(cluster `clus_id')
 test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
 mat `d_p2'  = nullmat(`d_p2'),r(p)
}
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
 mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
 eret mat `mat' = ``mat''
}
end

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"

keep SchoolID  tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25 treatment treatarm SchoolID DistrictID FGS_T3 FGS_T7
rename DistrictID DistID
/*t23 t24 t25 */
gen TeacherCertificate=(t16>=3) & !missing(t16)

label var t21 "Travel time from house to school"
label var tchsex "Male"
label var TeacherCertificate "Teaching Certificate"
recode tchsex (2=0)
replace t05=2013-t05
replace t07=2013-t07
label var t05 "Age (in 2013)"
label var t07 "Years of experience (in 2013)"



*
recode t10 (2=0)
 eststo clear
eststo: my_ptest  tchsex t05 t07 t21 TeacherCertificate if FGS_T3==1, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryTeacherTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$latexcodesfinals/summaryTeacherTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

 eststo clear
eststo: my_ptest  tchsex t05 t07 t21 TeacherCertificate if FGS_T7==1, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryTeacherTotal_T7.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$latexcodesfinals/summaryTeacherTotal_T7.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


local studentcontrol Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza
label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Normalized Swahili test score"
label var LagZ_hisabati "Normalized Math test score"
label var LagZ_kiingereza "Normalized English test score"
replace attendance_T3=1-attendance_T3
replace attendance_T7=1-attendance_T7

label var attendance_T3 "Attrited in year 1"
label var attendance_T7 "Attrited in year 2"


eststo clear
eststo: my_ptest  `studentcontrol' attendance_T3 attendance_T7, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryStudentsTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$latexcodesfinals/summaryStudentsTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/School.dta", replace
label var s1451_T1 "Kitchen"
label var s1452_T1 "Library"
label var s1453_T1 "Playground"
label var s1454_T1 "Staff room"
label var s1455_T1 "Outer wall"
label var s1456_T1 "Newspaper"
label var SingleShift_T1 "Single shift"
label var computersYN_T1 "Computers"
label var s120_T1  "Electricity"
label var s118_T1 "Classes outside"
label var PipedWater_T1 "Piped Water"
label var NoWater_T1 "No Water"
label var ToiletsStudents_T1 "Toilets/Students"
label var ClassRoomsStudents_T1 "Classrooms/Students"
replace TeacherStudents_T1=1/TeacherStudents_T1
label var TeacherStudents_T1 "Students/Teachers"
label var s188_T1 "Breakfast"
label var s175_T1 "Preschool"
label var s200_T1 "Track students"
label var s108_T1 "Urban"
label var SizeSchoolCommittee_T1 "Size School Committee"
label var KeepRecords_T1 "Spending records"
label var noticeboard_T1 "Noticeboard with spending information"
label var PropCommitteeFemale_T1 "Female/Committee"
label var PropCommitteeTeachers_T1 "Teacher/Committee"
label var PropCommitteeParents_T1 "Parents/Committee"
label var StudentsTotal_T1 "Enrolled students"


 label var InfrastructureIndex_T1 "Infrastructure Index (PCA)"
  label var PTR_T1 "Pupil-teacher ratio"
 
 *local schoolcontro2lb   InfrastructureIndex  s120_T1   SingleShift_T1  TeacherStudents_T1  s200_T1 s108_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 
local schoolcontro2lb PTR_T1  SingleShift_T1 s120_T1 InfrastructureIndex_T1 s108_T1 StudentsTotal_T1
eststo clear
eststo: my_ptest  `schoolcontro2lb', by(treatarm) clus_id(SchoolID) strat_id(DistrictID)
esttab using "$latexcodesfinals/summarySchoolTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$latexcodesfinals/summarySchoolTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


clear 
use "$base_out/Consolidated/Household_Baseline2013.dta", clear
keep HHID wrokyn asset_1 expn12 DistID upidst SchoolID IndexPoverty IndexKowledge IndexEngagement  treatment treatarm  HHSize upidst
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2013.dta", update keepus(HHID wrokyn HHSize DistID upidst SchoolID IndexPoverty treatment treatarm)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2014.dta", update keepus(HHID wrokyn HHSize expn13 DistID upidst SchoolID IndexPoverty IndexEngagement treatment treatarm)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2014.dta", update keepus(HHID wrokyn HHSize DistID upidst SchoolID IndexPoverty  treatment treatarm)
drop _merge
gen LagExpenditure=expn13
replace LagExpenditure=expn12 if LagExpenditure==. & expn12!=.

label var IndexPoverty "Wealth Index (PCA)"
label var IndexEngagement "Engagement Index (PCA)"
label var HHSize "HH size"
replace LagExpenditure=LagExpenditure
label var LagExpenditure "Expenditure (prior to intervention) in TZS"


eststo clear
my_ptest HHSize IndexPoverty LagExpenditure , by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryHouseholds.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc))  d_p2(star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$latexcodesfinals/summaryHouseholds.csv", label replace  nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc))  d_p2(star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


**************************************************************
************************ TABLE 2 - SUMMARY TWAWEZA EXPENDITURE
**************************************************************


capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2 , nocons vce(cluster `clus_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p'  = nullmat(`d_p'),r(p)
 lincom (TD1-TD2)
 mat `se_3' = nullmat(`se_3'), r(se)
 qui estpost tabstat `var' , by(`by') statistics(mean sem)
 matrix A=e(mean)
 matrix B=e(semean)
 mat `mu_1' = nullmat(`mu_1'), A[1,1]
 mat `mu_2' = nullmat(`mu_2'), A[1,2]
 mat `mu_3' = nullmat(`mu_3'), A[1,1]-A[1,2]
 mat `se_1' = nullmat(`se_1'), B[1,1]
 mat `se_2' = nullmat(`se_2'), B[1,2]

}
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 se_3 d_p {
 mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 se_3  d_p {
 eret mat `mat' = ``mat''
}
end


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"

foreach i in 3 7{
	egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
	replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
	label variable gdrctb`grade'PS_T3 "\\$ Textbooks/Student Grd `grade'"
	label variable gdrctb`grade'PS_T7 "\\$ Textbooks/Student Grd `grade'"
}


foreach i in 3 7{
label variable ExpendText_T`i' "\\$ Textbooks/Student"  
label variable administrative_expensesPS_T`i' "\\$ Admin./Student"
label variable teaching_aid_expensesPS_T`i' "\\$ Teaching Aid/Student"
label variable student_expensesPS_T`i' "\\$ Student/Student"
label variable Teacher_expensesPS_T`i' "\\$ Teacher/Student"
label variable Construction_expensesPS_T`i' "\\$ Construction/Student" 

label variable administrative_twawezaPS_T`i' "\\$ Admin./Student"
label variable teaching_aid_twawezaPS_T`i' "\\$ Teaching Aid/Student"
label variable student_twawezaPS_T`i' "\\$ Student/Student"
label variable Teacher_twawezaPS_T`i' "\\$ Teacher/Student"
label variable Construction_twawezaPS_T`i' "\\$ Construction/Student" 

label variable studentsTeacherRatio_T`i' "Teacher Ratio"
label variable studentsVolunteerRatio_T`i' "Vol. Ratio"
label variable strategicly_change_teachers_T`i' "HT changed teachers"
label variable strategicly_change_teachers_T`i' "HT changed students"
label variable administrative_expenses_T`i' "\\$ Admin."
label variable student_expenses_T`i' "\\$ Student"  
label variable Teacher_expenses_T`i' "\\$ Teacher"
}

label var TimesCommitteeMet2013_T3 "No. Committee meetings"
label var debtyn_T3 "Debts"
label var ifnbyn_Since_T1 "Notice Board"
label var ifscyn_Since_T1 "Committee Meetings"
label var ifpmyn_Since_T1 "Parents Meetings"
label var ifotyn_Since_T1 "Others"


local treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
*local varcontrol  male higher_degree t05 t07 t08 t10 t21 t23
local schoolcontrol s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1


foreach var in administrative_twaweza student_twaweza teaching_aid_twaweza Teacher_twaweza Construction_twaweza Savings_twaweza{
	capture replace `var'_T3=. if treatarm>=3
	capture replace `var'_T7=. if treatarm>=3
	capture replace `var'PS_T3=. if treatarm>=3
	capture replace `var'PS_T7=. if treatarm>=3
}

egen TotalExpenditure_T3=rowtotal(administrative_expensesPS_T3 student_expensesPS_T3 teaching_aid_expensesPS_T3 Teacher_expensesPS_T3 Construction_expensesPS_T3), missing
egen TotalExpenditure_T7=rowtotal(administrative_expensesPS_T7 student_expensesPS_T7 teaching_aid_expensesPS_T7 Teacher_expensesPS_T7 Construction_expensesPS_T7), missing

label var TotalExpenditure_T3 Total
label var TotalExpenditure_T7 Total

egen TotalExpenditureTwa_T3=rowtotal(administrative_twawezaPS_T3 student_twawezaPS_T3 teaching_aid_twawezaPS_T3 Teacher_twawezaPS_T3 Construction_twawezaPS_T3), missing
egen TotalExpenditureTwa_T7=rowtotal(administrative_twawezaPS_T7 student_twawezaPS_T7 teaching_aid_twawezaPS_T7 Teacher_twawezaPS_T7 Construction_twawezaPS_T7), missing

label var TotalExpenditureTwa_T3 Total
label var TotalExpenditureTwa_T7 Total

foreach var in TotalExpenditureTwa administrative_twawezaPS student_twawezaPS teaching_aid_twawezaPS Teacher_twawezaPS Construction_twawezaPS{
	gen `var'_TT=`var'_T3+`var'_T7
	_crcslbl `var'_TT `var'_T7 
}

gen Unaccounted_twawezaPS_T3=10000-TotalExpenditureTwa_T3
gen Unaccounted_twawezaPS_T7=10000-TotalExpenditureTwa_T7

label var Unaccounted_twawezaPS_T3 "Unspent/unaccounted funds"
label var Unaccounted_twawezaPS_T7 "Unspent/unaccounted funds"

keep TotalExpenditureTwa_* administrative_twawezaPS_* student_twawezaPS_* teaching_aid_twawezaPS_* Teacher_twawezaPS_* Construction_twawezaPS_* Unaccounted_twawezaPS_* treatarm SchoolID

reshape long TotalExpenditureTwa_T@ administrative_twawezaPS_T@ student_twawezaPS_T@ teaching_aid_twawezaPS_T@ Teacher_twawezaPS_T@ Construction_twawezaPS_T@ Unaccounted_twawezaPS_T@, i(SchoolID) j(time) string

label variable TotalExpenditureTwa_T "\\$ Total/Student"  
label variable administrative_twawezaPS_T "\\$ Admin./Student"
label variable teaching_aid_twawezaPS_T "\\$ Teaching Aid/Student"
label variable student_twawezaPS_T "\\$ Student/Student"
label variable Teacher_twawezaPS_T "\\$ Teacher/Student"
label variable Construction_twawezaPS_T "\\$ Construction/Student" 
label var Unaccounted_twawezaPS_T "Unspent/unaccounted funds"


drop if time=="T"
eststo clear
estpost  tabstat TotalExpenditureTwa_T administrative_twawezaPS_T student_twawezaPS_T teaching_aid_twawezaPS_T Teacher_twawezaPS_T Construction_twawezaPS_T  ///
 Unaccounted_twawezaPS_T if treatarm==2, by(time) statistics(mean semean) columns(statistics) listwise

*Will need to delete first row later
esttab using "$latexcodesfinals/SummaryTwawezaExp_CG.tex", main(mean %9.2fc) aux(semean %9.2fc) nolines nostar unstack noobs nonote nonumber fragment collabels(none) replace  label nogaps nomtitles nodepvars  

********************************************************************
*********************** TABLE 3 - EXPENDITURE, SUBSTITUTION, PARENT
********************************************************************




use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
gen TreatmentControl=0 
replace TreatmentControl=1 if treatment=="Control"
label var TreatmentControl "Control"

foreach i in 3 7{
	egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
	replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
	label variable gdrctb`grade'PS_T3 "/\$ Textbooks/Student Grd `grade'"
	label variable gdrctb`grade'PS_T7 "/\$ Textbooks/Student Grd `grade'"
}


foreach i in 3 7{
label variable ExpendText_T`i' "/\$ Textbooks/Student"  
label variable administrative_expensesPS_T`i' "/\$ Admin./Student"
label variable teaching_aid_expensesPS_T`i' "/\$ Teaching Aid/Student"
label variable student_expensesPS_T`i' "/\$ Student/Student"
label variable Teacher_expensesPS_T`i' "/\$ Teacher/Student"
label variable Construction_expensesPS_T`i' "/\$ Construction/Student" 

label variable administrative_twawezaPS_T`i' "/\$ Admin./Student"
label variable teaching_aid_twawezaPS_T`i' "/\$ Teaching Aid/Student"
label variable student_twawezaPS_T`i' "/\$ Student/Student"
label variable Teacher_twawezaPS_T`i' "/\$ Teacher/Student"
label variable Construction_twawezaPS_T`i' "/\$ Construction/Student" 

label variable studentsTeacherRatio_T`i' "Teacher Ratio"
label variable studentsVolunteerRatio_T`i' "Vol. Ratio"
label variable strategicly_change_teachers_T`i' "HT changed teachers"
label variable strategicly_change_teachers_T`i' "HT changed students"
label variable administrative_expenses_T`i' "/\$ Admin."
label variable student_expenses_T`i' "/\$ Student"  
label variable Teacher_expenses_T`i' "/\$ Teacher"
}

label var TimesCommitteeMet2013_T3 "No. Committee meetings"
label var debtyn_T3 "Debts"
label var ifnbyn_Since_T1 "Notice Board"
label var ifscyn_Since_T1 "Committee Meetings"
label var ifpmyn_Since_T1 "Parents Meetings"
label var ifotyn_Since_T1 "Others"



egen TotalExpenditure_T3=rowtotal(administrative_expensesPS_T3 student_expensesPS_T3 teaching_aid_expensesPS_T3 Teacher_expensesPS_T3 Construction_expensesPS_T3), missing
egen TotalExpenditure_T7=rowtotal(administrative_expensesPS_T7 student_expensesPS_T7 teaching_aid_expensesPS_T7 Teacher_expensesPS_T7 Construction_expensesPS_T7), missing

label var TotalExpenditure_T3 Total
label var TotalExpenditure_T7 Total

egen TotalExpenditureTwa_T3=rowtotal(administrative_twawezaPS_T3 student_twawezaPS_T3 teaching_aid_twawezaPS_T3 Teacher_twawezaPS_T3 Construction_twawezaPS_T3), missing
egen TotalExpenditureTwa_T7=rowtotal(administrative_twawezaPS_T7 student_twawezaPS_T7 teaching_aid_twawezaPS_T7 Teacher_twawezaPS_T7 Construction_twawezaPS_T7), missing

label var TotalExpenditureTwa_T3 Total
label var TotalExpenditureTwa_T7 Total

egen  TotalSub_M_T3=rowtotal(GovermentCG_M_T3 GovermentOther_M_T3 LocalGoverment_M_T3 NGO_M_T3 Parents_M_T3 Other_M_T3), missing
egen  TotalSub_M_T7=rowtotal(GovermentCG_M_T7 GovermentOther_M_T7 LocalGoverment_M_T7 NGO_M_T7 Parents_M_T7 Other_M_T7), missing

egen  TotalSub_D_T1=rowtotal(GovermentCG_D_T1 GovermentOther_D_T1 LocalGoverment_D_T1 NGO_D_T1 Parents_D_T1 Other_D_T1), missing
egen  TotalSub_D_T3=rowtotal(GovermentCG_D_T3 GovermentOther_D_T3 LocalGoverment_D_T3 NGO_D_T3 Parents_D_T3 Other_D_T3), missing
egen  TotalSub_D_T7=rowtotal(GovermentCG_D_T7 GovermentOther_D_T7 LocalGoverment_D_T7 NGO_D_T7 Parents_D_T7 Other_D_T7), missing

replace TotalSub_D_T1=(TotalSub_D_T1>0) if !missing(TotalSub_D_T1)
replace TotalSub_D_T3=(TotalSub_D_T3>0) if !missing(TotalSub_D_T3)
replace TotalSub_D_T7=(TotalSub_D_T7>0) if !missing(TotalSub_D_T7)

label var TotalSub_M_T3 Total
label var TotalSub_M_T7 Total

label var TotalSub_D_T3 Any
label var TotalSub_D_T7 Any


rename administrative_expensesPS* admin_expPS*
rename administrative_twawezaPS* admin_exptwaPS*
rename teaching_aid_twawezaPS* teaching_aid_twaPS*
rename Construction_twawezaPS* Construction_twaPS*

** NOW IW WANT DEPVAR2 TO BE ON TOP OF TWA EXPENDITURE


gen TotalExpenditure_T_T3=TotalExpenditure_T3
gen admin_expPS_T_T3=admin_expPS_T3
gen student_expensesPS_T_T3=student_expensesPS_T3
gen teaching_aid_expensesPS_T_T3=teaching_aid_expensesPS_T3
gen Teacher_expensesPS_T_T3=Teacher_expensesPS_T3
gen Construction_expensesPS_T_T3=Construction_expensesPS_T3

gen TotalExpenditure_T_T7=TotalExpenditure_T7
gen admin_expPS_T_T7=admin_expPS_T7
gen student_expensesPS_T_T7=student_expensesPS_T7
gen teaching_aid_expensesPS_T_T7=teaching_aid_expensesPS_T7
gen Teacher_expensesPS_T_T7=Teacher_expensesPS_T7
gen Construction_expensesPS_T_T7=Construction_expensesPS_T7

replace TotalExpenditure_T3=TotalExpenditure_T3-TotalExpenditureTwa_T3
replace admin_expPS_T3=admin_expPS_T3-admin_exptwaPS_T3
replace student_expensesPS_T3=student_expensesPS_T3-student_twawezaPS_T3
replace teaching_aid_expensesPS_T3=teaching_aid_expensesPS_T3-teaching_aid_twaPS_T3
replace Teacher_expensesPS_T3=Teacher_expensesPS_T3-Teacher_twawezaPS_T3
replace Construction_expensesPS_T3=Construction_expensesPS_T3-Construction_twaPS_T3

replace TotalExpenditure_T7=TotalExpenditure_T7-TotalExpenditureTwa_T7
replace admin_expPS_T7=admin_expPS_T7-admin_exptwaPS_T7
replace student_expensesPS_T7=student_expensesPS_T7-student_twawezaPS_T7
replace teaching_aid_expensesPS_T7=teaching_aid_expensesPS_T7-teaching_aid_twaPS_T7
replace Teacher_expensesPS_T7=Teacher_expensesPS_T7-Teacher_twawezaPS_T7
replace Construction_expensesPS_T7=Construction_expensesPS_T7-Construction_twaPS_T7

rename teaching_aid_expenses* teaching_aid_exp*
rename Construction_expenses* Construction_exp*

global depvars1 TotalExpenditureTwa admin_exptwaPS student_twawezaPS teaching_aid_twaPS Teacher_twawezaPS Construction_twaPS 
global depvars2 TotalExpenditure admin_expPS student_expensesPS teaching_aid_expPS Teacher_expensesPS Construction_expPS
global depvars2_b TotalExpenditure_T admin_expPS_T student_expensesPS_T teaching_aid_expPS_T Teacher_expensesPS_T Construction_expPS_T       
global depvars3 TotalSub_M GovermentCG_M GovermentOther_M LocalGoverment_M NGO_M Parents_M Other_M



foreach var in $schoolcontrol{
sum `var' if treatment=="Control"
	replace `var'=`var'-r(mean)
}

***** 1, TOTAL EXPENDITURE FROM THE GRANT
eststo clear
foreach var in $depvars1{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo twa_`var': reg `var'_T $treatmentlist  (i.DistrictID)##T2, vce(cluster SchoolID)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab twa* using "$latexcodesfinals/School_ExpenditureTWA.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab twa* using "$latexcodesfinals/School_ExpenditureTWA.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")



foreach time in 3 7{
	foreach var in $depvars1{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo twa_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol (i.DistrictID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab twa*`time' using "$latexcodesfinals/School_ExpenditureTWA`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab twa*`time' using "$latexcodesfinals/School_ExpenditureTWA`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")
}

 
***** 2, TOTAL EXPENDITURE (not grant)
foreach var in $depvars2{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo s_`var': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab s* using "$latexcodesfinals/School_Expenditure.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab s* using "$latexcodesfinals/School_Expenditure.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

foreach time in 3 7{
	foreach var in $depvars2{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo s_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab s*`time' using "$latexcodesfinals/School_Expenditure_`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab s*`time' using "$latexcodesfinals/School_Expenditure_`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")
}
preserve
keep TotalExpenditure* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
save "$base_out/Consolidated/SchoolExpenditure_HH.dta", replace
restore


***** 3, TOTAL EXPENDITURE (school_leve)
foreach var in $depvars2_b{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo ts_`var': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab ts* using "$latexcodesfinals/School_Expenditure_Total.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc a2 a2 a2 a2 a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab ts* using "$latexcodesfinals/School_Expenditure_Total.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc a2 a2 a2 a2 a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

foreach time in 3 7{
	foreach var in $depvars2_b{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo ts_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab ts*`time' using "$latexcodesfinals/School_Expenditure_Total_`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab ts*`time' using "$latexcodesfinals/School_Expenditure_Total_`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")
}


***** Not in main table, but, SUBSTITUTION


foreach var in $depvars3{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo sub_`var': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab sub_* using "$latexcodesfinals/OtherFunding.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab sub_* using "$latexcodesfinals/OtherFunding.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

foreach time in 3 7{
	foreach var in $depvars3{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo sub_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab sub_*`time' using "$latexcodesfinals/OtherFunding_`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab sub_*`time' using "$latexcodesfinals/OtherFunding_`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")
}
*** 4, PARENTAL


use "$base_out/Consolidated/Household.dta", clear
drop if treatment==""
drop if DistID==.
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"



global depvars   Expenditure_FC AdultAttendsSchoolsMeeting AdultMeetsTeacher AdultGivesSchool AdultAtHome FCTutoring breakfast MoreBooks
global treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
global varcontrol  bnkacc prdyyn  ltcbyn workyn NumHHMembers wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8 
*wrokyn

global depvars2  Expenditure_FC expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139

label var expn131_T3 "Fees"
label var expn131_T7 "Fees"

label var expn132_T3 "Textbooks"
label var expn132_T7 "Textbooks"

label var expn133_T3 "Other books"
label var expn133_T7 "Other books"

label var expn134_T3 "Supplies"
label var expn134_T7 "Supplies"

label var expn135_T3 "Uniforms"
label var expn135_T7 "Uniforms"

label var expn136_T3 "Tutoring"
label var expn136_T7 "Tutoring"

label var expn137_T3 "Transport"
label var expn137_T7 "Transport"

label var expn138_T3 "Food"
label var expn138_T7 "Food"

label var expn139_T3 "Others"
label var expn139_T7 "Others"


foreach var in $varcontrol{
	gen Lag`var'=.
	replace Lag`var'=`var'_T1 if !missing(`var'_T1)
	replace Lag`var'=`var'_T5 if !missing(`var'_T5) & missing(Lag`var')
}


foreach j in 3 7{
	label var MoreBooks_T`j' "More Books Provided"
	label var Expenditure_FC_T`j' "Total Expenditure"
	label var AdultAttendsSchoolsMeeting_T`j' "Attend Meetings"
	label var AdultMeetsTeacher_T`j' "Meet Teacher"
	label var AdultGivesSchool_T`j' "Donate"
	label var AdultAtHome_T`j' "Adult at home"
	label var FCTutoring_T`j' "Tutoring"
	label var breakfast_T`j' "Breakfast"
}

sum Expenditure_FC_T3, d
replace Expenditure_FC_T3=r(p95) if Expenditure_FC_T3>r(p95) & !missing(Expenditure_FC_T3)
sum Expenditure_FC_T7, d
replace Expenditure_FC_T7=r(p95) if Expenditure_FC_T7>r(p95) & !missing(Expenditure_FC_T7)
		
sum Expenditure_FC_2012_T1, d
replace Expenditure_FC_2012_T1=r(p95) if Expenditure_FC_2012_T1>r(p95) & !missing(Expenditure_FC_2012_T1)
sum Expenditure_FC_2013_T5, d
replace Expenditure_FC_2013_T5=r(p95) if Expenditure_FC_2013_T5>r(p95) & !missing(Expenditure_FC_2013_T5)
		
		
gen LagExpenditure=Expenditure_FC_2012_T1 
replace LagExpenditure=Expenditure_FC_2013_T5 if LagExpenditure==. & Expenditure_FC_2013_T5!=.

global varcontrol Lagbnkacc Lagprdyyn  Lagltcbyn Lagworkyn LagNumHHMembers Lagwall_mud Lagfloor_mud Lagroof_durable LagimproveWater LagimproveSanitation LagHHElectricty Lagasset_1 Lagasset_2 Lagasset_3 Lagasset_4 Lagasset_5 Lagasset_6 Lagasset_7 Lagasset_8 LagExpenditure
*global varcontrol IndexPoverty HHSize IndexEngagement LagExpenditure
	
drop *T5
drop *T1	

/*this is new, simply collapse the data*/
keep Expenditure_FC* expn13* $treatmentlist $varcontrol DistID SchoolID treatment
collapse (mean) Expenditure_FC* expn13* $varcontrol $treatmentlist, by(SchoolID DistID treatment)
label var TreatmentCG "Grants"
label var TreatmentCOD "Incentives"
label var TreatmentBoth "Combo"
merge 1:1 SchoolID using "$base_out/Consolidated/SchoolExpenditure_HH.dta"

foreach var in $depvars2{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-792=.)
	eststo hh_`var': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab hh* using "$latexcodesfinals/Household.tex", se ar2 booktabs label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab hh* using "$latexcodesfinals/Household.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars2{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-792=.)
		eststo hh_`var'_`time': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	
	esttab hh*_`time' using "$latexcodesfinals/Household_`time'.tex", se ar2 booktabs label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc a2 a2 a2 a2 a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab hh*_`time' using "$latexcodesfinals/Household_`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc a2 a2 a2 a2 a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}





save "$base_out/Consolidated/SchoolExpenditure_HH.dta", replace

gen PerChildExpenditure_T3=TotalExpenditure_T_T3+Expenditure_FC_T3
gen PerChildExpenditure_T7=TotalExpenditure_T_T7+Expenditure_FC_T7

eststo perchild_T3:  reg PerChildExpenditure_T3 $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T3 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

eststo perchild_T7: reg PerChildExpenditure_T7 $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T7 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

reshape long PerChildExpenditure_T@, i(SchoolID) j(T) string
encode T, gen(T2)

eststo perchild:  reg PerChildExpenditure_T $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]


*********** COMBINE THE RESULTS **************


esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hh_Expenditure_FC perchild  using "$latexcodesfinals/Subs.tex", se ar2 booktabs label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab  twa_TotalExpenditureTwa   s_TotalExpenditure ts_TotalExpenditure_T hh_Expenditure_FC perchild  using "$latexcodesfinals/Subs.csv", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

foreach time in 3 7{
	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hh_Expenditure_FC_`time' perchild_T`time'  using "$latexcodesfinals/Subs_`time'.tex", se ar2 booktabs label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hh_Expenditure_FC_`time' perchild_T`time'  using "$latexcodesfinals/Subs_`time'.csv", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "Combo-Grants" "p-value (\$H_0\$:Combo-Grants=0)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")
}



esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hh_Expenditure_FC perchild  using "$latexcodesfinals/Subs_CG.tex", se ar2 booktabs label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG  ) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hh_Expenditure_FC_`time' perchild_T`time'  using "$latexcodesfinals/Subs_CG_`time'.tex", se ar2 booktabs label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG ) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	}


********************************************************************
*********************** textbooks
********************************************************************
use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge



gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
gen TreatmentControl=0 
replace TreatmentControl=1 if treatment=="Control"
label var TreatmentControl "Control"


foreach i in 3 7{
egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
label variable gdrctb`grade'PS_T3 "\\$ Textbooks/Student Grd `grade'"
label variable gdrctb`grade'PS_T7 "\\$ Textbooks/Student Grd `grade'"
}

forvalue grade=1/7{
gen gdrctbPS_T3`grade'= gdrctb`grade'PS_T3
gen gdrctbPS_T7`grade'= gdrctb`grade'PS_T7
}


preserve
keep gdrctbPS_T* SchoolID DistrictID $schoolcontrol $treatmentlist treatment
reshape long gdrctbPS_T3 gdrctbPS_T7, i(SchoolID) j(Grade)
reshape long gdrctbPS_T@ , i(SchoolID Grade) j(T)
drop if T!=3 & T!=7
gen FG=(Grade<=3)
eststo books_0: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==0, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum gdrctbPS_T if treatment=="Control" & FG==0
estadd scalar ymean2=r(mean)

eststo books_1: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==1, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum gdrctbPS_T if treatment=="Control" & FG==1
estadd scalar ymean2=r(mean)

rename TreatmentCG TreatmentCG2
rename TreatmentCOD TreatmentCOD2
rename TreatmentBoth TreatmentBoth2
eststo books_2: reg gdrctbPS_T c.($treatmentlist)##c.FG c.FG##(c.($schoolcontrol)  i.DistrictID)##T, vce(cluster SchoolID)
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]
sum gdrctbPS_T if treatment=="Control" & FG==1
scalar def mean1=r(mean)
sum gdrctbPS_T if treatment=="Control" & FG==0
scalar def mean2=r(mean)
estadd scalar ymean2=scalar(mean1)-scalar(mean2)

esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2.tex", se booktabs label fragment nolines nogaps nomtitles ///
rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))

esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2.csv", se label fragment nolines nogaps nomtitles ///
rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))
restore



foreach time in 3 7{
	preserve
	keep gdrctbPS_T* SchoolID DistrictID $schoolcontrol $treatmentlist treatment
	reshape long gdrctbPS_T3 gdrctbPS_T7, i(SchoolID) j(Grade)
	reshape long gdrctbPS_T@ , i(SchoolID Grade) j(T)
	drop if T!=3 & T!=7
	drop if T!=`time'
	gen FG=(Grade<=3)

	eststo books_0: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==0, vce(cluster SchoolID)
	test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	sum gdrctbPS_T if treatment=="Control" & FG==0
	estadd scalar ymean2=r(mean)

	eststo books_1: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==1, vce(cluster SchoolID)
	test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	sum gdrctbPS_T if treatment=="Control" & FG==1
	estadd scalar ymean2=r(mean)

	rename TreatmentCG TreatmentCG2
	rename TreatmentCOD TreatmentCOD2
	rename TreatmentBoth TreatmentBoth2
	eststo books_2: reg gdrctbPS_T c.($treatmentlist)##c.FG c.FG##(c.($schoolcontrol)  i.DistrictID)##T, vce(cluster SchoolID)
	test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]
	test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]
	sum gdrctbPS_T if treatment=="Control" & FG==1
	scalar def mean1=r(mean)
	sum gdrctbPS_T if treatment=="Control" & FG==0
	scalar def mean2=r(mean)
	estadd scalar ymean2=scalar(mean1)-scalar(mean2)


	esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2_`time'.tex", se booktabs label fragment nolines nogaps nomtitles ///
	rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
	keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
	b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
	coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
	"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))

	esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2_`time'.csv", se label fragment nolines nogaps nomtitles ///
	rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
	keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
	b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
	coeflabels(TreatmentBoth "Combo" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
	"Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$=Interaction=0)" "Combo-Grants" "p-value (\$H_0\$=Combo-Grants=0)"))
	restore
}



/* ///
"Combo-CG" "p-value (\$H_0\$=Combo-CG=0)" ///
"Combo-CG (FY)" "p-value (\$H_0\$=Combo-CG (FY)=0)")) /// */

********************************************************************
*********************** MAIN RESULTS ON TEST SCORES
********************************************************************

**********************
**************Student
************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

label var Z_kiswahili_T3 Swahili
label var Z_kiswahili_T7 Swahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"


eststo clear
foreach time in T3 T7{
foreach var in $AggregateDep_Karthik{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol, vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}  
}
 


esttab  using "$latexcodesfinals/RegTestScores_NoControls.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab  using "$latexcodesfinals/RegTestScores_NoControls.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

eststo clear
foreach time in T3 T7{
foreach var in $AggregateDep_Karthik{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}
esttab *_`time' using "$latexcodesfinals/RegTestScores_`time'_CG.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   ///
keep(TreatmentCG)  stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes  
}
 


esttab  using "$latexcodesfinals/RegTestScores.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab  using "$latexcodesfinals/RegTestScores.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab  using "$latexcodesfinals/RegTestScores_CG.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(TreatmentCG)  stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes

pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist  Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}

label var Z_kiswahili_T4 Swahili
label var Z_kiswahili_T8 Swahili
label var Z_kiingereza_T4 English
label var Z_kiingereza_T8 English
label var Z_hisabati_T4 Math
label var Z_hisabati_T8 Math
label var Z_ScoreFocal_T4 "Combined (PCA)"
label var Z_ScoreFocal_T8 "Combined (PCA)"

eststo clear
foreach time in T4 T8{
foreach var in $AggregateDep_Karthik{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist_int} i.GradeID_`time'  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
}
}

esttab  using "$latexcodesfinals/RegTestScores_highstakes.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Incentives)" "p-value (\$H_0\$:Combo-Incentives=0)" "") star(suma)) ///
nonotes


*************************************************************
**************Diff between high-and-low**********************
*************************************************************

eststo clear
foreach var in $AggregateDep_Karthik{
		capture drop resid_T3
		reg `var'_T3  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID)
		predict resid_T3,resid
				
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time
		save `file1'
		restore
		
		capture drop resid_T4
		reg `var'_T4  i.GradeID_T4  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		predict resid_T4,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T4 resid
		keep resid SchoolID  $treatmentlist_int time
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 if TreatmentCG==.
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		eststo `var'_yr1:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
		test (_b[c.TreatmentBoth2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentBoth2#c.time]
		
		test (_b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentCOD2#c.time]
		
		test (_b[TreatmentCG2]+_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[TreatmentCG2]-_b[c.TreatmentBoth2#c.time]+ _b[c.TreatmentCOD2#c.time]
		restore
		
}



foreach var in $AggregateDep_Karthik{
		capture drop resid_T7
		reg `var'_T7  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID)
		predict resid_T7,resid
		reg resid_T7	$treatmentlist		
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T7 resid
		keep resid SchoolID  $treatmentlist time
		save `file1'
		restore
		
		reg `var'_T8 $treatmentlist_int i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		capture drop resid_T8
		reg `var'_T8 i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		predict resid_T8,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T8 resid
		keep resid SchoolID  $treatmentlist_int time
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 if TreatmentCG==.
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		eststo `var'_yr2:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
		test (_b[c.TreatmentBoth2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentBoth2#c.time]
		
		test (_b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentCOD2#c.time]
		
		test (_b[TreatmentCG2]+_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=_b[TreatmentCG2]-_b[c.TreatmentBoth2#c.time]+ _b[c.TreatmentCOD2#c.time]
		restore
		
}


esttab *_yr1 *_yr2 using "$latexcodesfinals/RegTestScores_Difference.tex", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep( )  ///
coeflabel(treatmentCOD "Incentives(High-low)"  TreatmentBoth "Combo(High-low)") ///
stats( suma2 std_err2 suma1 std_err1 suma3 std_err3, fmt(a2 a2 a2 a2 a2 a2) labels("Incentives(High-low)" "p-value(\$\Delta\$Incentives=0)" "Combo(High-low)" "p-value(\$\Delta\$Combo=0)" "Interaction(High-low)" "p-value(\$\Delta\$Interaction=0)")) ///
nonotes


*************************************************************
*************************************************************


eststo clear
foreach time in T3 T7{
foreach var in passmath passkis passeng{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}  
}
 


esttab  using "$latexcodesfinals/RegPassTest.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   nomtitles  nolines nogaps  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab  using "$latexcodesfinals/RegPassTest_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   nomtitles  nolines nogaps  ///
keep(TreatmentCOD) stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes

esttab  using "$latexcodesfinals/RegPassTest.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   nomtitles  nolines nogaps  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes




foreach time in T3 T7{
	capture matrix drop Coef
	capture matrix drop Error
	foreach var in $AggregateDep_Karthik{
		reg `var'_`time'  $treatmentlist i.DistID $studentcontrol  $schoolcontrol $HHcontrol  , vce(cluster SchoolID) 
		lincom _b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		scalar error2=r(se)
		scalar error2=error2^2
		mat B=e(b)
		mat E=vecdiag(e(V))
		mat Coef  = nullmat(Coef) \ [B[1,1..3],r(estimate)]
		mat Error  = nullmat(Error) \ [E[1,1..3], error2]
	}
	preserve
	clear
	svmat Coef
	export delimited using "$latexcodesfinals/Coef_`time'.csv", replace
	clear
	svmat Error
	export delimited using "$latexcodesfinals/Error_`time'.csv", replace
	clear
	restore
}



eststo clear
foreach time in T3 T7{
foreach var in $AggregateDep_Karthik{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol if LagGrade>=2 & LagGrade<=3, vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}  
}
 


esttab  using "$latexcodesfinals/RegTestScores_2yeareffect.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab  using "$latexcodesfinals/RegTestScores_2yeareffect.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes


preserve
keep Z_hisabati* Z_kiswahili* Z_kiingereza* Z_ScoreFocal* $treatmentlist DistID  $schoolcontrol HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  upid SchoolID
drop *T8
drop *T4
drop *T5

gen TreatCG_Total=(TreatmentCG==1 |  TreatmentBoth==1)
gen TreatCOD_Total=(TreatmentCOD==1 |  TreatmentBoth==1)
gen TreatCGCOD_Total=TreatCG_Total*TreatCOD_Total

label var TreatCG_Total "Grants"
label var TreatCOD_Total "Incentives"
label var TreatCGCOD_Total "Interaction(Combo-Grants-Incentives)"

reg   Z_kiingereza_T7 TreatmentCG TreatmentCOD TreatmentBoth i.LagGrade  i.DistID  $studentcontrol  $schoolcontrol $HHcontrol , vce(cluster SchoolID) 
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
reghdfe   Z_kiingereza_T7 TreatCG_Total TreatCOD_Total TreatCGCOD_Total   $studentcontrol  $schoolcontrol $HHcontrol , vce(cluster SchoolID) ab(LagGrade  DistID)
matrix A=e(V)
matrix B=vecdiag(A)
matmap B C, map(sqrt(@))
matrix sd = C*1.959964
		
		
		
eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo:   reg `var'_`time' TreatCG_Total TreatCOD_Total TreatCGCOD_Total i.LagGrade  i.DistID  $studentcontrol  $schoolcontrol $HHcontrol , vce(cluster SchoolID) 
		estadd ysumm
		matrix A=e(V)
		matrix B=vecdiag(A)
		matmap B C, map(sqrt(@))
		estadd matrix sd = C*1.959964

		esttab  using "$latexcodesfinals/RegInteractionsExplictly.tex", ar2 booktabs label b(%9.2fc)aux(sd) nocon nonumber /// 
		star(* 0.05) ///
		replace  mgroups("Year 1" "Year 2", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
		keep(TreatCG_Total TreatCOD_Total TreatCGCOD_Total) stats(N, labels("N. of obs.")) ///
		nonotes
	}  
}
foreach var in Z_kiingereza_T3 Z_kiingereza_T7 Z_hisabati_T3 Z_hisabati_T7 Z_kiswahili_T3 Z_kiswahili_T7 Z_ScoreFocal_T3 Z_ScoreFocal_T7  TreatCG_Total TreatCOD_Total TreatCGCOD_Total{
	reg `var' i.LagGrade  i.DistID  $studentcontrol  $schoolcontrol $HHcontrol , vce(cluster SchoolID) 
predict `var'_resid,resid
}
keep *_resid SchoolID TreatCG_Total TreatCOD_Total TreatCGCOD_Total
saveold "$base_out/Consolidated/RegFinales_McClosky.dta", replace version(12)
restore

********************************************************************
*********************** COD ONLY TABLE
********************************************************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


*First lets create the tables Karthik Wants

label var Z_kiswahili_T3 Swahili
label var Z_kiswahili_T7 Swahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"
pca Z_kiswahili_C~7  Z_kiingere~C_T7 Z_hisabati_C_T7
predict Z_ScoreFocal_C_T7,score
pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}



	

*Main comparison, difference in means
foreach var in $AggregateDep_Karthik{
	eststo clear


	*With common supoort and common students-EDI 
	capture drop resid_T3
	reg `var'_T3  i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol, vce(cluster SchoolID)
	predict resid_T3, resid
	eststo `var'_1_edi:   reg resid_T3 $treatmentlist, vce(cluster SchoolID)
	estadd ysumm
	test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
	
	preserve
	tempfile file1
	drop if e(sample)==0
	gen time=1
	rename resid_T3 resid
	keep resid SchoolID  $treatmentlist time
	save `file1'
	restore
	
	capture drop resid_T4	
	*With common supoort and common students-TWA test
	reg `var'_T4 i.GradeID_T4  i.DistID $schoolcontrol, vce(cluster SchoolID)
	predict resid_T4, resid
	eststo `var'_1_twa:   reg resid_T4 $treatmentlist_int    , vce(cluster SchoolID)  
	estadd ysumm
	test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

	preserve
	tempfile file2
	drop if e(sample)==0
	gen time=2
	rename resid_T4 resid
	keep resid SchoolID  $treatmentlist_int time
	save `file2'
	restore
	
	preserve
	clear
	use `file1'
	append using `file2'
	recode time (2=1) (1=0)
	replace TreatmentCG=0 if TreatmentCG==.
	rename TreatmentCG TreatmentCG2
	rename TreatmentCOD TreatmentCOD2
	rename TreatmentBoth TreatmentBoth2
	
	eststo `var'_1:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
	estadd ysumm
	test (_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]
	restore




	capture drop resid_T7
	reg `var'_T7  i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol, vce(cluster SchoolID)
	predict resid_T7, resid
	eststo `var'_2_edi:   reg resid_T7 $treatmentlist, vce(cluster SchoolID)
	estadd ysumm
	test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
	
	preserve
	tempfile file1
	drop if e(sample)==0
	gen time=1
	rename resid_T7 resid
	keep resid SchoolID  $treatmentlist time
	save `file1'
	restore
	

	capture drop resid_T8
	*With common supoort and common students-TWA test
	reg `var'_T8 i.GradeID_T8  i.DistID $schoolcontrol, vce(cluster SchoolID)
	predict resid_T8, resid
	eststo `var'_2_twa:   reg resid_T8 $treatmentlist_int    , vce(cluster SchoolID)  
	
	estadd ysumm
	test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
	
	preserve
	tempfile file2
	drop if e(sample)==0
	gen time=2
	rename resid_T8 resid
	keep resid SchoolID  $treatmentlist_int time
	save `file2'
	restore
	
	preserve
	clear
	use `file1'
	append using `file2'
	recode time (2=1) (1=0)
	replace TreatmentCG=0 if TreatmentCG==.
	rename TreatmentCG TreatmentCG2
	rename TreatmentCOD TreatmentCOD2
	rename TreatmentBoth TreatmentBoth2
	
	eststo `var'_2:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
	estadd ysumm
	test (_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]
	restore
	

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_COD_Yr1Yr2.tex", se ar2 booktabs label nonumb /// 
coeflabel(c.TreatmentBoth2#c.time Combo c.TreatmentCOD2#c.time COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep(TreatmentCOD)  ///
stats(N , fmt(%9.0fc   ) labels ("N. of obs." ) )

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_COD_Yr1Yr2.csv", se ar2 label nonumb /// 
coeflabel(c.TreatmentBoth2#c.time Combo c.TreatmentCOD2#c.time COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep(TreatmentCOD)  ///
stats(N , fmt(%9.0fc   ) labels ("N. of obs." ) )

}





********************************************************************
*********************** PASS HIGH-STAKES
********************************************************************
use "$basein/4 Intervention/TwaEL_2013/TwaTestData.dta", clear



foreach name in "k" "e" "m"{
	forval i=1/3  {
		gen PR_`i'_`name'=Passed_`i'`name'/NrTests_`i'
		if "`name'"=="k" label var PR_`i'_`name' "K S`i'"
		if "`name'"=="e" label var PR_`i'_`name' "E S`i'"
		if "`name'"=="m" label var PR_`i'_`name' "M S`i'"
	}
}

keep SchoolID  PR_1_k- PR_3_m
reshape long PR_@_k PR_@_e PR_@_m, i(SchoolID) j(Grade)
rename PR__k Kis_Pass_T4
rename PR__e Eng_Pass_T4
rename PR__m Math_Pass_T4

tempfile temp1
save `temp1'


use "$basein/4 Intervention/TwaEL_2014/TwaTestData_stutested", clear
collapse  (mean) Kis_Pass Eng_Pass Math_Pass, by( SchoolID Grade )
rename Kis_Pass Kis_Pass_T8
rename Eng_Pass Eng_Pass_T8
rename Math_Pass Math_Pass_T8

merge 1:1 SchoolID Grade  using `temp1'
drop  if _merge!=3
drop _merge

merge m:1  SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus( SchoolID treatment treatarm DistID)
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" 
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" 
label var TreatmentBoth "Combo"

eststo clear
foreach time in T4 T8{
foreach var in Math_Pass Kis_Pass Eng_Pass{
eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID i.Grade, vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
sum `var'_`time' if TreatmentCG==0 & TreatmentCOD==0 & TreatmentBoth==0 & e(sample)==1
estadd scalar ymean2=r(mean)

}  
}
 


esttab  using "$latexcodesfinals/RegPassTest_HighStakes.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles  nolines nogaps ///
keep(TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc %9.2gc a2 a2) labels("N. of obs." "Control mean" "Interaction(Combo-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes


esttab  using "$latexcodesfinals/RegPassTest_HighStakes_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles  nolines nogaps ///
keep(TreatmentCOD) stats(N ymean2,fmt(%9.0fc %9.2gc) labels("N. of obs." "Control mean")) ///
nonotes


esttab  using "$latexcodesfinals/RegPassTest_HighStakes.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines nogaps  ///
keep(TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc %9.2gc a2 a2) labels("N. of obs." "Control mean" "Interaction(Combo-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

exit

********************************************************************
*********************** OTHER SUBJECTS/GRADES
********************************************************************

************************************
************* 2013 *****************
************************************

use "$base_out/Student_PSLE_2013.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"


eststo clear
eststo m1_2013: reg Pass  $treatmentlist $schoolcontrol i.DistID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2013: reg AVERAGE  $treatmentlist $schoolcontrol i.DistID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 
  
use "$base_out/School_PSLE_2013.dta",clear
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge 

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
  
eststo m3_2013:  reg Students $treatmentlist $schoolcontrol i.DistID
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]  


** do lee bound's, so that # of test takers is on average the same, dropping the left tail
use "$base_out/Student_PSLE_2013.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
*drop non matched schools
drop if CAND=="" 
drop if SX==""
preserve
collapse (count) SX2, by(SchoolID DistID treatment treatarm)
reg SX2 ib(none).treatarm i.DistID, nocons 
restore

*Calculate number of test takers per school
bys SchoolID: gen Num=_N
bys SchoolID: gen n=_n
replace Num=. if Num!=n
*mean number per treatment (and district)
bys DistID treatment: egen N=mean(Num)
replace N=round(N)
*minimum number across treatment arms (by district)
by DistID: egen N2=min(N)
*How many i need to trim from each school
gen N3=N-N2
sort SchoolID AVERAGE
tabstat AVERAGE , by(treatment) statistics(mean N)
drop if n<N3
tabstat AVERAGE , by(treatment) statistics(mean N)

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
preserve
collapse (count) SX2, by(SchoolID DistID treatment treatarm)
reg SX2 ib(none).treatarm i.DistID, nocons 
restore


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"


eststo m1_2013_Lee: reg Pass  $treatmentlist $schoolcontrol i.DistID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2013_Lee: reg AVERAGE  $treatmentlist $schoolcontrol i.DistID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]   	
  
************************************
************* 2014 *****************
************************************

use "$base_out/Student_PSLE_2014.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"



eststo m1_2014: reg Pass  $treatmentlist $schoolcontrol i.DistID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2014: reg AVERAGE  $treatmentlist $schoolcontrol i.DistID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 
  
use "$base_out/School_PSLE_2014.dta",clear
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge 

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
  
eststo m3_2014:  reg Students $treatmentlist $schoolcontrol i.DistID
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 



** do lee bound's, so that # of test takers is on average the same, dropping the left tail
use "$base_out/Student_PSLE_2014.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
*drop non matched schools
drop if CAND=="" 
drop if SX==""
preserve
collapse (count) SX2, by(SchoolID DistID treatment treatarm)
reg SX2 ib(none).treatarm i.DistID, nocons 
restore

*Calculate number of test takers per school
bys SchoolID: gen Num=_N
bys SchoolID: gen n=_n
replace Num=. if Num!=n
*mean number per treatment (and district)
bys DistID treatment: egen N=mean(Num)
replace N=round(N)
*minimum number across treatment arms (by district)
by DistID: egen N2=min(N)
*How many i need to trim from each school
gen N3=N-N2
sort SchoolID AVERAGE
tabstat AVERAGE , by(treatment) statistics(mean N)
drop if n<N3
tabstat AVERAGE , by(treatment) statistics(mean N)

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"
preserve
collapse (count) SX2, by(SchoolID DistID treatment treatarm)
reg SX2 ib(none).treatarm i.DistID, nocons 
restore


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"


eststo m1_2014_Lee: reg Pass  $treatmentlist $schoolcontrol i.DistID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2014_Lee: reg AVERAGE  $treatmentlist $schoolcontrol i.DistID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]   	
     	  

************************************
************* Science *****************
************************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

*First lets create the tables Karthik Wants

label var Z_sayansi_T3 "Year 1"
label var Z_sayansi_T7 "Year 2"


foreach time in T3 T7{
foreach var in Z_sayansi{
eststo est_`var'_`time':  reg `var'_`time' $treatmentlist i.DistID $studentcontrol $schoolcontrol $HHcontrol , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}  

}


esttab est_Z_sayansi_T3 est_Z_sayansi_T7 m1_2013 m2_2013 m3_2013 m1_2014 m2_2014 m3_2014 using "$latexcodesfinals/RegOtherGradesSubjects.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 National Exam 2013" "Grade 7 National Exam 2014", pattern(1 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Year 1 (2013)" "Year 2 (2014)"   "Pass" "Average score" "Test takers" "Pass" "Average score" "Test takers") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab est_Z_sayansi_T3 est_Z_sayansi_T7 m1_2013 m2_2013  m1_2014 m2_2014  using "$latexcodesfinals/RegOtherGradesSubjects_nonumber.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 National Exam 2013" "Grade 7 National Exam 2014", pattern(1 0 1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Year 1 (2013)" "Year 2 (2014)"   "Pass" "Average score"  "Pass" "Average score") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes


esttab est_Z_sayansi_T3 est_Z_sayansi_T7 m1_2013 m2_2013 m3_2013 m1_2014 m2_2014 m3_2014 using "$latexcodesfinals/RegOtherGradesSubjects.csv", se ar2  label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 National Exam 2013" "Grade 7 National Exam 2014", pattern(1 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Year 1 (2013)" "Year 2 (2014)"   "Pass" "Average score" "Test takers" "Pass" "Average score" "Test takers") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2) labels("N. of obs." "Mean control group" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes


esttab  m1_2013 m1_2013_Lee m2_2013 m2_2013_Lee  m1_2014 m1_2014_Lee m2_2014 m2_2014_Lee using "$latexcodesfinals/PLSE_lee.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Grade 7 National Exam 2013" "Grade 7 National Exam 2014", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Pass" "Pass (Lee)" "Score" "Score (Lee)"   "Pass" "Pass (Lee)" "Score" "Score (Lee bound)") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes

esttab m1_2013 m1_2013_Lee m2_2013 m2_2013_Lee  m1_2014 m1_2014_Lee m2_2014 m2_2014_Lee using "$latexcodesfinals/PLSE_lee.csv", se ar2  label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Grade 7 National Exam 2013" "Grade 7 National Exam 2014", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Pass" "Pass (Lee)" "Score" "Score (Lee)"   "Pass" "Pass (Lee)" "Score" "Score (Lee bound)") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2) labels("N. of obs." "Mean control group" "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(suma)) ///
nonotes




********************************************************************
*********************** EDI VS TWA
********************************************************************


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

*First lets create the tables Karthik Wants

label var Z_kiswahili_T3 Swahili
label var Z_kiswahili_T7 Swahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"
pca Z_kiswahili_C~7  Z_kiingere~C_T7 Z_hisabati_C_T7
predict Z_ScoreFocal_C_T7,score
pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist Z_ScoreFocal_C_T7 Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}



*Transition of Z-scores
foreach var in $AggregateDep_Karthik{
eststo clear

*Original results-EDI
eststo:   reg `var'_T7 $treatmentlist i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "Yes"
estadd local TimingFE "No"
estadd local whattest "Survey"
estadd local whatschools "All"
estadd local whatQ "All"
estadd local whatStudents "All"

*Original results-EDI without schools
eststo:   reg `var'_T7  $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if SchoolTWA_T7==1 , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "Yes"
estadd local TimingFE "No"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "All"
estadd local whatStudents "All"



*With common supoort-EDI
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol  if SchoolTWA_T7==1 , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "All"


*With common supoort and common students-EDI
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol  if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"

*With common supoort and common students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"



*With common supoort and common students-TWA test, but without student controls
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID   $schoolcontrol    if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "No"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"

*With common supoort and all students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.GradeID_T8  i.DistID   $schoolcontrol , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "No"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "All"



if "`var'"=="Z_kiswahili" local name Swahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math
else if "`var'"=="Z_ScoreFocal" local name Combined


esttab using "$latexcodesfinals/Reg`var'_InvVsEDI.tex", se ar2 booktabs label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01) nomtitles ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels("N. of obs." "Combo-COD[-CG]" "p-value")  star(suma)) 

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI.csv", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01) nomtitles ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels("N. of obs." "Combo-COD[-CG]" "p-value")  star(suma)) 

}

*Main comparison, difference in means
foreach var in $AggregateDep_Karthik{
eststo clear


*With common supoort and common students-EDI 
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]


*With common supoort and common students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

preserve
drop if SchoolTWA_T7==0
drop if `var'_C_T7==.
drop if `var'_T8==.
drop if attgrade_T7!=GradeID_T7
rename `var'_C_T7 `var'_EDI
rename `var'_T8 `var'_TWA
drop if upid==""
keep `var'_EDI `var'_TWA $treatmentlist TreatmentCOD2 TreatmentBoth2 DistID  $schoolcontrol HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  WeekIntvTest_T* upid SchoolID
reshape long `var', i(upid) j(Test) string
gen Twaweza=(Test=="_TWA")
eststo: reg `var'  (c.TreatmentCOD2 c.TreatmentBoth2  i.LagGrade  i.DistID c.(${studentcontrol}  ${schoolcontrol} ${HHcontrol}))##c.Twaweza, vce(cluster SchoolID)  
estadd ysumm
test (_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]

restore


if "`var'"=="Z_kiswahili" local name Swahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math
else if "`var'"=="Z_sayansi" local name Science


esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp.tex", se ar2 booktabs label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combo c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combo-COD" "p-value" "") star(p))

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp.csv", se ar2 label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combo c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combo-COD" "p-value" "") star(p))

}
foreach var in $AggregateDep_int{
gen Matched_`var'=(`var'_C_T7!=. & `var'_T8!=.)
}

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p2
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
 reg `var'  TD1 TD2   `if', nocons vce(cluster `clus_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p2'  = nullmat(`d_p2'),r(p)
 matrix A=e(b)
 lincom (TD1-TD2)
 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
 mat `se_3' = nullmat(`se_3'), r(se)
 sum `var' if TD2==1 & e(sample)==1
 mat `mu_1' = nullmat(`mu_1'), r(mean)
 mat `se_1' = nullmat(`se_1'), r(sd)
 sum `var' if TD1==1 & e(sample)==1
 mat `mu_2' = nullmat(`mu_2'),r(mean)
 mat `se_2' = nullmat(`se_2'), r(sd)
 
}
foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3  d_p2 {
 mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3   d_p2 {
 eret mat `mat' = ``mat''
}
end
label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Swahili test score"
label var LagZ_hisabati "Math test score"
label var LagZ_kiingereza "English test score"

 eststo clear
eststo: my_ptest  LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza, by(Matched_Z_hisabati) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/Balance_Matches.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p2))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) ") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles



********************************************************************
*********************** TEACHER EFFORT
********************************************************************



use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
drop if upid==""
merge 1:1 upid using "$base_out/Consolidated/TAttendace.dta"
drop if _merge!=3
drop _merge
rename atttch_T2 atttch_T3
rename atttch_T6 atttch_T7
keep if consnt_T3==1 &  tcnsnt_T7==1

*keep if FGS_T7==1 & FGS_T3==1

replace atttch_T3=0 if atttch_T3==. & FGS_T3==1
replace atttch_T7=0 if atttch_T7==. & FGS_T7==1


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"


global depvarsFinales atttch  test_F_sub_F_yrs   tutoring_F_sub_F_yrs remedial_F_sub_F_yrs teaching_inputs    
global depvarsFinales2  t271  t275 t276 t277 TimeAtSchool

foreach var in $depvarsFinales{
	replace `var'_T3=. if FGS_T3!=1
	replace `var'_T7=. if FGS_T7!=1
}

eststo clear
foreach var in $depvarsFinales{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo m_`var': reg `var'_T $treatmentlist (c.($schoolcontrol) c.($teachercontrol) i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


esttab using "$latexcodesfinals/RegTeacher.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


foreach time in 3 7{
eststo clear
foreach var in $depvarsFinales{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
drop if T!="`time'" 
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist (c.($schoolcontrol) c.($teachercontrol) i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}

esttab using "$latexcodesfinals/RegTeacher_`time'.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


esttab using "$latexcodesfinals/RegTeacher_`time'.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p,fmt(%9.0fc a2  a2 a2)  labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}


eststo clear
foreach var in $depvarsFinales2{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist (c.($schoolcontrol) c.($teachercontrol) i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher2.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


esttab using "$latexcodesfinals/RegTeacher2.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


foreach time in 3 7{
eststo clear
foreach var in $depvarsFinales2{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
drop if T!="`time'"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist (c.($schoolcontrol) c.($teachercontrol) i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher2_`time'.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")


esttab using "$latexcodesfinals/RegTeacher2_`time'.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of Dep. Var." "Interaction(Combo-Grants-Incentives)" "p-value (\$H_0\$:Interaction=0)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
}

*******************************************************
*********** HETEROGENEITY******************************
*******************************************************

**********************
**************Student
************************
use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear



tabulate LagGrade, gen(GRD)
tabulate Lagmale, gen(sex)


eststo clear
foreach var in $subjects{

	foreach cov in Lagmale LagAge LagZ_`var' {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum `cov'
		replace `cov'=`cov'-r(mean) if !missing(`cov')
		capture gen TCG`cov'=TreatmentCG*`cov' 
		capture gen TCOD`cov'=TreatmentCOD*`cov'  
		capture gen TBoth`cov'=TreatmentBoth*`cov' 
		eststo:  reg Z_`var'_T  $treatmentlist  TCG`cov'  TCOD`cov' TBoth`cov' Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']	  
		restore
	}

}

esttab using "$latexcodesfinals\Heter_Stud.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(Grants*Covariate Incentives*Covariate Combo*Covariate) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  ) ///
rename( ///
 TCGLagmale Grants*Covariate TCODLagmale Incentives*Covariate TBothLagmale Combo*Covariate  ///
 TCGLagAge Grants*Covariate TCODLagAge Incentives*Covariate TBothLagAge Combo*Covariate ///
 TCGLagZ_hisabati Grants*Covariate TCODLagZ_hisabati Incentives*Covariate TBothLagZ_hisabati Combo*Covariate ///
 TCGLagZ_kiswahili Grants*Covariate TCODLagZ_kiswahili Incentives*Covariate TBothLagZ_kiswahili Combo*Covariate ///
 TCGLagZ_kiingereza Grants*Covariate TCODLagZ_kiingereza Incentives*Covariate TBothLagZ_kiingereza Combo*Covariate ///
 TCGLagZ_ScoreFocal Grants*Covariate TCODLagZ_ScoreFocal Incentives*Covariate TBothLagZ_ScoreFocal Combo*Covariate ///
 ) /// 
mgroups("Math" "Swahili" "English" "Combined", pattern(1 0 0 1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score") title("Heterogeneity by student characteristics for `name'") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


esttab using "$latexcodesfinals\Heter_Stud.csv", se ar2 label fragment nolines ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(Grants*Covariate Incentives*Covariate Combo*Covariate `cov') ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N ,fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Lagmale Covariate  LagAge Covariate   LagZ_`var' Covariate) ///
rename( ///
 TCGLagmale Grants*Covariate TCODLagmale Incentives*Covariate TBothLagmale Combo*Covariate  ///
 TCGLagAge Grants*Covariate TCODLagAge Incentives*Covariate TBothLagAge Combo*Covariate ///
 TCGLagZ_hisabati Grants*Covariate TCODLagZ_hisabati Incentives*Covariate TBothLagZ_hisabati Combo*Covariate ///
 TCGLagZ_kiswahili Grants*Covariate TCODLagZ_kiswahili Incentives*Covariate TBothLagZ_kiswahili Combo*Covariate ///
 TCGLagZ_kiingereza Grants*Covariate TCODLagZ_kiingereza Incentives*Covariate TBothLagZ_kiingereza Combo*Covariate ///
 TCGLagZ_ScoreFocal Grants*Covariate TCODLagZ_ScoreFocal Incentives*Covariate TBothLagZ_ScoreFocal Combo*Covariate ///
 ) /// 
mtitles("Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score" "Gender" "Age" "Lag test score") title("Heterogeneity by student characteristics for `name'") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")



**********************
**************TEACHER
************************



use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear



eststo clear
foreach var in $subjects{

	foreach cov in TeachingCareer_T3 t23Average TeacherIndexRecall {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"!="TeacherIndexRecall") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"=="TeacherIndexRecall") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}

esttab using "$latexcodesfinals\Heter_Teacher.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(TCGCOV TCODCOV TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combo*Covariate   ) ///
mgroups("Math" "Swahili" "English" "Combined", pattern(1 0 0 1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" )  ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


esttab using "$latexcodesfinals\Heter_School.csv", se ar2 label fragment nolines ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(TCGCOV TCODCOV TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combo*Covariate   ) ///
mtitles("Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" ///
"Motivation" "Salary" "Memory" )  ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


**********************
**************SCHOOL
************************


use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear



eststo clear
foreach var in $subjects{

	foreach cov in IndexFacilities_T1 PTR_T1 IndexManagerial_T1 {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"=="PTR_T1") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"!="PTR_T1") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}

esttab using "$latexcodesfinals\Heter_School.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(TCGCOV TCODCOV TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combo*Covariate   ) ///
mgroups("Math" "Swahili" "English" "Combined", pattern(1 0 0 1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" )  ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


esttab using "$latexcodesfinals\Heter_School.csv", se ar2 label fragment nolines ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(TCGCOV TCODCOV TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combo*Covariate   ) ///
mtitles("Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" ///
"Facilities" "PTR" "Management" )  ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")



** JUST THE COMBINED
*******************************************************
*********** HETEROGENEITY******************************
*******************************************************
eststo clear
**********************
**************Student
************************
use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear
drop if upid==""
bys upid: gen N=_N
drop if N==2




tabulate LagGrade, gen(GRD)
tabulate Lagmale, gen(sex)



foreach var in ScoreFocal{

	foreach cov in Lagmale LagAge LagZ_`var' {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum `cov'
		replace `cov'=`cov'-r(mean) if !missing(`cov')
		capture gen TCG`cov'=TreatmentCG*`cov' 
		capture gen TCOD`cov'=TreatmentCOD*`cov'  
		capture gen TBoth`cov'=TreatmentBoth*`cov' 
		eststo:  reg Z_`var'_T  $treatmentlist  TCG`cov'  TCOD`cov' TBoth`cov' Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']	  
		restore
	}

}




**********************
**************TEACHER
************************



use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear
drop if upid==""
bys upid: gen N=_N
drop if N==2



foreach var in ScoreFocal{

	foreach cov in TeachingCareer_T3 t23Average TeacherIndexRecall {
		preserve
		keep  Z_ScoreFocal_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_ScoreFocal_T1
		capture drop Z_ScoreFocal_T5
		capture drop Z_ScoreFocal_T4
		capture drop Z_ScoreFocal_T8
		capture drop Z_ScoreFocal_T*_*
		reshape long Z_ScoreFocal_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"!="TeacherIndexRecall") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"=="TeacherIndexRecall") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}


**********************
**************SCHOOL
************************


use "$base_out\Consolidated\Student_School_House_Teacher_Char.dta", clear
drop if upid==""
bys upid: gen N=_N
drop if N==2


foreach var in ScoreFocal{

	foreach cov in IndexFacilities_T1 PTR_T1 IndexManagerial_T1 {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"=="PTR_T1") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"!="PTR_T1") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}


esttab using "$latexcodesfinals\Heter_focal.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(Grants*Covariate Incentives*Covariate Combo*Covariate ) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  ) ///
rename( ///
 TCGLagmale Grants*Covariate TCODLagmale Incentives*Covariate TBothLagmale Combo*Covariate  ///
 TCGLagAge Grants*Covariate TCODLagAge Incentives*Covariate TBothLagAge Combo*Covariate ///
 TCGLagZ_hisabati Grants*Covariate TCODLagZ_hisabati Incentives*Covariate TBothLagZ_hisabati Combo*Covariate ///
 TCGLagZ_kiswahili Grants*Covariate TCODLagZ_kiswahili Incentives*Covariate TBothLagZ_kiswahili Combo*Covariate ///
 TCGLagZ_kiingereza Grants*Covariate TCODLagZ_kiingereza Incentives*Covariate TBothLagZ_kiingereza Combo*Covariate ///
 TCGLagZ_ScoreFocal Grants*Covariate TCODLagZ_ScoreFocal Incentives*Covariate TBothLagZ_ScoreFocal Combo*Covariate ///
 TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combo*Covariate) /// 
 mgroups("Student" "Teacher" "School", pattern(1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Gender" "Age" "Lag test score" "Motivation" "Salary" "Memory" "Facilities" "PTR" "Management") title("Heterogeneity by student characteristics for `name'") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


