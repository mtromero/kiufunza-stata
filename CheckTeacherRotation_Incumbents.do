capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4 d_p d_p2
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
	reg `var' TD1 TD2 TD3 TD4 `if', nocons vce(cluster `clus_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p'  = nullmat(`d_p'),r(p)
	matrix A=e(b)
	matrix B=e(V)
	mat `mu_1' = nullmat(`mu_1'), A[1,1]
	mat `mu_2' = nullmat(`mu_2'), A[1,2]
	mat `mu_3' = nullmat(`mu_3'), A[1,3]
	mat `mu_4' = nullmat(`mu_4'), A[1,4]
	mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
	mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
	mat `se_3' = nullmat(`se_3'), sqrt(B[3,3])
	mat `se_4' = nullmat(`se_4'), sqrt(B[4,4])
	reghdfe `var' TD1 TD2 TD3 TD4 `if',  vce(cluster `clus_id') abs( i.`strat_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p2'  = nullmat(`d_p2'),r(p)
}
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	eret mat `mat' = ``mat''
}
end

use "$basein/1 Baseline/Teacher/Subjects_noPII.dta" , clear
replace SubjectID=4 if SubjectID>=5
label define I_Subjects 5 "", modify
label define I_Subjects 6 "", modify
label define I_Subjects 7 "", modify
label define I_Subjects 8 "", modify
label define I_Subjects 9 "", modify
label define I_Subjects 4 "Other", modify
drop syllabus  prevSubject combinationCode
collapse (sum) currentSubject, by(DistrictID SchoolID TeacherID CurrentGradeID SubjectID)
replace currentSubject=0 if currentSubject==.
replace currentSubject=1 if currentSubject>1

reshape wide currentSubject, i(DistrictID SchoolID TeacherID CurrentGradeID) j( SubjectID)
rename currentSubject1 SBJ_M
rename currentSubject2 SBJ_K
rename currentSubject3 SBJ_E
rename currentSubject4 SBJ_O
gen CurrentGradeID2="_GRD"+string(CurrentGradeID)
drop CurrentGradeID
reshape wide SBJ_M SBJ_K SBJ_E SBJ_O, i(DistrictID SchoolID TeacherID ) j( CurrentGradeID2) string

reshape long SBJ_M_GRD@ SBJ_K_GRD@ SBJ_E_GRD@ SBJ_O_GRD@, i(DistrictID SchoolID TeacherID ) j(Grade)
reshape long SBJ_@_GRD , i(DistrictID SchoolID TeacherID Grade ) j(Subject) string
keep if SBJ__GRD==1
rename SBJ__GRD SBJ__GRD_T1
mmerge SchoolID TeacherID using "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", ukeep(ETTeacherID R6TeacherID) uif(TeacherID!=.) type(n:1)
drop if _merge==2
drop _merge
save "$base_out/Consolidated/Teacher_RotationSubjectGrade.dta", replace


use "$basein/3 Endline/Teacher/ETGrdSub_noPII.dta", clear 
keep  ETTeacherID tgrdyn ETGradeID  ETGrdSubID

replace ETGrdSubID=4 if ETGrdSubID>=5
label define I_Subjects 5 "", modify
label define I_Subjects 6 "", modify
label define I_Subjects 7 "", modify
label define I_Subjects 8 "", modify
label define I_Subjects 9 "", modify
label define I_Subjects 4 "Other", modify
collapse (sum) tgrdyn, by(ETTeacherID ETGradeID  ETGrdSubID)
replace tgrdyn=0 if tgrdyn==.
replace tgrdyn=1 if tgrdyn>1

reshape wide tgrdyn, i(ETTeacherID ETGradeID) j(ETGrdSubID )
rename tgrdyn1 SBJ_M
rename tgrdyn2 SBJ_K
rename tgrdyn3 SBJ_E
rename tgrdyn4 SBJ_O
gen ETGradeID2="_GRD"+string(ETGradeID)
drop ETGradeID
reshape wide SBJ_M SBJ_K SBJ_E SBJ_O, i(ETTeacherID ) j( ETGradeID2) string
foreach var of varlist SBJ_M_GRD1- SBJ_E_GRD7{
	replace `var'=0 if `var'==.
}

reshape long SBJ_M_GRD@ SBJ_K_GRD@ SBJ_E_GRD@ SBJ_O_GRD@, i(ETTeacherID) j(Grade)
reshape long SBJ_@_GRD , i(ETTeacherID Grade ) j(Subject) string
keep if SBJ__GRD==1
rename SBJ__GRD SBJ__GRD_T3
mmerge  ETTeacherID using "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", ukeep(TeacherID R6TeacherID) uif(ETTeacherID!=.) type(n:1)
drop if _merge==2
drop _merge
tempfile temp
save `temp', replace
use "$base_out/Consolidated/Teacher_RotationSubjectGrade.dta", clear
merge 1:1 ETTeacherID Subject Grade using `temp'
drop if _merge==2
drop _merge
save "$base_out/Consolidated/Teacher_RotationSubjectGrade.dta", replace




use "$basein/8 Endline 2014/Final Data/Teacher/R6TGrdSub2_noPII.dta", clear
replace R6TGrdSub2ID=4 if R6TGrdSub2ID>=5
label define I_Subjects2 5 "", modify
label define I_Subjects2 6 "", modify
label define I_Subjects2 7 "", modify
label define I_Subjects2 8 "", modify
label define I_Subjects2 9 "", modify
label define I_Subjects2 4 "Other", modify
replace tgrdyn=0 if tgrdyn==2
collapse (sum) tgrdyn, by(SchoolID R6TeacherID R6TGrdSub2ID R6TGradeID)
replace tgrdyn=0 if tgrdyn==.
replace tgrdyn=1 if tgrdyn>1
reshape wide tgrdyn, i(SchoolID R6TeacherID R6TGradeID) j(R6TGrdSub2ID)
rename tgrdyn1 SBJ_M
rename tgrdyn2 SBJ_K
rename tgrdyn3 SBJ_E
rename tgrdyn4 SBJ_O
gen CurrentGradeID2="_GRD"+string(R6TGradeID)
drop R6TGradeID
reshape wide SBJ_M SBJ_K SBJ_E SBJ_O, i(SchoolID R6TeacherID ) j( CurrentGradeID2) string


reshape long SBJ_M_GRD@ SBJ_K_GRD@ SBJ_E_GRD@ SBJ_O_GRD@, i(SchoolID R6TeacherID) j(Grade)
reshape long SBJ_@_GRD , i(SchoolID R6TeacherID Grade ) j(Subject) string
keep if SBJ__GRD==1
rename SBJ__GRD SBJ__GRD_T7
label drop I_Subjects2

mmerge SchoolID R6TeacherID using "$basein/8 Endline 2014/ID Linkage/R6_Teacher_ID_Linkdage_noPII.dta", ukeep(TeacherID ETTeacherID) uif(TeacherID!=. & R6TeacherID!=. ) type(n:1)
drop if _merge!=3
drop _merge
save `temp', replace
use "$base_out/Consolidated/Teacher_RotationSubjectGrade.dta", clear
merge 1:1 SchoolID TeacherID Subject Grade using `temp'
drop if _merge==2
drop _merge
save "$base_out/Consolidated/Teacher_RotationSubjectGrade.dta", replace


gen rotateT3=(SBJ__GRD_T3!=1)
gen rotateT7=(SBJ__GRD_T7!=1)

merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus(treatarm treatment DistID) update replace


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combo"



label var rotateT3 "No longer teaching grade/subject (Yr1)"
label var rotateT7 "No longer teaching grade/subject (Yr2)"

eststo clear
eststo: my_ptest  rotateT3 rotateT7, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/TeacherRotation_Incumbent.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles
