capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
 reg `var' TD1 TD2 TD3 TD4 `if', nocons vce(cluster `clus_id')
 test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
 mat `d_p'  = nullmat(`d_p'),r(p)
 matrix A=e(b)
 matrix B=e(V)
 mat `mu_1' = nullmat(`mu_1'), A[1,1]
 mat `mu_2' = nullmat(`mu_2'), A[1,2]
 mat `mu_3' = nullmat(`mu_3'), A[1,3]
 mat `mu_4' = nullmat(`mu_4'), A[1,4]
 mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
 mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
 mat `se_3' = nullmat(`se_3'), sqrt(B[3,3])
 mat `se_4' = nullmat(`se_4'), sqrt(B[4,4])
}
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p {
 mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p {
 eret mat `mat' = ``mat''
}
end

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"

keep SchoolID  tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25 treatment treatarm SchoolID
/*t23 t24 t25 */
gen TeacherCertificate=(t16>=3) & !missing(t16)

label var t21 "Travel time from house to school"
label var tchsex "Male"
label var TeacherCertificate "Teaching Certificate"
recode tchsex (2=0)

recode t10 (2=0)
 eststo clear
eststo: xi: my_ptest  tchsex t05 t07 t21 TeacherCertificate, by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summaryTeacherTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryTeacherTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


local AggregateDep 	Z_hisabati Z_kiswahili Z_kiingereza Z_sayansi Z_ScoreKisawMath Z_ScoreFocal Z_ScoreTotal /*this should be added in the future...*/
local schoolcontrol MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 
local studentcontrol Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza

label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Swahili test score"
label var LagZ_hisabati "Math test score"
label var LagZ_kiingereza "English test score"





eststo clear
estpost tabstat `studentcontrol', by(LagGrade) statistics(mean sd) columns(statistics) listwise
esttab using "$results/LatexCode/SummaryGradeBase.tex", main(mean) aux(sd) nostar unstack noobs nonote nomtitle nonumber label replace booktabs


label var attendance_T3 "Tested in 2013"
label var attendance_T7 "Tested in 2014"
eststo clear
eststo: xi: my_ptest  `studentcontrol' attendance_T3 attendance_T7, by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summaryStudentsTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentsTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

eststo clear
eststo: xi: my_ptest  attendance_T3 attendance_T7, by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/Attendance.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/Attendance.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


  foreach v of varlist s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 {
 	local l`v' : variable label `v'
        if `"`l`v''"' == "" {
 		local l`v' "`v'"
 	}
 }
collapse (mean) `schoolcontrol' treatarm (first) treatment, by (SchoolID)
label var MeanGrade_LagZ_kiswahili "Average Swahili test score"
label var MeanGrade_LagZ_kiingereza "Average Math test score"
label var MeanGrade_LagZ_hisabati "Average English test score"
label var s1451_T1 "Kitchen"
label var s1452_T1 "Library"
label var s1453_T1 "Playground"
label var s1454_T1 "Staff room"
label var s1455_T1 "Outer wall"
label var s1456_T1 "Newspaper"
label var SingleShift_T1 "Single shift"
label var computersYN_T1 "Computers"
label var s120_T1  "Electricity"
label var s118_T1 "Classes outside"
label var PipedWater_T1 "Piped Water"
label var NoWater_T1 "No Water"
label var ToiletsStudents_T1 "Toilets/Students"
label var ClassRoomsStudents_T1 "Classrooms/Students"
replace TeacherStudents_T1=1/TeacherStudents_T1
label var TeacherStudents_T1 "Students/Teachers"
label var s188_T1 "Breakfast"
label var s175_T1 "Preschool"
label var s200_T1 "Track students"
label var s108_T1 "Urban"
label var SizeSchoolCommittee_T1 "Size School Committee"
label var KeepRecords_T1 "Spending records"
label var noticeboard_T1 "Noticeboard with spending information"
label var PropCommitteeFemale_T1 "Female/Committee"
label var PropCommitteeTeachers_T1 "Teacher/Committee"
label var PropCommitteeParents_T1 "Parents/Committee"
label var StudentsTotal_T1 "Enrolled students"


 
   foreach v of varlist s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 {
 	label var `v' "`l`v''"
  }
 
 
eststo clear
 eststo: quietly estpost summarize `schoolcontrol', listwise
esttab using "$results/LatexCode/summarySchoolBase.tex", cells("mean(fmt(a2))  sd(fmt(a2)) min(fmt(a2)) max(fmt(a2))") nomtitle nonumber label replace booktabs

egen InfrastructureIndex=rowtotal(s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1)
label var InfrastructureIndex "Infrastructure Index (0-6)"


local schoolcontro2l   s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1  computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 
local schoolcontro2lb   InfrastructureIndex  s120_T1   SingleShift_T1  TeacherStudents_T1  s200_T1 s108_T1  StudentsTotal_T1 /* StudentsGr_T1 */ 

eststo clear
eststo: xi: my_ptest  `schoolcontro2lb', by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summarySchoolTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summarySchoolTotal.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a2)) mu_2(fmt(a2)) mu_3(fmt(a2)) mu_4(fmt(a2)) d_p(star pvalue(d_p) fmt(a2))" "se_1(fmt(a2) par) se_2(fmt(a2) par) se_3(fmt(a2) par) se_4(fmt(a2) par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles




use "$base_out/Consolidated/Student.dta", clear

label var Z_kiswahili_T1 "Z-score kiswahili"
label var Z_kiingereza_T1 "Z-score math"
label var Z_hisabati_T1 "Z-score english"
label var Z_ScoreKisawMath_T1 "Z-score kiswahili+math"
label var Z_ScoreFocal_T1 "Z-score focal"
label var Age_T1 "Age"
gen male=1 if Gender_T1==1
replace male=0 if Gender_T1==2
label var male "Male"
drop if GradeID_T1!=1
xi: my_ptest  Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1  Age_T1 male_T1, by(treatarm) clus_id(SchoolID)

esttab using "$results/LatexCode/summaryStudentBase1.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentBase1.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Student.dta", clear

label var Z_kiswahili_T1 "Z-score kiswahili"
label var Z_kiingereza_T1 "Z-score math"
label var Z_hisabati_T1 "Z-score english"
label var Z_ScoreKisawMath_T1 "Z-score kiswahili+math"
label var Z_ScoreFocal_T1 "Z-score focal"
label var Age_T1 "Age"
gen male=1 if Gender_T1==1
replace male=0 if Gender_T1==2
label var male "Male"
drop if GradeID_T1!=2
xi: my_ptest   Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1   Age_T1 male_T1, by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summaryStudentBase2.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentBase2.csv", label replace   nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Student.dta", clear

label var Z_kiswahili_T1 "Z-score kiswahili"
label var Z_kiingereza_T1 "Z-score math"
label var Z_hisabati_T1 "Z-score english"
label var Z_ScoreKisawMath_T1 "Z-score kiswahili+math"
label var Z_ScoreFocal_T1 "Z-score focal"
label var Age_T1 "Age"
gen male=1 if Gender_T1==1
replace male=0 if Gender_T1==2
label var male "Male"

drop if GradeID_T1!=3
xi: my_ptest   Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1   Age_T1 male_T1, by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summaryStudentBase3.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentBase3.csv", label replace  nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Student.dta", clear
label var Z_kiswahili_T1 "Z-score kiswahili"
label var Z_kiingereza_T1 "Z-score math"
label var Z_hisabati_T1 "Z-score english"
label var Z_ScoreKisawMath_T1 "Z-score kiswahili+math"
label var Z_ScoreFocal_T1 "Z-score focal"
label var Age_T1 "Age"
gen male=1 if Gender_T1==1
replace male=0 if Gender_T1==2
label var male "Male"


xi: my_ptest   Z_kiswahili_T1 Z_kiingereza_T1 Z_hisabati_T1   Age_T1 male_T1, by(treatarm) clus_id(SchoolID)

esttab using "$results/LatexCode/summaryStudentBase.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none) ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentBase.csv", label replace  nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none) ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Clustered standard errors, by school, in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


clear 
use "$base_out/Consolidated/School.dta", clear


eststo clear
my_ptest StudentsGr1_T1 StudentsGr2_T1 StudentsGr3_T1 s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 SizeSchoolCommittee_T1 TimesCommitteeMet2012_T1 KeepRecords_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1 PipedWater_T1 NoWater_T1 SingleShift_T1, by(treatarm) clus_id(SchoolID)

esttab using "$results/LatexCode/summaryBaseSchool.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryBaseSchool.csv", label replace  nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles



clear 
use "$base_out/Consolidated/Household.dta", replace

egen AssetIndex=rowtotal(asset_1_T1 asset_2_T1 asset_3_T1 asset_4_T1 asset_5_T1 asset_6_T1 asset_7_T1 asset_8_T1)
label var AssetIndex "Asset Index (0-8)"
label var Expenditure_FC_2013_T1 "Expenditure in education (2013)"


eststo clear
my_ptest  NumHHMembers_T1 AssetIndex Expenditure_FC_2013_T1   floor_mud_T1 , by(treatarm) clus_id(SchoolID)
esttab using "$results/LatexCode/summaryHouseholds.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryHouseholds.csv", label replace  nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles





***********************************
*************** BASELINE 2014 ********
***********************************


use "$base_out/Consolidated/Student.dta", clear
label var Z_kiswahili_T5 "Z-score kiswahili"
label var Z_kiingereza_T5 "Z-score math"
label var Z_hisabati_T5 "Z-score english"
label var Z_ScoreKisawMath_T5 "Z-score kiswahili+math"
label var Z_ScoreFocal_T5 "Z-score focal"
xi: my_ptest  Z_kiswahili_T5 Z_kiingereza_T5 Z_hisabati_T5 Z_ScoreKisawMath_T5 Z_ScoreFocal_T5, by(treatarm) clus_id(SchoolID)

esttab using "$results/LatexCode/summaryStudentBase_2ndYr.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/summaryStudentBase_2ndYr.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


use "$base_out/Consolidated/Household.dta",clear



xi: my_ptest  workyn_T5 asset_1_T5 asset_2_T5 asset_3_T5 asset_4_T5 asset_5_T5 asset_6_T5 asset_7_T5 asset_8_T5 breakfast_T5, by(treatarm) clus_id(SchoolID)


esttab using "$results/LatexCode/Baseline_2ndyearHH.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

esttab using "$results/LatexCode/Baseline_2ndyearHH.csv", label replace ///
nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3))  d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

/*
use "$basein/6 Baseline 2014/Data/household/R4hhMember_noPII.dta", clear
merge m:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepusing(treatment treatarm)
drop _merge
drop if HHID==.
save "$base_out/6 Baseline 2014/household/R4hhMember_noPII.dta", replace
use "$basein/6 Baseline 2014/Supplementing/UPID Linkage Tables/R4_Student_HH_ID_Linkage_noPII.dta"
drop if HHID==.
keep HHID GradeID
merge 1:m HHID using "$base_out/6 Baseline 2014/household/R4hhMember_noPII.dta"
drop _merge
drop if GradeID!=1



drop if relchd!=2
replace hstedu=. if hstedu==-98
replace hstedu=. if hstedu==-99
gen complete_primary=0 if hstedu<17
replace complete_primary=1 if hstedu>=17
 
 
 
xi: my_ptest  hstedu, by(treatment) clus_id(SchoolID)


esttab using "$results/LatexCode/Baseline_2ndyear_HH2.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels("Both" "CG" "COD" "Control" "p-value")  ///
cells("mu_1(fmt(a3)) mu_2(fmt(a3)) mu_3(fmt(a3)) mu_4(fmt(a3)) d_p(star pvalue(d_p))" "se_1(par) se_2(par) se_3(par) se_4(par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis /\ /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }")
*/
