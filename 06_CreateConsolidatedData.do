******** NORMAL *********8
  
use "$base_out/Consolidated/Student.dta", clear
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
gen upidst=upid
*Merge HH invariant characteristics
 merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2013.dta", update keepus(HHSize IndexPoverty IndexEngagement expn12)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2013.dta", update keepus(HHSize IndexPoverty)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2014.dta", update keepus(HHSize IndexPoverty IndexEngagement expn13)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2014.dta", update keepus(HHSize IndexPoverty )
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TestTiming.dta", keepus(DiffIntvTestRestTest)
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge 
merge m:1 SchoolID using "$base_out/Consolidated/SchoolDatesTest.dta"
drop _merge 
foreach var in male t05 t07 t08 t10 t21 higher_degree t23 t24 t25{
rename `var' `var'Average
}
/*
merge m:1 SchoolID GradeID_T7 stdgrp_T7 using "$base_out/Consolidated/TeachingSchedulle_T7.dta"
drop if _merge==2
drop _merge 
*/

gen StudentsGr_T1=StudentsGr1_T1 if GradeID_T3==1
replace StudentsGr_T1=StudentsGr2_T1 if GradeID_T3==2
replace StudentsGr_T1=StudentsGr3_T1 if GradeID_T3==3


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" | treatment_T8==2
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" | treatment_T8==1
label var TreatmentBoth "Combo"

foreach var in  Z_hisabati Z_kiswahili Z_kiingereza  Z_ScoreFocal seenUwezoTests preSchoolYN male{
gen Lag`var'=.
replace Lag`var'=`var'_T1 if !missing(`var'_T1)
replace Lag`var'=`var'_T5 if !missing(`var'_T5)
}
gen LagExpenditure=expn13
replace LagExpenditure=expn12 if LagExpenditure==. & expn12!=.

foreach var in  HHSize IndexPoverty IndexEngagement LagExpenditure {
gen Missing`var'=!missing(`var')
replace `var'=0 if `var'==.
}

/*
gen LagExpenditure=.
replace LagExpenditure=Expenditure_FC_2012_T1 if !missing(Expenditure_FC_2012_T1)
replace LagExpenditure=Expenditure_FC_2013_T5 if !missing(Expenditure_FC_2013_T5)


foreach var in  Z_hisabati Z_kiswahili Z_kiingereza{
gen SD_Lag`var'_pass=.
replace SD_Lag`var'_pass=SD_`var'_pass_T1 if !missing(`var'_T1)
replace SD_Lag`var'_pass=SD_`var'_pass_T5 if !missing(`var'_T5)
}
 */ 



gen LagAge=.
replace LagAge=Age_T1+1 if !missing(Age_T1)
replace LagAge=Age_T5 if !missing(Age_T5)
gen LagGrade=GradeID_T7
replace LagGrade=4 if GradeID_T3==3
replace LagGrade=GradeID_T8 if LagGrade==.
 
gen DiffDias=date_T8-date_T7
gen DiffDias2=dateTWA-dateEDI
replace DiffDias=. if DiffDias<-365 | DiffDias>365
replace DiffDias2=. if DiffDias2<-365 | DiffDias2>365
	 
merge m:1 SchoolID LagGrade using "$base_out/Consolidated/SchoolsGradeAverageScores.dta", keepus(MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati)
 
foreach subject in kiswahili kiingereza hisabati ScoreFocal{
	foreach time in T3 T7{
		drop Z_`subject'_`time'
		gen Z_`subject'_`time'=IRT_`subject'_`time'
	}
}
drop IRT* 
drop _merge
/*
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
*/
bys upid: gen  N=_N
drop if Z_ScoreFocal_T3==. & N>1 & upid!=""
drop N
save "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", replace
	
/*

******** NORMAL+TWAWEZA *********8
  
use "$base_out/Consolidated/Student_TWA.dta", clear
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
gen upidst=upid
*Merge HH invariant characteristics
 merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2013.dta", update keepus(HHSize IndexPoverty IndexEngagement expn12)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2013.dta", update keepus(HHSize IndexPoverty)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2014.dta", update keepus(HHSize IndexPoverty IndexEngagement expn13)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2014.dta", update keepus(HHSize IndexPoverty )
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TestTiming.dta", keepus(DiffIntvTestRestTest)
drop _merge
merge m:1 SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge 
merge m:1 SchoolID using "$base_out/Consolidated/SchoolDatesTest.dta"
drop _merge 
foreach var in male t05 t07 t08 t10 t21 higher_degree t23 t24 t25{
rename `var' `var'Average
}
/*
merge m:1 SchoolID GradeID_T7 stdgrp_T7 using "$base_out/Consolidated/TeachingSchedulle_T7.dta"
drop if _merge==2
drop _merge 
*/

gen StudentsGr_T1=StudentsGr1_T1 if GradeID_T3==1
replace StudentsGr_T1=StudentsGr2_T1 if GradeID_T3==2
replace StudentsGr_T1=StudentsGr3_T1 if GradeID_T3==3


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Inputs"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" | treatment_T8==2
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" | treatment_T8==1
label var TreatmentBoth "Combo"

foreach var in  Z_hisabati Z_kiswahili Z_kiingereza seenUwezoTests preSchoolYN male{
gen Lag`var'=.
replace Lag`var'=`var'_T1 if !missing(`var'_T1)
replace Lag`var'=`var'_T5 if !missing(`var'_T5)
}



gen LagExpenditure=expn13
replace LagExpenditure=expn12 if LagExpenditure==. & expn12!=.

foreach var in  HHSize IndexPoverty IndexEngagement LagExpenditure {
gen Missing`var'=!missing(`var')
replace `var'=0 if `var'==.
}

gen LagAge=.
replace LagAge=Age_T1+1 if !missing(Age_T1)
replace LagAge=Age_T5 if !missing(Age_T5)
gen LagGrade=GradeID_T7
replace LagGrade=4 if GradeID_T3==3
replace LagGrade=GradeID_T8 if LagGrade==.
 
gen DiffDias=date_T8-date_T7
gen DiffDias2=dateTWA-dateEDI
replace DiffDias=. if DiffDias<-365 | DiffDias>365
replace DiffDias2=. if DiffDias2<-365 | DiffDias2>365
	 
merge m:1 SchoolID LagGrade using "$base_out/Consolidated/SchoolsGradeAverageScores.dta", keepus(MeanGrade_LagZ_kiswahili MeanGrade_LagZ_kiingereza MeanGrade_LagZ_hisabati)
drop _merge
save "$base_out/Consolidated/Student_School_House_Teacher_Char_TWA.dta", replace
	

  

  
