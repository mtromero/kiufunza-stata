*Change Combo to Combination

**************************************************************
************************* TABLE 1 - SUMMARY STATISTICS *******
**************************************************************

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4 d_p d_p2
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
	reg `var' TD1 TD2 TD3 TD4 `if', nocons vce(cluster `clus_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p'  = nullmat(`d_p'),r(p)
	matrix A=e(b)
	matrix B=e(V)
	mat `mu_1' = nullmat(`mu_1'), A[1,1]
	mat `mu_2' = nullmat(`mu_2'), A[1,2]
	mat `mu_3' = nullmat(`mu_3'), A[1,3]
	mat `mu_4' = nullmat(`mu_4'), A[1,4]
	mat `se_1' = nullmat(`se_1'), sqrt(B[1,1])
	mat `se_2' = nullmat(`se_2'), sqrt(B[2,2])
	mat `se_3' = nullmat(`se_3'), sqrt(B[3,3])
	mat `se_4' = nullmat(`se_4'), sqrt(B[4,4])
	reghdfe `var' TD1 TD2 TD3 TD4 `if',  vce(cluster `clus_id') abs( i.`strat_id')
	test (_b[TD1] == _b[TD2]== _b[TD3]= _b[TD4])
	mat `d_p2'  = nullmat(`d_p2'),r(p)
}
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3 mu_4  se_1 se_2 se_3 se_4  d_p d_p2 {
	eret mat `mat' = ``mat''
}
end

use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"

keep SchoolID  tchsex t05 t06 t07 t08 t10 t16 t17 t19 t21 t22 t23 t24 t25 treatment treatarm SchoolID DistrictID FGS_T3 FGS_T7
rename DistrictID DistID
gen TeacherCertificate=(t16>=3) & !missing(t16)

label var t21 "Travel time from house to school"
label var tchsex "Male"
label var TeacherCertificate "Teaching Certificate"
recode tchsex (2=0)
replace t05=2013-t05
replace t07=2013-t07
label var t05 "Age (in 2013)"
label var t07 "Years of experience (in 2013)"




recode t10 (2=0)

eststo clear
eststo: my_ptest  tchsex t05 t07 TeacherCertificate if FGS_T3==1, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryTeacherTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

reg tchsex i.treatarm i.DistID if FGS_T3==1, nocons vce(cluster SchoolID)
sum tchsex if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_teachers.tex", write replace
file write newfile "`tempm'"
file close newfile
			
			
			

eststo clear
eststo: my_ptest  tchsex t05 t07 TeacherCertificate if FGS_T7==1, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryTeacherTotal_T7.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles




use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear




local studentcontrol Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza
label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Normalized Kiswahili test score"
label var LagZ_hisabati "Normalized math test score"
label var LagZ_kiingereza "Normalized English test score"
replace attendance_T3=1-attendance_T3
replace attendance_T7=1-attendance_T7

label var attendance_T3 "Attrited in year 1"
label var attendance_T7 "Attrited in year 2"
drop if upid==""

eststo clear
eststo: my_ptest  `studentcontrol' attendance_T3 attendance_T7, by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryStudentsTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


reg Lagmale i.treatarm i.DistID, nocons vce(cluster SchoolID)
sum Lagmale if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_students.tex", write replace
file write newfile "`tempm'"
file close newfile

use "$base_out/Consolidated/School.dta", replace
label var s1451_T1 "Kitchen"
label var s1452_T1 "Library"
label var s1453_T1 "Playground"
label var s1454_T1 "Staff room"
label var s1455_T1 "Outer wall"
label var s1456_T1 "Newspaper"
label var SingleShift_T1 "Single shift"
label var computersYN_T1 "Computers"
label var s120_T1  "Electricity"
label var s118_T1 "Classes outside"
label var PipedWater_T1 "Piped Water"
label var NoWater_T1 "No Water"
label var ToiletsStudents_T1 "Toilets/Students"
label var ClassRoomsStudents_T1 "Classrooms/Students"
replace TeacherStudents_T1=1/TeacherStudents_T1
label var TeacherStudents_T1 "Students/Teachers"
label var s188_T1 "Breakfast"
label var s175_T1 "Preschool"
label var s200_T1 "Track students"
label var s108_T1 "Urban"
label var SizeSchoolCommittee_T1 "Size School Committee"
label var KeepRecords_T1 "Spending records"
label var noticeboard_T1 "Noticeboard with spending information"
label var PropCommitteeFemale_T1 "Female/Committee"
label var PropCommitteeTeachers_T1 "Teacher/Committee"
label var PropCommitteeParents_T1 "Parents/Committee"
label var StudentsTotal_T1 "Enrolled students"

egen StdsAll=rowtotal(StudentsGr1_T1- StudentsGr7_T1), missing
egen StdsFocal=rowtotal(StudentsGr1_T1- StudentsGr3_T1), missing

label var InfrastructureIndex_T1 "Infrastructure index (PCA)"
label var PTR_T1 "Pupil-teacher ratio"

local schoolcontro2lb PTR_T1  SingleShift_T1  InfrastructureIndex_T1 s108_T1 StudentsTotal_T1
eststo clear
eststo: my_ptest  `schoolcontro2lb', by(treatarm) clus_id(SchoolID) strat_id(DistrictID)
esttab using "$latexcodesfinals/summarySchoolTotal.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc)) d_p2(star pvalue(d_p2) fmt(%9.2fc))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) se_4(fmt(%9.2fc) par)  .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles

reg PTR_T1 i.treatarm i.DistrictID, nocons vce(cluster SchoolID)
sum PTR_T1 if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_school.tex", write replace
file write newfile "`tempm'"
file close newfile



clear 
use "$base_out/Consolidated/Household_Baseline2013.dta", clear
keep HHID wrokyn asset_1 expn12 DistID upidst SchoolID IndexPoverty IndexKowledge IndexEngagement  treatment treatarm  HHSize upidst
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2013.dta", update keepus(HHID wrokyn HHSize DistID upidst SchoolID IndexPoverty treatment treatarm)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Baseline2014.dta", update keepus(HHID wrokyn HHSize expn13 DistID upidst SchoolID IndexPoverty IndexEngagement treatment treatarm)
drop _merge
merge m:1 upidst using "$base_out/Consolidated/Household_Endline2014.dta", update keepus(HHID wrokyn HHSize DistID upidst SchoolID IndexPoverty  treatment treatarm)
drop _merge
gen LagExpenditure=expn13
replace LagExpenditure=expn12 if LagExpenditure==. & expn12!=.

label var IndexPoverty "Wealth index (PCA)"
label var IndexEngagement "Engagement index (PCA)"
label var HHSize "HH size"
replace LagExpenditure=LagExpenditure
label var LagExpenditure "Pre-treatment expenditure (TZS)"


eststo clear
my_ptest HHSize IndexPoverty LagExpenditure , by(treatarm) clus_id(SchoolID) strat_id(DistID)
esttab using "$latexcodesfinals/summaryHouseholds.tex", label replace  booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc)) mu_4(fmt(%9.2fc))  d_p2(star pvalue(d_p2))" "se_1(par) se_2(par) se_3(par) se_4(par) .") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles


reg HHSize i.treatarm i.DistID, nocons vce(cluster SchoolID)
sum HHSize if e(sample)==1
local tempm=string(r(N), "%9.0fc")
file open newfile using "$latexcodesfinals/N_balance_household.tex", write replace
file write newfile "`tempm'"
file close newfile

**************************************************************
************************ TABLE 2 - SUMMARY TWAWEZA EXPENDITURE
**************************************************************


capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
	 reg `var' TD1 TD2 , nocons vce(cluster `clus_id')
	 test (_b[TD1]- _b[TD2]== 0)
	 mat `d_p'  = nullmat(`d_p'),r(p)
	 lincom (TD1-TD2)
	 mat `se_3' = nullmat(`se_3'), r(se)
	 qui estpost tabstat `var' , by(`by') statistics(mean sem)
	 matrix A=e(mean)
	 matrix B=e(semean)
	 mat `mu_1' = nullmat(`mu_1'), A[1,1]
	 mat `mu_2' = nullmat(`mu_2'), A[1,2]
	 mat `mu_3' = nullmat(`mu_3'), A[1,1]-A[1,2]
	 mat `se_1' = nullmat(`se_1'), B[1,1]
	 mat `se_2' = nullmat(`se_2'), B[1,2]

}
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 se_3 d_p {
	mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3  se_1 se_2 se_3  d_p {
	eret mat `mat' = ``mat''
}
end


use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"


gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"

foreach i in 3 7{
	egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
	replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
	label variable gdrctb`grade'PS_T3 "\\$ Textbooks/Student Grd `grade'"
	label variable gdrctb`grade'PS_T7 "\\$ Textbooks/Student Grd `grade'"
}


foreach i in 3 7{
	label variable ExpendText_T`i' "\\$ Textbooks/Student"  
	label variable administrative_expensesPS_T`i' "\\$ Admin./Student"
	label variable teaching_aid_expensesPS_T`i' "\\$ Teaching Aid/Student"
	label variable student_expensesPS_T`i' "\\$ Student/Student"
	label variable Teacher_expensesPS_T`i' "\\$ Teacher/Student"
	label variable Construction_expensesPS_T`i' "\\$ Construction/Student" 
	label variable textbook_expensesPS_T`i' "\\$ Textbooks/Student"
	
	label variable administrative_twawezaPS_T`i' "\\$ Admin./Student"
	label variable teaching_aid_twawezaPS_T`i' "\\$ Teaching Aid/Student"
	label variable student_twawezaPS_T`i' "\\$ Student/Student"
	label variable Teacher_twawezaPS_T`i' "\\$ Teacher/Student"
	label variable Construction_twawezaPS_T`i' "\\$ Construction/Student" 
	label variable textbook_twawezaPS_T`i' "\\$ Textbooks/Student"
	
	label variable studentsTeacherRatio_T`i' "Teacher Ratio"
	label variable studentsVolunteerRatio_T`i' "Vol. Ratio"
	label variable strategicly_change_teachers_T`i' "HT changed teachers"
	label variable strategicly_change_teachers_T`i' "HT changed students"
	label variable administrative_expenses_T`i' "\\$ Admin."
	label variable student_expenses_T`i' "\\$ Student"  
	label variable Teacher_expenses_T`i' "\\$ Teacher"
}

label var TimesCommitteeMet2013_T3 "No. Committee meetings"
label var debtyn_T3 "Debts"
label var ifnbyn_Since_T1 "Notice Board"
label var ifscyn_Since_T1 "Committee Meetings"
label var ifpmyn_Since_T1 "Parents Meetings"
label var ifotyn_Since_T1 "Others"


local treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
local schoolcontrol s1451_T1 s1452_T1 s1453_T1 s1454_T1 s1455_T1 s1456_T1 s1431_T1 s1432_T1 s1433_T1 s1434_T1 s1435_T1 computersYN_T1 s120_T1 s118_T1 PipedWater_T1 NoWater_T1 SingleShift_T1 ToiletsStudents_T1 ClassRoomsStudents_T1 TeacherStudents_T1 s188_T1 s175_T1 s200_T1 s108_T1 SizeSchoolCommittee_T1 KeepRecords_T1 noticeboard_T1 PropCommitteeFemale_T1 PropCommitteeTeachers_T1 PropCommitteeParents_T1  StudentsTotal_T1


foreach var in administrative_twaweza student_twaweza teaching_aid_twaweza Teacher_twaweza Construction_twaweza Savings_twaweza{
	capture replace `var'_T3=. if treatarm>=3
	capture replace `var'_T7=. if treatarm>=3
	capture replace `var'PS_T3=. if treatarm>=3
	capture replace `var'PS_T7=. if treatarm>=3
}

egen TotalExpenditure_T3=rowtotal(administrative_expensesPS_T3 student_expensesPS_T3 teaching_aid_expensesPS_T3 Teacher_expensesPS_T3 Construction_expensesPS_T3 textbook_expensesPS_T3), missing
egen TotalExpenditure_T7=rowtotal(administrative_expensesPS_T7 student_expensesPS_T7 teaching_aid_expensesPS_T7 Teacher_expensesPS_T7 Construction_expensesPS_T7 textbook_expensesPS_T7), missing

label var TotalExpenditure_T3 Total
label var TotalExpenditure_T7 Total

egen TotalExpenditureTwa_T3=rowtotal(administrative_twawezaPS_T3 student_twawezaPS_T3 teaching_aid_twawezaPS_T3 Teacher_twawezaPS_T3 Construction_twawezaPS_T3 textbook_twawezaPS_T3), missing
egen TotalExpenditureTwa_T7=rowtotal(administrative_twawezaPS_T7 student_twawezaPS_T7 teaching_aid_twawezaPS_T7 Teacher_twawezaPS_T7 Construction_twawezaPS_T7 textbook_twawezaPS_T7), missing

label var TotalExpenditureTwa_T3 Total
label var TotalExpenditureTwa_T7 Total



gen Unaccounted_twawezaPS_T3=10000-TotalExpenditureTwa_T3
gen Unaccounted_twawezaPS_T7=10000-TotalExpenditureTwa_T7
foreach var in TotalExpenditureTwa administrative_twawezaPS student_twawezaPS teaching_aid_twawezaPS Teacher_twawezaPS Construction_twawezaPS textbook_twawezaPS Unaccounted_twawezaPS{
	gen `var'_TT=(`var'_T3+`var'_T7)/2
	_crcslbl `var'_TT `var'_T7 
}
label var Unaccounted_twawezaPS_T3 "Unspent/unaccounted funds"
label var Unaccounted_twawezaPS_T7 "Unspent/unaccounted funds"

keep textbook_twawezaPS_* TotalExpenditureTwa_* administrative_twawezaPS_* student_twawezaPS_* teaching_aid_twawezaPS_* Teacher_twawezaPS_* Construction_twawezaPS_* Unaccounted_twawezaPS_* treatarm SchoolID DistrictID

reshape long TotalExpenditureTwa_T@ administrative_twawezaPS_T@ textbook_twawezaPS_T@ student_twawezaPS_T@ teaching_aid_twawezaPS_T@ Teacher_twawezaPS_T@ Construction_twawezaPS_T@ Unaccounted_twawezaPS_T@ , i(SchoolID) j(time) string

label variable TotalExpenditureTwa_T "TZS Total/Student"  
label variable administrative_twawezaPS_T "TZS Admin./Student"
label variable teaching_aid_twawezaPS_T "TZS Teaching Aid/Student"
label variable student_twawezaPS_T "TZS Student/Student"
label variable Teacher_twawezaPS_T "TZS Teacher/Student"
label variable Construction_twawezaPS_T "TZS Construction/Student" 
label variable textbook_twawezaPS_T "TZS Textbooks/Student" 
label var Unaccounted_twawezaPS_T "TZS Unspent funds"

label variable TotalExpenditureTwa_T "Total"  
label variable administrative_twawezaPS_T "Admin."
label variable teaching_aid_twawezaPS_T "Teaching aids"
label variable student_twawezaPS_T "Students"
label variable Teacher_twawezaPS_T "Teachers"
label variable Construction_twawezaPS_T "Construction" 
label variable textbook_twawezaPS_T "Textbooks"
label var Unaccounted_twawezaPS_T "Unspent funds"


*drop if time=="T"
replace time = "9" if time=="T"
destring time, replace
label define year 3 "Year 1" 7 "Year 2" 9 "Average"
label values time year
 
gen tot=TotalExpenditureTwa_T +Unaccounted_twawezaPS_T

label var TotalExpenditureTwa_T "Total Expenditure"
label var tot "Total Value of CG"
eststo clear
estpost  tabstat  administrative_twawezaPS_T student_twawezaPS_T textbook_twawezaPS_T teaching_aid_twawezaPS_T Teacher_twawezaPS_T Construction_twawezaPS_T  ///
		  TotalExpenditureTwa_T Unaccounted_twawezaPS_T tot if treatarm==2, by(time) statistics(mean semean) columns(statistics) listwise nototal
*TotalExpenditureTwa_T

esttab using "$latexcodesfinals/SummaryTwawezaExp_CG.tex", main(mean %9.2fc) aux(semean %9.2fc)  nostar unstack noobs nonote nonumber fragment   replace  label nomtitles nodepvars  

eststo clear
estpost  tabstat  administrative_twawezaPS_T student_twawezaPS_T textbook_twawezaPS_T teaching_aid_twawezaPS_T Teacher_twawezaPS_T Construction_twawezaPS_T  ///
		  TotalExpenditureTwa_T Unaccounted_twawezaPS_T tot if treatarm==2 & time==7, statistics(mean semean) columns(variables) 
esttab using "$latexcodesfinals/SummaryTwawezaExp_CG_landscape.tex" , ///
 cells("administrative student textbook teaching Teacher Construction TotalExpenditure Unaccounted tot") ///
nomtitle nonumber replace label collabels(none) fragment noobs nolines


eststo clear
estpost  tabstat  administrative_twawezaPS_T student_twawezaPS_T textbook_twawezaPS_T teaching_aid_twawezaPS_T Teacher_twawezaPS_T Construction_twawezaPS_T  ///
		  TotalExpenditureTwa_T Unaccounted_twawezaPS_T tot if treatarm==1, by(time) statistics(mean semean) columns(statistics) listwise nototal
*TotalExpenditureTwa_T

esttab using "$latexcodesfinals/SummaryTwawezaExp_Combo.tex", main(mean %9.2fc) aux(semean %9.2fc)  nostar unstack noobs nonote nonumber fragment collabels(none) replace  label nomtitles nodepvars  


capt prog drop my_ptest_diff
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest_diff, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strataid(varname numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname  mu_3 se_3 d_p
capture drop TD*
tab `by', gen(TD)
foreach var of local varlist {
	 reghdfe `var' TD1 TD2 ,  vce(cluster `clus_id') absorb(`strataid')
	 test (_b[TD1]- _b[TD2]== 0)
	 mat `d_p'  = nullmat(`d_p'),r(p)
	 lincom (TD1-TD2)
	 mat `mu_3' = nullmat(`mu_3'), r(estimate)
	 mat `se_3' = nullmat(`se_3'), r(se)	

}
foreach mat in mu_3   se_3 d_p {
	mat coln ``mat'' = `varlist'
}
eret local cmd "my_ptest_diff"
foreach mat in mu_3  se_3  d_p {
	eret mat `mat' = ``mat''
}
end


my_ptest_diff administrative_twawezaPS_T student_twawezaPS_T textbook_twawezaPS_T teaching_aid_twawezaPS_T Teacher_twawezaPS_T Construction_twawezaPS_T  ///
		  TotalExpenditureTwa_T Unaccounted_twawezaPS_T tot if treatarm==1  | treatarm==2, by(treatarm) clus_id(SchoolID) strataid(DistrictID) statistics(mean semean) columns(statistics) listwise nototal
esttab using "$latexcodesfinals/SummaryTwawezaExp_Diff.tex", label replace  fragment nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01) ///
collabels(none)  ///
cells("mu_3(fmt(%9.2fc) star pvalue(d_p))" "se_3(par)") ///
addnotes("/specialcell{Standard errors in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") nolines nomtitles gaps
********************************************************************
*********************** TABLE 3 - EXPENDITURE, SUBSTITUTION, PARENT
********************************************************************




use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
gen TreatmentControl=0 
replace TreatmentControl=1 if treatment=="Control"
label var TreatmentControl "Control"

label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

foreach i in 3 7{
	egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
	replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
	label variable gdrctb`grade'PS_T3 "/\$ Textbooks/Student Grd `grade'"
	label variable gdrctb`grade'PS_T7 "/\$ Textbooks/Student Grd `grade'"
}


foreach i in 3 7{
	label variable ExpendText_T`i' "/\$ Textbooks/Student"  
	label variable administrative_expensesPS_T`i' "/\$ Admin./Student"
	label variable teaching_aid_expensesPS_T`i' "/\$ Teaching Aid/Student"
	label variable student_expensesPS_T`i' "/\$ Student/Student"
	label variable Teacher_expensesPS_T`i' "/\$ Teacher/Student"
	label variable Construction_expensesPS_T`i' "/\$ Construction/Student" 

	label variable administrative_twawezaPS_T`i' "/\$ Admin./Student"
	label variable teaching_aid_twawezaPS_T`i' "/\$ Teaching Aid/Student"
	label variable student_twawezaPS_T`i' "/\$ Student/Student"
	label variable Teacher_twawezaPS_T`i' "/\$ Teacher/Student"
	label variable Construction_twawezaPS_T`i' "/\$ Construction/Student" 

	label variable studentsTeacherRatio_T`i' "Teacher Ratio"
	label variable studentsVolunteerRatio_T`i' "Vol. Ratio"
	label variable strategicly_change_teachers_T`i' "HT changed teachers"
	label variable strategicly_change_teachers_T`i' "HT changed students"
	label variable administrative_expenses_T`i' "/\$ Admin."
	label variable student_expenses_T`i' "/\$ Student"  
	label variable Teacher_expenses_T`i' "/\$ Teacher"
}

label var TimesCommitteeMet2013_T3 "No. Committee meetings"
label var debtyn_T3 "Debts"
label var ifnbyn_Since_T1 "Notice Board"
label var ifscyn_Since_T1 "Committee Meetings"
label var ifpmyn_Since_T1 "Parents Meetings"
label var ifotyn_Since_T1 "Others"

/*
egen TotalExpenditure_T3=rowtotal(administrative_expensesPS_T3 student_expensesPS_T3 teaching_aid_expensesPS_T3 Teacher_expensesPS_T3 Construction_expensesPS_T3), missing
egen TotalExpenditure_T7=rowtotal(administrative_expensesPS_T7 student_expensesPS_T7 teaching_aid_expensesPS_T7 Teacher_expensesPS_T7 Construction_expensesPS_T7), missing
*/

egen TotalExpenditure_T3=rowtotal(administrative_expensesPS_T3 student_expensesPS_T3 teaching_aid_expensesPS_T3 Teacher_expensesPS_T3 Construction_expensesPS_T3 textbook_expensesPS_T3), missing
egen TotalExpenditure_T7=rowtotal(administrative_expensesPS_T7 student_expensesPS_T7 teaching_aid_expensesPS_T7 Teacher_expensesPS_T7 Construction_expensesPS_T7 textbook_expensesPS_T7), missing

label var TotalExpenditure_T3 Total
label var TotalExpenditure_T7 Total

egen TotalExpenditureTwa_T3=rowtotal(administrative_twawezaPS_T3 student_twawezaPS_T3 teaching_aid_twawezaPS_T3 Teacher_twawezaPS_T3 Construction_twawezaPS_T3 textbook_twawezaPS_T3), missing
egen TotalExpenditureTwa_T7=rowtotal(administrative_twawezaPS_T7 student_twawezaPS_T7 teaching_aid_twawezaPS_T7 Teacher_twawezaPS_T7 Construction_twawezaPS_T7 textbook_twawezaPS_T7), missing

/*
egen TotalExpenditureTwa_T3=rowtotal(administrative_twawezaPS_T3 student_twawezaPS_T3 teaching_aid_twawezaPS_T3 Teacher_twawezaPS_T3 Construction_twawezaPS_T3), missing
egen TotalExpenditureTwa_T7=rowtotal(administrative_twawezaPS_T7 student_twawezaPS_T7 teaching_aid_twawezaPS_T7 Teacher_twawezaPS_T7 Construction_twawezaPS_T7), missing
*/

label var TotalExpenditureTwa_T3 Total
label var TotalExpenditureTwa_T7 Total

egen  TotalSub_M_T3=rowtotal(GovermentCG_M_T3 GovermentOther_M_T3 LocalGoverment_M_T3 NGO_M_T3 Parents_M_T3 Other_M_T3), missing
egen  TotalSub_M_T7=rowtotal(GovermentCG_M_T7 GovermentOther_M_T7 LocalGoverment_M_T7 NGO_M_T7 Parents_M_T7 Other_M_T7), missing

egen  TotalSub_D_T1=rowtotal(GovermentCG_D_T1 GovermentOther_D_T1 LocalGoverment_D_T1 NGO_D_T1 Parents_D_T1 Other_D_T1), missing
egen  TotalSub_D_T3=rowtotal(GovermentCG_D_T3 GovermentOther_D_T3 LocalGoverment_D_T3 NGO_D_T3 Parents_D_T3 Other_D_T3), missing
egen  TotalSub_D_T7=rowtotal(GovermentCG_D_T7 GovermentOther_D_T7 LocalGoverment_D_T7 NGO_D_T7 Parents_D_T7 Other_D_T7), missing

replace TotalSub_D_T1=(TotalSub_D_T1>0) if !missing(TotalSub_D_T1)
replace TotalSub_D_T3=(TotalSub_D_T3>0) if !missing(TotalSub_D_T3)
replace TotalSub_D_T7=(TotalSub_D_T7>0) if !missing(TotalSub_D_T7)

label var TotalSub_M_T3 Total
label var TotalSub_M_T7 Total

label var TotalSub_D_T3 Any
label var TotalSub_D_T7 Any


rename administrative_expensesPS* admin_expPS*
rename administrative_twawezaPS* admin_exptwaPS*
rename teaching_aid_twawezaPS* teaching_aid_twaPS*
rename Construction_twawezaPS* Construction_twaPS*

** NOW IW WANT DEPVAR2 TO BE ON TOP OF TWA EXPENDITURE


gen TotalExpenditure_T_T3=TotalExpenditure_T3
gen admin_expPS_T_T3=admin_expPS_T3
gen student_expensesPS_T_T3=student_expensesPS_T3
gen teaching_aid_expensesPS_T_T3=teaching_aid_expensesPS_T3
gen Teacher_expensesPS_T_T3=Teacher_expensesPS_T3
gen Construction_expensesPS_T_T3=Construction_expensesPS_T3

gen TotalExpenditure_T_T7=TotalExpenditure_T7
gen admin_expPS_T_T7=admin_expPS_T7
gen student_expensesPS_T_T7=student_expensesPS_T7
gen teaching_aid_expensesPS_T_T7=teaching_aid_expensesPS_T7
gen Teacher_expensesPS_T_T7=Teacher_expensesPS_T7
gen Construction_expensesPS_T_T7=Construction_expensesPS_T7

replace TotalExpenditure_T3=TotalExpenditure_T3-TotalExpenditureTwa_T3
replace admin_expPS_T3=admin_expPS_T3-admin_exptwaPS_T3
replace student_expensesPS_T3=student_expensesPS_T3-student_twawezaPS_T3
replace teaching_aid_expensesPS_T3=teaching_aid_expensesPS_T3-teaching_aid_twaPS_T3
replace Teacher_expensesPS_T3=Teacher_expensesPS_T3-Teacher_twawezaPS_T3
replace Construction_expensesPS_T3=Construction_expensesPS_T3-Construction_twaPS_T3

replace TotalExpenditure_T7=TotalExpenditure_T7-TotalExpenditureTwa_T7
replace admin_expPS_T7=admin_expPS_T7-admin_exptwaPS_T7
replace student_expensesPS_T7=student_expensesPS_T7-student_twawezaPS_T7
replace teaching_aid_expensesPS_T7=teaching_aid_expensesPS_T7-teaching_aid_twaPS_T7
replace Teacher_expensesPS_T7=Teacher_expensesPS_T7-Teacher_twawezaPS_T7
replace Construction_expensesPS_T7=Construction_expensesPS_T7-Construction_twaPS_T7

rename teaching_aid_expenses* teaching_aid_exp*
rename Construction_expenses* Construction_exp*

global depvars1 TotalExpenditureTwa admin_exptwaPS student_twawezaPS teaching_aid_twaPS Teacher_twawezaPS Construction_twaPS 
global depvars2 TotalExpenditure admin_expPS student_expensesPS teaching_aid_expPS Teacher_expensesPS Construction_expPS
global depvars2_b TotalExpenditure_T admin_expPS_T student_expensesPS_T teaching_aid_expPS_T Teacher_expensesPS_T Construction_expPS_T       
global depvars3 TotalSub_M GovermentCG_M GovermentOther_M LocalGoverment_M NGO_M Parents_M Other_M

label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

foreach var in $schoolcontrol{
	sum `var' if treatment=="Control"
	replace `var'=`var'-r(mean)
}

***** 1, TOTAL EXPENDITURE FROM THE GRANT
eststo clear
foreach var in $depvars1{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo twa_`var': reg `var'_T $treatmentlist $schoolcontrol  (i.DistrictID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab twa* using "$latexcodesfinals/School_ExpenditureTWA.tex", se ar2 label fragment nolines ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars1{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo twa_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol (i.DistrictID)##T2, vce(cluster SchoolID)
		estadd ysumm
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab twa*`time' using "$latexcodesfinals/School_ExpenditureTWA`time'.tex", se ar2 label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}

 
***** 2, TOTAL EXPENDITURE (not grant)
foreach var in $depvars2{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo s_`var': reg `var'_T $treatmentlist $schoolcontrol  (i.DistrictID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab s* using "$latexcodesfinals/School_Expenditure.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars2{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo s_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol  ( i.DistrictID)##T2, vce(cluster SchoolID)
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab s*`time' using "$latexcodesfinals/School_Expenditure_`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


}
preserve
keep TotalExpenditure* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
save "$base_out/Consolidated/SchoolExpenditure_HH.dta", replace
restore


***** 3, TOTAL EXPENDITURE (school_leve)
foreach var in $depvars2_b{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo ts_`var': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab ts* using "$latexcodesfinals/School_Expenditure_Total.tex", se ar2 label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc a2 a2 a2 a2 a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars2_b{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo ts_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab ts*`time' using "$latexcodesfinals/School_Expenditure_Total_`time'.tex", se ar2 label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}


***** Not in main table, but, SUBSTITUTION


foreach var in $depvars3{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
	eststo sub_`var': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
esttab sub_* using "$latexcodesfinals/OtherFunding.tex", se ar2 label fragment nolines nogaps ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars3{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistrictID SchoolID  treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
		eststo sub_`var'_`time': reg `var'_T $treatmentlist $schoolcontrol ( i.DistrictID)##T2, vce(cluster SchoolID)
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)		
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	esttab sub_*`time' using "$latexcodesfinals/OtherFunding_`time'.tex", se ar2 label fragment nolines nogaps ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}
*** 4, PARENTAL


use "$base_out/Consolidated/Household.dta", clear
drop if treatment==""
drop if DistID==.
gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"



global depvars   Expenditure_FC AdultAttendsSchoolsMeeting AdultMeetsTeacher AdultGivesSchool AdultAtHome FCTutoring breakfast MoreBooks


global treatmentlist TreatmentCG TreatmentCOD TreatmentBoth
global varcontrol  bnkacc prdyyn  ltcbyn workyn NumHHMembers wall_mud floor_mud roof_durable improveWater improveSanitation HHElectricty  asset_1 asset_2 asset_3 asset_4 asset_5 asset_6 asset_7 asset_8 


global depvars2  Expenditure_FC expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139
global depvars2b  Expenditure2_FC expn131 expn132 expn133 expn134 expn135 expn136 expn137 expn138 expn139

label var expn131_T3 "Fees"
label var expn131_T7 "Fees"

label var expn132_T3 "Textbooks"
label var expn132_T7 "Textbooks"

label var expn133_T3 "Other books"
label var expn133_T7 "Other books"

label var expn134_T3 "Supplies"
label var expn134_T7 "Supplies"

label var expn135_T3 "Uniforms"
label var expn135_T7 "Uniforms"

label var expn136_T3 "Tutoring"
label var expn136_T7 "Tutoring"

label var expn137_T3 "Transport"
label var expn137_T7 "Transport"

label var expn138_T3 "Food"
label var expn138_T7 "Food"

label var expn139_T3 "Others"
label var expn139_T7 "Others"

foreach time in T3 T7{
	gen Expenditure2_FC_`time'=Expenditure_FC_`time'-expn135_`time'-expn138_`time'
}

foreach var in $varcontrol{
	gen Lag`var'=.
	replace Lag`var'=`var'_T1 if !missing(`var'_T1)
	replace Lag`var'=`var'_T5 if !missing(`var'_T5) & missing(Lag`var')
}


foreach j in 3 7{
	label var MoreBooks_T`j' "More Books Provided"
	label var Expenditure_FC_T`j' "Total Expenditure"
	label var AdultAttendsSchoolsMeeting_T`j' "Attend Meetings"
	label var AdultMeetsTeacher_T`j' "Meet Teacher"
	label var AdultGivesSchool_T`j' "Donate"
	label var AdultAtHome_T`j' "Adult at home"
	label var FCTutoring_T`j' "Tutoring"
	label var breakfast_T`j' "Breakfast"
}

sum Expenditure_FC_T3, d
replace Expenditure_FC_T3=r(p95) if Expenditure_FC_T3>r(p95) & !missing(Expenditure_FC_T3)
sum Expenditure_FC_T7, d
replace Expenditure_FC_T7=r(p95) if Expenditure_FC_T7>r(p95) & !missing(Expenditure_FC_T7)

sum Expenditure2_FC_T3, d
replace Expenditure2_FC_T3=r(p95) if Expenditure2_FC_T3>r(p95) & !missing(Expenditure2_FC_T3)
sum Expenditure2_FC_T7, d
replace Expenditure2_FC_T7=r(p95) if Expenditure2_FC_T7>r(p95) & !missing(Expenditure2_FC_T7)
	
			
sum Expenditure_FC_2012_T1, d
replace Expenditure_FC_2012_T1=r(p95) if Expenditure_FC_2012_T1>r(p95) & !missing(Expenditure_FC_2012_T1)
sum Expenditure_FC_2013_T5, d
replace Expenditure_FC_2013_T5=r(p95) if Expenditure_FC_2013_T5>r(p95) & !missing(Expenditure_FC_2013_T5)
		
		
gen LagExpenditure=Expenditure_FC_2012_T1 
replace LagExpenditure=Expenditure_FC_2013_T5 if LagExpenditure==. & Expenditure_FC_2013_T5!=.

global varcontrol Lagbnkacc Lagprdyyn  Lagltcbyn Lagworkyn LagNumHHMembers Lagwall_mud Lagfloor_mud Lagroof_durable LagimproveWater LagimproveSanitation LagHHElectricty Lagasset_1 Lagasset_2 Lagasset_3 Lagasset_4 Lagasset_5 Lagasset_6 Lagasset_7 Lagasset_8 LagExpenditure
	
drop *T5
drop *T1	

/*this is new, simply collapse the data*/
keep Expenditure2_FC* Expenditure_FC* expn13* $treatmentlist $varcontrol DistID SchoolID treatment
collapse (mean) Expenditure2_FC* Expenditure_FC* expn13* $varcontrol $treatmentlist, by(SchoolID DistID treatment)
label var TreatmentCG "Grants"
label var TreatmentCOD "Incentives"
label var TreatmentBoth "Combination"
merge 1:1 SchoolID using "$base_out/Consolidated/SchoolExpenditure_HH.dta"

foreach var in $depvars2{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-792=.)
	eststo hh_`var': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)		
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"
esttab hh* using "$latexcodesfinals/Household.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

foreach var in $depvars2b{
	preserve
	keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
	reshape long `var'_T@, i(SchoolID) j(T) string
	drop if T!="3" & T!="7"
	encode T, gen(T2)
	recode `var'_T (-792=.)
	eststo hhb_`var': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
	sum `var'_T if treatment=="Control" | treatment=="CG"
	estadd scalar N2=r(N)		
	estadd ysumm
	sum `var'_T`i' if treatment=="Control"
	estadd scalar ymean2=r(mean)
	test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	restore
}
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"
esttab hhb* using "$latexcodesfinals/Household_B.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	foreach var in $depvars2{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-792=.)
		eststo hh_`var'_`time': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	label var TreatmentCOD "Incentives (\$\alpha_2\$)"
	label var TreatmentCG "Grants (\$\alpha_1\$)"
	label var TreatmentBoth "Combination (\$\alpha_3\$)"
	esttab hh*_`time' using "$latexcodesfinals/Household_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc  %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}

foreach time in 3 7{
	foreach var in $depvars2b{
		preserve
		keep `var'* $treatmentlist $schoolcontrol DistID SchoolID treatment
		reshape long `var'_T@, i(SchoolID) j(T) string
		drop if T!="`time'"
		encode T, gen(T2)
		recode `var'_T (-792=.)
		eststo hhb_`var'_`time': quietly  reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
		sum `var'_T if treatment=="Control" | treatment=="CG"
		estadd scalar N2=r(N)
		estadd ysumm
		sum `var'_T`i' if treatment=="Control"
		estadd scalar ymean2=r(mean)
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
		restore
	}
	label var TreatmentCOD "Incentives (\$\alpha_2\$)"
	label var TreatmentCG "Grants (\$\alpha_1\$)"
	label var TreatmentBoth "Combination (\$\alpha_3\$)"
	esttab hhb*_`time' using "$latexcodesfinals/Household_B_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc  %9.2fc %9.2fc a2 %9.2fc a2) labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}




save "$base_out/Consolidated/SchoolExpenditure_HH.dta", replace

gen PerChildExpenditure_T3=TotalExpenditure_T_T3+Expenditure_FC_T3
gen PerChildExpenditure_T7=TotalExpenditure_T_T7+Expenditure_FC_T7

gen PerChildExpenditure2_T3=TotalExpenditure_T_T3+Expenditure2_FC_T3
gen PerChildExpenditure2_T7=TotalExpenditure_T_T7+Expenditure2_FC_T7


eststo perchild_T3:  reg PerChildExpenditure_T3 $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T3 if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure_T3 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

eststo perchild_T7: reg PerChildExpenditure_T7 $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T7 if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure_T7 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

preserve
reshape long PerChildExpenditure_T@, i(SchoolID) j(T) string
encode T, gen(T2)

eststo perchild:  reg PerChildExpenditure_T $treatmentlist $schoolcontrol  i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure_T if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure_T if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
restore


*********8 V2 without food an uniforms


eststo perchild2_T3:  reg PerChildExpenditure2_T3 $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure2_T3 if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure2_T3 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

eststo perchild2_T7: reg PerChildExpenditure2_T7 $treatmentlist $schoolcontrol  i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure2_T7 if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure2_T7 if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]

preserve
reshape long PerChildExpenditure2_T@, i(SchoolID) j(T) string
encode T, gen(T2)

eststo perchild2:  reg PerChildExpenditure2_T $treatmentlist $schoolcontrol i.DistrictID, vce(cluster SchoolID)
estadd ysumm
sum PerChildExpenditure2_T if treatment=="Control" | treatment=="CG"
estadd scalar N2=r(N)
sum PerChildExpenditure2_T if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth] -_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]


*********** COMBINE THE RESULTS **************
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"
restore

esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hh_Expenditure_FC perchild  using "$latexcodesfinals/Subs.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hhb_Expenditure2_FC perchild2  using "$latexcodesfinals/Subs_B.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hh_Expenditure_FC_`time' perchild_T`time'  using "$latexcodesfinals/Subs_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hhb_Expenditure2_FC_`time' perchild2_T`time'  using "$latexcodesfinals/Subs_B_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control" "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)""\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1=0\$)")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}



esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hh_Expenditure_FC perchild  using "$latexcodesfinals/Subs_CG.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N2 ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG  ) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

esttab twa_TotalExpenditureTwa  s_TotalExpenditure ts_TotalExpenditure_T hhb_Expenditure2_FC perchild2  using "$latexcodesfinals/Subs_B_CG.tex", se ar2 booktabs label fragment nolines  ///
star(* 0.10 ** 0.05 *** 0.01) replace  ///
stats(N2 ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG  ) nonumbers nomtitle ///
nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")


foreach time in 3 7{
	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hh_Expenditure_FC_`time' perchild_T`time'  using "$latexcodesfinals/Subs_CG_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N2 ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG ) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

	esttab twa_TotalExpenditureTwa_`time'   s_TotalExpenditure_`time' ts_TotalExpenditure_T_`time' hhb_Expenditure2_FC_`time' perchild2_T`time'  using "$latexcodesfinals/Subs_B_CG_`time'.tex", se ar2 booktabs label fragment nolines  ///
	star(* 0.10 ** 0.05 *** 0.01) replace  ///
	stats(N2 ymean2, fmt(%9.0fc %9.2fc)  labels ("N. of obs." "Mean control")) substitute(/_ _) ///
	b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG ) nonumbers nomtitle ///
	nonotes addnotes("/specialcell{Standard errors in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }" "/tiny{\\$ Textbooks/Student is the total amount spent in text books divided by the total number of students enrolled in school; \\$ Admin./Student is the total amount spent in administrative personnel salaries, rent and utilities, maintenance, and other administrative expenses divide by the total number of students; \\$ Student/Student is the total amount spent in scholarships and food for students divided by the total number of students; \\$ Teacher/Student is the amount spent in teacher salaries, bonuses and training divided by the total number of students; Teacher Ratio is the teacher students ratios at the school and Vol. Ratio is the volunteer students ratio.}")

}


********************************************************************
*********************** textbooks
********************************************************************
use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm
merge 1:m SchoolID using "$base_out/3 Endline/School/TeacherAverage.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge



gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
gen TreatmentControl=0 
replace TreatmentControl=1 if treatment=="Control"
label var TreatmentControl "Control"


foreach i in 3 7{
	egen ExpendText_T`i'=rowtotal(gdrctb1_T`i'-gdrctb7_T`i')
	replace ExpendText_T`i'=ExpendText_T`i'/TotalNumberStudents_T`i'
}

forvalue grade=1/7{
	label variable gdrctb`grade'PS_T3 "\\$ Textbooks/Student Grd `grade'"
	label variable gdrctb`grade'PS_T7 "\\$ Textbooks/Student Grd `grade'"
}

forvalue grade=1/7{
	gen gdrctbPS_T3`grade'= gdrctb`grade'PS_T3
	gen gdrctbPS_T7`grade'= gdrctb`grade'PS_T7
}

preserve
keep gdrctbPS_T* SchoolID DistrictID $schoolcontrol $treatmentlist treatment
reshape long gdrctbPS_T3 gdrctbPS_T7, i(SchoolID) j(Grade)
reshape long gdrctbPS_T@ , i(SchoolID Grade) j(T)
drop if T!=3 & T!=7
gen FG=(Grade<=3)
eststo books_0: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==0, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum gdrctbPS_T if treatment=="Control" & FG==0
estadd scalar ymean2=r(mean)

eststo books_1: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==1, vce(cluster SchoolID)
test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
sum gdrctbPS_T if treatment=="Control" & FG==1
estadd scalar ymean2=r(mean)

rename TreatmentCG TreatmentCG2
rename TreatmentCOD TreatmentCOD2
rename TreatmentBoth TreatmentBoth2
eststo books_2: reg gdrctbPS_T c.($treatmentlist)##c.FG c.FG##(c.($schoolcontrol)  i.DistrictID)##T, vce(cluster SchoolID)
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]
test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
estadd scalar p2=r(p)
estadd scalar suma2=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]
sum gdrctbPS_T if treatment=="Control" & FG==1
scalar def mean1=r(mean)
sum gdrctbPS_T if treatment=="Control" & FG==0
scalar def mean2=r(mean)
estadd scalar ymean2=scalar(mean1)-scalar(mean2)

esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2.tex", se booktabs label fragment nolines  nomtitles ///
rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
coeflabels(TreatmentBoth "Combination (\$\alpha_3\$)" TreatmentCOD "Incentives (\$\alpha_2\$)" TreatmentCG "Grants (\$\alpha_1\$)") ///
stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
"\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "\$\alpha_3-\alpha_1\$" "p-value (\$\alpha_3-\alpha_1\$=0)")) substitute(/_ _)
restore



foreach time in 3 7{
	preserve
	keep gdrctbPS_T* SchoolID DistrictID $schoolcontrol $treatmentlist treatment
	reshape long gdrctbPS_T3 gdrctbPS_T7, i(SchoolID) j(Grade)
	reshape long gdrctbPS_T@ , i(SchoolID Grade) j(T)
	drop if T!=3 & T!=7
	drop if T!=`time'
	gen FG=(Grade<=3)

	eststo books_0: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==0, vce(cluster SchoolID)
	test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	sum gdrctbPS_T if treatment=="Control" & FG==0
	estadd scalar ymean2=r(mean)

	eststo books_1: reg gdrctbPS_T c.($treatmentlist) (c.($schoolcontrol)  i.DistrictID)##T if FG==1, vce(cluster SchoolID)
	test (_b[TreatmentBoth]- _b[TreatmentCOD]-_b[TreatmentCG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	test (_b[TreatmentBoth]-_b[TreatmentCG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[TreatmentBoth]-_b[TreatmentCG]
	sum gdrctbPS_T if treatment=="Control" & FG==1
	estadd scalar ymean2=r(mean)

	rename TreatmentCG TreatmentCG2
	rename TreatmentCOD TreatmentCOD2
	rename TreatmentBoth TreatmentBoth2
	eststo books_2: reg gdrctbPS_T c.($treatmentlist)##c.FG c.FG##(c.($schoolcontrol)  i.DistrictID)##T, vce(cluster SchoolID)
	test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
	estadd scalar p=r(p)
	estadd scalar suma=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCOD2#c.FG]-_b[c.TreatmentCG2#c.FG]
	test (_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]=0)
	estadd scalar p2=r(p)
	estadd scalar suma2=_b[c.TreatmentBoth2#c.FG]-_b[c.TreatmentCG2#c.FG]
	sum gdrctbPS_T if treatment=="Control" & FG==1
	scalar def mean1=r(mean)
	sum gdrctbPS_T if treatment=="Control" & FG==0
	scalar def mean2=r(mean)
	estadd scalar ymean2=scalar(mean1)-scalar(mean2)


	esttab books_0 books_1 books_2  using "$latexcodesfinals/School_textbook_extra2_`time'.tex", se booktabs label fragment nolines  nomtitles ///
	rename(c.TreatmentBoth2#c.FG TreatmentBoth c.TreatmentCOD2#c.FG TreatmentCOD c.TreatmentCG2#c.FG TreatmentCG) ///
	keep(TreatmentBoth TreatmentCOD TreatmentCG) star(* 0.10 ** 0.05 *** 0.01) replace ///
	b(%9.2fc)se(%9.2fc)nocon  nonumbers  ///
	coeflabels(TreatmentBoth "Combination" TreatmentCOD "Incentives" TreatmentCG "Grants") ///
	stats(N ymean2 suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc a2 %9.2fc a2)  labels ("N. of obs." "Mean control"   ///
	"\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "\$\alpha_3-\alpha_1\$" "p-value (\$H_0=\alpha_3-\alpha_1\$=0)")) substitute(/_ _)

	restore
}



/* ///
"Combo-CG" "p-value (\$H_0\$=Combo-CG=0)" ///
"Combo-CG (FY)" "p-value (\$H_0\$=Combo-CG (FY)=0)")) /// */

********************************************************************
*********************** MAIN RESULTS ON TEST SCORES
********************************************************************

**********************
**************Student
************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"





label var Z_kiswahili_T3 Kiswahili
label var Z_kiswahili_T7 Kiswahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"


eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	}  
}
 


esttab  using "$latexcodesfinals/RegTestScores_NoControls.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles  nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)


eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $school_average, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	}  
}
 


esttab  using "$latexcodesfinals/RegTestScores_MeanControls.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles  nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)



eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar p2=r(p)
		estadd scalar suma2=_b[TreatmentBoth] - _b[TreatmentCOD]
		
		test (_b[TreatmentBoth] - 1.117*_b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p3=r(p)
		test (_b[TreatmentBoth] -2.05*_b[TreatmentCG]=0)
		estadd scalar p4=r(p)
		test (_b[TreatmentBoth] -3.45*_b[TreatmentCOD]=0)
		estadd scalar p5=r(p)
		
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=r(N)
	}
	esttab *_`time' using "$latexcodesfinals/RegTestScores_`time'_CG.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace   ///
	keep(TreatmentCG)  stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
	nonotes substitute(\_ _)
	
	esttab *_`time' using "$latexcodesfinals/RegTestScores_`time'_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
	star(* 0.10 ** 0.05 *** 0.01) ///
	replace   ///
	keep(TreatmentCOD)  stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
	nonotes substitute(\_ _)
}
 
reg Z_ScoreFocal_T3 ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 

reg Z_ScoreFocal_T7 ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
test (_b[TreatmentBoth] -1.03*_b[TreatmentCG]=0)
test (_b[TreatmentBoth] -3.46*_b[TreatmentCOD]=0)
test (_b[TreatmentBoth] - 1.117*_b[TreatmentCOD]-_b[TreatmentCG]=0)
test (1.035*_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
exit
esttab  using "$latexcodesfinals/RegTestScores.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p suma2 p2, fmt(%9.0fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "\$\alpha_5:=\alpha_3-\alpha_2\$" "p-value (\$\alpha_5=0\$)")) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_pvaluesALL.csv", fragment se ar2 label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p suma2 p2 p3 p4 p5, fmt(%9.0fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "\$\alpha_5:=\alpha_3-\alpha_2\$" "p-value (\$\alpha_5=0\$)" "p-value (\$\alpha_3-1.117\alpha_2-\alpha_1=0\$)"  "p-value (\$2.05\alpha_3=\alpha_1\$)" "p-value (\$3.45\alpha_3=\alpha_2\$)")) ///
nonotes substitute(\_ _)
exit
esttab  using "$latexcodesfinals/RegTestScores_noalpha5.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc %9.2fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)")) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCOD)  stats(N2, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes substitute(\_ _)


esttab  using "$latexcodesfinals/RegTestScores_CG.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCG ) stats(N, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes substitute(\_ _)


eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=r(N)
		
	}
}
 


esttab  using "$latexcodesfinals/RegTestScores_LS_same_HS.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "Interaction \$(\alpha_4) =\alpha_3-\alpha_2\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_LS_same_HS_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines ///
keep(TreatmentCOD)  stats(N2, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes substitute(\_ _)


pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist  Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}

label var Z_kiswahili_T4 Kiswahili
label var Z_kiswahili_T8 Kiswahili
label var Z_kiingereza_T4 English
label var Z_kiingereza_T8 English
label var Z_hisabati_T4 Math
label var Z_hisabati_T8 Math
label var Z_ScoreFocal_T4 "Combined (PCA)"
label var Z_ScoreFocal_T8 "Combined (PCA)"

preserve

eststo clear
foreach time in  T4{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time'  i.GradeID_`time'  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		estadd scalar p=.
		estadd scalar suma=.
		estadd scalar N=., replace
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=.
		
	}
}

foreach time in  T8{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist_int} i.GradeID_`time'  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		sum `var'_`time' if TreatmentCOD==1 & TreatmentBoth!=1
		estadd scalar N2=r(N)
		
		test (_b[TreatmentBoth] -3.45*_b[TreatmentCOD]=0)
		estadd scalar p5=r(p)
	}
}
restore
label var TreatmentCOD "Incentives (\$\beta_2\$)"
label var TreatmentCG "Grants (\$\beta_1\$)"
label var TreatmentBoth "Combination (\$\beta_3\$)"

esttab  using "$latexcodesfinals/RegTestScores_highstakes.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
cells (b(star fmt (%9.2fc) vacant(".")) se(par fmt(%9.2fc)) ) ///
star(* 0.10 ** 0.05 *** 0.01) collabels(none)  ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc %9.2fc %9.2fc) labels("N. of obs." "\$\beta_5:=\beta_3-\beta_2\$" "p-value (\$\beta_5=0\$)" "")) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_pvaluesALL.csv", fragment se ar2  label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
cells (b(star fmt (%9.2fc) vacant(".")) se(par fmt(%9.2fc)) ) ///
star(* 0.10 ** 0.05 *** 0.01) collabels(none)  ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p p5, fmt(%9.0fc %9.2fc %9.2fc %9.2fc) labels("N. of obs." "\$\beta_5:=\beta_3-\beta_2\$" "p-value (\$\beta_5=0\$)" "p-value (\$\beta_3=3.45\beta_2\$)")) ///
nonotes substitute(\_ _)

esttab  using "$latexcodesfinals/RegTestScores_highstakes_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
cells (b(star fmt (%9.2fc) vacant(".")) se(par fmt(%9.2fc)) ) ///
star(* 0.10 ** 0.05 *** 0.01) collabels(none)  ///
replace  nomtitles ///
keep(TreatmentCOD) stats(N2, fmt(%9.0fc) labels("N. of obs.")) ///
nonotes substitute(\_ _)

preserve
eststo clear
foreach time in  T4{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time'  i.GradeID_`time'  i.DistID  $school_average , vce(cluster SchoolID) 
		estadd scalar p=.
		estadd scalar suma=.
		estadd scalar N=., replace
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=.
		
	}
}

foreach time in  T8{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist_int} i.GradeID_`time'  i.DistID  $school_average , vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		sum `var'_`time' if TreatmentCOD==1 & TreatmentBoth!=1
		estadd scalar N2=r(N)
	}
}
restore
label var TreatmentCOD "Incentives (\$\beta_2\$)"
label var TreatmentCG "Grants (\$\beta_1\$)"
label var TreatmentBoth "Combination (\$\beta_3\$)"

esttab  using "$latexcodesfinals/RegTestScores_highstakes_MeanControls.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber nolines /// 
cells (b(star fmt (%9.2fc) vacant(".")) se(par fmt(%9.2fc)) ) ///
star(* 0.10 ** 0.05 *** 0.01) collabels(none)  ///
replace  nomtitles ///
keep(TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\beta_4:=\beta_3-\beta_2\$" "p-value (\$\beta_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)

*************************************************************
**************Diff between high-and-low**********************
*************************************************************



eststo clear
foreach var in $AggregateDep_Karthik{
		capture drop resid_T3
		reg `var'_T3  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID)
		predict resid_T3,resid
				
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time
		save `file1'
		restore
		
		capture drop resid_T4
		reg `var'_T4  i.GradeID_T4  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		predict resid_T4,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T4 resid
		keep resid SchoolID  $treatmentlist_int time
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 if TreatmentCG==.
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		eststo `var'_yr1:  reg resid time, vce(cluster SchoolID)
		
		estadd scalar std_err1=.
		estadd scalar suma1=.
		
		
		estadd scalar std_err2=.
		estadd scalar suma2=.
		
		
		estadd scalar std_err3=.
		estadd scalar suma3=.
		restore
		
}



foreach var in $AggregateDep_Karthik{
		capture drop resid_T7
		reg `var'_T7  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID)
		predict resid_T7,resid
		reg resid_T7	$treatmentlist		
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T7 resid
		keep resid SchoolID  $treatmentlist time
		save `file1'
		restore
		reg `var'_T8 $treatmentlist_int i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		capture drop resid_T8
		reg `var'_T8 i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		predict resid_T8,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T8 resid
		keep resid SchoolID  $treatmentlist_int time
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 if TreatmentCG==.
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		eststo `var'_yr2:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
		test (_b[c.TreatmentBoth2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=(_b[c.TreatmentBoth2#c.time])
		
		test (_b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=(_b[c.TreatmentCOD2#c.time])
		
		test (_b[TreatmentCG2]+_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=-(_b[TreatmentCG2]-_b[c.TreatmentBoth2#c.time]+ _b[c.TreatmentCOD2#c.time])
		
		test (_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err4=r(p)
		estadd scalar suma4=(_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time])
		restore
		
}
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

esttab *_yr1 *_yr2 using "$latexcodesfinals/RegTestScores_Difference.tex", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps  nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep( )  ///
coeflabel(treatmentCOD "Incentives \$(\beta_2-\alpha_2\$)"  TreatmentBoth "Combination \$(\beta_3-\alpha_3\$)") ///
stats( suma2 std_err2 suma1 std_err1  suma4 std_err4, fmt(%9.2fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc %9.2fc) labels("\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0 \$)" "\$\beta_3-\alpha_3\$" "p-value(\$\beta_3-\alpha_3=0\$)" "\$\beta_5-\alpha_5\$" "p-value(\$\beta_5-\alpha_5=0 \$)")) ///
nonotes substitute(\_ _)


esttab *_yr1 *_yr2 using "$latexcodesfinals/RegTestScores_Difference_COD.tex", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps  nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD ) keep( )  ///
coeflabel(treatmentCOD "Incentives \$(\beta_2-\alpha_2\$)") ///
stats( suma2 std_err2, fmt(a2 a2) labels("\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0 \$)")) ///
nonotes substitute(\_ _)

****** SOLO SAME SCHOOLS LOW STAKES *******

eststo clear
foreach var in $AggregateDep_Karthik{
		capture drop resid_T3
		reg `var'_T3  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID)
		predict resid_T3 if  SchoolTWA_T7==1,resid
				
		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T3 resid
		keep resid SchoolID  $treatmentlist time SchoolTWA_T7
		save `file1'
		restore
		
		capture drop resid_T4
		reg `var'_T4  i.GradeID_T4  i.DistID  $schoolcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID) 
		predict resid_T4 if  SchoolTWA_T7==1,resid
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T4 resid
		keep resid SchoolID  $treatmentlist_int time SchoolTWA_T7
		save `file2'
		restore
		
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 if TreatmentCG==.
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		eststo `var'_yr1:  reg resid time if  SchoolTWA_T7==1, vce(cluster SchoolID)
		
		estadd scalar std_err1=.
		estadd scalar suma1=.
		
		
		estadd scalar std_err2=.
		estadd scalar suma2=.
		
		
		estadd scalar std_err3=.
		estadd scalar suma3=.
		restore
		
}



foreach var in $AggregateDep_Karthik{
		capture drop resid_T7
		capture drop residCG
		capture drop residCOD
		capture drop residBoth
		reg `var'_T7  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID)
		predict resid_T7 if e(sample)==1,resid 
		reg TreatmentCG  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID)
		predict residCG if e(sample)==1,resid  
		reg TreatmentCOD  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID)
		predict residCOD if e(sample)==1,resid  
		reg TreatmentBoth  i.DistID $studentcontrol $schoolcontrol $HHcontrol if  SchoolTWA_T7==1, vce(cluster SchoolID)
		predict residBoth if e(sample)==1,resid  
		

		preserve
		tempfile file1
		drop if e(sample)==0
		gen time=0
		rename resid_T7 resid
		keep resid SchoolID  $treatmentlist time SchoolTWA_T7 residCG residCOD residBoth
		save `file1'
		*save temp1
		restore
		
		reg `var'_T8 $treatmentlist_int i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		capture drop resid_T8
		capture drop residCG
		capture drop residCOD
		capture drop residBoth
		reg `var'_T8 i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID) 
		predict resid_T8 if e(sample)==1,resid 
		reg TreatmentCG  i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID)
		predict residCG if e(sample)==1,resid  
		reg TreatmentCOD i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID)
		predict residCOD if e(sample)==1,resid  
		reg TreatmentBoth  i.GradeID_T8  i.DistID  $schoolcontrol , vce(cluster SchoolID)
		predict residBoth if e(sample)==1,resid  
		
		preserve
		tempfile file2
		drop if e(sample)==0
		gen time=1
		rename resid_T8 resid
		keep resid SchoolID  $treatmentlist_int time SchoolTWA_T7 residCG residCOD residBoth
		save `file2'
		*save temp2, replace
		restore
		
		/*
		use temp1, clear
		append using temp2
		
		*/
		preserve
		clear
		use `file1'
		append using `file2'
		replace TreatmentCG=0 
		rename TreatmentCG TreatmentCG2
		rename TreatmentCOD TreatmentCOD2
		rename TreatmentBoth TreatmentBoth2
		
		replace TreatmentCG2=residCG
		replace TreatmentCOD2=residCOD
		replace TreatmentBoth2=residBoth
		
		eststo `var'_yr2:  reg resid c.(TreatmentCG2 TreatmentCOD2 TreatmentBoth2)##c.time, vce(cluster SchoolID)
		test (_b[c.TreatmentBoth2#c.time]=0)
		estadd scalar std_err1=r(p)
		estadd scalar suma1=_b[c.TreatmentBoth2#c.time]
		
		test (_b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err2=r(p)
		estadd scalar suma2=_b[c.TreatmentCOD2#c.time]
		
		test (_b[TreatmentCG2]+_b[c.TreatmentBoth2#c.time]- _b[c.TreatmentCOD2#c.time]=0)
		estadd scalar std_err3=r(p)
		estadd scalar suma3=-(_b[TreatmentCG2]-_b[c.TreatmentBoth2#c.time]+ _b[c.TreatmentCOD2#c.time])
		restore
		
}
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

esttab *_yr1 *_yr2 using "$latexcodesfinals/RegTestScores_Difference_LS_same_HS.tex", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines  nomtitles nogaps ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep( )  ///
coeflabel(treatmentCOD "Incentives \$(\beta_2-\alpha_2\$)"  TreatmentBoth "Combination \$(\beta_3-\alpha_3\$)") ///
stats( suma2 std_err2 suma1 std_err1 suma3 std_err3, fmt(a2 a2 a2 a2 a2 a2) labels("\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0 \$)" "\$\beta_3-\alpha_3\$" "p-value(\$\beta_3-\alpha_3=0\$)" "\$\beta_4-\alpha_4\$" "p-value( \$\beta_4-\alpha_4=0 \$)")) ///
nonotes substitute(\_ _)

esttab *_yr1 *_yr2 using "$latexcodesfinals/RegTestScores_Difference_LS_same_HS_COD.tex", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines  nomtitles nogaps ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.time TreatmentCOD  c.TreatmentBoth2#c.time TreatmentBoth) keep( )  ///
coeflabel(treatmentCOD "Incentives \$(\beta_2-\alpha_2\$)"  TreatmentBoth "Combination \$(\beta_3-\alpha_3\$)") ///
stats( suma2 std_err2, fmt(a2 a2 ) labels("\$\beta_2-\alpha_2\$" "p-value(\$\beta_2-\alpha_2=0 \$)")) ///
nonotes substitute(\_ _)

*************************************************************
***************  2 YEAR EFFECT ******************************
*************************************************************


foreach time in T3 T7{
	capture matrix drop Coef
	capture matrix drop Error
	foreach var in $AggregateDep_Karthik{
		reg `var'_`time'  $treatmentlist i.DistID $studentcontrol  $schoolcontrol $HHcontrol  , vce(cluster SchoolID) 
		lincom _b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		scalar error2=r(se)
		scalar error2=error2^2
		mat B=e(b)
		mat E=vecdiag(e(V))
		mat Coef  = nullmat(Coef) \ [B[1,1..3],r(estimate)]
		mat Error  = nullmat(Error) \ [E[1,1..3], error2]
	}
	preserve
	clear
	svmat Coef
	export delimited using "$latexcodesfinals/Coef_`time'.csv", replace
	clear
	svmat Error
	export delimited using "$latexcodesfinals/Error_`time'.csv", replace
	clear
	restore
}



eststo clear
foreach time in T3 T7{
	foreach var in $AggregateDep_Karthik{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol if LagGrade>=2 & LagGrade<=3, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
	}  
}
 


esttab  using "$latexcodesfinals/RegTestScores_2yeareffect.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  nomtitles nolines  ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N suma p, fmt(%9.0fc a2 a2) labels("N. of obs." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)




*************************************************************
**************		CHEATING       **********************
*************************************************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear

collapse (mean) Z_*, by(SchoolID)
drop if Z_kiswahili_T8==. | Z_kiswahili_T7	==.

foreach var in $AggregateDep_int{
	egen rank_`var'_T8=rank(`var'_T8)  
	egen rank_`var'_T7=rank(`var'_T7) 
	gen Diff_Ranking_`var'=abs(rank_`var'_T8-rank_`var'_T7)
}
gen Diff=Diff_Ranking_Z_hisabati +Diff_Ranking_Z_kiswahili +Diff_Ranking_Z_kiingereza

twoway (scatter rank_Z_hisabati_T8 rank_Z_hisabati_T7)
twoway (scatter rank_Z_kiswahili_T8 rank_Z_kiswahili_T7)
twoway (scatter rank_Z_kiingereza_T8 rank_Z_kiingereza_T7)


*************************************************************
**************	ATTRITION HIGH-STALES  **********************
*************************************************************

use "$basein/4 Intervention/TwaEL_2014/TwaTestData_2014_allstudents", clear
destring StuID, replace
drop if StuID==-999
drop if StuID==.
merge 1:m StuID using "$basein/4 Intervention/TwaEL_2014/TwaTestData_stutested"
gen new=(_merge==2)
gen missing=(_merge==1)
gen twice=(_merge==3)

collapse (sum) new missing twice, by(SchoolID)
merge 1:1 SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus( treatment treatarm DistID)
drop _merge



********************************************************************
*********************** PASS HIGH-STAKES
********************************************************************
use "$basein/4 Intervention/TwaEL_2013/TwaTestData.dta", clear



foreach name in "k" "e" "m"{
	forval i=1/3  {
		gen PR_`i'_`name'=Passed_`i'`name'/NrTests_`i'
		if "`name'"=="k" label var PR_`i'_`name' "K S`i'"
		if "`name'"=="e" label var PR_`i'_`name' "E S`i'"
		if "`name'"=="m" label var PR_`i'_`name' "M S`i'"
	}
}

keep SchoolID  PR_1_k- PR_3_m
reshape long PR_@_k PR_@_e PR_@_m, i(SchoolID) j(Grade)
rename PR__k Kis_Pass_T4
rename PR__e Eng_Pass_T4
rename PR__m Math_Pass_T4

tempfile temp1
save `temp1'


use "$basein/4 Intervention/TwaEL_2014/TwaTestData_stutested", clear
collapse  (mean) Kis_Pass Eng_Pass Math_Pass, by( SchoolID Grade )
rename Kis_Pass Kis_Pass_T8
rename Eng_Pass Eng_Pass_T8
rename Math_Pass Math_Pass_T8

merge 1:1 SchoolID Grade  using `temp1'
drop  if _merge!=3
drop _merge

merge m:1  SchoolID using "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", keepus( SchoolID treatment treatarm DistID)
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG" 
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD" 
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both" 
label var TreatmentBoth "Combination"
label var TreatmentCOD "Incentives (\$\beta_2\$)"
label var TreatmentBoth "Combination (\$\beta_3\$)"
reg Eng_Pass_T8 ${treatmentlist}  i.DistID i.Grade, vce(cluster SchoolID) 
gen empty_T4=0
gen empty_T8=0
replace empty_T4=. if  e(sample)==0
replace empty_T8=. if  e(sample)==0
replace empty_T4=. if TreatmentCG==1 | TreatmentCOD==1 | TreatmentBoth==1
replace empty_T8=. if TreatmentCG==1 | TreatmentCOD==1 | TreatmentBoth==1
eststo clear
foreach time in T4 T8{
	foreach var in Math_Pass Kis_Pass Eng_Pass empty {
		replace `var'_`time'=100*`var'_`time'
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID i.Grade, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		sum `var'_`time' if TreatmentCG==0 & TreatmentCOD==0 & TreatmentBoth==0 & e(sample)==1
		estadd scalar ymean2=r(mean)
		sum `var'_`time' if treatment=="Control" | treatment=="COD" 
		estadd scalar N2=r(N)
		if("`var'"=="empty") estadd scalar ymean2=., replace
		if("`var'"=="empty") estadd scalar N2=., replace
		if("`var'"=="empty") estadd scalar suma=., replace
	}  
}
 

label var TreatmentCOD "Incentives (\$\gamma_2\$)"
label var TreatmentCG "Grants"
label var TreatmentBoth "Combination (\$\gamma_3\$)"
esttab  using "$latexcodesfinals/RegPassTest_HighStakes.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) noomitted  ///
replace  nomtitles  nolines  ///
keep(TreatmentCOD TreatmentBoth) stats(N2 ymean2 suma p, fmt(%9.0fc %9.2fc %9.2gc %9.2gc) labels("N. of obs." "Control mean" " \$\gamma_3-\gamma_2\$" "p-value (\$\gamma_3-\gamma_2=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)



eststo clear
foreach time in T4 T8{
	foreach var in Math_Pass Kis_Pass Eng_Pass empty{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID i.Grade, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		sum `var'_`time' if TreatmentCG==0 & TreatmentCOD==0 & TreatmentBoth==0 & e(sample)==1
		estadd scalar ymean2=r(mean)
		sum `var'_`time' if treatment=="Control" | treatment=="COD" 
		estadd scalar N2=r(N)
		if("`var'"=="empty") estadd scalar ymean2=., replace
		if("`var'"=="empty") estadd scalar N2=., replace
		
	}  
}
 
label var TreatmentCOD "Incentives"
label var TreatmentCG "Grants"
label var TreatmentBoth "Combination"
esttab  using "$latexcodesfinals/RegPassTest_HighStakes_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) noomitted  ///
replace  nomtitles  nolines  ///
keep(TreatmentCOD) stats(N2 ymean2, fmt(%9.0fc %9.2fc) labels("N. of obs." "Control mean")) ///
nonotes substitute(\_ _)


*************************************************************
********************* REG PASS LOW***************************
*************************************************************
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
label var TreatmentCOD "Incentives"
label var TreatmentCG "Grants"
label var TreatmentBoth "Combination"
gen empty_T3=0
gen empty_T7=0
replace empty_T3=. if TreatmentCG==1 | TreatmentCOD==1 | TreatmentBoth==1
replace empty_T7=. if TreatmentCG==1 | TreatmentCOD==1 | TreatmentBoth==1

eststo clear
foreach time in T3 T7{
	foreach var in passmath passkis passeng{
		replace `var'_`time'=100*`var'_`time'
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		sum `var'_`time' if TreatmentCG==0 & TreatmentCOD==0 & TreatmentBoth==0 & e(sample)==1
		estadd scalar ymean2=r(mean)
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=r(N)
		if("`var'"=="empty") estadd scalar ymean2=., replace
		if("`var'"=="empty") estadd scalar N2=., replace
	}  
}
 

esttab  using "$latexcodesfinals/RegPassTest.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   nomtitles  nolines   ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc %9.2fc %9.2gc %9.2gc) labels("N. of obs." "Control mean" "\$\alpha_4=\alpha_3-\alpha_2\-\alpha_1\$)" "p-value (\$\alpha_1=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)


eststo clear
foreach time in T3 T7{
	foreach var in passmath passkis passeng empty{
		eststo est_`var'_`time':   reg `var'_`time' ${treatmentlist}  i.DistID $studentcontrol $schoolcontrol $HHcontrol, vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar p=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		sum `var'_`time' if TreatmentCG==0 & TreatmentCOD==0 & TreatmentBoth==0 & e(sample)==1
		estadd scalar ymean2=r(mean)
		sum `var'_`time' if treatment=="Control" | treatment=="COD"
		estadd scalar N2=r(N)
		if("`var'"=="empty") estadd scalar ymean2=., replace
		if("`var'"=="empty") estadd scalar N2=., replace
	}  
}
 


esttab  using "$latexcodesfinals/RegPassTest_COD.tex", fragment se ar2 booktabs label b(%9.2fc)se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace   nomtitles  nolines noomitted  ///
keep(TreatmentCOD) stats(N2 ymean2, fmt(%9.0fc %9.2fc) labels("N. of obs." "Control mean")) ///
nonotes substitute(\_ _)




********************************************************************
*********************** OTHER SUBJECTS/GRADES
********************************************************************

************************************
************* 2013 *****************
************************************

use "$base_out/Student_PSLE_2013.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"

label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Score"


eststo clear
eststo m1_2013: reg Pass  $treatmentlist $schoolcontrol i.DistrictID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2013: reg AVERAGE  $treatmentlist $schoolcontrol i.DistrictID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 
  
use "$base_out/School_PSLE_2013.dta",clear
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge 

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
  
eststo m3_2013:  reg Students $treatmentlist $schoolcontrol i.DistrictID
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]  


** do lee bound's, so that # of test takers is on average the same, dropping the left tail
use "$base_out/Student_PSLE_2013.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
*drop non matched schools
drop if CAND=="" 
drop if SX==""
preserve
collapse (count) SX2, by(SchoolID DistrictID treatment treatarm)
reg SX2 ib(none).treatarm i.DistrictID, nocons 
restore

*Calculate number of test takers per school
bys SchoolID: gen Num=_N
bys SchoolID: gen n=_n
replace Num=. if Num!=n
*mean number per treatment (and district)
bys DistrictID treatment: egen N=mean(Num)
replace N=round(N)
*minimum number across treatment arms (by district)
by DistrictID: egen N2=min(N)
*How many i need to trim from each school
gen N3=N-N2
sort SchoolID AVERAGE
tabstat AVERAGE , by(treatment) statistics(mean N)
drop if n<N3
tabstat AVERAGE , by(treatment) statistics(mean N)

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
preserve
collapse (count) SX2, by(SchoolID DistrictID treatment treatarm)
reg SX2 ib(none).treatarm i.DistrictID, nocons 
restore


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"


eststo m1_2013_Lee: reg Pass  $treatmentlist $schoolcontrol i.DistrictID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2013_Lee: reg AVERAGE  $treatmentlist $schoolcontrol i.DistrictID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]   	
  
************************************
************* 2014 *****************
************************************

use "$base_out/Student_PSLE_2014.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"



eststo m1_2014: reg Pass  $treatmentlist $schoolcontrol i.DistrictID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2014: reg AVERAGE  $treatmentlist $schoolcontrol i.DistrictID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 
  
use "$base_out/School_PSLE_2014.dta",clear
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge 

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
  
eststo m3_2014:  reg Students $treatmentlist $schoolcontrol i.DistrictID
estadd ysumm
sum Students if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG] 



** do lee bound's, so that # of test takers is on average the same, dropping the left tail
use "$base_out/Student_PSLE_2014.dta",clear
encode SX, gen(SX2)
sort SchoolID
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
*drop non matched schools
drop if CAND=="" 
drop if SX==""
preserve
collapse (count) SX2, by(SchoolID DistrictID treatment treatarm)
reg SX2 ib(none).treatarm i.DistrictID, nocons 
restore

*Calculate number of test takers per school
bys SchoolID: gen Num=_N
bys SchoolID: gen n=_n
replace Num=. if Num!=n
*mean number per treatment (and district)
bys DistrictID treatment: egen N=mean(Num)
replace N=round(N)
*minimum number across treatment arms (by district)
by DistrictID: egen N2=min(N)
*How many i need to trim from each school
gen N3=N-N2
sort SchoolID AVERAGE
tabstat AVERAGE , by(treatment) statistics(mean N)
drop if n<N3
tabstat AVERAGE , by(treatment) statistics(mean N)

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
preserve
collapse (count) SX2, by(SchoolID DistrictID treatment treatarm)
reg SX2 ib(none).treatarm i.DistrictID, nocons 
restore


gen Pass=(AVERAGE>=3)
label var Pass "Pass"
label var AVERAGE "Average score"


eststo m1_2014_Lee: reg Pass  $treatmentlist $schoolcontrol i.DistrictID i.SX2 , vce(cluster SchoolID)
estadd ysumm
sum Pass if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]

eststo m2_2014_Lee: reg AVERAGE  $treatmentlist $schoolcontrol i.DistrictID i.SX2, vce(cluster SchoolID)
estadd ysumm
sum AVERAGE if treatment=="Control"
estadd scalar ymean2=r(mean)
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]   	
     	  

************************************
************* Science *****************
************************************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"
*First lets create the tables Karthik Wants

label var Z_sayansi_T3 "Year 1"
label var Z_sayansi_T7 "Year 2"


foreach time in T3 T7{
foreach var in Z_sayansi{
eststo est_`var'_`time':  reg `var'_`time' $treatmentlist i.DistrictID $studentcontrol $schoolcontrol $HHcontrol , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
}  

}


esttab est_Z_sayansi_T3 est_Z_sayansi_T7 m1_2013 m2_2013 m3_2013 m1_2014 m2_2014 m3_2014 using "$latexcodesfinals/RegOtherGradesSubjects.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 PSLE 2013" "Grade 7 PSLE 2014", pattern(1 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Year 1" "Year 2"   "Pass" "Score" "Test takers" "Pass" "Score" "Test takers") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "\$\alpha_4 =\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)


esttab est_Z_sayansi_T3 est_Z_sayansi_T7 m1_2013 m2_2013  m1_2014 m2_2014  using "$latexcodesfinals/RegOtherGradesSubjects_nonumber.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 PSLE 2013" "Grade 7 PSLE 2014", pattern(1 0 1 0 1 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Year 1" "Year 2"   "Pass" "Score"  "Pass" "Score") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "\$\alpha_4 =\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)



esttab  m1_2013 m1_2013_Lee m2_2013 m2_2013_Lee  m1_2014 m1_2014_Lee m2_2014 m2_2014_Lee using "$latexcodesfinals/PLSE_lee.tex", se ar2 fragment booktabs nolines label b(%9.2fc) se(%9.2fc)nocon nonumber /// 
star(* 0.10 ** 0.05 *** 0.01) ///
replace  mgroups("Science" "Grade 7 PSLE 2013" "Grade 7 PSLE 2014", pattern(1 0 0 0 1 0 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mlabel("Pass" "Pass (Lee)" "Score" "Score (Lee)"   "Pass" "Pass (Lee)" "Score" "Score (Lee bound)") ///
keep(TreatmentCG TreatmentCOD TreatmentBoth) stats(N ymean2 suma p, fmt(%9.0fc a2 a2 a2)  labels("N. of obs." "Mean control group" "\$\alpha_4 =\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(suma)) ///
nonotes substitute(\_ _)




********************************************************************
*********************** EDI VS TWA
********************************************************************


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear


gen TreatmentCOD2=TreatmentCOD
gen TreatmentBoth2=TreatmentBoth

*First lets create the tables Karthik Wants

label var Z_kiswahili_T3 Kiswahili
label var Z_kiswahili_T7 Kiswahili
label var Z_kiingereza_T3 English
label var Z_kiingereza_T7 English
label var Z_hisabati_T3 Math
label var Z_hisabati_T7 Math
label var Z_ScoreFocal_T3 "Combined (PCA)"
label var Z_ScoreFocal_T7 "Combined (PCA)"
pca Z_kiswahili_C~7  Z_kiingere~C_T7 Z_hisabati_C_T7
predict Z_ScoreFocal_C_T7,score
pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

forvalues val=1/4{
	foreach var of varlist Z_ScoreFocal_C_T7 Z_ScoreFocal_T8{
		qui sum `var' if GradeID_T7==`val' & treatarm==4
		qui replace `var'=(`var'-r(mean))/r(sd) if GradeID_T7==`val'
	}
}



*Transition of Z-scores
foreach var in $AggregateDep_Karthik{
eststo clear

*Original results-EDI
eststo:   reg `var'_T7 $treatmentlist i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "Yes"
estadd local TimingFE "No"
estadd local whattest "Survey"
estadd local whatschools "All"
estadd local whatQ "All"
estadd local whatStudents "All"

*Original results-EDI without schools
eststo:   reg `var'_T7  $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if SchoolTWA_T7==1 , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "Yes"
estadd local TimingFE "No"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "All"
estadd local whatStudents "All"



*With common supoort-EDI
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol  if SchoolTWA_T7==1 , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "All"


*With common supoort and common students-EDI
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol  if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Survey"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"

*With common supoort and common students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "Yes"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"



*With common supoort and common students-TWA test, but without student controls
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID   $schoolcontrol    if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "No"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "Common"

*With common supoort and all students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.GradeID_T8  i.DistID   $schoolcontrol , vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
estadd local studentfe "No"
estadd local schoolfe "Yes"
estadd local weekfe "No"
estadd local TimingFE "Yes"
estadd local whattest "Intervention"
estadd local whatschools "Common"
estadd local whatQ "Common"
estadd local whatStudents "All"



if "`var'"=="Z_kiswahili" local name Kiswahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math
else if "`var'"=="Z_ScoreFocal" local name Combined


esttab using "$latexcodesfinals/Reg`var'_InvVsEDI.tex", se ar2 booktabs label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01) nomtitles ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels("N. of obs." "Combination-COD[-CG]" "p-value")  star(suma)) 

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI.csv", se ar2 label nonumb /// 
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
keep(TreatmentCOD TreatmentBoth) star(* 0.10 ** 0.05 *** 0.01) nomtitles ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels("N. of obs." "Combination-COD[-CG]" "p-value")  star(suma)) 

}

*Main comparison, difference in means
foreach var in $AggregateDep_Karthik{
eststo clear


*With common supoort and common students-EDI 
eststo:   reg `var'_C_T7 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]


*With common supoort and common students-TWA test
eststo:   reg `var'_T8 $treatmentlist_int i.LagGrade  i.DistID $studentcontrol  $schoolcontrol $HHcontrol   if attgrade_T7==GradeID_T7 & SchoolTWA_T7==1 & `var'_C_T7!=. & `var'_T8!=., vce(cluster SchoolID) 
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]

preserve
drop if SchoolTWA_T7==0
drop if `var'_C_T7==.
drop if `var'_T8==.
drop if attgrade_T7!=GradeID_T7
rename `var'_C_T7 `var'_EDI
rename `var'_T8 `var'_TWA
drop if upid==""
keep `var'_EDI `var'_TWA $treatmentlist TreatmentCOD2 TreatmentBoth2 DistID  $schoolcontrol HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  WeekIntvTest_T* upid SchoolID
reshape long `var', i(upid) j(Test) string
gen Twaweza=(Test=="_TWA")
eststo: reg `var'  (c.TreatmentCOD2 c.TreatmentBoth2  i.LagGrade  i.DistID c.(${studentcontrol}  ${schoolcontrol} ${HHcontrol}))##c.Twaweza, vce(cluster SchoolID)  
estadd ysumm
test (_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[c.TreatmentBoth2#c.Twaweza]- _b[c.TreatmentCOD2#c.Twaweza]

restore


if "`var'"=="Z_kiswahili" local name Kiswahili
else if "`var'"=="Z_kiingereza"  local name English	
else if "`var'"=="Z_hisabati" local name Math
else if "`var'"=="Z_sayansi" local name Science


esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp.tex", se ar2 booktabs label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combination c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combination-COD" "p-value" "") star(p))

esttab using "$latexcodesfinals/Reg`var'_InvVsEDI_Comp.csv", se ar2 label nonumb /// 
coeflabel(c.TreatmentBoth2#c.Twaweza Combination c.TreatmentCOD2#c.Twaweza COD ) ///
replace  b(%9.2fc)se(%9.2fc)nocon fragment nolines nogaps nomtitles ///
star(* 0.10 ** 0.05 *** 0.01) ///
rename(c.TreatmentCOD2#c.Twaweza TreatmentCOD  c.TreatmentBoth2#c.Twaweza TreatmentBoth) keep(TreatmentBoth TreatmentCOD)  ///
stats(N suma p, fmt(%9.0fc  a2 a2) labels ("N. of obs." "Combination-COD" "p-value" "") star(p))

}

*******************
******** BALANCE BETWEEN MATCHS AND UNMATCHS
*******************

use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
merge m:1 SchoolID using "$basein/4 Intervention/TwaEL_2013/TwaTestData.dta",keepus(SchoolID)
drop if _merge!=3
drop if GradeID_T7==. & GradeID_T3==.

foreach var in $AggregateDep_int{
gen Matched_`var'_yr2=(`var'_C_T7!=. & `var'_T8!=.) if GradeID_T7!=.
}
foreach var in $AggregateDep_int{
gen Matched_`var'_yr1=(`var'_T4!=. & `var'_T3!=.) if GradeID_T3!=.
}

capt prog drop my_ptest
*! version 1.0.0  14aug2007  Ben Jann
program my_ptest, eclass
*clus(clus_var)
syntax varlist [if] [in], by(varname) clus_id(varname numeric) strat_id(varlist numeric) [ * ] /// clus_id(clus_var)  

marksample touse
markout `touse' `by'
tempname mu_1 mu_2 mu_3 se_1 se_2 se_3 d_p2
capture drop TD*
tab `by' , gen(TD)
foreach var of local varlist {
 reghdfe `var'  TD1 TD2   `if', vce(cluster `clus_id') abs(`strat_id')
 test (_b[TD1]- _b[TD2]== 0)
 mat `d_p2'  = nullmat(`d_p2'),r(p)
 matrix A=e(b)
 lincom (TD1-TD2)
 mat `mu_3' = nullmat(`mu_3'), A[1,2]-A[1,1]
 mat `se_3' = nullmat(`se_3'), r(se)
 sum `var' if TD2==1 & e(sample)==1
 mat `mu_1' = nullmat(`mu_1'), r(mean)
 mat `se_1' = nullmat(`se_1'), r(sd)
 sum `var' if TD1==1 & e(sample)==1
 mat `mu_2' = nullmat(`mu_2'),r(mean)
 mat `se_2' = nullmat(`se_2'), r(sd)
 
}
foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3  d_p2 {
 mat coln ``mat'' = `varlist'
}
 local cmd "my_ptest"
foreach mat in mu_1 mu_2 mu_3   se_1 se_2 se_3   d_p2 {
 eret mat `mat' = ``mat''
}
end
label var LagseenUwezoTests "Seen Uwezo Test"
label var LagpreSchoolYN "Went to Preschool"
label var Lagmale "Male"
label var LagAge "Age"
label var LagZ_kiswahili "Kiswahili test score"
label var LagZ_hisabati "Math test score"
label var LagZ_kiingereza "English test score"
reg Matched_Z_hisabati_yr1 TreatmentCG TreatmentCOD TreatmentBoth, vce(cluster SchoolID)
reg Matched_Z_hisabati_yr2 TreatmentCG TreatmentCOD TreatmentBoth, vce(cluster SchoolID) 

eststo clear
eststo: my_ptest  LagpreSchoolYN Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza, by(Matched_Z_hisabati_yr1) clus_id(SchoolID) strat_id(DistID)
eststo: my_ptest  LagpreSchoolYN Lagmale LagAge LagZ_kiswahili LagZ_hisabati LagZ_kiingereza, by(Matched_Z_hisabati_yr2) clus_id(SchoolID) strat_id(DistID)

esttab using "$latexcodesfinals/Balance_Matches.tex", label replace ///
booktabs nomtitle nonumbers noobs nodep star(* 0.10 ** 0.05 *** 0.01)  collabels(none)  ///
cells("mu_1(fmt(%9.2fc)) mu_2(fmt(%9.2fc)) mu_3(fmt(%9.2fc) star pvalue(d_p2))" "se_1(fmt(%9.2fc) par) se_2(fmt(%9.2fc) par) se_3(fmt(%9.2fc) par) ") ///
addnotes("/specialcell{Standard errors, clustered at the school level, in parenthesis // /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") fragment nolines nogaps nomtitles



********************************************************************
*********************** TEACHER EFFORT
********************************************************************



use "$basein/3 Endline/Supplementing/R_EL_schools_noPII.dta", clear
keep SchoolID treatment treatarm DistID
merge 1:m SchoolID using "$base_out/Consolidated/Teacher.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/School.dta"
drop _merge
merge m:1 SchoolID using "$base_out/Consolidated/SchoolsAverageScores.dta"
drop if _merge!=3
drop _merge
drop if upid==""
merge 1:1 upid using "$base_out/Consolidated/TAttendace.dta"
drop if _merge!=3
drop _merge
rename atttch_T2 atttch_T3
rename atttch_T6 atttch_T7
keep if consnt_T3==1 &  tcnsnt_T7==1

*keep if FGS_T7==1 & FGS_T3==1

replace atttch_T3=0 if atttch_T3==. & FGS_T3==1
replace atttch_T7=0 if atttch_T7==. & FGS_T7==1
rename attcls_T6 attcls_T7
replace attcls_T7=0 if attcls_T7==. & FGS_T7==1
gen attcls_T3=.

gen TreatmentCG=0 
replace TreatmentCG=1 if treatment=="CG"
label var TreatmentCG "Grants"
gen TreatmentCOD=0 
replace TreatmentCOD=1 if treatment=="COD"
label var TreatmentCOD "Incentives"
gen TreatmentBoth=0 
replace TreatmentBoth=1 if treatment=="Both"
label var TreatmentBoth "Combination"
label var TreatmentCOD "Incentives (\$\alpha_2\$)"
label var TreatmentCG "Grants (\$\alpha_1\$)"
label var TreatmentBoth "Combination (\$\alpha_3\$)"

global depvarsFinales atttch attcls  test_F_sub_F_yrs   tutoring_F_sub_F_yrs remedial_F_sub_F_yrs    
global depvarsFinales2  t271  t275 t276 t277 TimeAtSchool

foreach var in $depvarsFinales{
	replace `var'_T3=. if FGS_T3!=1
	replace `var'_T7=. if FGS_T7!=1
}

eststo clear
foreach var in $depvarsFinales{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo m_`var': reg `var'_T $treatmentlist  $schoolcontrol ( i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)


esttab using "$latexcodesfinals/RegTeacher.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)

/*
foreach time in 3 7{
eststo clear
foreach var in $depvarsFinales{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
drop if T!="`time'" 
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist $schoolcontrol ( i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}

esttab using "$latexcodesfinals/RegTeacher_`time'.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)


esttab using "$latexcodesfinals/RegTeacher_`time'.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p,fmt(%9.0fc a2  a2 a2)  labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)
}
*/

eststo clear
foreach var in $depvarsFinales2{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist $schoolcontrol (i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher2.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)


esttab using "$latexcodesfinals/RegTeacher2.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)


foreach time in 3 7{
eststo clear
foreach var in $depvarsFinales2{
preserve
keep `var'* $treatmentlist $schoolcontrol $teachercontrol DistID SchoolID upid
reshape long `var'_T@, i(upid) j(T) string
drop if T!="3" & T!="7"
drop if T!="`time'"
encode T, gen(T2)
recode `var'_T (-99=.) (-98=.) (99=.) (98=.) 
sum `var'_T,d
replace `var'_T=r(p99) if `var'_T>r(p99) & !missing(`var'_T)
replace `var'_T=r(p1) if `var'_T<r(p1) & !missing(`var'_T)
*c.($schoolcontrol) c.($teachercontrol)
eststo: reg `var'_T $treatmentlist $schoolcontrol ( i.DistID)##T2, vce(cluster SchoolID)
estadd ysumm
test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
estadd scalar p=r(p)
estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
restore
}
esttab using "$latexcodesfinals/RegTeacher2_`time'.tex", se ar2 booktabs label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)


esttab using "$latexcodesfinals/RegTeacher2_`time'.csv", se ar2 label fragment nolines nogaps nomtitles ///
replace  stats(N ymean suma p, fmt(%9.0fc a2  a2 a2) labels ("N. of obs." "Mean of dep. var." "\$\alpha_4:=\alpha_3-\alpha_2-\alpha_1\$" "p-value (\$\alpha_4=0\$)" "") star(p)) ///
star(* 0.10 ** 0.05 *** 0.01)   b(%9.2fc)se(%9.2fc)nocon  keep(TreatmentCG TreatmentCOD TreatmentBoth) nonumbers  ///
nonotes addnotes("/specialcell{Clustered standard errors, by school, in parenthesis//  /sym{*} /(p<0.10/), /sym{**} /(p<0.05/), /sym{***} /(p<0.01/) }") substitute(/_ _)
}



** JUST THE COMBINED
*******************************************************
*********** HETEROGENEITY******************************
*******************************************************

eststo clear
**********************
**************Student
************************


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if upid==""
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
bys upid: gen  N=_N
drop if Z_ScoreFocal_T3==. & N>1




tabulate LagGrade, gen(GRD)
tabulate Lagmale, gen(sex)



foreach var in ScoreFocal{

	foreach cov in Lagmale LagAge LagZ_`var' {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum `cov'
		replace `cov'=`cov'-r(mean) if !missing(`cov')
		capture gen TCG`cov'=TreatmentCG*`cov' 
		capture gen TCOD`cov'=TreatmentCOD*`cov'  
		capture gen TBoth`cov'=TreatmentBoth*`cov' 
		eststo:  reg Z_`var'_T  $treatmentlist  TCG`cov'  TCOD`cov' TBoth`cov' Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBoth`cov'] - _b[TCOD`cov']-_b[TCG`cov']	  
		restore
	}

}




**********************
**************TEACHER
************************



use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if upid==""
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
bys upid: gen N=_N
drop if Z_ScoreFocal_T3==. & N>1

****REPLACE FOR AGE AND GENDER
foreach var in ScoreFocal{

	foreach cov in maleAverage t23Average t66 {
		preserve
		keep  Z_ScoreFocal_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_ScoreFocal_T1
		capture drop Z_ScoreFocal_T5
		capture drop Z_ScoreFocal_T4
		capture drop Z_ScoreFocal_T8
		capture drop Z_ScoreFocal_T*_*
		reshape long Z_ScoreFocal_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"!="TeacherIndexRecall") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"=="TeacherIndexRecall") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}


**********************
**************SCHOOL
************************


use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if upid==""
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
bys upid: gen  N=_N
drop if Z_ScoreFocal_T3==. & N>1

foreach var in ScoreFocal{

	foreach cov in IndexFacilities_T1 StudentsTotal_T1 IndexManagerial_T1 {
		preserve
		keep  Z_`var'_* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol `cov'  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		reshape long Z_`var'_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'
		sum Covar,d
		if("`cov'"=="PTR_T1") replace Covar=Covar-r(mean) if !missing(Covar)
		if("`cov'"!="PTR_T1") replace Covar=(Covar>r(p50)) if !missing(Covar)
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  $treatmentlist  TCGCOV  TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]-_b[TreatmentCG]
		test (_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]-_b[TCGCOV]	  
		restore
	}

}


esttab using "$latexcodesfinals/Heter_focal.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.2fc)se(%9.2fc)nocon nonum  ///
keep(Grants*Covariate Incentives*Covariate Combination*Covariate ) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  ) ///
rename( ///
 TCGLagmale Grants*Covariate TCODLagmale Incentives*Covariate TBothLagmale Combination*Covariate  ///
 TCGLagAge Grants*Covariate TCODLagAge Incentives*Covariate TBothLagAge Combination*Covariate ///
 TCGLagZ_hisabati Grants*Covariate TCODLagZ_hisabati Incentives*Covariate TBothLagZ_hisabati Combination*Covariate ///
 TCGLagZ_kiswahili Grants*Covariate TCODLagZ_kiswahili Incentives*Covariate TBothLagZ_kiswahili Combination*Covariate ///
 TCGLagZ_kiingereza Grants*Covariate TCODLagZ_kiingereza Incentives*Covariate TBothLagZ_kiingereza Combination*Covariate ///
 TCGLagZ_ScoreFocal Grants*Covariate TCODLagZ_ScoreFocal Incentives*Covariate TBothLagZ_ScoreFocal Combination*Covariate ///
 TCGCOV Grants*Covariate TCODCOV Incentives*Covariate TBothCOV Combination*Covariate) /// 
 mgroups("Student" "Teacher" "School", pattern(1 0 0 1 0 0 1 0 0) prefix(\multicolumn{@span}{c}{) suffix(}) span erepeat(\cmidrule(lr){@span}))  ///
mtitles("Male" "Age" "Lagged score" "Male" "Salary" "Motivation" "Facilities" "Enrollment" "Management") title("Heterogeneity by student characteristics for `name'") ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")

**********************
**************DATE
************************

eststo clear
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
bys upid: gen  N=_N
drop if Z_ScoreFocal_T3==. & N>1
gen IDDate_T4_format=date( IDDate_T4,"DMY",2050)
format IDDate_T4_format %td

bys SchoolID: egen DateEDI_T3=mode(date_T3), maxmode
bys SchoolID: egen DateTWA_T3=mode(IDDate_T4_format), maxmode

bys SchoolID: egen DateEDI_T7=mode(date_T7), maxmode
bys SchoolID: egen DateTWA_T7=mode(dateTWA_T8), maxmode

gen Difference_T3=DateTWA_T3-DateEDI_T3
gen Difference_T7=DateTWA_T7-DateEDI_T7
/*
sum Difference_T7, d
replace Difference_T7=. if Difference_T7>r(p99)

sum Difference_T3, d
replace Difference_T3=. if Difference_T3>r(p99)
*/

foreach var in $subjects{

	foreach cov in Difference {
		preserve
		keep  Z_`var'_* Difference_T* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		
		
		
		reshape long Z_`var'_T@ Difference_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'_T
		sum Covar,d
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		encode T, gen(T2)
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar c.($studentcontrol )#i.T2  c.($schoolcontrol )#i.T2 c.($HHcontrol )#i.T2  i.DistID#i.T2 i.LagGrade#i.T2 i.T2 ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	  
		restore
	}
	
	

}


esttab using "$latexcodesfinals/Heter_Date2.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*Difference(Days) TBothCOV Combination*Difference(Days)) ///
nomtitle ///
nonotes 




foreach var in $subjects{

	foreach cov in Difference {
		preserve
		keep  Z_`var'_* Difference_T* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		
		
		
		reshape long Z_`var'_T@ Difference_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'_T
		sum Covar,d
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo `var'_T3:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  if T=="3",vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	 
		
		eststo `var'_T7:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  if T=="7",vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	 
		restore
	}
	
	

}


esttab *T3 using "$latexcodesfinals/Heter_Date2_T3.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*Difference(Days) TBothCOV Combination*Difference(Days)) ///
nomtitle ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


esttab *T7 using "$latexcodesfinals/Heter_Date2_T7.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*Difference(Days) TBothCOV Combination*Difference(Days)) ///
nomtitle ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")



**********************
**************DATE SOLUTE
************************

eststo clear
use "$base_out/Consolidated/Student_School_House_Teacher_Char.dta", clear
drop if upid=="R1STU09272102" & SchoolID!=927
drop if upid=="R1STU10072102" & SchoolID!=1007
drop if upid=="R1STU10072110" & SchoolID!=1007
bys upid: gen  N=_N
drop if Z_ScoreFocal_T3==. & N>1
gen IDDate_T4_format=date( IDDate_T4,"DMY",2050)
format IDDate_T4_format %td

bys SchoolID: egen DateEDI_T3=mode(date_T3), maxmode
bys SchoolID: egen DateTWA_T3=mode(IDDate_T4_format), maxmode

bys SchoolID: egen DateEDI_T7=mode(date_T7), maxmode
bys SchoolID: egen DateTWA_T7=mode(dateTWA_T8), maxmode

gen Difference_T3=abs(DateTWA_T3-DateEDI_T3)
gen Difference_T7=abs(DateTWA_T7-DateEDI_T7)
/*
sum Difference_T7, d
replace Difference_T7=. if Difference_T7>r(p99)

sum Difference_T3, d
replace Difference_T3=. if Difference_T3>r(p99)
*/

foreach var in $subjects{

	foreach cov in Difference {
		preserve
		keep  Z_`var'_* Difference_T* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		
		
		
		reshape long Z_`var'_T@ Difference_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'_T
		sum Covar,d
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		encode T, gen(T2)
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar c.($studentcontrol )#i.T2  c.($schoolcontrol )#i.T2 c.($HHcontrol )#i.T2  i.DistID#i.T2 i.LagGrade#i.T2 i.T2 ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	  
		restore
	}
	
	

}


esttab using "$latexcodesfinals/Heter_Date.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*\$|\$Difference(Days)\$|\$ TBothCOV Combination*\$|\$Difference(Days)\$|\$) ///
nomtitle ///
nonotes 


preserve
collapse (median) DateTWA_T3 DateEDI_T3 DateTWA_T7 DateEDI_T7 Difference_T3 Difference_T7, by(SchoolID treatment treatarm)
format %td DateTWA_T3 DateEDI_T3 DateTWA_T7 DateEDI_T7

twoway (histogram DateTWA_T3, fcolor(gs10) fintensity(50) lcolor(none)) (histogram DateEDI_T3, fcolor(none) fintensity(50) lcolor(none)), ///
legend(order(1 "High-stakes" 2 "Low-stakes")) graphregion(color(white))
graph export "$graphs/Dates_T3.pdf", replace


twoway (histogram DateTWA_T7, fcolor(gs10) fintensity(50) lcolor(none)) (histogram DateEDI_T7, fcolor(none) fintensity(50) lcolor(none)), ///
legend(order(1 "High-stakes" 2 "Low-stakes")) graphregion(color(white))
graph export "$graphs/Dates_T7.pdf", replace



gen DateT3=DateEDI_T3
replace DateT3=DateTWA_T3 if DateTWA_T3!=.
foreach tr in "COD" "Control" "Both"{

	sum DateT3
	twoway (histogram DateTWA_T3 if treatment=="`tr'", fcolor(gs10) fintensity(50) lcolor(none)) (histogram DateEDI_T3 if treatment=="`tr'", fcolor(none) fintensity(50) lcolor(none)), ///
	legend(order(1 "High-stakes" 2 "Low-stakes")) graphregion(color(white)) xscale(range(`r(min)' `r(max)'))
	graph export "$graphs/Dates_T3_`tr'.pdf", replace

	sum DateEDI_T7
	twoway (histogram DateTWA_T7 if treatment=="`tr'", fcolor(gs10) fintensity(50) lcolor(none)) (histogram DateEDI_T7 if treatment=="`tr'", fcolor(none) fintensity(50) lcolor(none)), ///
	legend(order(1 "High-stakes" 2 "Low-stakes")) graphregion(color(white)) xscale(range(`r(min)' `r(max)'))
	graph export "$graphs/Dates_T7_`tr'.pdf", replace

}

	sum DateT3
	twoway  (histogram DateEDI_T3 if treatment=="CG", fcolor(none) fintensity(50) lcolor(none)) (histogram DateTWA_T3 if treatment=="CG", fcolor(gs10) fintensity(50) lcolor(none)), ///
	legend(order(2 "High-stakes" 1 "Low-stakes")) graphregion(color(white)) xscale(range(`r(min)' `r(max)'))
	graph export "$graphs/Dates_T3_CG.pdf", replace

	sum DateEDI_T7
	twoway (histogram DateEDI_T7 if treatment=="CG", fcolor(none) fintensity(50) lcolor(none)) (histogram DateTWA_T7 if treatment=="CG", fcolor(gs10) fintensity(50) lcolor(none)) , ///
	legend(order(2 "High-stakes" 1 "Low-stakes")) graphregion(color(white)) xscale(range(`r(min)' `r(max)'))
	graph export "$graphs/Dates_T7_`tr'.pdf", replace


restore

foreach var in $subjects{

	foreach cov in Difference {
		preserve
		keep  Z_`var'_* Difference_T* $treatmentlist  HHSize MissingHHSize IndexPoverty MissingIndexPoverty IndexEngagement MissingIndexEngagement LagExpenditure MissingLagExpenditure LagseenUwezoTests LagpreSchoolYN Lagmale LagAge LagGrade LagZ_kiswahili LagZ_hisabati LagZ_kiingereza  $schoolcontrol  DistID WeekIntvTest_T* LagGrade upid SchoolID
		drop if upid==""
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T4
		capture drop Z_`var'_T8
		capture drop Z_`var'_T*_*
		
		
		
		reshape long Z_`var'_T@ Difference_T@ WeekIntvTest_T@, i(upid) j(T) string
		gen Covar=`cov'_T
		sum Covar,d
		capture drop TCGCOV
		capture drop TCODCOV
		capture drop TBothCOV
		
		capture gen TCGCOV=TreatmentCG*Covar
		label var TCGCOV "CG * Cov"
		capture gen TCODCOV=TreatmentCOD*Covar 
		label var TCGCOV "COD * Cov"
		capture gen TBothCOV=TreatmentBoth*Covar
		label var TCGCOV "Both * Cov"
		eststo `var'_T3:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  if T=="3",vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	 
		
		eststo `var'_T7:  reg Z_`var'_T  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar $studentcontrol  $schoolcontrol $HHcontrol  i.DistID i.LagGrade  if T=="7",vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	 
		restore
	}
	
	

}


esttab *T3 using "$latexcodesfinals/Heter_Date_T3.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*\$|\$Difference(Days)\$|\$ TBothCOV Combination*\$|\$Difference(Days)\$|\$) ///
nomtitle ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")


esttab *T7 using "$latexcodesfinals/Heter_Date_T7.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD TCODCOV TreatmentBoth TBothCOV) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  TCODCOV Incentives*\$|\$Difference(Days)\$|\$ TBothCOV Combination*\$|\$Difference(Days)\$|\$) ///
nomtitle ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")

/*
bys SchoolID: egen DateEDI=mode(date_T7), maxmode

gen Difference_T8=abs(dateTWA_T8-DateEDI)

sum Difference_T8, d
replace Difference_T8=. if Difference_T8>r(p99)

pca Z_kiswahili_T8  Z_kiingere~T8 Z_hisabati_T8
predict Z_ScoreFocal_T8,score

eststo clear
foreach var in $subjects{

	foreach cov in Difference {
		preserve
		keep if Z_hisabati_T8!=. 
		keep  Z_`var'_* Difference_T* $treatmentlist_int  $schoolcontrol GradeID_*  DistID SchoolID
		gen upid=_n
		capture drop Z_`var'_T1
		capture drop Z_`var'_T5
		capture drop Z_`var'_T3
		capture drop Z_`var'_T7
		capture drop Z_`var'_T*_*
		gen Covar=Difference_T8
	

		capture drop TCODCOV
		capture drop TBothCOV

		capture gen TCODCOV=TreatmentCOD*Covar 
		capture gen TBothCOV=TreatmentBoth*Covar

		eststo:  reg Z_`var'_T8  TreatmentCOD TreatmentBoth   TCODCOV TBothCOV Covar $schoolcontrol i.GradeID_T8   i.DistID  ,vce(cluster SchoolID) 
		estadd ysumm
		test (_b[TreatmentBoth] - _b[TreatmentCOD]=0)
		estadd scalar pval1=r(p)
		estadd scalar suma=_b[TreatmentBoth] - _b[TreatmentCOD]
		test (_b[TBothCOV] - _b[TCODCOV]=0)
		estadd scalar pval2=r(p)
		estadd scalar suma2=_b[TBothCOV] - _b[TCODCOV]	  
		restore
	}

}


esttab using "$latexcodesfinals/Heter_Date_high.tex", se ar2 booktabs nolines label fragment ///
replace  b(%9.3fc)se(%9.3fc)nocon nonum  ///
keep(TreatmentCOD Incentives*Difference(Days) TreatmentBoth Combo*Difference(Days) ) ///
star(* 0.10 ** 0.05 *** 0.01)  stats(N , fmt(%9.0fc) labels ("N. of obs.")) ///
varlabels(Covar Covariate  ) ///
rename(  TCODCOV Incentives*Difference(Days) TBothCOV Combo*Difference(Days)) /// 
nomtitle ///
nonotes addnotes("\specialcell{Clustered standard errors, by school, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }" "\tiny The independent variable is the standardized test score. Each regression has a different covariate interacted with the treatment dummies. The column title indicates the covariate interacted. Baseline score is the standardized test score at the beginning of the first year; Grade $k$ is equal to one, if the student is in grade $k$; Male is a equal to one if the student is male; Age is the age in years of the student. ")
*/
